#!/usr/bin/make -f
# -*- makefile -*-
DEB_HOST_MULTIARCH ?= $(shell dpkg-architecture -qDEB_HOST_MULTIARCH)
export DPKG_EXPORT_BUILDFLAGS=1
export DEB_BUILD_MAINT_OPTIONS=hardening=+all
export LIBTOOLIZE=false
include /usr/share/dpkg/buildflags.mk
FC = $(shell basename $(shell readlink -f /usr/bin/gfortran))
ifneq ($(shell grep VERSION_CODENAME /etc/os-release),VERSION_CODENAME=buster)
	FFLAGS += -fallow-invalid-boz
	FFLAGS += -std=legacy
endif
DCLVERNUM=7.5.2
DCLVER=752

%:
	dh $@ --no-parallel --with fortran_mod

override_dh_autoreconf:
#	autoconf -f -i

override_dh_auto_configure-arch:
	CC=gcc FC=$(FC) FFLAGS="$(FFLAGS)" CFLAGS="$(CFLAGS) $(CPPFLAGS)" \
	   dh_auto_configure -- $(dpkg-buildflags --export=configure) --with-gtk3

override_dh_auto_configure-indep:
#	do nothing

override_dh_auto_test:
#	do nothing

override_dh_auto_install-arch:
	dh_auto_install -- prefix=$(CURDIR)/debian/tmp/usr
	rm $(CURDIR)/debian/tmp/usr/bin/dclconfig
	mv $(CURDIR)/debian/tmp/usr/bin/dclconfig$(DCLVER) \
	  $(CURDIR)/debian/tmp/usr/bin/dclconfig
	$(FC) $(FFLAGS) $(CPPFLAGS) $(LDFLAGS) -shared -Wl,-soname,libf77dcl.so.7 \
	  -o $(CURDIR)/debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH)/libf77dcl.so.$(DCLVERNUM) \
	  `find $(CURDIR) -name \*.o` $(shell pkg-config gtk+-3.0 --libs)
	( cd $(CURDIR)/debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH) ; \
	  ln -s libf77dcl.so.$(DCLVERNUM) libf77dcl.so.7 )
	( cd $(CURDIR)/debian/tmp/usr/lib/$(DEB_HOST_MULTIARCH) ; \
	  ln -s libf77dcl.so.$(DCLVERNUM) libf77dcl.so )

override_dh_auto_install-indep:
#	do nothing

override_dh_install-arch:
	dh_install -a --sourcedir=debian/tmp
	sed -i -e 's% -fdebug-prefix-map.*=\.%%g ' -e 's% -ffile-prefix-map.*=\.%%g ' \
	  $(CURDIR)/debian/libf77dcl-dev/usr/bin/dclconfig


override_dh_install-indep:
	dh_install -i --sourcedir=$(CURDIR)
	find $(CURDIR)/debian/dcl-f77-docs/ -name Makefile.win | xargs rm -f
	rm -f $(CURDIR)/debian/dcl-f77-docs/usr/share/doc/dcl-f77-docs/doc/f90/parm/parm.aux
	rm -f $(CURDIR)/debian/dcl-f77-docs/usr/share/doc/dcl-f77-docs/doc/f90/parm/parm.dvi
	rm -f $(CURDIR)/debian/dcl-f77-docs/usr/share/doc/dcl-f77-docs/doc/f90/parm/parm.idx
	rm -f $(CURDIR)/debian/dcl-f77-docs/usr/share/doc/dcl-f77-docs/doc/f90/parm/parm.out
	rm -f $(CURDIR)/debian/dcl-f77-docs/usr/share/doc/dcl-f77-docs/doc/f90/parm/parm.toc

override_dh_auto_clean:
	[ ! -f Mkinclude ] || $(MAKE) distclean
	dh_auto_clean
	rm -f $(CURDIR)/debian/libf77dcl*.triggers
	rm -f $(CURDIR)/lib/lib*.so
	rm -f $(CURDIR)/lib/lib*.so.*
	rm -f $(CURDIR)/src/misc1/fiolib/fcpack.f
	rm -fr $(CURDIR)/include/dcl

override_dh_compress:
	dh_compress -X.png -X.f -X.dat

override_dh_installdocs:
	dh_installdocs -A README
