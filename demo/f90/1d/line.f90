
program line

  use dcl
  integer,parameter :: nmax = 40
  real,dimension(0:nmax) :: x,y1,y2,y3,y4,y5,y6

    x  = (/( n,n=0,nmax )/) / real(nmax) ! 0≦x≦1
    y1 = sin( 2.*DCL_PI * x ) + 1.5      ! y = sin(2πx)+C
    y2 = sin( 2.*DCL_PI * x ) + 1.0
    y3 = sin( 2.*DCL_PI * x ) + 0.5
    y4 = sin( 2.*DCL_PI * x ) - 0.5
    y5 = sin( 2.*DCL_PI * x ) - 1.0
    y6 = sin( 2.*DCL_PI * x ) - 1.5

!-- グラフ ----

    call DclOpenGraphics()
    call DclNewFrame

    call DclSetWindow( 0., 1., -3., 3. )        ! ウィンドウの設定
    call DclSetViewPort( 0.2, 0.8, 0.2, 0.8 )   ! ビューポートの設定
    call DclSetTransFunction                    ! 正規変換の確定

    call DclDrawScaledAxis                      ! 座標軸を描く

    call DclDrawLine( x, y1 )                   ! 通常(type=1,index=1)
    call DclDrawLine( x, y2, index=5 )          ! 太さを変える
    call DclDrawLine( x, y3, index=21 )         ! 色を変える
    call DclDrawLine( x, y4, type=2 )           ! 線種を変える
    call DclDrawLine( x, y5, type=4, index=43 ) ! 全部変える

    call DclSetLineType( 3 )                    ! 前もって線種を指定
    call DclSetLineIndex( 30 )                  ! 前もって色を指定
    call DclDrawLine( x, y6 )

    call DclCloseGraphics

end program
