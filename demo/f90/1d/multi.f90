
program multi

  use dcl
  integer, parameter :: n=200, m=5
  real, dimension(0:n) :: x, y0, y1, y2, t
  real, dimension(m) :: a

!-- データ ----

    do j=1,m
      a(j) = (-1)**j *2./((j*2-1)*DCL_PI)
    end do

    x = (/( real(i)/n, i=0,n )/)
    t  = 2*DCL_PI*x

    y0 = -0.5
    where (x >= 1./4. .and. x <= 3./4.) y0 = 0.5
    y1 = a(1)*cos(t)
    y2 = 0.
    do j=1,m
      y2 = y2 + a(j)*cos((j*2-1)*t)
    end do

!-- グラフ ----

    call DclOpenGraphics()
    call DclNewFrame

    call DclScalingPoint( x, y0 )
    call DclScalingPoint( x, y1 )
    call DclScalingPoint( x, y2 )

    call DclFitScalingParm
    call DclSetTransFunction

    call DclDrawScaledAxis

    call DclDrawLine ( x, y0, index=5 )
    call DclDrawLine ( x, y1, type=3 )
    call DclDrawLine ( x, y2, type=2, index=2 )

    call DclCloseGraphics
end program
