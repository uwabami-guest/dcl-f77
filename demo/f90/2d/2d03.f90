
program sample_2d03

  use dcl
  integer,parameter :: nx=18, ny=18
  real,parameter :: xmin=  0, xmax=360, dx1=20, dx2=60
  real,parameter :: ymin=-90, ymax=+90, dy1=10, dy2=30
  real,parameter :: pi=3.141592, drad=pi/180, dz=0.05, dp=0.2
  real,dimension(0:nx,0:ny) :: p

    do j = 0, ny
      do i = 0, nx
        alon = ( xmin + (xmax-xmin) * i/nx ) * drad
        alat = ( ymin + (ymax-ymin) * j/ny ) * drad
        slat = sin(alat)
        p(i,j) = cos(alon) * (1-slat**2) * sin(2*pi*slat) + dz
      end do
    end do

    call DclOpenGraphics()
!    rmiss = DclGetReal( 'GLOBAL:rmiss' )
    call DclGetParm( 'GLOBAL:rmiss', rmiss )
    call DclSetParm( 'GRAPH:LSOFTF', .false. )
    call DclNewFrame

    call DclSetWindow( xmin, xmax, ymin, ymax )
    call DclSetViewPort( 0.2, 0.8, 0.2, 0.8 )
    call DclSetTransNumber( DCL_UNI_UNI )
    call DclSetTransFunction

    call DclSetShadeLevel( rmiss,  -dp, 201 )
    call DclSetShadeLevel(    dp, dp*2, 401 )
    call DclSetShadeLevel( dp*2, rmiss, 402 )
    call DclShadeContour( p )

    call DclDrawAxis( 'bt', dx2, dx1 )
    call DclDrawTitle( 'b', 'LONGITUDE', 0.0 )

    call DclDrawAxis( 'lr', dy2, dy1 )
    call DclDrawTitle( 'l', 'LATITUDE', 0.0 )

    call DclSetContourLevel( p, dp )
    call DclDrawContour( p )

    call DclCloseGraphics

end program
