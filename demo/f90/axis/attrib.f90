
program axis02

  use dcl

    call DclOpenGraphics()

    call DclNewFrame

    call DclSetWindow( -180.0, +180.0, -90.0, +90.0 )
    call DclSetViewPort( 0.2, 0.8, 0.3, 0.7 )
    call DclSetTransFunction

    call DclDrawAxis( 'bt', 60.0, 10.0 )
    call DclDrawTitle( 'b', 'Longitude', 0.0 )
    call DclDrawTitle( 'b', '<- west      east ->', 0.0 )

    call DclDrawAxis( 'lr', 30.0, 10.0 )
    call DclDrawTitle( 'l', 'Latitude', 0.0 )
    call DclDrawTitle( 'l', '<- SH    NH ->', 0.0 )

    call DclDrawTitle( 't', 'Main Title', 0.0, 2 )

    call DclCloseGraphics

end program
