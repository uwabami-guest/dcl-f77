
program axis04

  use dcl
  integer,parameter :: days=90
  type(dcl_date) :: date

    date%year = 1991
    date%month = 4
    date%day = 1

    call DclOpenGraphics()
    call DclNewFrame

    call DclSetWindow( 0.0, real(days), -1.0, +1.0 )
    call DclSetViewPort( 0.2, 0.8, 0.2, 0.8 )
    call DclSetTransFunction

    call DclDrawAxisCalendar( 'bt', date, nd=days )

    call DclDrawAxis( 'l', 0.25, 0.05 )
!    call DclSetParm( 'AXIS:labelyr', .true. )
!    call uysfmt( '(f6.2)' )
!!    call DclSetYLabelFormat( '(f6.2)' )
    call DclDrawAxis( 'r', 0.25, 0.05 )
    call DclDrawTitle( 'l', 'Correlation', 0.0 )

    call DclDrawTitle( 't', 'Calendar', 0.0, 2 )

    call DclCloseGraphics

end program
