!-----------------------------------------------------------------------
program ucpk01
  use dcl

  integer,parameter :: jd0=19920401
  character(len=32) :: ctl

    call DclOpenGraphics( -abs(DclSelectDevice()) )

    call DclSetParm( 'GRAPH:lfull',.true.)
    call DclSetAxisFactor(0.7)
    call DclSetAspectRatio(0.75,1.0)
    call DclDivideFrame('t',1,7)

    call DclNewFrame
    nd=30
    call DclSetWindow(0.0,real(nd),0.0,1.0)
    call DclSetViewPort(0.1,0.9,0.13,0.14)
    call DclSetTransNumber(1)
    call DclSetTransFunction
    call DclDrawXAxisCalendar('b',jd0,nd)
    ctl='DclDrawXAxisCalendar (30days)'
    call DclDrawXSubTitle('b',ctl,0.0)

    call DclNewFrame
    nd=90
    call DclSetWindow(0.0,real(nd),0.0,1.0)
    call DclSetViewPort(0.1,0.9,0.13,0.14)
    call DclSetTransNumber(1)
    call DclSetTransFunction
    call DclDrawXAxisCalendar('b',jd0,nd)
    ctl='DclDrawXAxisCalendar (90days)'
    call DclDrawXSubTitle('b',ctl,0.0)

    call DclNewFrame
    nd=180
    call DclSetWindow(0.0,real(nd),0.0,1.0)
    call DclSetViewPort(0.1,0.9,0.13,0.14)
    call DclSetTransNumber(1)
    call DclSetTransFunction
    call DclDrawXAxisCalendar('b',jd0,nd)
    ctl='DclDrawXAxisCalendar (180days)'
    call DclDrawXSubTitle('b',ctl,0.0)

    call DclNewFrame
    nd=400
    call DclSetWindow(0.0,real(nd),0.0,1.0)
    call DclSetViewPort(0.1,0.9,0.13,0.14)
    call DclSetTransNumber(1)
    call DclSetTransFunction
    call DclDrawXAxisCalendar('b',jd0,nd)
    ctl='DclDrawXAxisCalendar (400days)'
    call DclDrawXSubTitle('b',ctl,0.0)

    call DclNewFrame
    nd=60
    call DclSetWindow(0.0,real(nd),0.0,1.0)
    call DclSetViewPort(0.1,0.9,0.1,0.11)
    call DclSetTransNumber(1)
    call DclSetTransFunction
    call DclDrawXAxisDay('b',jd0,nd)
    ctl='DclDrawXAxisDay (60days)'
    call DclDrawXSubTitle('b',ctl,0.0)

    call DclNewFrame
!    call uzinit
    nd=120
    call DclSetWindow(0.0,real(nd),0.0,1.0)
    call DclSetViewPort(0.1,0.9,0.1,0.11)
    call DclSetTransNumber(1)
    call DclSetTransFunction
    call DclDrawXAxisMonth('b',jd0,nd)
    ctl='DclDrawXAxisMonth (120days)'
    call DclDrawXSubTitle('b',ctl,0.0)

    call DclNewFrame
!    call uzinit
    nd=2000
    call DclSetWindow(0.0,real(nd),0.0,1.0)
    call DclSetViewPort(0.1,0.9,0.1,0.11)
    call DclSetTransNumber(1)
    call DclSetTransFunction
    call DclDrawXAxisYear('b',jd0,nd)
    ctl='DclDrawXAxisYear (2000days)'
    call DclDrawXSubTitle('b',ctl,0.0)

    call DclCloseGraphics

end program
