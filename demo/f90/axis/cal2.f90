!-----------------------------------------------------------------------
!     Copyright (c) 2002 GFD Dennou Club. All Rights Reserved.
!-----------------------------------------------------------------------
program calendar2

  use dcl
  integer,parameter :: days=720
  type(dcl_date) :: date

    date%year = 1981
    date%month = 12
    date%day = 1

    call DclOpenGraphics()
    call DclSetAxisFactor( 0.8 )
    call DclNewFrame

    call DclSetWindow( -180.0, 180.0, real(days), 0.0 )
    call DclSetViewPort( 0.2, 0.8, 0.2, 0.8 )
    call DclSetTransFunction

    call DclDrawAxis( 'bt', 10.0, 60.0 )
    call DclDrawTitle( 'b', 'Longitude', 0.0 )

    call DclDrawAxisCalendar( 'l', date, nd=days )
    call DclSetParm( 'DRAW_RIGHT_LABEL', .true. )
    call DclDrawAxis( 'r', 20.0, 100.0 )
    call DclSetParm( 'RIGHT_TITLE_ANGLE', -1 )
    call DclDrawTitle( 'r', 'Day Number', 0.0 )

    call DclDrawTitle( 't', 'Calendar', sw=2 )

    call DclCloseGraphics

end program
