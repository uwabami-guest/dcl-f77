!-----------------------------------------------------------------------
!     Copyright (c) 2002 GFD Dennou Club. All Rights Reserved.
!-----------------------------------------------------------------------
program line

  use dcl

    call DclOpenGraphics
    call DclNewFrame

! 正規座標系で線分を描く
    call DclDrawLineNormalized( (/0.1,0.9/),(/0.9,0.9/) )
    call DclDrawLineNormalized( (/0.1,0.5,0.9/),(/0.8,0.75,0.8/),index=3 )

! ユーザー座標系で線分を描く
!                       xmin, xmax, ymin, ymax
    call DclSetWindow  (  0., 360., -90.,  90. )
    call DclSetViewPort( 0.1,  0.9,  0.2,  0.7 )
    call DclSetTransFunction

    call DclDrawViewPortFrame(1) ! ビューポートの枠を描く

    call DclDrawLine( (/-40.,160./), (/45.,-45./), type=2 )
    call DclDrawLine( (/-40.,160./), (/-45.,45./), type=2 )

    call DclSetParm('ENABLE_CLIPPING',.true.) ! クリッピング制御する
    call DclDrawLine( (/200.,400./), (/45.,-45./), index=21 )
    call DclDrawLine( (/200.,400./), (/-45.,45./), index=21 )

    call DclCloseGraphics

end program
