!-----------------------------------------------------------------------
!     Copyright (c) 2002 GFD Dennou Club. All Rights Reserved.
!-----------------------------------------------------------------------
program marker

  use dcl
  integer,parameter :: n=41
  real :: x(n), y(n)

    dt = 4.* 3.14159 / (n-1)
    do i=1,n
      y(i) = sin(dt*(i-1))*0.15
      x(i) = real(i-1)/real(n-1)
    end do

    call DclOpenGraphics
    call DclNewFrame
    call DclSetWindow( 0.0, 1.0, -0.8, 0.2)
    call DclSetViewPort( 0.1, 0.9,  0.1, 0.9)
    call DclSetTransNumber( 1)
    call DclSetTransFunction

    call DclSetParm( 'ENABLE_CLIPPING', .true.)
    call DclDrawViewPortFrame(1)

    call DclDrawMarker(x, y)                    ! <-- マーカー描画 (1列目)
    call DclDrawMarker(x, y-0.1, type=2)        ! <-- マーカー描画 (2列目)
    call DclDrawMarker(x, y-0.2, type=3)        ! <-- マーカー描画 (3列目)
    call DclDrawMarker(x, y-0.3, type=2,height=0.06)! <-- マーカー描画 (4列目)
    call DclDrawMarker(x, y-0.4, index=2)       ! <-- マーカー描画 (5列目)
    call DclDrawMarker(x, y-0.5, index=3,type=2)! <-- マーカー描画 (6列目)
    call DclDrawLine(x, y-0.5)                  ! <-- 折れ線描画

    call DclCloseGraphics

end program
