!-----------------------------------------------------------------------
!     Copyright (c) 2002 GFD Dennou Club. All Rights Reserved.
!-----------------------------------------------------------------------
program shade

  use dcl
  real :: upx3(3), upy3(3), upx6(6), upy6(6), upxs(61), upys(61)

    a = 0.8
    th = 3.14159 * 2 / 3
    do i=1, 3
      upx3(i) = a*sin(th*i)
      upy3(i) = a*cos(th*i)
    end do

    th = 3.14159 * 2 / 6
    do i=1, 6
      upx6(i) = a*sin(th*i)
      upy6(i) = a*cos(th*i)
    end do

    th = 3.14159 * 4 / 60
    do i=1, 61
      upxs(i) = a*(i-31) / 30.
      upys(i) = a*sin(th*(i-1))
    end do

    call DclOpenGraphics

    call DclSetParm( 'ENABLE_SOFTFILL',.true.) ! <-- ソフトフィルの指定
    call DclNewFrame

    call DclSetWindow(-1., 1., -1., 1.)
    call DclSetViewPort(0., 0.5, 0., 0.5)
    call DclSetTransNumber( 1)
    call DclSetTransFunction
    call DclDrawLine(upx3, upy3)
    call DclShadeRegion(upx3, upy3)              ! <-- 網かけ (左下)

    call DclNewFig

    call DclSetWindow(-1., 1., -1., 1.)
    call DclSetViewPort(0., 0.5, 0.5, 1.)
    call DclSetTransFunction
    call DclSetShadePattern(101)
    call DclShadeRegion(upx6, upy6)              ! <-- 横線 (左上)

    call DclNewFig

    call DclSetWindow(-1., 1., -1., 1.)
    call DclSetViewPort(0.5, 1., 0., 0.5)
    call DclSetTransFunction
    call DclShadeRegion(upx6, upy6, 201)        ! <-- 斜線 (右下)

    call DclNewFig

    call DclSetWindow(-1., 1., -1., 1.)
    call DclSetViewPort(0.5, 1., 0.5, 1.)
    call DclSetTransFunction
    call DclShadeRegion(upxs, upys, 601)       ! <-- 横線 (右上)

    call DclCloseGraphics

end program
