
program anim

  use dcl

  integer,parameter :: nmax=40, kmax=40
  real :: x(0:nmax), y(0:nmax)
  character(len=5) :: title

    dt = 2.*3.14159 / nmax

    call DclSetParm('alternate',.true.)
    call DclSetParm('wait',.false.)
    call DclOpenGraphics(1)

    do k = 1,kmax
      x = (/ (dt*n, n=0,nmax) /)
      y = sin( x + dt*k/kmax*nmax )

      call DclNewFrame
      call DclSetWindow(  0., 2.*3.14159,  -1.5,   1.5 )
      call DclSetViewPort( 0.2, 0.8, 0.2, 0.8 )
      call DclSetTransFunction

      call DclDrawScaledAxis
      title(1:2) = 't='
      write(title(3:5),'(i3)') k
      call DclDrawTitle('t',title)

      call DclDrawLine( x, y )
    end do

    call DclCloseGraphics

end program

