
program layout04

  use dcl

    call DclOpenGraphics()

    call DclSetParm( 'GRAPH:lfull', .true. )
    call DclSetFrameMargin( 0., 0., 0.08, 0.08 )
    call DclSetAspectRatio( 1., 0.6 )
    call DclSetFrameTitle( 'figure title', 't',  0.,  0., 0.03, 1 )
    call DclSetFrameTitle( 'program.name', 'b', -1., -1., 0.02, 2 )
    call DclSetFrameTitle( 'page:#page',   'b',  1., -1., 0.02, 3 )

    call DclNewFrame
    call DclSetViewPort( 0.1,0.9,0.06,0.54 )
    call DclDrawViewportFrame( 1 )
    call DclDrawTextNormalized( 0.5, 0.5, 'figure' )

    call DclCloseGraphics

end program
