!-----------------------------------------------------------------------
!     Copyright (c) 2002 GFD Dennou Club. All Rights Reserved.
!-----------------------------------------------------------------------
program regional

  use dcl
  integer,parameter :: np=2
  character(len=32) :: cttl
  character(len=3) :: ctr(np) = (/'MER','CON'/)

    call DclOpenGraphics

    call DclSetAspectRatio( 2.0, 1.0 )
    call DclDivideFrame( 'y', 2, 1 )

    do i=1,np
      call DclNewFrame

      call DclSetWindow( 123.0, 147.0, 30.0, 46.0)
      call DclSetViewPort( 0.1, 0.9, 0.1, 0.9 )
      call DclTransShortToNum(ctr(i),ntr)
      call DclSetTransNumber( ntr )
      call DclFitMapParm
      call DclSetTransFunction

      call DclSetParm( 'ENABLE_CLIPPING', .true. )
      call DclDrawDeviceWindowFrame( 1 )
      call DclDrawViewPortFrame( 1 )
      call DclTransShortToLong( ctr(i), cttl )
      call DclDrawTextProjected( 0.5, 0.95, cttl, height=0.03, index=3 )

!      call DclDrawMap( 'coast_japan' )
      call DclDrawMap( 'coast_world' )
      call DclDrawGlobe
    end do

    call DclCloseGraphics

end program
