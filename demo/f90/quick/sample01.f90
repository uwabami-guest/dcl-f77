
program sample01

  use dcl
  integer,parameter :: nmax = 400
  real,dimension(0:nmax) :: x,y,t

!-- リサジューの図 ----

    t = (/( n,n=0,nmax )/) / real(nmax)     ! 媒介変数(0≦t≦1)
    x = 1.e2  * sin( 2.*DCL_PI * 4.*t )     ! x = Asin(2πω t)
    y = 1.e-3 * cos( 2.*DCL_PI * 5.*t ) + 6.! y = Bcos(2πω't)+C

!-- グラフ ----

    call DclOpenGraphics()          ! 出力装置のオープン
    call DclNewFrame                ! 新しい描画領域を作成する

    call DclDrawScaledGraph( x, y ) ! おまかせでグラフを描く

    call DclCloseGraphics           ! 出力装置のクローズ

end program
