*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM U2D4

      PARAMETER( NX=21, NY=21 )
      PARAMETER( XMIN=-10, XMAX=10, YMIN=-10, YMAX=10 )
      PARAMETER( KMAX=5, PMIN=0, PMAX=1 )
      REAL U(NX,NY), V(NX,NY), P(NX,NY)

      DO 10 J=1,NY
      DO 10 I=1,NX
        X = XMIN + (XMAX-XMIN)*(I-1)/(NX-1)
        Y = YMIN + (YMAX-YMIN)*(J-1)/(NY-1)
        U(I,J) =  X
        V(I,J) = -Y
        P(I,J) = EXP( -X**2/64 -Y**2/25 )
   10 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL SGLSET( 'LSOFTF', .TRUE. )
      CALL GRFRM

      CALL GRSWND( XMIN, XMAX, YMIN, YMAX )
      CALL USPFIT
      CALL GRSTRF

      CALL USDAXS
      CALL UGVECT( U, NX, V, NX, NX, NY )
      CALL UDCNTR( P, NX, NX, NY )

      DP = (PMAX-PMIN)/KMAX
      DO 20 K=1,KMAX
        TLEV1 = (K-1)*DP
        TLEV2 = TLEV1 + DP
        IPAT  = 600 + K - 1
        CALL UESTLV( TLEV1, TLEV2, IPAT )
   20 CONTINUE

      CALL UETONE( P, NX, NX, NY )

      CALL GRCLS

      END
