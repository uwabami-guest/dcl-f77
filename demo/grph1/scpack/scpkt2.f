*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM SCPKT2


      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL SGLSET('L2TO3', .TRUE.)
      CALL SGISET('IFONT', 2)

      CALL SGOPN( IWS )
    
      X0 = -3
      DX = .3
      DO 10 I=1, 20 
        CALL SGFRM
        CALL SGSWND(0., 1., 0., 1.)
        CALL SGSVPT(0., 1., 0., 1.)
        CALL SGSTRN(1)
        CALL SGSTRF

        X = X0 + DX*I
        CALL SGRSET('XEYE3', X)
        CALL SCSPRJ

        CALL SLPVPR(1)
        CALL SGTXZV(0.5, 0.5, 'DENNOU', 0.1, 0, 0, 3)

   10 CONTINUE

      CALL SGCLS

      END
