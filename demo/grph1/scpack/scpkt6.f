*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM SCPKT6

      PARAMETER( NX=37, NY=37 )
      PARAMETER(  XMIN=   0,  XMAX=360,  YMIN=0,  YMAX= 180 )

      REAL ALON(NX), ALAT(NY), P(NX,NY)
      REAL XP(3), YP(3), ZP(3)


      DO 10 I=1,NX
        ALON(I) = XMIN + (XMAX-XMIN)*(I-1)/(NX-1)
   10 CONTINUE
      DO 20 J=1,NY
        ALAT(J) = YMIN + (YMAX-YMIN)*(J-1)/(NY-1)
   20 CONTINUE

      DO 30 J=1,NY
        DO 30 I=1,NX
        P(I,J) = 1.
   30 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I) ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL SGOPN( IWS )
      CALL SGFRM
      CALL SGLSET('LDEG', .TRUE.)

*---------------- 3-D ------------------

      CALL SCSORG(1., 0., 0., 0.)
      CALL SCSTRN(3)
      CALL SCSTRF

      CALL SCSEYE(  -5., -3.,  2. )
      CALL SCSOBJ(  0.0,  0.0,  0.0 )
      CALL SCSPRJ

      CALL SCSPLI(1)
      CALL SCSTNP(2999,4999)

      DO 40 J=NY-1, 1, -1
      DO 40 I=NX-1, 1, -1
        XP(1) = ALON(I)
        YP(1) = ALAT(J)
        ZP(1) = P(I,J)
        XP(2) = ALON(I)
        YP(2) = ALAT(J+1)
        ZP(2) = P(I,J+1)
        XP(3) = ALON(I+1)
        YP(3) = ALAT(J+1)
        ZP(3) = P(I+1,J+1)

        CALL SCTNU(ZP, YP, XP)
        CALL SCPLU(3, ZP, YP, XP)

        XP(1) = ALON(I+1)
        YP(1) = ALAT(J+1)
        ZP(1) = P(I+1,J+1)
        XP(2) = ALON(I+1)
        YP(2) = ALAT(J)
        ZP(2) = P(I+1,J)
        XP(3) = ALON(I)
        YP(3) = ALAT(J)
        ZP(3) = P(I,J)

        CALL SCTNU(ZP, YP, XP)
        CALL SCPLU(3, ZP, YP, XP)

   40 CONTINUE

      CALL SGCLS

      END
