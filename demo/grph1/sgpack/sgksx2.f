*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM SGKSX2

      REAL      UX(5), UY(5), VX(5), VY(5)

      DATA      UX / 2.0, 4.0, 5.0, 7.0, 8.0 /
      DATA      UY / 4.0, 5.0, 4.0, 5.0, 4.0 /
      DATA      VX / 0.2, 0.4, 0.5, 0.7, 0.8 /
      DATA      VY / 0.5, 0.6, 0.5, 0.6, 0.5 /


      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL SGOPN( IWS )

*     TEST POLYLINE, POLYMARKER, TEXT PRIMITIVE IN WC.
*     TEST POLYLINE, POLYMARKER, TEXT PRIMITIVE IN NDC.
*     TRANSFORMATION FUNCTION : LINEAR*LINEAR
*     NO-CLIPPING, FONT NO.=1

      CALL SGFRM

      CALL SGISET( 'IFONT', 1 )

      CALL SGSWND( 0.0, 10.0, 0.0, 10.0 )
      CALL SGSVPT( 0.0, 1.0, 0.0, 1.0 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL SGLNZU( 0.1, 0.1, 9.9, 0.1, 1 )
      CALL SGLNZU( 9.9, 0.1, 9.9, 9.9, 1 )
      CALL SGLNZU( 9.9, 9.9, 0.1, 9.9, 1 )
      CALL SGLNZU( 0.1, 9.9, 0.1, 0.1, 1 )

      CALL SGPLZU( 5, UX, UY, 2, 3 )
      CALL SGPMZU( 5, UX, UY, 5, 3, 0.04 )
      CALL STFTRF( UX(1), UY(1), VX1, VY1 )
      CALL STFTRF( UX(4), UY(4), VX2, VY2 )
      CALL SGLNZV( VX1, VY1, VX2, VY2, 2 )
      CALL SGTXZU( 5.0, 8.5, 'TESTU', 0.1, 0, 0, 3 )

      CALL SGSWND( 2.5, 7.5, 2.5, 7.5 )
      CALL SGSVPT( 0.25, 0.75, 0.25, 0.75 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL SGPLZV( 5, VX, VY, 3, 1 )
      CALL SGPMZV( 5, VX, VY, 4, 1, 0.02 )
      CALL SGTXZV( 0.5, 0.75, 'TESTV', 0.1, 0, 0, 3 )

*     TEST POLYLINE, POLYMARKER, TEXT PRIMITIVE IN WC.
*     TEST POLYLINE, POLYMARKER, TEXT PRIMITIVE IN NDC.
*     TRANSFORMATION FUNCTION : LINEAR*LINEAR
*     CLIPPING, FONT NO.=2

      CALL SGFRM

      CALL SGISET( 'IFONT', 2 )

      CALL SGSWND( 0.0, 10.0, 0.0, 10.0 )
      CALL SGSVPT( 0.0, 1.0, 0.0, 1.0 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL SGLNZU( 0.1, 0.1, 9.9, 0.1, 1 )
      CALL SGLNZU( 9.9, 0.1, 9.9, 9.9, 1 )
      CALL SGLNZU( 9.9, 9.9, 0.1, 9.9, 1 )
      CALL SGLNZU( 0.1, 9.9, 0.1, 0.1, 1 )

      CALL SGPLZU( 5, UX, UY, 2, 3 )
      CALL SGPMZU( 5, UX, UY, 5, 3, 0.04 )
      CALL STFTRF( UX(1), UY(1), VX1, VY1 )
      CALL STFTRF( UX(4), UY(4), VX2, VY2 )
      CALL SGLNZV( VX1, VY1, VX2, VY2, 2 )
      CALL SGTXZU( 5.0, 8.5, 'TESTU', 0.1, 0, 0, 3 )

      CALL SGSWND( 2.5, 7.5, 2.5, 7.5 )
      CALL SGSVPT( 0.25, 0.75, 0.25, 0.75 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL SGLSET( 'LCLIP', .TRUE. )
      CALL SLPVPR( 1 )
      CALL SGPLZV( 5, VX, VY, 3, 1 )
      CALL SGPMZV( 5, VX, VY, 4, 1, 0.02 )
      CALL SGTXZV( 0.5, 0.75, 'TESTV', 0.1, 0, 0, 3 )

      CALL SGCLS

      END
