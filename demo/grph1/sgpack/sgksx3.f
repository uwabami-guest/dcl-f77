*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM SGKSX3

      PARAMETER (NP=100)

      REAL      UX(NP), UY(NP)


      DO 10 I=1,NP
        UX(I)=I
        UY(I)=EXP(-I*0.03)*SIN(I*3.14/180*20)
   10 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ(*,*) IWS
      IWS=+ABS(IWS)

      CALL SGOPN( IWS )

      CALL SLRAT( 2.0, 1.0 )
      CALL SLDIV( 'Y', 2, 1 )
      CALL SLMGN( 0.1, 0.1 ,0.1, 0.1 )

*     TEST POLYLINE, POLYMARKER, TEXT PRIMITIVE IN WC.
*     TEST POLYLINE, POLYMARKER, TEXT PRIMITIVE IN NDC.
*     TRANSFORMATION FUNCTION : LINEAR*LINEAR

      CALL SGFRM

      CALL SGSWND( 1.0, REAL(NP), -1.0, +1.0 )
      CALL SGSVPT( 0.1, 0.9, 0.1, 0.9 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL SLPWWR( 1 )

      CALL SGLNZU( 1.0, 0.0, REAL(NP), 0.0, 1 )
      CALL SGLNZU( 1.0, -1.0, 1.0, +1.0, 1 )

      CALL SGPLZU( NP, UX, UY, 2, 3 )
      CALL SGPMZU( NP, UX, UY, 5, 3, 0.02 )
      CALL SGTXZV( 0.5, 0.9, 'TEST1', 0.05, 0, 0, 3 )

*     TEST POLYLINE, POLYMARKER, TEXT PRIMITIVE IN WC.
*     TEST POLYLINE, POLYMARKER, TEXT PRIMITIVE IN NDC.
*     TRANSFORMATION FUNCTION : LOG*LINEAR

      CALL SGFRM

      CALL SGSWND( 1.0, REAL(NP), -1.0, +1.0 )
      CALL SGSVPT( 0.1, 0.9, 0.1, 0.9 )
      CALL SGSTRN( 3 )
      CALL SGSTRF

      CALL SLPWWR( 1 )

      CALL GLLSET( 'LMISS', .TRUE. )
      CALL SGISET( 'NPMSKIP' , 2 )
      UX( 5)=999.0
      UY(20)=999.0

      CALL SGLNZU( 1.0, 0.0, REAL(NP), 0.0, 1 )
      CALL SGLNZU( 1.0, -1.0, 1.0, +1.0, 1 )

      CALL SGPLZU( NP, UX, UY, 3, 3 )
      CALL SGPMZU( NP, UX, UY, 2, 3, 0.02 )
      CALL SGTXZV( 0.5, 0.9, 'TEST2', 0.05, 0, 0, 3 )

      CALL SGCLS

      END
