*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM SGLIDX

      PARAMETER (DX=0.01,DD=0.08)

      CHARACTER CHR*2


      WRITE(*,*) ' WORKSTATION ID (I) ? ; '
      CALL SGPWSN
      READ(*,*) IW

      CALL SGOPN(IW)
      CALL SLMGN(0.05,0.05,0.00,0.10)
      CALL SLSTTL('DEMONSTRATION FOR LINE INDEX','T',0.0,0.0,0.018,1)

      CALL SGFRM
      DO 20 J=0,9
        DO 10 I=0,9
          IDX=J*10+I
          IF (IDX.EQ.0) GO TO 10
          X1=I*0.1+DX
          X2=X1+DD
          Y1=1.0-J*0.1-DX
          Y2=Y1-DD
          WRITE(CHR,'(I2)') IDX
          CALL SGLNZV(X1,Y1,X2,Y2,IDX)
          CALL SGTXZV(X1+DX*2,Y2+DX*2,CHR,0.015,0,1,1)
   10   CONTINUE
   20 CONTINUE

      CALL SGCLS

      END
