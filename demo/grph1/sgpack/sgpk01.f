*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM SGPK01

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN                  ! <-- 使用可能なデバイス名一覧を出力
      READ(*,*) IWS

      CALL SGOPN(IWS)              ! <-- デバイスをオープンする.

      CALL SGFRM                   ! <-- ページを用意する. 

      X1 = 0.1
      X2 = 0.9
      CALL SGLNV(X1, 0.9, X2, 0.9) ! <-- デフォルトのLINE INDEXで直線描画

      CALL SGSLNI(2)               ! <-- LINE INDEX を2に設定
      CALL SGLNV(X1, 0.8, X2, 0.8)

      CALL SGSLNI(3)
      CALL SGLNV(X1, 0.7, X2, 0.7)

      CALL SGSLNI(4)
      CALL SGLNV(X1, 0.6, X2, 0.6)

      CALL SGLNZV(X1, 0.4, X2, 0.4, 1 ) !  <-- LINE INDEX を指定して描画
      CALL SGLNZV(X1, 0.3, X2, 0.3, 2 )
      CALL SGLNZV(X1, 0.2, X2, 0.2, 3 )
      CALL SGLNZV(X1, 0.1, X2, 0.1, 4 )

      CALL SGCLS

      END
