*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM SGPK04

      PARAMETER(N=41)
      DIMENSION X(N), Y(N)


      DT = 4.* 3.14159 / (N-1)
      DO 10 I=1,N
        Y(I) = SIN(DT*(I-1))*0.15
        X(I) = REAL(I-1)/REAL(N-1)
   10 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL SGOPN(IWS)
      CALL SGFRM
      CALL SGSWND( 0.0, 1.0, -0.8, 0.2)
      CALL SGSVPT( 0.1, 0.9,  0.1, 0.9)
      CALL SGSTRN( 1)
      CALL SGSTRF

      CALL SGLSET('LCLIP', .TRUE.)
      CALL SLPVPR(1)

      CALL SGPMU(N, X, Y)                    ! <-- マーカー描画 (1列目)

      CALL SGSWND( 0.0, 1.0, -0.7, 0.3)
      CALL SGSTRF
      CALL SGSPMT(2)                         ! <-- マーカー TYPE 設定(+)
      CALL SGPMU(N, X, Y)                    ! <-- マーカー描画 (2列目)

      CALL SGSWND( 0.0, 1.0, -0.6, 0.4)
      CALL SGSTRF
      CALL SGSPMT(3)                         ! <-- マーカー TYPE 設定(*)
      CALL SGPMU(N, X, Y)                    ! <-- マーカー描画 (3列目)

      CALL SGSWND( 0.0, 1.0, -0.5, 0.5)
      CALL SGSTRF
      CALL SGSPMT(4)                         ! <-- マーカー TYPE 設定(o)
      CALL SGPMU(N, X, Y)                    ! <-- マーカー描画 (4列目)

      CALL SGSWND( 0.0, 1.0, -0.4, 0.6)
      CALL SGSTRF
      CALL SGSPMI(2)                         ! <-- マーカー INDEX 設定
      CALL SGPMU(N, X, Y)                    ! <-- マーカー描画 (5列目)

      CALL SGSWND( 0.0, 1.0, -0.3, 0.7)
      CALL SGSTRF
      CALL SGSPMI(3)
      CALL SGSPMT(2)

      CALL SGPLU(N, X, Y)                    ! <-- 折れ線描画
      CALL SGPMU(N, X, Y)                    ! <-- マーカー描画 (6列目)

      N1=N/4
      Y(N1-1) = 999.                         ! <-- 欠損値
      Y(N1  ) = 999.
      Y(N1+1) = 999.

      N2=N1*3
      Y(N2-1) = 999.
      Y(N2+1) = 999.

      CALL SGSWND( 0.0, 1.0, -0.2, 0.8)
      CALL SGSTRF
      CALL GLLSET('LMISS',.TRUE.)
      CALL SGSPMI(2)
      CALL SGSPMT(3)

      CALL SGPLU(N, X, Y)                    ! <-- 折れ線描画
      CALL SGPMU(N, X, Y)                    ! <-- マーカー描画 (7列目)

      CALL SGCLS

      END
