*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM SGPK06

      PARAMETER(N=37)
      DIMENSION X(N), Y(N)


      DT = 2.* 3.14159 / (N-1)
      R = 0.2
      DO 10 I=1, N
        X(I) = R*SIN(DT*(I-1)) + 0.5
        Y(I) = R*COS(DT*(I-1)) + 0.5
   10 CONTINUE

      XC = 0.5
      YC = 0.5

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL SGOPN(IWS)
*----------------------------- page 1 ---------------------------------
      CALL SGFRM
      CALL SGSTXI(3)
      CALL SGSPLI(2)

*                XMIN, XMAX, YMIN, YMAX
      CALL SGSWND( 0.,   1.,   0.,   1.)
      CALL SGSVPT( 0.,   1.,   0.,   1.)
      CALL SGSTRN( 1)
      CALL SGSTRF

      CALL SLPVPR(1)
      CALL SGPLU(N, X, Y)                      ! <-- 円描画
      CALL SGTXU(XC, YC, 'SGTXU')              ! <-- テキスト

*----------------------------- page 2 ---------------------------------
      CALL SGFRM

*                 XMIN, XMAX, YMIN, YMAX
      CALL SGSWND( 0.0,  1.0,  0.0,  1.0)      ! <-- 1x1
      CALL SGSVPT( 0.1,  0.4,  0.6,  0.9)      ! <-- 小さな view port
      CALL SGSTRN( 1)
      CALL SGSTRF

      CALL SLPVPR(1)
      CALL SGPLU(N, X, Y)                      ! <-- 円描画
      CALL SGTXU(XC, YC, 'SGTXU')              ! <-- テキスト

*                XMIN, XMAX, YMIN, YMAX
      CALL SGSWND( 0.0,  1.0,  0.3,  0.7)      ! <-- ゆがんだ window
      CALL SGSVPT( 0.6,  0.9,  0.6,  0.9)
      CALL SGSTRN( 1)
      CALL SGSTRF

      CALL SLPVPR(1)
      CALL SGPLU(N, X, Y)                      ! <-- 円描画
      CALL SGTXU(XC, YC, 'SGTXU')              ! <-- テキスト

*                XMIN, XMAX, YMIN, YMAX
      CALL SGSWND( 0.0,  1.0, -0.5,  0.5)      ! <-- はみ出し window
      CALL SGSVPT( 0.1,  0.4,  0.1,  0.4)
      CALL SGSTRN( 1)
      CALL SGSTRF

      CALL SLPVPR(1)
      CALL SGPLU(N, X, Y)                      ! <-- 円描画
      CALL SGTXU(XC, YC, 'SGTXU')              ! <-- テキスト

      CALL SGLSET('LCLIP', .TRUE.)             ! <-- クリッピング
*                XMIN, XMAX, YMIN, YMAX
      CALL SGSWND( 0.0,  1.0, -0.5,  0.5)
      CALL SGSVPT( 0.6,  0.9,  0.1,  0.4)
      CALL SGSTRN( 1)
      CALL SGSTRF

      CALL SLPVPR(1)
      CALL SGPLU(N, X, Y)                      ! <-- 円描画
      CALL SGTXU(XC, YC, 'SGTXU')              ! <-- テキスト

      CALL SGCLS

      END
