*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM SLPK03

      WRITE(*,*) 'WORKSTATION ID ? '
      CALL SGPWSN
      READ (*,*) IWS

      CALL SGOPN(IWS)

      CALL SGLSET('LFULL', .TRUE.)
      CALL SLMGN(0., 0., 0.08, 0.08)

      CALL SLRAT(1., 0.6)                              ! <-- 縦横比を指定
      CALL SLSTTL('FIGURE TITLE',  'T',  0., -1., 0.02, 1)
      CALL SLSTTL('page:#PAGE',    'B',  1.,  1., 0.02, 2)
      CALL SLSTTL('PROG.NAME',     'B', -1.,  1., 0.02, 3)

      CALL SGFRM

      CALL SLPWWR(1)
      CALL SGTXZV(0.01, 0.02, 'LOWER LEFT',  0.02, 0, -1, 3)
      CALL SGTXZV(0.01, 0.58, 'UPPER LEFT',  0.02, 0, -1, 3)
      CALL SGTXZV(0.99, 0.02, 'LOWER RIGHT', 0.02, 0,  1, 3)
      CALL SGTXZV(0.99, 0.58, 'UPPER RIGHT', 0.02, 0,  1, 3)

      CALL SGCLS

      END
