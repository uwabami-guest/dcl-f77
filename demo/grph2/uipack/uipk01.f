      PROGRAM UIPK01

      REAL A(2), B(2)

      CALL SWISET('WINDOW_HEIGHT', 300)
      CALL SWISET('WINDOW_WIDTH', 300)

      WRITE(*,*) ' WORKSTATION ID (I) ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL GROPN(IWS)
      CALL GRFRM
      CALL GRSWND(0., 256., 0., 256.)
      CALL GRSVPT(.1, .9, .1, .9)
      CALL GRSTRN(1)
      CALL GRSTRF

      DO I = 1, 255

         A(1) = 1. * I
         A(2) = 1. * I

         DO J = 1, 255

            B(1) = 1. * (J - 1)
            B(2) = 1. * J

            CALL SGPLXU(2, A, B, 1, 1, ISGRGB(I,J,0))

         END DO
      END DO

      CALL UXAXDV('T', 16., 64.)
      CALL UXAXDV('B', 16., 64.)
      CALL UYAXDV('L', 16., 64.)
      CALL UYAXDV('R', 16., 64.)

      CALL GRCLS

      END
