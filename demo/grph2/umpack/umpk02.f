*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM UMPK02

      PARAMETER (NP=14)

      INTEGER   NTR(NP)
      CHARACTER CTTL*32

      DATA NTR /   10,   11,   12,   13,   14,   15,
     +             20,   21,   22,   23,   30,   31,   32,   33/


      WRITE(*,*) ' WORKSTATION IS (I) ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL GROPN( -ABS(IWS) )

      CALL SLRAT( 2.0, 3.0 )
      CALL SLDIV( 'Y', 2, 3 )

      CALL UMISET( 'INDEXMJ', 1 )
      CALL UMISET( 'ITYPEMN', 1 )

      DO 10 I=1,NP

        CALL GRFRM

*       CALL GRSMPL( 0.0, 90.0, 0.0 )
        CALL GRSVPT( 0.1, 0.9, 0.1, 0.9 )
        CALL GRSTRN( NTR(I) )
        CALL UMPFIT
        CALL GRSTRF

        CALL SGLSET( 'LCLIP', .TRUE. )
        CALL SLPWWR( 1 )
        CALL SLPVPR( 1 )
        CALL SGTRNL( NTR(I), CTTL )
        CALL SGTXZR( 0.5, 0.95, CTTL, 0.03, 0, 0, 3 )

        CALL UMPMAP( 'coast_world' )
        CALL UMPGLB

        IF ( NTR(I).EQ.23 ) THEN
          CALL GRFRM
          CALL GRFRM
        END IF

   10 CONTINUE

      CALL GRCLS

      END
