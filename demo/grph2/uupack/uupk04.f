*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM UUPK04

      PARAMETER(N=20, M=5)
      REAL  Y1(N), Y2(N), A(M)

      CALL GLRGET('RUNDEF', RUNDEF)

*-----------------------------------------------------------------------

      DT = 1./(N-1)
      PI = 3.14159
      DO 50 J=1, M
        JJ = J*2-1
        A(J) = (-1)**J *2./(JJ*PI)
  50  CONTINUE

      DO 100 I=1, N
        T     = DT*(I-1)*2*PI
        Y1(I) = A(1)*COS(T)
        Y2(I) = 0.
        DO 150 J=1, M
          JJ = J*2-1
          YY = A(J)*COS(JJ*T)
          Y2(I)  = Y2(I) + YY
  150   CONTINUE

  100 CONTINUE

*-----------------------------------------------------------------------

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN(IWS)
      CALL GRFRM
      CALL SGLSET('LCLIP', .TRUE.)

      CALL USSPNT(N, RUNDEF, Y1)
      CALL USSPNT(N, RUNDEF, Y2)
      CALL GRSWND(0., 1., RUNDEF, RUNDEF)
      CALL USPFIT
      CALL GRSTRF

      CALL USDAXS

      CALL UUSARP(2999, 3999)
      CALL UVBRA(N, RUNDEF, Y1, Y2)
      CALL UVBRF(N, RUNDEF, Y1, Y2)

      CALL UUSLNI(41)
      CALL UVBRL(N, RUNDEF, Y1)
      CALL UUSLNT(3)
      CALL UVBRL(N, RUNDEF, Y2)

      CALL GRFRM

      CALL USSPNT(N, Y1, RUNDEF)
      CALL USSPNT(N, Y1, RUNDEF)
      CALL GRSWND(RUNDEF, RUNDEF, 0., 1.)
      CALL USPFIT
      CALL GRSTRF

      CALL USDAXS

      CALL UUSARP(2999, 3999)
      CALL UHBRA(N, Y1, Y2, RUNDEF)
      CALL UHBRF(N, Y1, Y2, RUNDEF)

      CALL UUSLNI(41)
      CALL UHBRL(N, Y1, RUNDEF)
      CALL UUSLNT(3)
      CALL UHBRL(N, Y2, RUNDEF)

      CALL GRCLS

      END
