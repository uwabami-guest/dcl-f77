*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM UXYZ08

      PARAMETER ( X1=-180, X2=+180, DX1=10, DX2=60 )
      PARAMETER ( Y1= -90, Y2= +90, DY1=10, DY2=30 )


      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL GROPN( IWS )

      CALL GRFRM

      CALL SGSWND( X1, X2, Y1, Y2 )
      CALL SGSVPT( 0.2, 0.8, 0.3, 0.7 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL UZISET( 'INNER', -1 )
      CALL UZRSET( 'UXUSER', 0.0 )
      CALL UZRSET( 'UYUSER', 0.0 )
      CALL UZLSET( 'LABELXU', .FALSE. )
      CALL UZLSET( 'LABELYU', .FALSE. )

      CALL UXAXDV( 'B', DX1, DX2 )
      CALL UXAXDV( 'T', DX1, DX2 )
      CALL UXSTTL( 'B', 'LONGITUDE', 0.0 )

      CALL UYAXDV( 'L', DY1, DY2 )
      CALL UYAXDV( 'R', DY1, DY2 )
      CALL UYSTTL( 'L', 'LATITUDE', 0.0 )

      CALL UXMTTL( 'T', 'UXAXDV/UYAXDV', 0.0 )

      CALL UZFACT( 0.5 )
      CALL UXAXDV( 'U', DX1, DX2 )
      CALL UXSTTL( 'U', 'EQ', +0.9 )
      CALL UYAXDV( 'U', DY1, DY2 )
      CALL UYSTTL( 'U', 'GM', -0.9 )
      CALL UZISET( 'INNER', +1 )
      CALL UXAXDV( 'U', DX1, DX2 )
      CALL UYAXDV( 'U', DY1, DY2 )

      CALL GRCLS

      END
