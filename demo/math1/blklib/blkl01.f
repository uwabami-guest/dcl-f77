*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM BLKL01

      PARAMETER (N=10,NR=3)

      REAL      RX(N),RR(NR)

      EXTERNAL  IBLKGT,IBLKGE,IBLKLT,IBLKLE

      DATA      RX/  0,  2,  5,  8, 10, 15, 20, 30, 50, 80/
      DATA      RR/ 3.0, 30.0, 30.00003 /


      CALL GLLSET('LEPSL',.TRUE.)
      DO 10 I=1,NR
        WRITE(*,'(A,10F4.0)') ' LIST OF RX (N=10) : ',RX
        WRITE(*,'(A,G16.7)') ' *** RR = ',RR(I)
        WRITE(*,'(A,I4)') ' *** IBLKGT(RX,N,RR) = ',IBLKGT(RX,N,RR(I))
        WRITE(*,'(A,I4)') ' *** IBLKGE(RX,N,RR) = ',IBLKGE(RX,N,RR(I))
        WRITE(*,'(A,I4)') ' *** IBLKLT(RX,N,RR) = ',IBLKLT(RX,N,RR(I))
        WRITE(*,'(A,I4)') ' *** IBLKLE(RX,N,RR) = ',IBLKLE(RX,N,RR(I))
   10 CONTINUE

      END
