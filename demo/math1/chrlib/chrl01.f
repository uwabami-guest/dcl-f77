*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM CHRL01

      CHARACTER CHR*8,CHR1*8,CHR2*8,CHR3*8

      EXTERNAL  LENB,LENC,LENY,LENZ


      CHR=' DENNOU '

      WRITE(*,'(3A)') ' CHR = ''',CHR,''''
      WRITE(*,'(A,I4)') ' LENB = ',LENB(CHR)
      WRITE(*,'(A,I4)') ' LENC = ',LENC(CHR)
      WRITE(*,'(A,I4)') ' LENY = ',LENY(CHR)
      WRITE(*,'(A,I4)') ' LENZ = ',LENZ(CHR)
      CHR1=CHR
      CALL CRADJ(CHR1)
      WRITE(*,'(3A)') ' CHR (RIGHT ADJUSTED) = ''',CHR1,''''
      CHR2=CHR
      CALL CLADJ(CHR2)
      WRITE(*,'(3A)') ' CHR (LEFT  ADJUSTED) = ''',CHR2,''''
      CHR3=CHR
      CALL CRVRS(CHR3)
      WRITE(*,'(3A)') ' CHR    (REVERSED)    = ''',CHR3,''''

      END
