*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM VIAL01

      PARAMETER (N=10)

      INTEGER   IX(N),IY(N)

      INTEGER   IFNA
      EXTERNAL  IFNA

      DATA      IX/  0,  1,  4,  6,  3, -4, 12,999, 23, -5/


      WRITE(*,'(A,10I6)') ' LIST OF IX : ',IX
      WRITE(*,'(A)') ' *** OPTION LMISS = .FALSE.'
      CALL VISET(IX,IY,10,1,1)
      WRITE(*,'(A)') ' AFTER CALLING VISET(IX,IY,10,1,1)'
      WRITE(*,'(A,10I6)') ' LIST OF IY : ',IY
      CALL VIINC(IY,IY,10,1,1,+1)
      WRITE(*,'(A)') ' AFTER CALLING VIINC(IY,IY,10,1,1,+1)'
      WRITE(*,'(A,10I6)') ' LIST OF IY : ',IY
      CALL VIFCT(IY,IY,10,1,1,-1)
      WRITE(*,'(A)') ' AFTER CALLING VIFCT(IY,IY,10,1,1,-1)'
      WRITE(*,'(A,10I6)') ' LIST OF IY : ',IY
      CALL VIFNA(IY,IY,10,1,1,IFNA)
      WRITE(*,'(A)') ' AFTER CALLING VIFNA(IY,IY,10,1,1,IFNA)'
      WRITE(*,'(A)') ' IFNA(I)=I*2+10'
      WRITE(*,'(A,10I6)') ' LIST OF IY : ',IY
      CALL VICON(IY,IY,10,1,1,0)
      WRITE(*,'(A)') ' AFTER CALLING VICON(IY,IY,10,1,1,0)'
      WRITE(*,'(A,10I6)') ' LIST OF IY : ',IY

      CALL GLLSET('LMISS',.TRUE.)
      WRITE(*,'(A)') ' *** OPTION LMISS = .TRUE.'
      CALL VISET(IX,IY,10,1,1)
      WRITE(*,'(A)') ' AFTER CALLING VISET(IX,IY,10,1,1)'
      WRITE(*,'(A,10I6)') ' LIST OF IY : ',IY
      CALL VIINC(IY,IY,10,1,1,+1)
      WRITE(*,'(A)') ' AFTER CALLING VIINC(IY,IY,10,1,1,+1)'
      WRITE(*,'(A,10I6)') ' LIST OF IY : ',IY
      CALL VIFCT(IY,IY,10,1,1,-1)
      WRITE(*,'(A)') ' AFTER CALLING VIFCT(IY,IY,10,1,1,-1)'
      WRITE(*,'(A,10I6)') ' LIST OF IY : ',IY
      CALL VIFNA(IY,IY,10,1,1,IFNA)
      WRITE(*,'(A)') ' AFTER CALLING VIFNA(IY,IY,10,1,1,IFNA)'
      WRITE(*,'(A)') ' IFNA(I)=I*2+10'
      WRITE(*,'(A,10I6)') ' LIST OF IY : ',IY
      CALL VICON(IY,IY,10,1,1,0)
      WRITE(*,'(A)') ' AFTER CALLING VICON(IY,IY,10,1,1,0)'
      WRITE(*,'(A,10I6)') ' LIST OF IY : ',IY

      END
*-----------------------------------------------------------------------
      INTEGER FUNCTION IFNA(I)

      IFNA=I*2+10

      END
