      program cslib01

      PARAMETER (NX=100, NY=100, LEVEL=256)
      REAL R(NX,NY),B(NX,NY),G(NX,NY)
      REAL RC(NX,NY),BC(NX,NY),GC(NX,NY)
      INTEGER I

	  do i=1,nx
		do j=1,ny
			r(i,j) = exp(-((i-nx/2. )**2.  + (j-ny/2.)**2. ) / 5000.)
			g(i,j) = exp(-((i-nx/1.1)**2.  + (j-ny/1.2)**2.) / 1000.)
			b(i,j) = exp(-((i-ny/3. )**2.  + (j-ny/5.)**2. ) / 3000.)
		end do
      end do
      CALL RNORML(R,RC,NX,NY,0.0,1.0)
      CALL RNORML(G,GC,NX,NY,0.2,1.0)
      CALL RNORML(B,BC,NX,NY,0.0,1.0)

      CALL SWISET('WINDOW_HEIGHT', 300)
      CALL SWISET('WINDOW_WIDTH', 300)

      WRITE(*,*) ' WORKSTATION ID (I) ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL GROPN(IWS)
      CALL GRFRM
      CALL GRSWND( -1., 1., -1., 1.)
      CALL GRSVPT(.1, .9, .1, .9)
      CALL GRSTRN(1)
      CALL GRSTRF

      call prcopn('DclPaintGrid3')
      call uipda3(rc, gc, bc, nx, nx, ny)
      call prccls('DclPaintGrid3')

      CALL UXAXDV('T', .1, .5)
      CALL UXAXDV('B', .1, .5)
      CALL UYAXDV('L', .1, .5)
      CALL UYAXDV('R', .1, .5)

      CALL GRCLS


      end
