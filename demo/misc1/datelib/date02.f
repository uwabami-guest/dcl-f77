*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM DATE02


      WRITE(*,*) '*** TEST FOR DATE COUNT'
      WRITE(*,*) '*** TYPE-1'
      IDATE=19890215
      WRITE(*,*) 'IDATE = ',IDATE
      CALL DATEF1(500,IDATE,JDATE)
      WRITE(*,*) 'AFTER CALLING DATEF1(500,IDATE,JDATE)'
      WRITE(*,*) 'JDATE = ',JDATE
      CALL DATEG1(ND,IDATE,JDATE)
      WRITE(*,*) 'AFTER CALLING DATEG1(ND,IDATE,JDATE)'
      WRITE(*,*) 'ND = ',ND
      WRITE(*,*) '*** TYPE-2'
      IY=1989
      ITD=46
      WRITE(*,*) 'IY = ',IY,' : ITD = ',ITD
      CALL DATEF2(-80,IY,ITD,JY,JTD)
      WRITE(*,*) 'AFTER CALLING DATEF2(-80,IY,ITD,JY,JTD)'
      WRITE(*,*) 'JY = ',JY,' : JTD = ',JTD
      CALL DATEG2(ND,IY,ITD,JY,JTD)
      WRITE(*,*) 'AFTER CALLING DATEG1(ND,IY,ITD,JY,JTD)'
      WRITE(*,*) 'ND = ',ND
      WRITE(*,*) '*** TYPE-3'
      IY=1989
      IM=2
      ID=15
      WRITE(*,*) 'IY = ',IY,' : IM = ',IM,' : ID = ',ID
      CALL DATEF3(120,IY,IM,ID,JY,JM,JD)
      WRITE(*,*) 'AFTER CALLING DATEF3(120,IY,IM,ID,JY,JM,JD)'
      WRITE(*,*) 'JY = ',JY,' : JM = ',JM,' : JD = ',JD
      CALL DATEG3(ND,IY,IM,ID,JY,JM,JD)
      WRITE(*,*) 'AFTER CALLING DATEG3(ND,IY,IM,ID,JY,JM,JD)'
      WRITE(*,*) 'ND = ',ND

      END
