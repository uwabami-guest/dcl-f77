*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM HEXL01

      CHARACTER CHEX*8


      CHEX='ABCD'
      LH=LENC(CHEX)
      CALL HEXDCI(CHEX(1:LH),IHEX)
      WRITE(*,*) 'CHEX = ',CHEX(1:LH)
      WRITE(*,*) 'IHEX = ',IHEX
      CALL HEXDIC(IHEX,CHEX(1:LH))
      WRITE(*,*) 'CHEX = ',CHEX(1:LH)

      END
