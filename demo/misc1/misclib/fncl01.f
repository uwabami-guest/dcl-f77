*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM FNCL01

      CHARACTER CNS*1,CVER*12


      WRITE(*,'(A)') ' *** TEST FOR DCLVNM'
      CALL DCLVNM(CVER)
      WRITE(*,'(2A)') ' VERSION NAME = ',CVER
      WRITE(*,'(A)') ' *** TEST FOR IUFOPN'
      IUNIT=IUFOPN()
      WRITE(*,'(A,I2)') ' AVAILABLE UNIT NUMBER = ',IUNIT
      WRITE(*,'(A)') ' *** TEST FOR CNS'
      WRITE(*,'(2A)') ' CNS(+1) = ',CNS(+1)
      WRITE(*,'(2A)') ' CNS(-1) = ',CNS(-1)

      END
