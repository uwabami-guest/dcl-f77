      PROGRAM TMPK2
      IMPLICIT NONE
      INTEGER NX, NY, THRES
      PARAMETER (NX=101, NY=101, THRES=1)
      REAL DX, DY, DYT
      PARAMETER (DX=6.2832/REAL(NX-1), DY=6.2832/REAL(NY-1))
      PARAMETER (DYT=360.0/REAL(NY-1))
      REAL ARROW_THRES
      PARAMETER (ARROW_THRES=1.0)
      REAL U(NX,NY), V(NX,NY)
      REAL X(NX), Y(NY), XT(NY), YT(NY)
      INTEGER I, J, SKIP, IWS
    
      WRITE(*,*) "SKIP NUM INPUT"
      READ(*,*) SKIP

      X=(/(DX*REAL(I-1),I=1,NX)/)
      Y=(/(DY*REAL(I-1-NY/2),I=1,NY)/)
      YT=(/(DYT*REAL(I-1-NY/2),I=1,NY)/)
      XT=(/(X(NX),I=1,NY)/)

      DO 21 J=1,NY
         DO 20 I=1,NX
            U(I,J)=-COS(Y(J))
            V(I,J)=COS(X(I))
 20      CONTINUE
 21   CONTINUE
    
      WRITE(*,*) ' WORKSTATION IS (I) ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM
      CALL GRSWND( X(1), X(NX), YT(1), YT(NY) )
      CALL GRSVPT( 0.2, 0.8, 0.2, 0.8 )
C      CALL USPFIT
      call GRSSIM( 0.5*0.6/x(nx), 0.0, 0.0 )
      CALL GRSTRN( 5 )
      CALL GRSTRF
C      CALL USDAXS
      CALL UZRSET( 'UYUSER', 0.0 )
      CALL UXPAXS( 'U', 1 )
      CALL UZRSET( 'UYUSER', -180.0 )
      CALL UXPAXS( 'U', 1 )

C      CALL UETONF( U, NX, NX, NY )
C      CALL UDCNTR( V, NX, NX, NY )
C-- 流線描画
      CALL TMISET( 'SKIPINTV', SKIP )
      CALL TMLSET( 'PERIODY', .TRUE. )
      CALL TMLSET( 'NODRSHRT', .TRUE. )
      CALL TMSTLA( X, YT, U, V, NX, NY )

      CALL UULIN( NY, XT, YT )

      CALL GRCLS

      END PROGRAM
