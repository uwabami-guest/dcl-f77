      PROGRAM TMPK3
      IMPLICIT NONE
      INTEGER NX, NY, THRES
      PARAMETER (NX=101, NY=101, THRES=1)
      REAL DX, DY, DXT, DYT
C      PARAMETER (DX=6.2832/REAL(NX-1), DY=3.14159/REAL(NY-1))
      PARAMETER (DX=6.2832/REAL(NX-1), DY=1.57079/REAL(NY-1))
      PARAMETER (DXT=360.0/REAL(NX-1), DYT=90.0/REAL(NY-1))
      REAL ARROW_THRES
      PARAMETER (ARROW_THRES=1.0)
      REAL U(NX,NY), V(NX,NY)
      REAL X(NX), Y(NY)
      REAL XT(NX), YT(NY)
      INTEGER I, J, SKIP, IWS
    
      WRITE(*,*) "SKIP NUM INPUT"
      READ(*,*) SKIP

      X=(/(DX*REAL(I-1),I=1,NX)/)
      Y=(/(DY*REAL(I-1-NY/2),I=1,NY)/)
      XT=(/(DXT*REAL(I-1),I=1,NX)/)
      YT=(/(DYT*REAL(I-1-NY/2),I=1,NY)/)

      WRITE(*,*) "Y, YT", Y(1), Y(NY), YT(1), YT(ny)
      DO 21 J=1,NY
         DO 20 I=1,NX
            U(I,J)=-COS(Y(J))
            V(I,J)=COS(X(I))
 20      CONTINUE
 21   CONTINUE
    
      WRITE(*,*) ' WORKSTATION IS (I) ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM
      CALL GRSWND( XT(1), XT(NX), YT(1), YT(NY) )
      CALL GRSVPT( 0.05, 0.95, 0.1, 0.9 )
      CALL SGSTRN( 10 )
C      CALL USPFIT
      CALL UMPFIT
      CALL GRSTRF

      CALL UMPMAP( 'coast_world' )
      CALL UMPGLB

C      CALL UETONF( U, NX, NX, NY )
C      CALL UDCNTR( V, NX, NX, NY )
C-- 流線描画
      CALL TMLSET( 'PERIODX', .TRUE. )
      CALL TMISET( 'SKIPINTV', SKIP )
      CALL TMSTLS( XT, YT, U, V, NX, NY )

      CALL GRCLS

      END PROGRAM
