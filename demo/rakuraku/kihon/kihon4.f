*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM KIHON4

      PARAMETER( NMAX=9, X1=0.1, XC=0.5, X2=0.9 )
      REAL Y(NMAX)

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL SGOPN( IWS )
      CALL SGFRM

*-- 罫線 ----
      DO 10 N=1,NMAX
        Y(N) = 0.1*(10-N)
        CALL SGLNV( X1, Y(N), X2, Y(N) )
  10  CONTINUE

      CALL SGLNV( XC, 0.05, XC, 0.95 )

*-- デフォルト ----
      CALL SGTXV( XC, Y(1), 'SGTXV' )

*-- 文字のラインインデクス ----
      CALL SGSTXI( 3 )
      CALL SGTXV( XC, Y(2), 'INDEX3' )

      CALL SGSTXI( 5 )
      CALL SGTXV( XC, Y(3), 'INDEX5' )

      CALL SGSTXI( 1 )

*-- 文字の大きさ ----
      CALL SGSTXS( 0.03 )
      CALL SGTXV( XC, Y(4), 'SMALL' )

      CALL SGSTXS( 0.07 )
      CALL SGTXV( XC, Y(5), 'LARGE' )

      CALL SGSTXS( 0.05 )

*-- 文字列のセンタリングオプション ----
      CALL SGSTXC( -1 )
      CALL SGTXV( XC, Y(6), 'LEFT' )

      CALL SGSTXC( 1 )
      CALL SGTXV( XC, Y(7), 'RIGHT' )

      CALL SGSTXC( 0 )

*-- 文字列の回転角 ----
      CALL SGSTXR( 10 )
      CALL SGTXV( XC, Y(8), 'ROTATION+' )

      CALL SGSTXR( -10 )
      CALL SGTXV( XC, Y(9), 'ROTATION-' )

      CALL SGCLS

      END
