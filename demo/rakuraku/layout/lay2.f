*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM LAY2

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL SGOPN( IWS )

      CALL SLMGN( 0.1, 0.1, 0.1, 0.1 )
      CALL SLSTTL( 'FIGURE TITLE', 'T',  0.,  0., 0.03, 1 )
      CALL SLSTTL( 'PROGRAM.NAME', 'B', -1.,  1., 0.02, 2 )
      CALL SLSTTL( '#DATE #TIME',  'B',  0.,  0., 0.02, 3 )
      CALL SLSTTL( 'page:#PAGE',   'B',  1., -1., 0.02, 4 )

      CALL SGFRM
      CALL SLPVPR( 1 )
      CALL SGTXV( 0.5, 0.5, 'FIGURE' )

      CALL SGCLS

      END
