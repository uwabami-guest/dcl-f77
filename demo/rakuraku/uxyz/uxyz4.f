*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM UXYZ4

      WRITE(*,*) ' WORKSTATION ID (I) ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

      CALL GRSWND( -50., 50., 1.E3, 0.4 )
      CALL GRSVPT(  0.2, 0.8,  0.2, 0.8 )
      CALL GRSTRN( 2 )
      CALL GRSTRF

      CALL UXAXDV( 'B', 5., 20. )
      CALL UXAXDV( 'T', 5., 20. )
      CALL UXSTTL( 'B', 'LATITUDE (deg)', 0. )

      CALL ULISET( 'IYTYPE', 3 )
      CALL ULYLOG( 'L', 3, 9 )
      CALL ULYLOG( 'R', 3, 9 )
      CALL UYSTTL( 'L', 'PRESSURE (hPa)', 0. )

      CALL UXMTTL( 'T', 'UXAXDV/ULYLOG', 0. )

      CALL GRCLS

      END
