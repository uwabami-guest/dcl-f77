*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM UXYZ5

      PARAMETER( NX1=21, NX2= 5 )
      PARAMETER( NY1= 0, NY2=18 )
      REAL      RX1(NX1), RX2(NX2), RY2(NY2)
      CHARACTER CX2(NX2)*4, CY2(NY2)*4

      DATA      RX1/-50,-45,-40,-35,-30,-25,-20,-15,-10, -5,  0,
     +                5, 10, 15, 20, 25, 30, 35, 40, 45, 50/
      DATA      RX2/  -40 ,  -20 ,    0 ,   20 ,   40 /
      DATA      CX2/'40S ','20S ','EQ  ','20N ','40N '/
      DATA      RY2/ 1000 , 850  , 700  , 500  , 400  , 300  ,
     +               250  , 200  , 150  , 100  , 70   , 50   ,
     +               30   , 10   , 5    , 2    , 1    , 0.4  /
      DATA      CY2/'1000','    ','    ','500 ','    ','    ',
     +              '    ','200 ','    ','100 ','    ','50  ',
     +              '30  ','10  ','5   ','2   ','1   ','.4  '/

      WRITE(*,*) ' WORKSTATION ID (I) ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

      CALL GRSWND( -50., 50., 1.E3, 0.4 )
      CALL GRSVPT(  0.2, 0.8,  0.2, 0.8 )
      CALL GRSTRN( 2 )
      CALL GRSTRF

      CALL UXAXLB( 'B', RX1, NX1, RX2, CX2, 4, NX2 )
      CALL UXAXLB( 'T', RX1, NX1, RX2, CX2, 4, NX2 )
      CALL UXSTTL( 'B', 'LATITUDE', 0. )

      CALL UYAXLB( 'L', DUMMY, NY1, RY2, CY2, 4, NY2 )
      CALL UYAXLB( 'R', DUMMY, NY1, RY2, CY2, 4, NY2 )
      CALL UYSTTL( 'L', 'PRESSURE (hPa)', 0. )

      CALL UXMTTL( 'T', 'UXAXLB/UYAXLB', 0. )

      CALL GRCLS

      END
