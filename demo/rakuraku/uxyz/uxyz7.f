*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM UXYZ7

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

      CALL GRSWND( 1990., 1995., -2.0, 2.0 )
      CALL GRSVPT(   0.2,   0.8,  0.3, 0.7 )
      CALL GRSTRN( 1 )
      CALL GRSTRF

      CALL UZRSET( 'UYUSER', 0. )
      CALL UZLSET( 'LBTWN', .TRUE. )
      CALL UXSFMT( '(I4)' )
      CALL UXAXDV( 'U', 0.25, 1. )
      CALL UZLSET( 'LBTWN', .FALSE. )
      CALL UXSTTL( 'U', 'YEAR', 1. )

      CALL UZISET( 'INNER', -1 )
      CALL UYSFMT( '(F4.1)' )
      CALL UYAXDV( 'L', 0.25, 1. )
      CALL UYSTTL( 'L', 'S.O.Index', 0. )

      CALL UXMTTL( 'T', 'UXAXDV/UYAXDV', 0. )

      CALL GRCLS

      END
