*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM UXYZ8

      PARAMETER( ID0=19911201, ND=180, RND=ND )

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL UZFACT( 0.7 )
      CALL UZLSET( 'LOFFSET', .TRUE. )
      CALL GRFRM

      CALL GRSWND( 0.0, RND, 0.0, 100. )
      CALL GRSVPT( 0.4, 0.9, 0.3,  0.8 )
      CALL GRSTRN( 1 )
      CALL GRSTRF

      CALL UCXACL( 'B', ID0, ND )
      CALL UCXACL( 'T', ID0, ND )
      CALL UXSAXS( 'B' )
      CALL UXAXDV( 'B', 10., 20. )
      CALL UXSTTL( 'B', 'DAY NUMBER', 0. )

      CALL UYAXDV( 'L', 5., 10. )
      CALL UYAXDV( 'R', 5., 10. )
      CALL UYSTTL( 'L', 'CELSIUS SCALE', 0. )

      CALL UYSAXS( 'L' )
      CALL UZRSET( 'YOFFSET', 273.15 )
      CALL UZRSET( 'YFACT  ', 1. )
      CALL UYAXDV( 'L', 5., 10. )
      CALL UYSTTL( 'L', 'KELVIN SCALE', 0. )

      CALL UYSAXS( 'L' )
      CALL UZRSET( 'YOFFSET', 32. )
      CALL UZRSET( 'YFACT  ', 1.8 )
      CALL UYAXDV( 'L', 10., 20. )
      CALL UYSTTL( 'L', 'FAHRENHEIT SCALE', 0. )

      CALL UXSTTL( 'T', '(LOFFSET=.TRUE.)', 0. )
      CALL UXMTTL( 'T', 'UXSAXS/UYSAXS', 0. )

      CALL GRCLS

      END

