      PROGRAM HOP

      PARAMETER( NMAX=400 )
      REAL X(NMAX), Y(NMAX)

*-- リサジューの図 ----
      DT = 2.*3.14159 / (NMAX-1)

      DO 10 N=1,NMAX
        T = DT*(N-1)
        X(N) = 1.E 2*SIN(4.*T)
        Y(N) = 1.E-3*COS(5.*T) + 6.
   10 CONTINUE

*-- グラフ ----
      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM

      CALL USSTTL( 'X-TITLE', 'x-unit', 'Y-TITLE', 'y-unit' )
      CALL USGRPH( NMAX, X, Y )

      CALL GRCLS

      END
