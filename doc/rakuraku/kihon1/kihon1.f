      PROGRAM KIHON1

      PARAMETER( NMAX=50 )
      REAL X(0:NMAX), Y1(0:NMAX), Y2(0:NMAX), Y3(0:NMAX)

      DO 10 N=0,NMAX
        X (N) = REAL(N)/REAL(NMAX)
        Y1(N) = X(N)**3
        Y2(N) = X(N)**2
        Y3(N) = SQRT(X(N))
   10 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL SGOPN( IWS )
      CALL SGFRM
      CALL SLPVPR( 1 )

      CALL SGPLV( NMAX+1, X, Y1 )
      CALL SGPMV( NMAX+1, X, Y2 )
      CALL SGTXV( 0.5, 0.5, 'SGTXV' )
      CALL SGLSET( 'LSOFTF', .TRUE. )
      CALL SGTNV( NMAX+1, X, Y3 )

      CALL SGCLS

      END
