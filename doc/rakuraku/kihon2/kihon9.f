      PROGRAM KIHON9

      PARAMETER( NMAX=40 )
      PARAMETER( PI=3.14159 )
      PARAMETER( XMIN=0., XMAX=4*PI, YMIN=-1., YMAX=1. )
      REAL X(0:NMAX), Y(0:NMAX)

      DT = XMAX/NMAX
      DO 10 N=0,NMAX
        X(N) = N*DT
        Y(N) = SIN(X(N))
   10 CONTINUE

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL SGOPN( IWS )
      CALL SGFRM

*--  ラインタイプ = 4 (デフォルト) ----
      CALL SGSWND( XMIN, XMAX, YMIN, YMAX )
      CALL SGSVPT(   0.,   1.,  0.7,  0.9 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL SGSPLT( 4 )
      CALL SGPLU( NMAX+1, X, Y )

*--  ラインタイプ = 4 (BITLEN*2) ----
      CALL SGSWND( XMIN, XMAX, YMIN, YMAX )
      CALL SGSVPT(   0.,   1.,  0.5,  0.7 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL SGRSET( 'BITLEN', 0.006 )
      CALL SGPLU( NMAX+1, X, Y )

      CALL SGRSET( 'BITLEN', 0.003 )

*--  ビットパターン ----
      CALL SGSWND( XMIN, XMAX, YMIN, YMAX )
      CALL SGSVPT(   0.,   1.,  0.3,  0.5 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL BITPCI( '1111111100100100', ITYPE )
      CALL SGSPLT( ITYPE )
      CALL SGPLU( NMAX+1, X, Y )

*--  ビットパターン(倍長) ----
      CALL SGSWND( XMIN, XMAX, YMIN, YMAX )
      CALL SGSVPT(   0.,   1.,  0.1,  0.3 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL SGISET( 'NBITS', 32 )
      CALL BITPCI( '10010010011111000111110001111100', ITYPE )
      CALL SGSPLT( ITYPE )
      CALL SGPLU( NMAX+1, X, Y )

      CALL SGCLS

      END
