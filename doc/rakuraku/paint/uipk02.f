      PROGRAM SAMPLE02

      PARAMETER (NX = 50, NY = 50)

      REAL Z(NX,NY)

      DO I = 1, NX
         DO J = 1, NY

            Z(I,J) = - (I - NX / 2.)**2 - (J - NY / 2.)**2

         END DO
      END DO

      CALL SWISET('WINDOW_HEIGHT', 300)
      CALL SWISET('WINDOW_WIDTH', 300)

      WRITE(*,*) ' WORKSTATION ID (I) ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL GROPN(IWS)
      CALL GRFRM
      CALL GRSWND(-1., 1., -1., 1.)
      CALL GRSVPT(.1, .9, .1, .9)
      CALL GRSTRN(1)
      CALL GRSTRF

      CALL UIPDAT(Z, NX, NX, NY)

      CALL UXAXDV('T', .1, .5)
      CALL UXAXDV('B', .1, .5)
      CALL UYAXDV('L', .1, .5)
      CALL UYAXDV('R', .1, .5)

      CALL GRCLS

      END
