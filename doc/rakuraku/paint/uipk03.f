      program UIPK03

	  PARAMETER (NX=100, NY=100, LEVEL=256)
      REAL R(NX,NY),B(NX,NY),G(NX,NY)
      REAL RC(NX,NY),BC(NX,NY),GC(NX,NY)
      REAL CL(LEVEL)
      INTEGER IR(LEVEL),IG(LEVEL),IB(LEVEL)
      INTEGER I
      INTEGER IMAGE(300)

	  do i=1,level
		CL(i) = 1.0*(i-1)/(level-1)
		call UIFPAC(level-i,0,0,ir(i))
		call UIFPAC(0,level-i,0,ig(i))
		call UIFPAC(0,0,level-i,ib(i))
	  end do
	  do i=1,nx
		do j=1,ny
			r(i,j) = exp(-((i-nx/2. )**2.  + (j-ny/2.)**2. ) / 5000.)
			g(i,j) = exp(-((i-nx/1.1)**2.  + (j-ny/1.2)**2.) / 1000.)
			b(i,j) = exp(-((i-ny/3. )**2.  + (j-ny/5.)**2. ) / 3000.)
		end do
      end do
      CALL RNORML(R,RC,NX,NY,0.0,1.0)
      CALL RNORML(G,GC,NX,NY,0.2,1.0)
      CALL RNORML(B,BC,NX,NY,0.0,1.0)

      CALL SWISET('WINDOW_HEIGHT', 300)
      CALL SWISET('WINDOW_WIDTH', 300)

      WRITE(*,*) ' WORKSTATION ID (I) ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL GROPN(IWS)
      CALL GRFRM
      CALL GRSWND( -1., 1., -1., 1.)
      CALL GRSVPT(.1, .9, .1, .9)
      CALL GRSTRN(1)
      CALL GRSTRF

      call UISCMP(ig(1),ig(level),ib(1),ib(level))

      call prcopn('DclPaintGrid3')

      if(nu1 /= nv1 .or. nu2 /= nv2) then
        call msgdmp('E', 'DclPaintGrid2', 
     *       'Size of arrays are not consistent.')
      end if

      call sgqvpt(vxmin, vxmax, vymin, vymax)

      call stfpr2(vxmin, vymin, rx, ry)
      call stfwtr(rx, ry, wx, wy)
      call swfint(wx, wy, ix1, iy1)

      call stfpr2(vxmax, vymax, rx, ry)
      call stfwtr(rx, ry, wx, wy)
      call swfint(wx, wy, ix2, iy2)

      call uipd3z(rc, gc, bc, nx, nx, ny, image, iwidth)

      call prccls('DclPaintGrid3')

      CALL UXAXDV('T', .1, .5)
      CALL UXAXDV('B', .1, .5)
      CALL UYAXDV('L', .1, .5)
      CALL UYAXDV('R', .1, .5)

      CALL GRCLS


      end program
