      PROGRAM QUICK2

      PARAMETER( NMAX=400 )
      REAL X(NMAX), Y(NMAX)

*-- リサジューの図 ----
      DT = 3.14159 / (NMAX-1)
      DO 10 N=1,NMAX
        T = DT*(N-1)
        X(N) = 1.E 5*SIN( 6.*T)
        Y(N) = 1.E-4*COS(14.*T) + 1.
   10 CONTINUE

*-- グラフ ----
      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN( IWS )
      CALL GRFRM
      CALL USGRPH( NMAX, X, Y )
      CALL GRCLS

      END
