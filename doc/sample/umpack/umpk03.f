*-----------------------------------------------------------------------
      PROGRAM UMPK03

      PARAMETER (NP=2)

      CHARACTER CTTL*32, CTR(NP)*3

      EXTERNAL  ISGTRC

      DATA CTR /'MER','CON'/


      WRITE(*,*) ' WORKSTATION IS (I) ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL GROPN( ABS(IWS) )

      CALL SLRAT( 2.0, 1.0 )
      CALL SLDIV( 'Y', 2, 1 )

      DO 10 I=1,NP

        CALL GRFRM

        CALL GRSWND( 123.0, 147.0, 30.0, 46.0)
        CALL GRSVPT( 0.1, 0.9, 0.1, 0.9 )
        CALL GRSTRN( ISGTRC(CTR(I)) )
        CALL UMPFIT
        CALL GRSTRF

        CALL SGLSET( 'LCLIP', .TRUE. )
        CALL SLPWWR( 1 )
        CALL SLPVPR( 1 )
        CALL SGTRNL( ISGTRC(CTR(I)), CTTL )
        CALL SGTXZR( 0.5, 0.95, CTTL, 0.03, 0, 0, 3 )

*       CALL UMPMAP( 'coast_japan' )
        CALL UMPMAP( 'coast_world' )
        CALL UMPGLB

   10 CONTINUE

      CALL GRCLS

      END
