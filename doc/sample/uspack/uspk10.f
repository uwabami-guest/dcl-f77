*-----------------------------------------------------------------------
      PROGRAM USPK10

      PARAMETER (N=400)
      REAL X(N), Y(N)

      PI = 3.14159
      DT = 2*PI/(N-1)

      DO 100 I=1, N
        T = DT*(I-1)
        X(I) = T
        Y(I) = 10.*SIN(2.*T)
  100 CONTINUE

*---------------------------- 1ST PAGE ---------------------------------

      CALL SWCSTX('FNAME','USPK10')
      CALL SWLSTX('LSEP',.TRUE.)

      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ (*,*) IWS

      CALL GROPN(IWS)
      CALL GRFRM
      CALL USGRPH(N, X, Y)

*---------------------------- 2ND PAGE ---------------------------------

      CALL GLRGET('RUNDEF', RUNDEF)

      CALL GRFRM
      CALL GRSWND(0., 2.*PI, RUNDEF, RUNDEF)

      CALL USRSET('DXL' , 1.25)
      CALL USRSET('DXT' , 0.25)

      CALL USRSET('XFAC', 0.1)
      CALL USRSET('YFAC', 10.)

      CALL USGRPH(N, X, Y)

*-----------------------------------------------------------------------

      CALL GRCLS

      END
