*-----------------------------------------------------------------------
      PROGRAM UXYZ09

      PARAMETER ( ID0=19811201, ND=180, RND=ND )
      PARAMETER ( RLAT1=20, RLAT2=80, DLAT1=5, DLAT2=10 )


      WRITE(*,*) ' WORKSTATION ID (I)  ? ;'
      CALL SGPWSN
      READ(*,*) IWS

      CALL GROPN( IWS )

      CALL UZFACT( 0.7 )

      CALL GRFRM

      CALL SGSWND( 0.0, RND, RLAT1, RLAT2 )
      CALL SGSVPT( 0.2, 0.8, 0.4, 0.8 )
      CALL SGSTRN( 1 )
      CALL SGSTRF

      CALL SLPVPR( 1 )

      CALL UXSAXS( 'B' )
      CALL UCXACL( 'B', ID0, ND )
      CALL UXSAXS( 'B' )
      CALL UXAXDV( 'B', 10.0, 20.0 )
      CALL UXSTTL( 'B', 'DAY NUMBER', 0.0 )
      CALL UXSAXS( 'T' )
      CALL UCXACL( 'T', ID0, ND )

      CALL UYSAXS( 'L' )
      CALL UYAXDV( 'L', DLAT1, DLAT2 )
      CALL UYSAXS( 'R' )
      CALL UYAXDV( 'R', DLAT1, DLAT2 )
      CALL UYSTTL( 'L', 'LATITUDE', 0.0 )

      CALL UXMTTL( 'T', 'UXSAXS/UYSAXS', 0.0 )

      CALL GRCLS

      END
