*-----------------------------------------------------------------------
*     PROGRAM FOR FONT FILE CONVERSION                       (95/05/31)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM CVFONT

      PARAMETER (NCHAR=256,LEN=6000,NP=8)
      PARAMETER (IU1=11,IU2=21)

      INTEGER   IPOSX(NCHAR),KX(NP),KY(NP)
      CHARACTER CKX(LEN)*1,CKY(LEN)*1,CDSNI*64,CDSNO*64
      LOGICAL   LEND

      EXTERNAL  INDXIF


*     / READ FONT FILE NAMES /

      READ(*,*) CDSNI,CDSNO

*     / OPEN FONT FILES /

      OPEN(IU1,FILE=CDSNI,FORM='FORMATTED',STATUS='OLD')
      REWIND(IU1)
      OPEN(IU2,FILE=CDSNO,FORM='UNFORMATTED',STATUS='NEW')
      REWIND(IU2)

*     / LOAD TEXT FONT /

      NC=0
      IPLST=0

   30 CONTINUE

        NC=NC+1
        NL=0

   20   CONTINUE

*         / READ FONT DATA /

          NL=NL+1
          READ(IU1,100) ID,(KX(I),KY(I),I=1,NP)
  100     FORMAT(I5,3X,8(I3,1X,I3,1X))

*         / CHECK END OF LINE /

          JD=INDXIF(KY,NP,1,-64)
          IF (JD.EQ.0) THEN
            JDL=NP
          ELSE
            JDL=JD
          END IF

*         / PACK IN CHARACTER /

          DO 10 J=1,JDL
            IPC=NP*(NL-1)+J+IPLST
            CKX(IPC)=CHAR(KX(J)+64)
            CKY(IPC)=CHAR(KY(J)+64)
   10     CONTINUE

*         / CHECK END OF SEGMENT /

          IF (JD.NE.0) THEN
            IF (KX(JD).EQ.-64) THEN
              LEND=.TRUE.
              IPOSX(NC)=IPLST+1
              IPLST=NP*(NL-1)+JD+IPLST
            ELSE
              LEND=.FALSE.
            END IF
          ELSE
            LEND=.FALSE.
          END IF
        IF (.NOT.LEND) GO TO 20

      IF (.NOT.(NC.EQ.NCHAR)) GO TO 30

*     / WRITE UNFORMATTED FILE /

      WRITE(IU2) IPOSX
      WRITE(IU2) CKX
      WRITE(IU2) CKY

*     / CLOSE FONT FILE /

      CLOSE(IU1)
      CLOSE(IU2)

      END
*-----------------------------------------------------------------------
*     INDXIF
*-----------------------------------------------------------------------
      INTEGER FUNCTION INDXIF(IX,N,JD,II)

      INTEGER   IX(*)


      INDXIF=0
      DO 10 I=1,N
        J=JD*(I-1)+1
        IF (IX(J).EQ.II) THEN
          INDXIF=I
          RETURN
        END IF
   10 CONTINUE

      END
