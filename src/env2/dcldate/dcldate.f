*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      PROGRAM DCLDAT

      CHARACTER CFORM*32,CTIMEZ*3

      DATA      CTIMEZ/'JST'/


      CFORM='WWW CCC DD ######## ### YYYY'
      CALL DATEQ1(IDATE)
      CALL TIMEQ1(ITIME)
      CALL DATEC1(CFORM,IDATE)
      CALL CLOWER(CFORM(2:3))
      CALL CLOWER(CFORM(6:7))
      CFORM(12:19)='HH:MM:SS'
      CALL TIMEC1(CFORM(12:19),ITIME)
      CFORM(21:23)=CTIMEZ
      CALL GLIGET('IOUNIT',IU)
      WRITE(IU,'(A)') CFORM

      END
