*-----------------------------------------------------------------------
*     CLIPPING (OLD INTERFACE, BUT EFFICIENT)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGSCLP(ICLIP)

      LOGICAL   LCLIP


      CALL MSGDMP('M','SGSCLP','THIS IS OLD INTERFACE - USE SGLSET !')

      IF (.NOT.(0.LE.ICLIP .AND. ICLIP.LE.1)) THEN
        CALL MSGDMP('E','SGSCLP','CLIPPING INDICATOR IS INVALID.')
      END IF

      CALL SGLSET('LCLIP',ICLIP.EQ.1)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQCLP(ICLIP)

      CALL MSGDMP('M','SGQCLP','THIS IS OLD INTERFACE - USE SGLGET !')

      CALL SGLGET('LCLIP',LCLIP)

      IF (LCLIP) THEN
        ICLIP=1
      ELSE
        ICLIP=0
      END IF

      END
