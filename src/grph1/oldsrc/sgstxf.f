*-----------------------------------------------------------------------
*     SGPACK
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGSTXF(IFONT)


      CALL MSGDMP('M','SGSTXF','THIS IS OLD INTERFACE - USE SGISET !')

      CALL SGISET('IFONT',IFONT)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQTXF(IFONT)

      CALL MSGDMP('M','SGQTXF','THIS IS OLD INTERFACE - USE SGIGET !')

      CALL SGIGET('IFONT',IFONT)

      RETURN
      END
