*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SUSWND(UXMIN,UXMAX,UYMIN,UYMAX,INTRF)


      CALL MSGDMP('M','SUSWND','THIS IS OLD INTERFACE - USE SGSWND !')

      CALL SGSWND(UXMIN,UXMAX,UYMIN,UYMAX)
      CALL SGSTRN(INTRF)
      CALL SGSTRF

      END
