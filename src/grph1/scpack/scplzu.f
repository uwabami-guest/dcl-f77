*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SCPLZU(N,UPX,UPY,UPZ,INDEX)

      REAL      UPX(*),UPY(*),UPZ(*)


      IF (N.LT.2) THEN
        CALL MSGDMP('E','SCPLZU','NUMBER OF POINTS IS LESS THAN 2.')
      END IF
      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','SCPLZU','POLYLINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','SCPLZU','POLYLINE INDEX IS LESS THAN 0.')
      END IF

      CALL SZL3OP(INDEX)
      CALL SZL3ZU(N,UPX,UPY,UPZ)
      CALL SZL3CL

      END
