*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SCPMZV(N,VPX,VPY,VPZ,ITYPE,INDEX,RSIZE)

      REAL      VPX(*),VPY(*),VPZ(*)


      IF (N.LT.1) THEN
        CALL MSGDMP('E','SCPMZV','NUMBER OF POINTS IS LESS THAN 1.')
      END IF
      IF (ITYPE.EQ.0) THEN
        CALL MSGDMP('M','SCPMZV','MARKER TYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','SCPMZV','POLYMARKER INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','SCPMZV','POLYMARKER INDEX IS LESS THAN 0.')
      END IF
      IF (RSIZE.EQ.0) THEN
        CALL MSGDMP('M','SCPMZV','MARKER SIZE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (RSIZE.LT.0) THEN
        CALL MSGDMP('E','SCPMZV','MARKER SIZE IS LESS THAN ZERO.')
      END IF

      CALL SZM3OP(ITYPE,INDEX,RSIZE)
      CALL SZM3ZV(N,VPX,VPY,VPZ)
      CALL SZM3CL

      END
