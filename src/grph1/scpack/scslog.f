*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SCSLOG(LXLOG3, LYLOG3, LZLOG3)

      LOGICAL LXLOG3, LYLOG3, LZLOG3


      CALL SGLSET('LXLOG3', LXLOG3)
      CALL SGLSET('LYLOG3', LYLOG3)
      CALL SGLSET('LZLOG3', LZLOG3)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SCQLOG(LXLOG3, LYLOG3, LZLOG3)

      CALL SGLGET('LXLOG3', LXLOG3)
      CALL SGLGET('LYLOG3', LYLOG3)
      CALL SGLGET('LZLOG3', LZLOG3)

      RETURN
      END
