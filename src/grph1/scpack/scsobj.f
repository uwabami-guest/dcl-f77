*-----------------------------------------------------------------------
*     3-D TRANSFORMATION
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SCSOBJ(XOBJ3, YOBJ3, ZOBJ3)


      CALL SGRSET('XOBJ3',XOBJ3)
      CALL SGRSET('YOBJ3',YOBJ3)
      CALL SGRSET('ZOBJ3',ZOBJ3)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SCQOBJ(XOBJ3, YOBJ3, ZOBJ3)

      CALL SGRGET('XOBJ3',XOBJ3)
      CALL SGRGET('YOBJ3',YOBJ3)
      CALL SGRGET('ZOBJ3',ZOBJ3)

      RETURN
      END
