*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SCSORG(SIMFAC, VXORG3, VYORG3, VZORG3)


      CALL SGRSET('SIMFAC3',SIMFAC)
      CALL SGRSET('VXORG3' ,VXORG3)
      CALL SGRSET('VYORG3' ,VYORG3)
      CALL SGRSET('VZORG3' ,VZORG3)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SCQORG(SIMFAC, VXORG3, VYORG3, VZORG3)

      CALL SGRGET('SIMFAC3',SIMFAC)
      CALL SGRGET('VXORG3' ,VXORG3)
      CALL SGRGET('VYORG3' ,VYORG3)
      CALL SGRGET('VZORG3' ,VZORG3)

      RETURN
      END
