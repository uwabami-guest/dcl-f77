*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SCTNZU(UPX,UPY,UPZ,ITPAT1,ITPAT2)

      REAL      UPX(*),UPY(*),UPZ(*)


      IF (ITPAT1.EQ.0 .OR. ITPAT2.EQ.0) THEN
        CALL MSGDMP('M','SCTNZU','TONE PAT. INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (ITPAT1.LT.0 .OR. ITPAT2.LE.0) THEN
        CALL MSGDMP('E','SCTNZU','TONE PAT. INDEX IS LESS THAN 0.')
      END IF

      CALL SZT3OP(ITPAT1, ITPAT2)
      CALL SZT3ZU(UPX,UPY,UPZ)
      CALL SZT3CL

      END
