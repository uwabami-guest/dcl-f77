*-----------------------------------------------------------------------
*     SGLGET / SGLSET / SGLSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGLGET(CP, LPARA)

      LOGICAL   LPARA
      CHARACTER CP*(*)

      LOGICAL   LP
      CHARACTER CX*8
      CHARACTER CL*40

      CALL SGLQID(CP, IDX)
      CALL SGLQVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGLSET(CP, LPARA)

      CALL SGLQID(CP, IDX)
      CALL SGLSVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGLSTX(CP, LPARA)

      LP = LPARA
      CALL SGLQID(CP, IDX)

*     / SHORT NAME /

      CALL SGLQCP(IDX, CX)
      CALL RTLGET('SG', CX, LP, 1)

*     / LONG NAME /

      CALL SGLQCL(IDX, CL)
      CALL RLLGET(CL, LP, 1)

      CALL SGLSVL(IDX,LP)

      RETURN
      END
