*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGPFLN

      CHARACTER LINE*100,CFLN*8,CFNM*8

      EXTERNAL  LENC


      CALL GLIGET('MSGUNIT',IU)
      CALL SWIGET('MAXFNU',MAXFNU)

      N=0
      LINE=' '
      DO 10 I=1,MAXFNU
        WRITE(CFNM,'(A6,I2.2)') 'FLNAME',I
        CALL SWCGET(CFNM,CFLN)
        NC=LENC(CFLN)
        IF (I.LT.10) THEN
          NX=4+NC
          WRITE(LINE(N+1:N+NX),'(TR1,I1,A1,A,A1)') I,':',CFLN(1:NC),','
        ELSE
          NX=5+NC
          WRITE(LINE(N+1:N+NX),'(TR1,I2,A1,A,A1)') I,':',CFLN(1:NC),','
        END IF
        N=N+NX
   10 CONTINUE
      WRITE(LINE(N:N+1),'(A2)') ' ;'
      WRITE(IU,'(A)') LINE(1:N+1)

      END
