*-----------------------------------------------------------------------
*     POLYLINE PRIMITIVE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGPLU(N,UPX,UPY)

      REAL      UPX(*),UPY(*),VPX(*),VPY(*),RPX(*),RPY(*)

      SAVE

      DATA      ITYPEZ/1/,INDEXZ/1/


      IF (N.LT.2) THEN
        CALL MSGDMP('E','SGPLU','NUMBER OF POINTS IS LESS THAN 2.')
      END IF
      IF (ITYPEZ.EQ.0) THEN
        CALL MSGDMP('M','SGPLU','LINETYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.EQ.0) THEN
        CALL MSGDMP('M','SGPLU','POLYLINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.LT.0) THEN
        CALL MSGDMP('E','SGPLU','POLYLINE INDEX IS LESS THAN 0.')
      END IF

      CALL SZPLOP(ITYPEZ,INDEXZ)
      CALL SZPLZU(N,UPX,UPY)
      CALL SZPLCL

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGPLV(N,VPX,VPY)

      IF (N.LT.2) THEN
        CALL MSGDMP('E','SGPLV','NUMBER OF POINTS IS LESS THAN 2.')
      END IF
      IF (ITYPEZ.EQ.0) THEN
        CALL MSGDMP('M','SGPLV','LINETYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.EQ.0) THEN
        CALL MSGDMP('M','SGPLV','POLYLINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.LT.0) THEN
        CALL MSGDMP('E','SGPLV','POLYLINE INDEX IS LESS THAN 0.')
      END IF

      CALL SZPLOP(ITYPEZ,INDEXZ)
      CALL SZPLZV(N,VPX,VPY)
      CALL SZPLCL

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGPLR(N,RPX,RPY)

      IF (N.LT.2) THEN
        CALL MSGDMP('E','SGPLR','NUMBER OF POINTS IS LESS THAN 2.')
      END IF
      IF (ITYPEZ.EQ.0) THEN
        CALL MSGDMP('M','SGPLR','LINETYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.EQ.0) THEN
        CALL MSGDMP('M','SGPLR','POLYLINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEXZ.LT.0) THEN
        CALL MSGDMP('E','SGPLR','POLYLINE INDEX IS LESS THAN 0.')
      END IF

      CALL SZPLOP(ITYPEZ,INDEXZ)
      CALL SZPLZR(N,RPX,RPY)
      CALL SZPLCL

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGSPLT(ITYPE)

      ITYPEZ=ITYPE

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQPLT(ITYPE)

      ITYPE=ITYPEZ

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGSPLI(INDEX)

      INDEXZ=INDEX

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQPLI(INDEX)

      INDEX=INDEXZ

      RETURN
      END
