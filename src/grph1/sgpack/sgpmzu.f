*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGPMZU(N,UPX,UPY,ITYPE,INDEX,RSIZE)

      REAL      UPX(*),UPY(*)


      IF (N.LT.1) THEN
        CALL MSGDMP('E','SGPMZU','NUMBER OF POINTS IS LESS THAN 1.')
      END IF
      IF (ITYPE.EQ.0) THEN
        CALL MSGDMP('M','SGPMZU','MARKER TYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','SGPMZU','POLYMARKER INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','SGPMZU','POLYMARKER INDEX IS LESS THAN 0.')
      END IF
      IF (RSIZE.EQ.0) THEN
        CALL MSGDMP('M','SGPMZU','MARKER SIZE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (RSIZE.LT.0) THEN
        CALL MSGDMP('E','SGPMZU','MARKER SIZE IS LESS THAN ZERO.')
      END IF

      CALL SZPMOP(ITYPE,INDEX,RSIZE)
      CALL SZPMZU(N,UPX,UPY)
      CALL SZPMCL

      END
