*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGSCMN(NC)

      LOGICAL LFCATR,LCMCH

      CALL SWCMLL
      CALL SWISTX('ICLRMAP',NC)

      CALL SWQFCC(LFCATR)
      CALL SWLGET('LCMCH',LCMCH)
      CALL SWIGET('IWS', IWS)
      IF ((IWS.GE.1).AND.(IWS.LE.4)) THEN
      IF (LCMCH) THEN
        IF ((.NOT.LFCATR).OR.(IWS.EQ.1)) THEN
          CALL MSGDMP('W','SGSCMN',
     *         'COLORMAP MIGHT NOT CHANGE IMMEDIATELY.')
        ENDIF
        CALL SWCLCH()
      ENDIF
      ENDIF
      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQCMN(NMAX)

      CALL SWCMLL
      CALL SWQCMN(NMAX)

      RETURN
      END
