*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGSMPL(PLX, PLY, PLROT)


      CALL SGRSET('PLX',PLX)
      CALL SGRSET('PLY',PLY)
      CALL SGRSET('PLROT',PLROT)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQMPL(PLX, PLY, PLROT)

      CALL SGRGET('PLX',PLX)
      CALL SGRGET('PLY',PLY)
      CALL SGRGET('PLROT',PLROT)

      RETURN
      END
