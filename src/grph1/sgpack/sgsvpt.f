*-----------------------------------------------------------------------
*     NORMALIZATION TRANSFORMATION
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGSVPT(VXMIN, VXMAX, VYMIN, VYMAX)


      CALL SGRSET('VXMIN',VXMIN)
      CALL SGRSET('VXMAX',VXMAX)
      CALL SGRSET('VYMIN',VYMIN)
      CALL SGRSET('VYMAX',VYMAX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQVPT(VXMIN, VXMAX, VYMIN, VYMAX)

      CALL SGRGET('VXMIN',VXMIN)
      CALL SGRGET('VXMAX',VXMAX)
      CALL SGRGET('VYMIN',VYMIN)
      CALL SGRGET('VYMAX',VYMAX)

      RETURN
      END
