*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGSWND(UXMIN, UXMAX, UYMIN, UYMAX)


      CALL SGRSET('UXMIN',UXMIN)
      CALL SGRSET('UXMAX',UXMAX)
      CALL SGRSET('UYMIN',UYMIN)
      CALL SGRSET('UYMAX',UYMAX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQWND(UXMIN, UXMAX, UYMIN, UYMAX)

      CALL SGRGET('UXMIN',UXMIN)
      CALL SGRGET('UXMAX',UXMAX)
      CALL SGRGET('UYMIN',UYMIN)
      CALL SGRGET('UYMAX',UYMAX)

      RETURN
      END
