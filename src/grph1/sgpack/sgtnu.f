*-----------------------------------------------------------------------
*     TONE PRIMITIVE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SGTNU(N,UPX,UPY)

      REAL      UPX(*),UPY(*),VPX(*),VPY(*),RPX(*),RPY(*)

      SAVE

      DATA      ITPATZ/1/


      IF (N.LT.3) THEN
        CALL MSGDMP('E','SGTNU','NUMBER OF POINTS IS LESS THAN 3.')
      END IF
      IF (ITPATZ.EQ.0) THEN
        CALL MSGDMP('M','SGTNU','TONE PAT. INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (ITPATZ.LT.0) THEN
        CALL MSGDMP('E','SGTNU','TONE PAT. INDEX IS LESS THAN 0.')
      END IF

      CALL SZTNOP(ITPATZ)
      CALL SZTNZU(N,UPX,UPY)
      CALL SZTNCL

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGTNV(N,VPX,VPY)

      IF (N.LT.3) THEN
        CALL MSGDMP('E','SGTNV','NUMBER OF POINTS IS LESS THAN 3.')
      END IF
      IF (ITPATZ.EQ.0) THEN
        CALL MSGDMP('M','SGTNV','TONE PAT. INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (ITPATZ.LT.0) THEN
        CALL MSGDMP('E','SGTNV','TONE PAT. INDEX IS LESS THAN 0.')
      END IF

      CALL SZTNOP(ITPATZ)
      CALL SZTNZV(N,VPX,VPY)
      CALL SZTNCL

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGTNR(N,RPX,RPY)

      IF (N.LT.3) THEN
        CALL MSGDMP('E','SGTNR','NUMBER OF POINTS IS LESS THAN 3.')
      END IF
      IF (ITPATZ.EQ.0) THEN
        CALL MSGDMP('M','SGTNR','TONE PAT. INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (ITPATZ.LT.0) THEN
        CALL MSGDMP('E','SGTNR','TONE PAT. INDEX IS LESS THAN 0.')
      END IF

      CALL SZTNOP(ITPATZ)
      CALL SZTNZR(N,RPX,RPY)
      CALL SZTNCL

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGSTNP(ITPAT)

      ITPATZ=ITPAT

      RETURN
*-----------------------------------------------------------------------
      ENTRY SGQTNP(ITPAT)

      ITPAT=ITPATZ

      RETURN
      END
