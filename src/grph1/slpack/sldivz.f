*-----------------------------------------------------------------------
*     BASIC SUBROUTINES
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SLDIVZ(XAMIN,XAMAX,YAMIN,YAMAX,CFORM,IX,IY,
     +                  XBMIN,XBMAX,YBMIN,YBMAX)

      REAL      XBMIN(IX*IY),XBMAX(IX*IY),YBMIN(IX*IY),YBMAX(IX*IY)
      CHARACTER CFORM*(*)

      LOGICAL   LCHREQ
      CHARACTER CF1*1

      EXTERNAL  LCHREQ


      CF1=CFORM(1:1)

      WXA=(XAMAX-XAMIN)/IX
      WYA=(YAMAX-YAMIN)/IY

      DO 10 I=1,IX*IY
        IF (LCHREQ(CF1,'T') .OR. LCHREQ(CF1,'L')) THEN
          JX=(I-1)/IY+1
          JY=MOD(I-1,IY)+1
        ELSE
          JX=MOD(I-1,IX)+1
          JY=(I-1)/IX+1
        END IF
        XBMIN(I)=XAMIN+WXA*(JX- 1)
        XBMAX(I)=XAMIN+WXA* JX
        YBMIN(I)=YAMIN+WYA*(IY-JY)
        YBMAX(I)=YAMIN+WYA*(IY-JY+1)
   10 CONTINUE

      END
