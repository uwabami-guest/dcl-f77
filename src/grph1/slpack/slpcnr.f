*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SLPCNR

      LOGICAL   LCRNR


      CALL SGLGET('LCORNER',LCRNR)
      IF (.NOT.LCRNR) RETURN

      CALL SGIGET('INDEX',INDEX)
      CALL SGRGET('CORNER',RC)

      CALL SLPWVC(INDEX,RC)

      END
