*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SLQRCT(LEV,IFRM,XAMIN,XAMAX,YAMIN,YAMAX)

      COMMON    /SLBLK1/ XMIN,XMAX,YMIN,YMAX,NN
      PARAMETER (MAXFR=1000,MAXLEV=3)
      INTEGER   NN(0:MAXLEV)
      REAL      XMIN(MAXFR),XMAX(MAXFR),YMIN(MAXFR),YMAX(MAXFR)

      EXTERNAL  ISUM0

      CALL SGIGET('NLEVEL',LEVC)

      IF (.NOT.(0.LE.LEV .AND. LEV.LE.LEVC)) THEN
        CALL MSGDMP('E','SLQRCT','LEVEL NUMBER IS INVALID.')
      END IF
      IF (IFRM.LE.0) THEN
        CALL MSGDMP('E','SLQRCT','FRAME NUMBER IS LESS THAN ZERO.')
      END IF

      JFRM=MOD(IFRM-1,NN(LEV))+1
      NM=JFRM+ISUM0(NN,LEV,1)
      XAMIN=XMIN(NM)
      XAMAX=XMAX(NM)
      YAMIN=YMIN(NM)
      YAMAX=YMAX(NM)

      END
