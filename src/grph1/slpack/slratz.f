*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SLRATZ(XAMIN,XAMAX,YAMIN,YAMAX,RX,RY)


      WXA=XAMAX-XAMIN
      WYA=YAMAX-YAMIN

      IF (WXA.LT.0 .OR. WYA.LT.0) THEN
        CALL MSGDMP('E','SLRATZ','RECTANGLE DEFINITION IS INVALID.')
      END IF
      IF (RX.LT.0 .OR. RY.LT.0) THEN
        CALL MSGDMP('E','SLRATZ',
     +    'PROPORTION PARAMETER IS LESS THAN ZERO.')
      END IF

      ARAT=WYA/WXA
      BRAT=RY/RX
      IF (ARAT.GE.BRAT) THEN
        XX=0
        YY=(1-BRAT/ARAT)/2
      ELSE
        XX=(1-ARAT/BRAT)/2
        YY=0
      END IF
      CALL SLMGNZ(XAMIN,XAMAX,YAMIN,YAMAX,XX,XX,YY,YY)

      END
