*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SLSTTL(CTTL,CSIDE,PX,PY,HT,NT)

      CHARACTER CTTL*(*),CSIDE*(*)

      PARAMETER (MAXTTL=5)

      REAL      PXZ(MAXTTL),PYZ(MAXTTL),HTZ(MAXTTL)
      CHARACTER CTTLZ(MAXTTL)*2048,CSIDEZ(MAXTTL)*1,CTTLW*2048,CS*1
      LOGICAL   LSET(MAXTTL),LCHREQ,LTITLE

      EXTERNAL  LCHREQ,LENC

      SAVE

      DATA      LSET/MAXTTL*.FALSE./


      CS=CSIDE(1:1)
      IF (.NOT.(LCHREQ(CS,'T') .OR. LCHREQ(CS,'B'))) THEN
        CALL MSGDMP('E','SLSTTL','SIDE PARAMETER IS INVALID.')
      END IF
      IF (.NOT.(-1.0.LE.PX .AND. PX.LE.+1.0
     +    .AND. -1.0.LE.PY .AND. PY.LE.+1.0)) THEN
        CALL MSGDMP('E','SLSTTL','POSITION PARAMETER IS INVALID.')
      END IF
      IF (.NOT.(HT.GE.0)) THEN
        CALL MSGDMP('E','SLSTTL','TEXT HEIGHT IS LESS THAN ZERO.')
      END IF
      IF (.NOT.(1.LE.NT .AND. NT.LE.MAXTTL)) THEN
        CALL MSGDMP('E','SLSTTL','TITLE NUMBER IS OUT OF RANGE.')
      END IF

      LSET(NT)=.TRUE.

      IF(LENC(CTTL).GT.1024) THEN
        CALL MSGDMP('W','SLSTTL',
     *          'STRING LENGTH TOO LONG.SHORTEND.')
        CTTLZ(NT)=CTTL(1:1024)
      ELSE
        CTTLZ(NT)=CTTL(1:LENC(CTTL))
      END IF
      CSIDEZ(NT)=CS
      PXZ(NT)=PX
      PYZ(NT)=PY
      HTZ(NT)=HT

      RETURN
*-----------------------------------------------------------------------
      ENTRY SLDTTL(NT)

      IF (.NOT.(1.LE.NT .AND. NT.LE.MAXTTL)) THEN
        CALL MSGDMP('E','SLDTTL','TITLE NUMBER IS OUT OF RANGE.')
      END IF

      LSET(NT)=.FALSE.

      RETURN
*-----------------------------------------------------------------------
      ENTRY SLPTTL

      CALL SGIGET('INDEX',INDEX)
      CALL SGLGET('LTITLE',LTITLE)

      IF (.NOT.LTITLE) RETURN

      DO 10 I=1,MAXTTL
        IF (LSET(I)) THEN
          CALL SLTLCV(CTTLZ(I),CTTLW,LC)
          CALL SLZTTL(CSIDEZ(I),CTTLW(1:LC),PXZ(I),PYZ(I),HTZ(I),INDEX)
        END IF
   10 CONTINUE

      RETURN
      END
