*-----------------------------------------------------------------------
*     INTEGER PARAMETER CONTROL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SWIQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA  = 13)
      PARAMETER (MAXWNU = @DCLNWS )
      PARAMETER (MAXFNU = 4 )

      INTEGER   IX(NPARA)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ,LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS(1) / 'MAXWNU  ' / , IX(1) / MAXWNU /
      DATA      CPARAS(2) / 'IWS     ' / , IX(2) / 0 /
      DATA      CPARAS(3) / 'IPOSX   ' / , IX(3) / -999 /
      DATA      CPARAS(4) / 'IPOSY   ' / , IX(4) / -999 /
      DATA      CPARAS(5) / 'IWIDTH  ' / , IX(5) / @IWIDTH  /
      DATA      CPARAS(6) / 'IHEIGHT ' / , IX(6) / @IHEIGHT /
      DATA      CPARAS(7) / 'MODE    ' / , IX(7) / 1 /
      DATA      CPARAS(8) / 'NLNSIZE ' / , IX(8) / 21 /
      DATA      CPARAS(9) / 'ICLRMAP ' / , IX(9) / 1 /
      DATA      CPARAS(10) / 'IDMPDGT ' / , IX(10) / 4 /
      DATA      CPARAS(11) / 'MAXFNU  ' / , IX(11) / MAXFNU /
      DATA      CPARAS(12) / 'IFL     ' / , IX(12) / 4 /
      DATA      CPARAS(13) / 'IBGPAGE ' / , IX(13) / 1 /
*      DATA      CPARAS(11) / 'IDMPFMT ' / , IX(11) / 1 /


*     / LONG NAME /

      DATA      CPARAL(1) / '****MAXWNU  ' /
      DATA      CPARAL(2) / '****IWS     ' /
      DATA      CPARAL(3) / 'WINDOW_X_POS' /
      DATA      CPARAL(4) / 'WINDOW_Y_POS' /
      DATA      CPARAL(5) / 'WINDOW_WIDTH' /
      DATA      CPARAL(6) / 'WINDOW_HEIGHT' /
      DATA      CPARAL(7) / '****MODE    ' /
      DATA      CPARAL(8) / '****NLSIZE  ' /
      DATA      CPARAL(9) / 'COLORMAP_NUMBER' /
      DATA      CPARAL(10) / 'DMPFILE_DIGITS' /
      DATA      CPARAL(11) / '****MAXFNU  ' /
      DATA      CPARAL(12) / '****IFL     ' /
*      DATA      CPARAL(11) / 'DMPFILE_FORMAT' /

      DATA      LFIRST / .TRUE. /

      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWIQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','SWIQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWIQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','SWIQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWIQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','SWIQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWIQVL(IDX, IPARA)

      IF (LFIRST) THEN
        CALL RTIGET('SW', CPARAS, IX, NPARA)
        CALL RLIGET(CPARAL, IX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IPARA = IX(IDX)
      ELSE
        CALL MSGDMP('E','SWIQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWISVL(IDX, IPARA)

      IF (LFIRST) THEN
        CALL RTIGET('SW', CPARAS, IX, NPARA)
        CALL RLIGET(CPARAL, IX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IX(IDX) = IPARA
      ELSE
        CALL MSGDMP('E','SWISVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWIQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
