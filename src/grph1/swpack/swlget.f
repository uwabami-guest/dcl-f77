*-----------------------------------------------------------------------
*     SWLGET / SWLSET / SWLSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SWLGET(CP, LPARA)

      LOGICAL   LPARA
      CHARACTER CP*(*)

      LOGICAL   LP
      CHARACTER CX*8
      CHARACTER CL*40

      CALL SWLQID(CP, IDX)
      CALL SWLQVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWLSET(CP, LPARA)

      CALL SWLQID(CP, IDX)
      CALL SWLSVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWLSTX(CP, LPARA)

      LP = LPARA
      CALL SWLQID(CP, IDX)

*     / SHORT NAME /

      CALL SWLQCP(IDX, CX)
      CALL RTLGET('SW', CX, LP, 1)

*     / LONG NAME /

      CALL SWLQCL(IDX, CL)
      CALL RLLGET(CL, LP, 1)

      CALL SWLSVL(IDX,LP)

      RETURN
      END
