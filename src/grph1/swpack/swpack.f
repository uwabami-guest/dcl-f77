*-----------------------------------------------------------------------
*     SWPACK
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SWDOPN

      INTEGER   IMAGE(*)
      REAL      WPX(*), WPY(*)
      LOGICAL   LFCATR, LWDATR, LCLATR, LTNATR, LIMATR, LPTATR
      CHARACTER COBJ*(*), COMM*(*), FONTNAME*(*), CHARS*(*)

      LOGICAL   LWAIT, LWAIT0, LWAIT1, LKEY, LDUMP, LALT,
     +          LCOLOR, LSEP, LPRINT, LFGBG, LOPEN, LCLIP
      CHARACTER CMSG*80, CLRMAP*1024, CBTMAP*1024,
     +          COUT*1024, CTTL*80, CMAP*80, DSPATH*80
      LOGICAL   LWND
      CHARACTER CIMGFMT*8
      REAL      RIMGCMP
      CHARACTER CNOPN*26

      INTEGER IDMPDGT
      LOGICAL LSTDOT

      REAL      RLWFACT

      LOGICAL   LFPROP, LCNTL
      INTEGER   ISUP, ISUB, IRST
      REAL      SMALL, SHIFT

      REAL VXMIN,VXMAX,VYMIN,VYMAX
      REAL WXMIN,WXMAX,WYMIN,WYMAX

      LOGICAL LVAL
      INTEGER   IVAL
      REAL RVAL

      INTEGER FNTNUM

      SAVE

      DATA      CNOPN / 'WORKSTATION IS NOT OPENED.' /
      DATA      LOPEN / .FALSE. /

*     / OPEN ( AND ACTIVATE ) WORKSTATION /

      CALL SGIGET('IWS', JWS)
      CALL SWIGET('IWS', IWS)
      CALL SWIGET('IFL', IFL)

      CMSG='GRPH1 : STARTED / IWS = ##.'
      CALL CHNGI(CMSG,'##',JWS,'(I2)')
      CALL MSGDMP('M','SWDOPN',CMSG)

*     / DEVICE DEPENDENT PARAMETERS /
      IF (IWS.EQ.1 .OR. IWS.EQ.2) THEN
        CALL SWIGET('IWIDTH ', IWIDTH)
        CALL SWIGET('IHEIGHT', IHEIGH)
        CALL SWIGET('ICLRMAP', ICLRMP)
        CALL SWLGET('LFGBG  ', LFGBG )
        CALL SWCGET('FNAME  ', COUT  )
        CALL SWCGET('TITLE  ', CTTL  )
        CALL SWIGET('IDMPDGT ', IDMPDGT)
        CALL SWIGET('IBGPAGE ',IBGPAGE)
        CALL SWCMLL
        CALL SWQFNM('CLRMAP ', CLRMAP)
        CALL GLCGET('DSPATH',  DSPATH)

        IF (CLRMAP.EQ.' ') THEN
          CALL MSGDMP('E','SWDOPN','COLORMAP FILE DOES NOT EXIST.')
        END IF

        IF (COUT.EQ.' ' .OR. COUT.EQ.'*') CALL OSGARG(0, COUT)
        IF (COUT.EQ.' ') THEN
          COUT = 'DCL'
          CALL CLOWER(COUT)
        END IF

        IF (CTTL.EQ.' ' .OR. CTTL.EQ.'*') CALL DCLVNM(CTTL)

      END IF

        CALL SWIGET('IPOSX  ', IPOSX )
        CALL SWIGET('IPOSY  ', IPOSY )
        CALL SWLGET('LWAIT  ', LWAIT )
        CALL SWLGET('LWAIT0 ', LWAIT0)
        CALL SWLGET('LWAIT1 ', LWAIT1)
        CALL SWLGET('LKEY   ', LKEY  )
        CALL SWLGET('LALT   ', LALT  )
        CALL SWLGET('LDUMP  ', LDUMP )
        CALL SWLGET('LSTDOT ', LSTDOT )

        CALL SWQFNM('BITMAP ', CBTMAP)
        IF (CBTMAP.EQ.' ') THEN
          CALL MSGDMP('E','SWDOPN','BITMAP FILE DOES NOT EXIST.')
        END IF

        CALL SWLGET('LWND   ', LWND  )
        CALL SWCGET('CIMGFMT ', CIMGFMT)
        CALL SWRGET('RIMGCMP ', RIMGCMP)

        CALL SWLGET('LSEP   ', LSEP  )

        CALL SWRGET('RLWFACT', RLWFACT)

        CALL SGLGET('LFPROP ', LFPROP)
        CALL SGLGET('LCNTL  ', LCNTL )
        CALL SGIGET('ISUP   ', ISUP  )
        CALL SGIGET('ISUB   ', ISUB  )
        CALL SGIGET('IRST   ', IRST  )
        CALL SGRGET('SMALL  ', SMALL )
        CALL SGRGET('SHIFT  ', SHIFT )

        CALL ZGLSET('lfprop  ',LENZ('lfprop'),LFPROP)
        CALL ZGLSET('lcntl   ',LENZ('lcntl') ,LCNTL)
        CALL ZGISET('isup    ',LENZ('isup')  ,ISUP)
        CALL ZGISET('isub    ',LENZ('isub')  ,ISUB)
        CALL ZGISET('irst    ',LENZ('irst')  ,IRST)
        CALL ZGRSET('small   ',LENZ('small') ,SMALL)
        CALL ZGRSET('shift   ',LENZ('shift') ,SHIFT)

        IF (CLRMAP.EQ.(DSPATH(1:LENC(DSPATH))//'colormap.x11')) THEN
          CALL SWQCMF(ICLRMP,CMAP)
          CALL SWCSET('CLRMAP ',CMAP)
        END IF

        CALL SWCSET('CLRMAP',CMAP)
        CALL SWQFNM('CLRMAP ', CLRMAP)

        IF (CLRMAP.EQ.' ') THEN
          CALL MSGDMP('E','SWDOPN','COLORMAP FILE DOES NOT EXIST.')
        END IF

        CALL ZGDOPN(JWS, IWIDTH, IHEIGH, IPOSX, IPOSY, IDMPDGT,
     +       IBGPAGE,RLWFACT,
     +       LWAIT, LWAIT0, LWAIT1, LALT, LSTDOT,LKEY, LDUMP, LWND,
     +       LFGBG,LSEP,IFL,CIMGFMT, RIMGCMP, CLRMAP, CBTMAP, COUT,
     +       CTTL)

      LOPEN = .TRUE.

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWDCLS

*     / ( DEACTIVATE AND ) CLOSE WORKSTATION /

      CALL ZGDCLS
      CMSG='GRPH1 : TERMINATED.'
      CALL MSGDMP('M','SWDCLS',CMSG)
      LOPEN = .FALSE.

      RETURN
*-----------------------------------------------------------------------
*     PAGE
*-----------------------------------------------------------------------
      ENTRY SWPOPN

*     / OPEN PAGE ( OR SCREEN ) /

      IF (.NOT. LOPEN)
     +   CALL MSGDMP('E', 'SWPOPN', CNOPN)
      CALL ZGPOPN

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWFLSH

*     / PAGE FLASH EXPLICIT /

      CALL ZGFLASH()

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWPCLS

*     / CLOSE PAGE ( OR SCREEN ) /
      IF (.NOT. LOPEN)
     +   CALL MSGDMP('E', 'SWPCLS', CNOPN)
      NPAGE=NPAGE+1
      CMSG='GRPH1 : PAGE = ### COMPLETED.'
      CALL CHNGI(CMSG,'###',NPAGE,'(I3)')
      CALL MSGDMP('M','SWPCLS',CMSG)
      CALL ZGPCLS

      RETURN
*-----------------------------------------------------------------------
*     OBJECT
*-----------------------------------------------------------------------
      ENTRY SWOOPN(COBJ, COMM)

*     / OPEN OBJECT /

      IF (.NOT. LOPEN)
     +   CALL MSGDMP('E', 'SWOOPN', CNOPN)
      CALL ZGOOPN(COBJ, COMM)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWOCLS(COBJ)

*     / CLOSE OBJECT /

      IF (.NOT. LOPEN)
     +   CALL MSGDMP('E', 'SWOCLS', CNOPN)
      CALL ZGOCLS(COBJ)

      RETURN
*-----------------------------------------------------------------------
*     FULL COLOR (2000/12/10 TEST)
*-----------------------------------------------------------------------
      ENTRY SWQFCC(LFCATR)

*     / INQUIRE FULL COLOR CAPABILITY /

      LFCATR = .TRUE.

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWSFCM(LFCMOD)

*     / SET FULL COLOR MODE /

      CALL ZGSFCM(LFCMOD)

      RETURN
*-----------------------------------------------------------------------
*     LINE
*-----------------------------------------------------------------------
      ENTRY SWSWDI(IWDIDX)

*     / SET LINE WIDTH INDEX /

      IF (.NOT. LOPEN)
     +   CALL MSGDMP('E', 'SWSWDI', CNOPN)
      CALL ZGSWDI(IWDIDX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWSCLI(ICLIDX,LFCATR)

*     / SET LINE COLOR INDEX /

      IF (.NOT. LOPEN)
     +   CALL MSGDMP('E', 'SWSCLI', CNOPN)
      CALL ZGSCLI(ICLIDX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWSLCL(ICOLOR)

*     / SET LINE COLOR IN 24BIT RGB /

      CALL ZGSLCL(ICOLOR)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWSFW(IWIDX)

*     / SET FONT WEIGHT /

      CALL ZGSFW(IWIDX)

      RETURN

*-----------------------------------------------------------------------
*     OPEN
*-----------------------------------------------------------------------
      ENTRY SWGOPN

*     / OPEN GRAPHIC SEGMENT /

      IF (.NOT. LOPEN)
     +   CALL MSGDMP('E', 'SWGOPN', CNOPN)
      CALL ZGGOPN

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWGMOV(WX,WY)

*     / PEN-UP MOVE /

      IF (.NOT. LOPEN)
     +   CALL MSGDMP('E', 'SWGMOV', CNOPN)
      CALL ZGGMOV(WX,WY)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWGPLT(WX,WY)

*     / PEN-DOWN MOVE /

      IF (.NOT. LOPEN)
     +   CALL MSGDMP('E', 'SWGPLT', CNOPN)
      CALL ZGGPLT(WX,WY)

      RETURN
*-----------------------------------------------------------------------
*     CLOSE
*-----------------------------------------------------------------------
      ENTRY SWGCLS

*     / CLOSE GRAPHIC SEGMENT /

      IF (.NOT. LOPEN)
     +   CALL MSGDMP('E', 'SWGCLS', CNOPN)
      CALL ZGGCLS

      RETURN
*-----------------------------------------------------------------------
*     TONE
*-----------------------------------------------------------------------
      ENTRY SWSTCL(ICOLOR)

*     / SET TONE COLOR IN 24BIT RGB /

      CALL ZGSTCL(ICOLOR)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWGTON(NP,WPX,WPY,ITPAT)

*     / HARD FILL /

      IF (.NOT. LOPEN)
     +   CALL MSGDMP('E', 'SWGTON', CNOPN)
      CALL ZGGTON(NP,WPX,WPY,ITPAT)

      RETURN
*-----------------------------------------------------------------------
*     IMAGE
*-----------------------------------------------------------------------
      ENTRY SWIOPN(IWX,IWY,IMW,IMH,
     +             WX1, WY1, WX2, WY2, WX3, WY3, WX4, WY4)

*     / OPEN IMAGE /

      IF (.NOT. LOPEN)
     +   CALL MSGDMP('E', 'SWIOPN', CNOPN)
      CALL ZGIOPN(IWX, IWY, IMW, IMH)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWIDAT(IMAGE, NLEN)

*     / IMAGE DATA /

      IF (.NOT. LOPEN)
     +   CALL MSGDMP('E', 'SWIDAT', CNOPN)
      CALL ZGIDAT(IMAGE, NLEN)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWICLR(IMAGE, NLEN)

*     / IMAGE DATA (FULL COLOR) /
      
      CALL ZGICLR(IMAGE, NLEN)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWICLS

*     / IMAGE CLOSE /

      IF (.NOT. LOPEN)
     +   CALL MSGDMP('E', 'SWICLS', CNOPN)
      CALL ZGICLS

      RETURN
*-----------------------------------------------------------------------
*     MOUSE
*-----------------------------------------------------------------------
      ENTRY SWQPNT(WX,WY,MB)

*     / INQUIRE POSITION OF POINTER /

      IF (.NOT. LOPEN)
     +   CALL MSGDMP('E', 'SWQPNT', CNOPN)
      CALL ZGQPNT(WX,WY,MB)

      RETURN
*-----------------------------------------------------------------------
*     TRANSFORMATION
*-----------------------------------------------------------------------
      ENTRY SWFINT(WX, WY, IWX, IWY)

*     / REAL TO INT /

      IF (.NOT. LOPEN)
     +   CALL MSGDMP('E', 'SWFINT', CNOPN)
      CALL ZGFINT(WX, WY, IWX, IWY)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWIINT(IWX, IWY, WX, WY)

*     / INT TO REAL /

      IF (.NOT. LOPEN)
     +   CALL MSGDMP('E', 'SWIINT', CNOPN)
      CALL ZGIINT(IWX, IWY, WX, WY)

      RETURN
*-----------------------------------------------------------------------
*     INQUIRY
*-----------------------------------------------------------------------
      ENTRY SWQWDC(LWDATR)

*     / INQUIRE LINE WIDTH CAPABILITY /

      CALL ZGQWDC(LWDATR)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWQCLC(LCLATR)

*     / INQUIRE LINE COLOR CAPABILITY /

      IF (.NOT. LOPEN)
     +   CALL MSGDMP('E', 'SWQCLC', CNOPN)
      CALL ZGQCLC(LCLATR)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWQTNC(LTNATR)

*     / INQUIRE HARD FILL CAPABILITY /

      CALL ZGQTNC(LTNATR)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWQIMC(LIMATR)

*     / INQUIRE BIT IMAGE CAPABILITY /

      CALL ZGQIMC(LIMATR)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWQPTC(LPTATR)

*     / INQUIRE MOUSE POINT CAPABILITY /

      CALL ZGQPTC(LPTATR)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWQRCT(WSXMN,WSXMX,WSYMN,WSYMX,FACT)

*     / INQUIRE WORKSTATION RECTANGLE /

      IF (.NOT. LOPEN)
     +   CALL MSGDMP('E', 'SWQRCT', CNOPN)
      CALL ZGQRCT(WSXMN,WSXMX,WSYMN,WSYMX,FACT)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWSROT(IWTROT)

*     / SET FRAME ROTATION FLAG /

      IF (.NOT. LOPEN)
     +   CALL MSGDMP('E', 'SWSROT', CNOPN)
      CALL ZGSROT(IWTROT)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWCLCH()

*     / CHANGE COLORMAP IMMIDIATELY /

      CALL SWIGET('ICLRMAP', ICLRMP)
      CALL SWQCMF(ICLRMP,CMAP)
      CALL SWCSET('CLRMAP',CMAP)
      CALL SWQFNM('CLRMAP ', CLRMAP)
      CALL SWLGET('LFGBG', LFGBG)
      IF (CLRMAP.EQ.' ') THEN
        CALL MSGDMP('E','SWDOPN','COLORMAP FILE DOES NOT EXIST.')
      END IF

      CALL ZGCLINI(CLRMAP,LFGBG)

      RETURN
*-----------------------------------------------------------------------
*     TEXT
*-----------------------------------------------------------------------
      ENTRY SWQTXW(CHARS,NCHZ,WXCH,WYCH)

*       / PASS SG* PARAMETERS TO ZGPACK /
      CALL SGLGET('LCNTL',LCNTL)
      CALL ZGLSET('lcntl ',LENZ('lcntl'), LCNTL)
      IF (LCNTL) THEN
        CALL SGIGET('ISUB', ISUB)
        CALL SGIGET('ISUP', ISUP)
        CALL SGIGET('IRST', IRST)
        CALL SGRGET('SMALL', SMALL)
        CALL SGRGET('SHIFT', SHIFT)
        CALL ZGISET('isub ',LENZ('isub'), ISUB)
        CALL ZGISET('isup ',LENZ('isup'), ISUP)
        CALL ZGISET('irst ',LENZ('irst'), IRST)
        CALL ZGRSET('small ',LENZ('small'), SMALL)
        CALL ZGRSET('shift ',LENZ('shift'), SHIFT)
      END IF

      CALL ZGQTXW(CHARS,NCHZ,WXCH,WYCH)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWFTFC(FONTNAME)

      CALL ZGFTFC(FONTNAME)

      RETURN

*-----------------------------------------------------------------------
      ENTRY SWFTNM(FNTNUM)

      CALL ZGNUMFONTS(FNTNUM)

      RETURN

*-----------------------------------------------------------------------
      ENTRY SWSLFT(FONTNAME)
      IF(IWS.EQ.2 .AND. IFL.NE.1) THEN
        CALL MSGDMP('M','SWSLFT',
     *    'DIALOG NEEDS WINDOW.DEFAULT FONT IS USED')
      ELSE
        CALL ZGSELECTFONT(FONTNAME)
      END IF

      RETURN

*-----------------------------------------------------------------------
      ENTRY SWGTFT(INUM,FONTNAME,IFMAX)

      CALL ZGFONTNAME(INUM,FONTNAME,IFMAX)

      RETURN

*-----------------------------------------------------------------------
      ENTRY SWLSFT()

      CALL ZGLISTFONTS()

      RETURN

*-----------------------------------------------------------------------
      ENTRY SWTXT(WX,WY,QSIZEXRATE,CHARS,NCZ,IROTA,ICENTZ)

      CALL SGPGET('LCLIP',LCLIP)
      CALL ZGRCLP()
      IF (LCLIP) THEN
        CALL SGQVPT(VXMIN,VXMAX,VYMIN,VYMAX)
        CALL STFWTR(VXMIN,VYMIN,WXMIN,WYMIN)
        CALL STFWTR(VXMAX,VYMAX,WXMAX,WYMAX)
        CALL ZGCLIP(WXMIN,WXMAX,WYMIN,WYMAX)
      ENDIF
*       / PASS SG* PARAMETERS TO ZGPACK /
      CALL SGLGET('LCNTL',LCNTL)
      CALL ZGLSET('lcntl ',LENZ('lcntl'), LCNTL)
      IF (LCNTL) THEN
        CALL SGIGET('ISUB', ISUB)
        CALL SGIGET('ISUP', ISUP)
        CALL SGIGET('IRST', IRST)
        CALL SGRGET('SMALL', SMALL)
        CALL SGRGET('SHIFT', SHIFT)
        CALL ZGISET('isub ',LENZ('isub'), ISUB)
        CALL ZGISET('isup ',LENZ('isup'), ISUP)
        CALL ZGISET('irst ',LENZ('irst'), IRST)
        CALL ZGRSET('small ',LENZ('small'), SMALL)
        CALL ZGRSET('shift ',LENZ('shift'), SHIFT)
      END IF
      CALL ZGTXT(WX,WY,QSIZEXRATE,CHARS,NCZ,IROTA,ICENTZ)
      RETURN

      END
