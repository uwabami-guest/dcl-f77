*-----------------------------------------------------------------------
*     SWPGET / SWPSET / SWPSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SWPGET(CP, IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8, CL*40

      CALL SWPQID(CP, IDX)
      CALL SWPQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWPSET(CP, IPARA)

      CALL SWPQID(CP, IDX)
      CALL SWPSVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SWPSTX(CP, IPARA)

      IP = IPARA
      CALL SWPQID(CP, IDX)
      CALL SWPQIT(IDX, IT)
      CALL SWPQCP(IDX, CX)
      CALL SWPQCL(IDX, CL)
      IF (IT .EQ. 1) THEN
        CALL RTIGET('SW', CX, IP, 1)
        CALL RLIGET(CL, IP, 1)
        CALL SWIQID(CP, IDX)
        CALL SWISVL(IDX, IP)
      ELSE IF (IT .EQ. 2) THEN
        CALL RTLGET('SW', CX, IP, 1)
        CALL RLLGET(CL, IP, 1)
        CALL SWLQID(CP, IDX)
        CALL SWLSVL(IDX, IP)
      ELSE IF (IT .EQ. 3) THEN
        CALL RTRGET('SW', CX, IP, 1)
        CALL RLRGET(CL, IP, 1)
        CALL SWRQID(CP, IDX)
        CALL SWRSVL(IDX, IP)
      END IF

      RETURN
      END
