*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZL3ZU(N,U3X,U3Y,U3Z)

      REAL      U3X(N),U3Y(N),U3Z(N)

      LOGICAL   LFLAG

      COMMON    /SZBPL1/ LMISS,RMISS
      LOGICAL   LMISS

      SAVE


      CALL SZOPL3

      IF (.NOT.LMISS) THEN
        CALL STFTR3(U3X(1), U3Y(1), U3Z(1), VX, VY, VZ)
        CALL SZMVL3(VX, VY, VZ)
        DO 10 I=2,N
          CALL STFTR3(U3X(I), U3Y(I), U3Z(I), VX, VY, VZ)
          CALL SZPLL3(VX, VY, VZ)
   10   CONTINUE
      ELSE
        LFLAG=.FALSE.
        DO 20 I=1,N
          IF (U3X(I).EQ.RMISS .OR. U3Y(I).EQ.RMISS
     +                        .OR. U3Z(I).EQ.RMISS) THEN
            LFLAG=.FALSE.
          ELSE IF (LFLAG) THEN
            CALL STFTR3(U3X(I), U3Y(I), U3Z(I), VX, VY, VZ)
            CALL SZPLL3(VX, VY, VZ)
          ELSE
            CALL STFTR3(U3X(I), U3Y(I), U3Z(I), VX, VY, VZ)
            CALL SZMVL3(VX, VY, VZ)
            LFLAG=.TRUE.
          END IF
   20   CONTINUE
      END IF

      CALL SZCLL3

      END
