*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZL3ZV(N,V3X,V3Y,V3Z)

      REAL      V3X(N),V3Y(N),V3Z(N)

      LOGICAL   LFLAG

      COMMON    /SZBPL1/ LMISS,RMISS
      LOGICAL   LMISS

      SAVE


      CALL SZOPL3

      IF (.NOT.LMISS) THEN
        CALL SZMVL3(V3X(1),V3Y(1), V3Z(1))
        DO 10 I=2,N
          CALL SZPLL3(V3X(I),V3Y(I), V3Z(I))
   10   CONTINUE
      ELSE
        LFLAG=.FALSE.
        DO 20 I=1,N
          IF (V3X(I).EQ.RMISS .OR. V3Y(I).EQ.RMISS
     +                        .OR. V3Z(I).EQ.RMISS) THEN
            LFLAG=.FALSE.
          ELSE IF (LFLAG) THEN
            CALL SZPLL3(V3X(I),V3Y(I), V3Z(I))
          ELSE
            CALL SZMVL3(V3X(I),V3Y(I), V3Z(I))
            LFLAG=.TRUE.
          END IF
   20   CONTINUE
      END IF

      CALL SZCLL3

      END
