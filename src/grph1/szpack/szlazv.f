*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZLAZV(VX1,VY1,VX2,VY2)

      COMMON    /SZBLA1/ LARRWZ,LPROPZ,AFACTZ,CONSTZ,ANGLEZ,
     +                   LATONZ,LUARWZ,CONSMZ,RDUNIT
      LOGICAL   LARRWZ,LPROPZ,LATONZ,LUARWZ

      EXTERNAL  RFPI

      SAVE


      R=SQRT((VX2-VX1)**2+(VY2-VY1)**2)
      IF (R.EQ.0) RETURN

      CALL SZOPLV
      CALL SZMVLV(VX1,VY1)
      CALL SZPLLV(VX2,VY2)
      CALL SZCLLV

      IF (.NOT.LARRWZ) RETURN

      PI=RFPI()

      IF (LPROPZ) THEN
        AR=R*AFACTZ
      ELSE
        AR=CONSTZ
      END IF

      XE=(VX2-VX1)/R*AR
      YE=(VY2-VY1)/R*AR
      CALL CR2C(-(PI-ANGLEZ*RDUNIT),XE,YE,XA1,YA1)
      CALL CR2C(-(PI+ANGLEZ*RDUNIT),XE,YE,XA2,YA2)

      IF (LATONZ) THEN
        CALL SZOPTV
        CALL SZSTTV(VX2+XA1,VY2+YA1)
        CALL SZSTTV(VX2,VY2)
        CALL SZSTTV(VX2+XA2,VY2+YA2)
        CALL SZCLTV
      ELSE
        CALL SZOPLV
        CALL SZMVLV(VX2+XA1,VY2+YA1)
        CALL SZPLLV(VX2,VY2)
        CALL SZPLLV(VX2+XA2,VY2+YA2)
        CALL SZCLLV
      END IF

  100 CONTINUE

      END
