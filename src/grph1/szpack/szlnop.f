*-----------------------------------------------------------------------
*     LINE SUBPRIMITIVE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZLNOP(INDEX)

      CHARACTER COBJ*80


      WRITE(COBJ,'(I8)') INDEX
      CALL CDBLK(COBJ)
      CALL SWOOPN('SZLN',COBJ)

      CALL SZSLTI(1,INDEX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZLNCL

      CALL SWOCLS('SZLN')

      RETURN
      END
