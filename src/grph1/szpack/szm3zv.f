*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZM3ZV(N,VPX,VPY,VPZ)

      REAL      VPX(*),VPY(*),VPZ(*)

      LOGICAL   LFLAG,LCLIPZ

      COMMON    /SZBPM1/ LMISS,RMISS,NPM
      LOGICAL   LMISS
      COMMON    /SZBPM2/ CMARK
      CHARACTER CMARK*1
      COMMON    /SZBTX3/ LCLIP
      LOGICAL   LCLIP

      SAVE


      LCLIPZ=LCLIP
      LCLIP=.FALSE.
      CALL STEPR2

      DO 10 I=1,N,NPM
        LFLAG=LMISS .AND. 
     +      (VPX(I).EQ.RMISS .OR. VPY(I).EQ.RMISS .OR. VPZ(I).EQ.RMISS)
        IF (.NOT.LFLAG) THEN
          CALL STFPR3(VPX(I), VPY(I), VPZ(I), RX, RY)
          CALL SZTXZV(RX,RY,CMARK)
        END IF
   10 CONTINUE

      LCLIP=LCLIPZ
      CALL STRPR2

      END
