*-----------------------------------------------------------------------
*     PLOT ROUTINE ON VC (CLIPPING - SOLID LINE FOR TEXT)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZOPSV

      LOGICAL   LVALID, LCONT, LMOVE

      COMMON    /SZBTX3/ LCLIP
      LOGICAL   LCLIP

      SAVE


      CALL SZOPLP

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZMVSV(VX,VY)

      IF (LCLIP) THEN
        CALL SZPCLL(VX, VY, VX, VY, LVALID, 1)
        IF (LVALID) THEN
          CALL SZMVLP(VX, VY)
        END IF
        VX0=VX
        VY0=VY
      ELSE
        CALL SZMVLP(VX, VY)
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZPLSV(VX,VY)

      IF (LCLIP) THEN
        CALL SZPCLL(VX0, VY0, VX, VY, LVALID, 1)
        IF (LVALID) THEN
   10     CONTINUE
            CALL SZGCLL(XX, YY, LCONT, LMOVE, 1)
            IF (LMOVE) THEN
              CALL SZMVLP(XX, YY)
            ELSE
              CALL SZPLLP(XX, YY)
            END IF
          IF (LCONT) GO TO 10
        END IF
        VX0=VX
        VY0=VY
      ELSE
        CALL SZPLLP(VX, VY)
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZCLSV

      CALL SZCLLP

      RETURN
      END
