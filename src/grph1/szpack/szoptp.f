*-----------------------------------------------------------------------
*     TONE ROUTINE ON VC (PERSPECTIVE TRANSFORMATION & SWITHCING)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZOPTP

      LOGICAL   LHARD

      LOGICAL   LHARDZ

      SAVE


      IF (LHARDZ) THEN
        CALL SZOPTR
      ELSE
        CALL SZOPTS
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZSTTP(VX,VY)

      IF (LHARDZ) THEN
        CALL STFPR2(VX, VY, RX, RY)
        CALL SZSTTR(RX, RY)
      ELSE
        CALL SZSTTS(VX, VY)
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZCLTP

      IF (LHARDZ) THEN
        CALL SZCLTR
      ELSE
        CALL SZCLTS
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZSTMD(LHARD)

      LHARDZ=LHARD

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZQTMD(LHARD)

      LHARD=LHARDZ

      RETURN
      END
