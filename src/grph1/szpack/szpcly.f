*-----------------------------------------------------------------------
*     CLIPPING ON TC - Y
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZPCLY(TX0, TY0, TX1, TY1, LVALID, LBOUND)

      PARAMETER (EPSIL=1.E-5)

      REAL      YB(2), XX(2), YY(2)
      LOGICAL   LVALID, LBOUND, LCONT, LIN0, LIN1, LRLTA, LRLEA

      EXTERNAL  LRLTA, LRLEA

      SAVE


      IF (LBOUND) THEN
        IF ( LRLTA(TY0, YB(1), EPSIL)) THEN
          IB0 = 1
        ELSE IF ( LRLTA(YB(2), TY0, EPSIL)) THEN
          IB0 = 2
        ELSE
          IB0 = 0
        END IF

        IF ( LRLTA(TY1, YB(1), EPSIL)) THEN
          IB1 = 1
        ELSE IF ( LRLTA(YB(2), TY1, EPSIL)) THEN
          IB1 = 2
        ELSE
          IB1 = 0
        END IF
      ELSE
        IF ( LRLEA(TY0, YB(1), EPSIL)) THEN
          IB0 = 1
        ELSE IF ( LRLEA(YB(2), TY0, EPSIL)) THEN
          IB0 = 2
        ELSE
          IB0 = 0
        END IF

        IF ( LRLEA(TY1, YB(1), EPSIL)) THEN
          IB1 = 1
        ELSE IF ( LRLEA(YB(2), TY1, EPSIL)) THEN
          IB1 = 2
        ELSE
          IB1 = 0
        END IF
      END IF

      LIN0 =  IB0.EQ.0
      LIN1 =  IB1.EQ.0
      LVALID = LIN0.OR.LIN1

      IF (LVALID) THEN
        IF (LIN0 .AND. LIN1) THEN
          XX(2) = TX1
          YY(2) = TY1
          N = 2
          RETURN

        ELSE
          IF (.NOT.LIN0) THEN
            CALL SZSGCL(TX1, TY1, TX0, TY0)
            YY(1) = YB(IB0)
            CALL SZQGCX(YY(1), XX(1))
            XX(2) = TX1
            YY(2) = TY1
            N = 1
          END IF

          IF (.NOT.LIN1) THEN
            CALL SZSGCL(TX0, TY0, TX1, TY1)
            YY(2) = YB(IB1)
            CALL SZQGCX(YY(2), XX(2))
            N=2
          END IF

        END IF
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZGCLY(TX, TY, LCONT)

      LCONT = N.EQ.1
      TX = XX(N)
      TY = YY(N)
      N  = N + 1

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZSCLY(YBND1, YBND2)

      YB(1) = YBND1
      YB(2) = YBND2

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZQCLY(YBND1, YBND2)

      YBND1 = YB(1)
      YBND2 = YB(2)

      RETURN
      END
