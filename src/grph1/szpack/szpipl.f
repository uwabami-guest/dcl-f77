*-----------------------------------------------------------------------
*     INTERPOLATION ON UC (LINEAR INTERPOLATION)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZPIPL(X0,Y0, X1, Y1, MODE)

      PARAMETER (EPSIL=1.E-5)

      LOGICAL   LINT, LCONT, LREQA

      COMMON    /SZBLS1/ LLNINT,LGCINT,RDXR,RDYR
      LOGICAL   LLNINT,LGCINT

      EXTERNAL  RFPI, LREQA

      SAVE

*     MODE = 0 : FOR LINE
*            1 : FOR TONE
*            2 : FOR ARROW
*         ELSE : FOR BOUNDARY


      PI = RFPI()

      IF (MODE.EQ.0) THEN
        LINT = LLNINT
      ELSE IF (MODE.EQ.1) THEN
        LINT = LLNINT .AND.
     +    .NOT. (LREQA(Y0,Y1,EPSIL) .AND. LREQA(ABS(Y0),PI/2,EPSIL))
      ELSE IF (MODE.EQ.2) THEN
        LINT = LLNINT
      ELSE
        LINT = .TRUE.
      ENDIF

      XX0 = X0
      YY0 = Y0
      XX1 = X1
      YY1 = Y1
      NN  = 1
      NEND = 1

      IF (.NOT.LINT) RETURN

      DX = X1-X0
      DY = Y1-Y0

      NEND = MAX (INT(ABS(DX/RDXR)), INT(ABS(DY/RDYR))) + 1
      IF (NEND .GE. 2) THEN
        DXX = DX/NEND
        DYY = DY/NEND
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZGIPL(X, Y, LCONT)

      LCONT = NN.LT.NEND
      IF (LCONT) THEN
        X = XX0+NN*DXX
        Y = YY0+NN*DYY
      ELSE
        X = XX1
        Y = YY1
      END IF
      NN = NN+1

      RETURN
      END
