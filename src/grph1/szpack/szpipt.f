*-----------------------------------------------------------------------
*     INTERPOLATION ON TC (GREAT CIRCLE INTERPOLATION)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZPIPT(TX0, TY0, TX1, TY1, MODE)

      PARAMETER (EPSIL=1.E-5)

      LOGICAL   LINTT, LREQA, LXMOD, LSTD, LCONT, LMAP

      COMMON    /SZBLS1/ LLNINT,LGCINT,RDXR,RDYR
      LOGICAL   LLNINT,LGCINT

      EXTERNAL  RFPI, LREQA

      SAVE

*     MODE=0 : FOR LINE
*          1 : FOR TONE
*          2 : FOR ARROW


      PI = RFPI()

      IF (MODE.EQ.0) THEN
        LINTT = LGCINT
      ELSE IF(MODE.EQ.1) THEN
        LINTT = LGCINT .AND.
     + .NOT. (LREQA(TY0,TY1,EPSIL) .AND. LREQA(ABS(TY0),PI/2,EPSIL))
      ELSE IF(MODE.EQ.2) THEN
        CALL STQTRF(LMAP)
        LINTT = LGCINT .AND. LMAP
      END IF

      XX0 = TX0
      YY0 = TY0
      XX1 = TX1
      YY1 = TY1
      NN  = 1
      NEND = 1

      IF (.NOT.LINTT) RETURN

      DX1 = SZXMOD (TX1 - TX0)
      DY1 = TY1-TY0

      ADX = ABS(DX1)
      ADY = ABS(DY1)

      IF (ADX.LT.RDXR .AND. ADY.LT.RDXR) RETURN

      LSTD  = .FALSE.
      IF ( LREQA(ABS(TY0), PI/2, EPSIL) ) THEN
        YLA = SIGN(PI/2, TY0)
        XLA = TX1
        LXMOD = ADX .GE. ADY
      ELSE IF ( LREQA(ABS(TY1), PI/2, EPSIL) ) THEN
        YLA = SIGN(PI/2, TY1)
        XLA = TX0
        LXMOD = ADX .GE. ADY
      ELSE IF ( LREQA(ADX, 0., EPSIL) ) THEN
        XLA = TX0
        LXMOD = .FALSE.
      ELSE IF ( LREQA(ADX, PI, EPSIL) ) THEN
        YLA = SIGN(PI/2, TY0+TY1)
        LXMOD = .TRUE.
      ELSE
*       / THE FOLLOWING PART IS NOT COMPLETE ! /
        LSTD  = .TRUE.
        LXMOD = ADX .GE. ADY
        CALL SZSGCL(TX0, TY0, TX1, TY1)
      END IF

      IF (LXMOD) THEN
        NEND = ADX/RDXR + 1
        DXX = DX1/NEND
      ELSE
        NEND = ADY/RDYR + 1
        DYY = DY1/NEND
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZGIPT(TX, TY, LCONT)

      LCONT = NN.LT.NEND
      IF (LCONT) THEN
        IF (LXMOD) THEN
          TX = XX0 + NN*DXX
          IF (LSTD) THEN
            CALL SZQGCY(TX, TY)
          ELSE
            TY = YLA
          END IF
        ELSE
          TY = YY0 + NN*DYY
          IF (LSTD) THEN
            CALL SZQGCX(TY, TX)
          ELSE
            TX = XLA
          END IF
        END IF
      ELSE
        TX = XX1
        TY = YY1
      END IF
      NN = NN+1

      RETURN
      END
