*-----------------------------------------------------------------------
*     POLYLINE PRIMITIVE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZPLOP(ITYPE,INDEX)

      CHARACTER COBJ*80

      COMMON    /SZBPL1/ LMISS,RMISS
      LOGICAL   LMISS

      SAVE


      CALL GLLGET('LMISS',LMISS)
      CALL GLRGET('RMISS',RMISS)

      WRITE(COBJ,'(2I8)') ITYPE,INDEX
      CALL CDBLK(COBJ)
      CALL SWOOPN('SZPL',COBJ)

      CALL SZSLTI(ITYPE,INDEX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY SZPLCL

      CALL SWOCLS('SZPL')

      RETURN
      END
