*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZPMZU(N,UPX,UPY)

      REAL      UPX(*),UPY(*)

      LOGICAL   LFLAG

      COMMON    /SZBPM1/ LMISS,RMISS,NPM
      LOGICAL   LMISS
      COMMON    /SZBPM2/ CMARK
      CHARACTER CMARK*1

      SAVE


      DO 10 I=1,N,NPM
        LFLAG=LMISS .AND. (UPX(I).EQ.RMISS .OR. UPY(I).EQ.RMISS)
        IF (.NOT.LFLAG) THEN
          CALL SZTXZU(UPX(I),UPY(I),CMARK)
        END IF
   10 CONTINUE

      END
