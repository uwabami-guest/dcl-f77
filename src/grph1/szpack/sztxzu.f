*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SZTXZU(UX,UY,CHARS)

      CHARACTER CHARS*(*)

      CALL STFTRF(UX,UY,VX,VY)
      CALL SZTXWV(VX,VY,CHARS)

      END
