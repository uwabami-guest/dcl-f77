*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE GROPN(IWS)


*     / OPEN /

      CALL SGISTX('IWS',IWS)
      CALL SGINIT

      RETURN
*-----------------------------------------------------------------------
      ENTRY GRFRM

*     / NEW FRAME /

      CALL SGFRM
      CALL GRINIT
      CALL USINIT
      CALL UZINIT
      CALL UMINIT
      CALL UUINIT
      CALL UWINIT

      RETURN
*-----------------------------------------------------------------------
      ENTRY GRFIG

*     / NEW FIGURE /

      CALL GRINIT
      CALL USINIT
      CALL UZINIT
      CALL UMINIT
      CALL UUINIT
      CALL UWINIT

      RETURN
*-----------------------------------------------------------------------
      ENTRY GRCLS

*     / CLOSE /

      CALL SGCLS

      RETURN
      END
