*-----------------------------------------------------------------------
*     UDSGXA / UDSGXB
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UDSGXA(XP,NX)

      REAL      XP(*)
      LOGICAL   LSETX


      CALL MSGDMP('M','UDSGXA','THIS IS OLD INTERFACE - USE UWSGXA !')

      CALL UWSGXA(XP,NX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDQGXA(XP,NX)

      CALL MSGDMP('M','UDQGXA','THIS IS OLD INTERFACE - USE UWQGXA !')

      CALL UWQGXA(XP,NX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDSGXB(UXMIN,UXMAX,NX)

      CALL MSGDMP('M','UDSGXB','THIS IS OLD INTERFACE - USE UWSGXB !')

      CALL UWSGXB(UXMIN,UXMAX,NX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDQGXB(UXMIN,UXMAX,NX)

      CALL MSGDMP('M','UDQGXB','THIS IS OLD INTERFACE - USE UWQGXB !')

      CALL UWQGXB(UXMIN,UXMAX,NX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDSGXZ(LSETX)

      CALL MSGDMP('M','UDSGXZ','THIS IS OLD INTERFACE - USE UWSGXZ !')

      CALL UWSGXZ(LSETX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDQGXZ(LSETX)

      CALL MSGDMP('M','UDQGXZ','THIS IS OLD INTERFACE - USE UWQGXZ !')

      CALL UWQGXZ(LSETX)

      RETURN
      END
