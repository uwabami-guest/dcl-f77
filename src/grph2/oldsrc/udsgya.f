*-----------------------------------------------------------------------
*     UDSGYA / UDSGYB
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UDSGYA(YP,NY)

      REAL      YP(*)
      LOGICAL   LSETY


      CALL MSGDMP('M','UDSGYA','THIS IS OLD INTERFACE - USE UWSGYA !')

      CALL UWSGYA(YP,NY)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDQGYA(YP,NY)

      CALL MSGDMP('M','UDQGYA','THIS IS OLD INTERFACE - USE UWQGYA !')

      CALL UWQGYA(YP,NY)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDSGYB(UYMIN,UYMAX,NY)

      CALL MSGDMP('M','UDSGYB','THIS IS OLD INTERFACE - USE UWSGYB !')

      CALL UWSGYB(UYMIN,UYMAX,NY)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDQGYB(UYMIN,UYMAX,NY)

      CALL MSGDMP('M','UDQGYB','THIS IS OLD INTERFACE - USE UWQGYB !')

      CALL UWQGYB(UYMIN,UYMAX,NY)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDSGYZ(LSETY)

      CALL MSGDMP('M','UDSGYZ','THIS IS OLD INTERFACE - USE UWSGYZ !')

      CALL UWSGYZ(LSETY)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDQGYZ(LSETY)

      CALL MSGDMP('M','UDQGYZ','THIS IS OLD INTERFACE - USE UWQGYZ !')

      CALL UWQGYZ(LSETY)

      RETURN
      END
