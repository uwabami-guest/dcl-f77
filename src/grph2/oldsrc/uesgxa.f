*-----------------------------------------------------------------------
*     UESGXA / UESGXB
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UESGXA(XP,NX)

      REAL      XP(*)
      LOGICAL   LSETX


      CALL MSGDMP('M','UESGXA','THIS IS OLD INTERFACE - USE UWSGXA !')

      CALL UWSGXA(XP,NX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UEQGXA(XP,NX)

      CALL MSGDMP('M','UEQGXA','THIS IS OLD INTERFACE - USE UWQGXA !')

      CALL UWQGXA(XP,NX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UESGXB(UXMIN,UXMAX,NX)

      CALL MSGDMP('M','UESGXB','THIS IS OLD INTERFACE - USE UWSGXB !')

      CALL UWSGXB(UXMIN,UXMAX,NX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UEQGXB(UXMIN,UXMAX,NX)

      CALL MSGDMP('M','UEQGXB','THIS IS OLD INTERFACE - USE UWQGXB !')

      CALL UWQGXB(UXMIN,UXMAX,NX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UESGXZ(LSETX)

      CALL MSGDMP('M','UESGXZ','THIS IS OLD INTERFACE - USE UWSGXZ !')

      CALL UWSGXZ(LSETX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UEQGXZ(LSETX)

      CALL MSGDMP('M','UEQGXZ','THIS IS OLD INTERFACE - USE UWQGXZ !')

      CALL UWQGXZ(LSETX)

      RETURN
      END
