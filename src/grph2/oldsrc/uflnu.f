*-----------------------------------------------------------------------
*     UFPACK
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UFLNU(UX1,UY1,UX2,UY2)


      CALL MSGDMP('M','UFLNU','THIS IS OLD INTERFACE - USE SGLAU !')

      CALL SGLAU(UX1,UY1,UX2,UY2)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UFLNV(VX1,VY1,VX2,VY2)

      CALL MSGDMP('M','UFLNV','THIS IS OLD INTERFACE - USE SGLAV !')

      CALL SGLAV(VX1,VY1,VX2,VY2)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UFLNR(RX1,RY1,RX2,RY2)

      CALL MSGDMP('M','UFLNR','THIS IS OLD INTERFACE - USE SGLAR !')

      CALL SGLAR(RX1,RY1,RX2,RY2)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UFSLNI(INDEX)

      CALL MSGDMP('M','UFSLNI','THIS IS OLD INTERFACE - USE SGSLAI !')

      CALL SGSLAI(INDEX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UFQLNI(INDEX)

      CALL MSGDMP('M','UFQLNI','THIS IS OLD INTERFACE - USE SGQLAI !')

      CALL SGQLAI(INDEX)

      RETURN
      END
