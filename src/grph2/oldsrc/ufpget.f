*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UFPGET(CP,IPARA)

      CHARACTER CP*(*)


      CALL MSGDMP('M','UFPGET','THIS IS OLD INTERFACE - USE SGPGET !')

      CALL SGPGET(CP,IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UFPSET(CP,IPARA)

      CALL MSGDMP('M','UFPSET','THIS IS OLD INTERFACE - USE SGPSET !')

      CALL SGPSET(CP,IPARA)

      RETURN
      END
