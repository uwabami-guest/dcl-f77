*-----------------------------------------------------------------------
*     USSTRF
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USSTRF


      CALL MSGDMP('W','USSTRF',
     +     'THIS IS OLD INTERFACE - USE USPFIT & GRSTRF !')

      CALL USPFIT
      CALL GRSTRF

      END
