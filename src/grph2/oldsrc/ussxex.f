*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USSXEX(N, X)

      REAL      X(*)


      CALL MSGDMP('W','USSXEX','THIS IS OLD INTERFACE - USE USSPNT !')

      CALL GLRGET('RUNDEF', RUNDEF)
      CALL USSPNT(N, X, RUNDEF)

      RETURN
*-----------------------------------------------------------------------
      ENTRY USQXEX(XMINQ, XMAXQ)

      CALL MSGDMP('W','USQXEX','THIS IS OLD INTERFACE - USE USRGET !')

      CALL USRGET('XDTMIN', XMINQ)
      CALL USRGET('XDTMAX', XMAXQ)

      RETURN
      END
