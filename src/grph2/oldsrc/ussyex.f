*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USSYEX(N, Y)

      REAL      Y(*)


      CALL MSGDMP('W','USSXEY','THIS IS OLD INTERFACE - USE USSPNT !')

      CALL GLRGET('RUNDEF', RUNDEF)
      CALL USSPNT(N, RUNDEF, Y)

      RETURN
*-----------------------------------------------------------------------
      ENTRY USQYEX(YMINQ, YMAXQ)

      CALL MSGDMP('W','USQYEX','THIS IS OLD INTERFACE - USE USRGET !')

      CALL USRGET('YDTMIN', YMINQ)
      CALL USRGET('YDTMAX', YMAXQ)

      RETURN
      END
