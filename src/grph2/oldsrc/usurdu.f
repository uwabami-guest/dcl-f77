*-----------------------------------------------------------------------
*     USPACK ROUND UMIN AND UMAX (UNIFORM)            S.Sakai  92/03/04
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USURDU(UMIN, UMAX, VMIN, VMAX)


      CALL MSGDMP('M', 'USURDU', 'THIS IS OLD INTAFACE')
      CALL USURDT(UMIN, UMAX, VMIN, VMAX, DUT)

      RETURN

      ENTRY USQDUT(DUTQ)
      CALL MSGDMP('M', 'USQDUT', 'THIS IS OLD INTAFACE')
      DUTQ = DUT

      END
