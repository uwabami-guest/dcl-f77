*-----------------------------------------------------------------------
*     UCIQNP / UCIQID / UCIQCP / UCIQVL / UCISVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UCIQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA = 3)
      PARAMETER (IUNDEF = -999)

      INTEGER   IX(NPARA)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ,LENC

      SAVE

      DATA      CPARAS( 1) /'NDAY    '/, IX( 1) /IUNDEF/
      DATA      CPARAS( 2) /'NCHAR   '/, IX( 2) /IUNDEF/
      DATA      CPARAS( 3) /'IUNDEF  '/, IX( 3) /IUNDEF/

      DATA      CPARAL( 1) / 'DAY_INTERVAL' /
      DATA      CPARAL( 2) / 'MONTH_NAME_LENGTH' /
      DATA      CPARAL( 3) / '----IUNDEF  ' /

      DATA      LFIRST / .TRUE. /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY UCIQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','UCIQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UCIQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','UCIQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UCIQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','UCIQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UCIQVL(IDX, IPARA)

      IF (LFIRST) THEN
        CALL RTIGET('UC', CPARAS, IX, NPARA)
        CALL RLIGET(CPARAL, IX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IPARA = IX(IDX)
      ELSE
        CALL MSGDMP('E','UCIQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UCISVL(IDX, IPARA)

      IF (LFIRST) THEN
        CALL RTIGET('UC', CPARAS, IX, NPARA)
        CALL RLIGET(CPARAL, IX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IX(IDX) = IPARA
      ELSE
        CALL MSGDMP('E','UCISVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UCIQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
