*-----------------------------------------------------------------------
*     UCYAYR : PLOT YEAR AXIS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UCYAYR(CSIDE,JD0,ND)

      CHARACTER CSIDE*1

      PARAMETER (N=50)

      REAL      UY(N)
      CHARACTER CH(N)*4
      LOGICAL   LABEL,LUYCHK,LBTWN


      IF (.NOT.LUYCHK(CSIDE)) THEN
        CALL MSGDMP('E','UCYAYR','SIDE PARAMETER IS INVALID.')
      END IF
      IF (JD0.LT.0) THEN
        CALL MSGDMP('E','UCYAYR','FIRST DATE IS LESS THAN 0.')
      END IF
      IF (ND.LE.0) THEN
        CALL MSGDMP('E','UCYAYR','DATE LENGTH IS LESS THAN 0.')
      END IF

      CALL UYPAXS(CSIDE,2)

      NN=1
      UY(NN)=0
      CALL DATE13(JD0,IY0,MO0,ID0)

      DO 10 I=1,ND
        CALL DATEF3(I,IY0,MO0,ID0,IYI,MOI,IDI)
        CALL DATE32(IYI,MOI,IDI,ITDI)
        IYL=NDYEAR(IYI)
        IF (ITDI.EQ.IYL .OR. I.EQ.ND) THEN
          NN=NN+1
          IF (NN.GT.N) THEN
            CALL MSGDMP('E','UCYAYR','WORKING AREA IS NOT ENOUGH.')
          END IF
          UY(NN)=I
          WRITE(CH(NN-1),'(I4)') IYI
          CALL CLADJ(CH(NN-1)(1:4))
        END IF
   10 CONTINUE

      CALL SGQVPT(VX1,VX2,VY1,VY2)
      CALL UZRGET('RSIZEL2',RSIZE)
      NCHX=(VY2-VY1)/RSIZE

      CALL UZIGET('ICENTY'//CSIDE,ICENT)
      CALL UZIGET('IROTLY'//CSIDE,IROTA)
      CALL UZIGET('IROTCY'//CSIDE,IROTC)
      CALL UZLGET('LBTWN',LBTWN)

      CALL UZISET('ICENTY'//CSIDE,0)
      IF (NN*4.LE.NCHX) THEN
        CALL UZISET('IROTLY'//CSIDE,IROTC)
      ELSE
        CALL UZISET('IROTLY'//CSIDE,0)
      END IF
      CALL UZLSET('LBTWN',.TRUE.)

      CALL UZRGET('RSIZET2',RTICK2)
      CALL UZRSET('RSIZET2',RTICK2*1.5)
      CALL UYPTMK(CSIDE,2,UY,NN)
      CALL UZRSET('RSIZET2',RTICK2)

      CALL UZLGET('LABELY'//CSIDE,LABEL)
      IF (LABEL) THEN
        CALL UYPLBL(CSIDE,2,UY,CH,4,NN)
      END IF

      CALL UZISET('ICENTY'//CSIDE,ICENT)
      CALL UZISET('IROTLY'//CSIDE,IROTA)
      CALL UZLSET('LBTWN',LBTWN)

      END
