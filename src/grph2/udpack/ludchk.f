*-----------------------------------------------------------------------
*     LUDCHK
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LUDCHK(I,J,K,L,IBR)

      INTEGER   IBR(NBR,0:*)

      PARAMETER (MAXNB=32)

      INTEGER   MASK(MAXNB)
      LOGICAL   LFRST
      CHARACTER CBPAT*(MAXNB)

      COMMON    /UDBLK1/ NB,LX,LY,NBR

      SAVE

      DATA      LFRST/.TRUE./


      IF (LFRST) THEN
        IF (NB.NE.MAXNB) THEN
          CALL MSGDMP('E','LUDCHK','MAXNB IS INAPPROPRIATE.')
        END IF
        DO 15 JJ=1,MAXNB
          DO 10 II=1,MAXNB
            IF (II.EQ.JJ) THEN
              CBPAT(II:II)='1'
            ELSE
              CBPAT(II:II)='0'
            END IF
   10     CONTINUE
          CALL CRVRS(CBPAT)
          CALL BITPCI(CBPAT,MASK(JJ))
   15   CONTINUE
        LFRST=.FALSE.
      END IF

      NN=LX*(LY*K+J)+I
      N1=NN/NB+1
      N2=MOD(NN,NB)
      LUDCHK=IAND(IBR(N1,L),MASK(N2+1)).EQ.MASK(N2+1)

      END
