*-----------------------------------------------------------------------
*     UDCNTR
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UDCNTR(Z,MX,NX,NY)

      REAL      Z(MX,*)

      PARAMETER (NBR2=1000*2)
      INTEGER   IBR(NBR2)


      CALL UDCNTZ(Z,MX,NX,NY,IBR,NBR2)

      END
