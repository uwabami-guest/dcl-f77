*-----------------------------------------------------------------------
*     UDGCLA
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UDGCLA(XMIN,XMAX,DX)

      CHARACTER CLABZ*8
      LOGICAL   LDASH,LABEL

      EXTERNAL  RGNGE,IRLE,IRGE,IMOD


      IF (.NOT.(XMIN.LT.XMAX)) THEN
        CALL MSGDMP('E','UDGCLA','XMIN SHOULD BE LEAST THAN XMAX.')
      END IF

      CALL UDIGET('INDXMJ  ',IDXMJ )
      CALL UDIGET('INDXMN  ',IDXMN )
      CALL UDLGET('LDASH   ',LDASH )
      CALL UDLGET('LABEL   ',LABEL )
      CALL UDIGET('ICYCLE  ',ICYCL )
      CALL UDIGET('ISOLID  ',ISOLID)
      CALL UDIGET('IDASH   ',IDASH )
      CALL UDIGET('NLEV    ',NLEV  )
      CALL UDRGET('RSIZEL  ',RSIZE )

      IF (DX.GT.0) THEN
        DZ=DX
      ELSE IF (DX.EQ.0) THEN
        DZ=RGNGE((XMAX-XMIN)/NLEV)
      ELSE
        NL=MAX(1,NINT(ABS(DX)))
        DZ=RGNGE((XMAX-XMIN)/NL)
      END IF

      ZMIN=IRGE(XMIN/DZ)*DZ
      ZMAX=IRLE(XMAX/DZ)*DZ
      N=NINT((ZMAX-ZMIN)/DZ)+1
      IOFST=IMOD(NINT(ZMIN/DZ),ICYCL)

      CALL UDICLV

      DO 10 I=1,N

*       / CONTOUR LEVEL /

        ZLEV=DZ*NINT((ZMIN+(I-1)*DZ)/DZ)

        CALL UDLABL(ZLEV,CLABZ)

        IF (MOD(I+IOFST-1,ICYCL).EQ.0) THEN

*         / MAJOR LINE /

          INDX=IDXMJ
          IF (LABEL) THEN
            HL=RSIZE
          ELSE
            HL=0
          END IF

        ELSE

*         / MINOR LINE /

          INDX=IDXMN
          HL=0

        END IF

*       / DASH OPTION FOR NEGATIVE CONTOUR LEVEL /

        IF (LDASH .AND. ZLEV.LT.0) THEN
          ITYP=IDASH
        ELSE
          ITYP=ISOLID
        END IF

*       / SET CONTOUR LEVEL /

        CALL UDSCLV(ZLEV,INDX,ITYP,CLABZ,HL)

   10 CONTINUE

      END
