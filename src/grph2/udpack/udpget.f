*-----------------------------------------------------------------------
*     UDPGET / UDPSET / UDPSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UDPGET(CP, IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8, CL*40


      CALL UDPQID(CP, IDX)
      CALL UDPQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDPSET(CP, IPARA)

      CALL UDPQID(CP, IDX)
      CALL UDPSVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UDPSTX(CP,IPARA)

      IP = IPARA
      CALL UDPQID(CP, IDX)
      CALL UDPQIT(IDX, IT)
      CALL UDPQCP(IDX, CX)
      CALL UDPQCL(IDX, CL)
      IF (IT .EQ. 1) THEN
        CALL RTIGET('UD', CX, IP, 1)
        CALL RLIGET(CL, IP, 1)
        CALL UDIQID(CP, IDX)
        CALL UDISVL(IDX, IP)
      ELSE IF (IT .EQ. 2) THEN
        CALL RTLGET('UD', CX, IP, 1)
        CALL RLLGET(CL, IP, 1)
        CALL UDLQID(CP, IDX)
        CALL UDLSVL(IDX, IP)
      ELSE IF (IT .EQ. 3) THEN
        CALL RTRGET('UD', CX, IP, 1)
        CALL RLRGET(CL, IP, 1)
        CALL UDRQID(CP, IDX)
        CALL UDRSVL(IDX, IP)
      END IF

      RETURN
      END
