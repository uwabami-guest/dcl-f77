*-----------------------------------------------------------------------
*     UEGTLA
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UEGTLA(XMIN,XMAX,DX)

      LOGICAL   LBOUND,LEPSL
      EXTERNAL  RGNGE,IRLE,IRGE


      IF (.NOT.(XMIN.LT.XMAX)) THEN
        CALL MSGDMP('E','UEGTLA','XMIN SHOULD BE LEAST THAN XMAX.')
      END IF

      CALL UEIGET('NLEV    ',NLEV  )
      CALL UEIGET('ITPAT   ',ITPAT )
      CALL UEIGET('ICOLOR1 ',ICLR1 )
      CALL UEIGET('ICOLOR2 ',ICLR2 )
      CALL UELGET('LBOUND  ',LBOUND)

      CALL GLLGET('LEPSL   ',LEPSL )

      CALL GLLSET('LEPSL   ',.TRUE.)

      IF (DX.GT.0) THEN
        DZ=DX
      ELSE IF (DX.EQ.0) THEN
        DZ=RGNGE((XMAX-XMIN)/NLEV)
      ELSE
        NL=MAX(1,NINT(ABS(DX)))
        DZ=RGNGE((XMAX-XMIN)/NL)
      END IF

      ZMIN=IRLE(XMIN/DZ)*DZ
      ZMAX=IRGE(XMAX/DZ)*DZ
      N=NINT((ZMAX-ZMIN)/DZ)

      CALL UEITLV

      DO 10 I=1,N

        TLEV1=DZ*NINT((ZMIN+(I-1)*DZ)/DZ)
        TLEV2=TLEV1+DZ
        IF (LBOUND .AND. I.EQ.1) TLEV1=MAX(TLEV1,XMIN)
        IF (LBOUND .AND. I.EQ.N) TLEV2=MIN(TLEV2,XMAX)

        IF (N.EQ.1) THEN
          IPAT=(ICLR2-ICLR1)/2*1000+ITPAT
        ELSE
          IPAT=NINT((ICLR2-ICLR1)/REAL(N-1)*(I-1)+ICLR1)*1000+ITPAT
        END IF

        CALL UESTLV(TLEV1,TLEV2,IPAT)

   10 CONTINUE

      CALL GLLSET('LEPSL   ',LEPSL )

      END
