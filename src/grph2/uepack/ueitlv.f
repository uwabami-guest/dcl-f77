*-----------------------------------------------------------------------
*     UEITLV / UESTLV
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UEITLV

      LOGICAL   LSET, LREQ1
      EXTERNAL  LREQ1

      LOGICAL   LSETZ
      CHARACTER CMSG*80

      COMMON    /UEBLK1/ TL1,TL2,IPT,NT,LASCND
      PARAMETER (MAXNT=100)
      LOGICAL   LASCND
      INTEGER   IPT(MAXNT)
      REAL      TL1(MAXNT),TL2(MAXNT)

      SAVE

      DATA      LSETZ/.FALSE./

      NT=0
      LSETZ=.FALSE.

      RETURN
*-----------------------------------------------------------------------
      ENTRY UESTLV(TLEV1,TLEV2,IPAT)

*     / INITIALIZATION /

      IF (.NOT.LSETZ) THEN
        CALL GLRGET('RMISS   ',RMISS )
        CALL GLRGET('REALMAX ',REALMX)
        CALL ISET0(IPT,NT,1,0)
        NT=0
        LSETZ  = .TRUE.
        LASCND = .TRUE.
      END IF

*     / CHECK TONE LEVEL /

      IF (TLEV1.EQ.RMISS .OR. TLEV2.EQ.RMISS) THEN
        IF (TLEV1.EQ.TLEV2) THEN
          CMSG='TLEV1 AND TLEV2 ARE MISSING VALUES.'
          CALL MSGDMP('E','UESTLV',CMSG)
        END IF
      ELSE
        IF (TLEV1.GE.TLEV2) THEN
          CMSG='TLEV1 IS GREATER THAN OR EQUAL TO TLEV2.'
          CALL MSGDMP('E','UESTLV',CMSG)
        END IF
      END IF

*     / CHECK IPAT /

      IF (IPAT.LT.0) THEN
        CMSG='TONE PATTERN NUMBER IS LESS THAN ZERO.'
        CALL MSGDMP('E','UESTLV',CMSG)
      END IF

*     / CHECK NUMBER OF TONE /

      IF (NT+1.GT.MAXNT) THEN
        CMSG='NUMBER OF TONE IS IN EXCESS OF MAXIMUM (###).'
        WRITE(CMSG(41:43),'(I3)') MAXNT
        CALL MSGDMP('E','UESTLV',CMSG)
      END IF

      NT=NT+1
      IF (TLEV1.EQ.RMISS) THEN
        TL1(NT)=-REALMX
      ELSE
        TL1(NT)=TLEV1
      END IF
      IF (TLEV2.EQ.RMISS) THEN
        TL2(NT)=+REALMX
      ELSE
        TL2(NT)=TLEV2
      END IF
      IPT(NT)=IPAT

      IF (NT.NE.1) THEN
        LASCND =  LASCND .AND. LREQ1(TL1(NT),TL2(NT-1))
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UEQTLV(TLEV1,TLEV2,IPAT,ITON)

      IF (ITON.LT.1.OR.ITON.GT.NT)THEN
        CMSG='LEVEL (##) OF TONE PATTERN IS OUT OF RANGE (1-##).'
        WRITE(CMSG(8:9),'(I2)') ITON
        WRITE(CMSG(47:48),'(I2)') NT
        CALL MSGDMP('E','UEQTLV',CMSG)
      END IF

      IF (TL1(ITON).EQ.-REALMX) THEN
        TLEV1=RMISS
      ELSE
        TLEV1=TL1(ITON)
      END IF
      IF (TL2(ITON).EQ.+REALMX) THEN
        TLEV2=RMISS
      ELSE
        TLEV2=TL2(ITON)
      END IF
      IPAT=IPT(ITON)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UEQNTL(NTON)

      NTON=NT

      RETURN
*-----------------------------------------------------------------------
      ENTRY UESTLZ(LSET)

      LSETZ=LSET

      RETURN
*-----------------------------------------------------------------------
      ENTRY UEQTLZ(LSET)

      LSET=LSETZ

      RETURN
      END
