*-----------------------------------------------------------------------
*     UELGET / UELSET / UELSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UELGET(CP, LPARA)

      LOGICAL   LPARA
      CHARACTER CP*(*)

      LOGICAL   LP
      CHARACTER CX*8
      CHARACTER CL*40


      CALL UELQID(CP, IDX)
      CALL UELQVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UELSET(CP, LPARA)

      CALL UELQID(CP, IDX)
      CALL UELSVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UELSTX(CP, LPARA)

      LP = LPARA
      CALL UELQID(CP, IDX)

*     / SHORT NAME /

      CALL UELQCP(IDX, CX)
      CALL RTLGET('UE', CX, LP, 1)

*     / LONG NAME /

      CALL UELQCL(IDX, CL)
      CALL RLLGET(CL, LP, 1)

      CALL UELSVL(IDX,LP)

      RETURN
      END
