*-----------------------------------------------------------------------
*     UEZCHK
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UEZCHK(Z,MX,NX,NY,CNAME,ISTAT)

      REAL      Z(MX,*)
      CHARACTER CNAME*(*)

      INTEGER   NSX(2),NPX(2),NQX(2)
      LOGICAL   LMISS,LSET,LTONE,LMADA
      CHARACTER CMSG*80

      COMMON    /UEBLK1/ TL1,TL2,IPT,NT,LASCND
      PARAMETER (MAXNT=100)
      LOGICAL   LASCND
      INTEGER   IPT(MAXNT)
      REAL      TL1(MAXNT),TL2(MAXNT)


*     / GET INTERNAL PARAMETERS /

      CALL GLLGET('LMISS   ',LMISS )
      CALL GLRGET('RMISS   ',RMISS )

*     / CHECK MIN & MAX /

      NSX(1)=MX
      NSX(2)=NY
      NPX(1)=1
      NPX(2)=1
      NQX(1)=NX
      NQX(2)=NY
      RMINZ=RVMIN(Z,NSX,NPX,NQX,2)
      RMAXZ=RVMAX(Z,NSX,NPX,NQX,2)

      LMADA = LMISS .AND. RMINZ.EQ.RMISS .AND. RMAXZ.EQ.RMISS
      ISTAT = 0

      IF (LMADA .OR. RMINZ.EQ.RMAXZ) THEN

*       / MESSAGE FOR MISSING OR CONSTANT FIELD /

        IF (LMADA) THEN
          CMSG='MISSING FIELD.'
          ISTAT = 1
        ELSE
          CMSG='CONSTANT (##########) FIELD.'
          WRITE(CMSG(11:20),'(1P,E10.3)') RMINZ
          ISTAT = 2
        END IF
        CALL MSGDMP('W',CNAME,CMSG)
      END IF

*     / GENERATE TONE LEVELS IF THEY HAVE NOT BEEN GENERATED YET /

      CALL UEQTLZ(LSET)
      IF (.NOT.LSET) THEN
        CALL UELGET('LTONE',LTONE)
        IF (LTONE) THEN
          CALL UEIGET('NLEV',NLEV)
          CALL UEGTLB(Z,MX,NX,NY,-REAL(NLEV))
        ELSE
          CALL UEIGET('IPAT',IPAT)
          CALL UERGET('RLEV',RLEV)
          CALL UESTLV(RMISS,RLEV,IPAT)
        END IF
        CALL UESTLZ(.FALSE.)
      END IF

      END
