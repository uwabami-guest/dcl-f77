*-----------------------------------------------------------------------
*     UGIGET / UGISET / UGISTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UGIGET(CP, IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40


      CALL UGIQID(CP, IDX)
      CALL UGIQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGISET(CP, IPARA)

      CALL UGIQID(CP, IDX)
      CALL UGISVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGISTX(CP, IPARA)

      IP = IPARA
      CALL UGIQID(CP, IDX)

*     / SHORT NAME /

      CALL UGIQCP(IDX, CX)
      CALL RTIGET('UG', CX, IP, 1)

*     / LONG NAME /

      CALL UGIQCL(IDX, CL)
      CALL RLIGET(CL, IP, 1)

      CALL UGISVL(IDX,IP)

      RETURN
      END
