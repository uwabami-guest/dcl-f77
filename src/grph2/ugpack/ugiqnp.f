*-----------------------------------------------------------------------
*     UGIQNP / UGIQID / UGIQCP / UGIQVL / UGISVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UGIQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA = 10)

      INTEGER   IX(NPARA)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ, LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS( 1) / 'INDEX   ' /, IX( 1) / 3 /
      DATA      CPARAS( 2) / 'ICENT   ' /, IX( 2) / 0 /
      DATA      CPARAS( 3) / 'ITYPE1  ' /, IX( 3) / 5 /
      DATA      CPARAS( 4) / 'ITYPE2  ' /, IX( 4) / 1 /
      DATA      CPARAS( 5) / 'IXINT   ' /, IX( 5) / 1 /
      DATA      CPARAS( 6) / 'IYINT   ' /, IX( 6) / 1 /
      DATA      CPARAS( 7) / 'IUNTTL  ' /, IX( 7) / 0 /
      DATA      CPARAS( 8) / 'IUINDX  ' /, IX( 8) / 3 /
      DATA      CPARAS( 9) / 'IUTXRO  ' /, IX( 9) / 0 /
      DATA      CPARAS(10) / 'IUTYRO  ' /, IX(10) / 90 /

*     / LONG NAME /

      DATA      CPARAL( 1) / 'VECTOR_INDEX' /
      DATA      CPARAL( 2) / 'VECTOR_GRID_POSITION' /
      DATA      CPARAL( 3) / 'MISS_VECTOR_MARKER_TYPE' /
      DATA      CPARAL( 4) / 'SMALL_VECTOR_MARKER_TYPE' /
      DATA      CPARAL( 5) / '****IXINT   ' /
      DATA      CPARAL( 6) / '****IYINT   ' /
      DATA      CPARAL( 7) / '****IUNTTL  ' /
      DATA      CPARAL( 8) / 'UNIT_VECTOR_TITLE_INDEX' /
      DATA      CPARAL( 9) / 'UNIT_VECTOR_X_TITLE_ANGLE' /
      DATA      CPARAL(10) / 'UNIT_VECTOR_Y_TITLE_ANGLE' /

      DATA      LFIRST / .TRUE. /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGIQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','UGIQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGIQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','UGIQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGIQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','UGIQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGIQVL(IDX, IPARA)

      IF (LFIRST) THEN
        CALL RTIGET('UG', CPARAS, IX, NPARA)
        CALL RLIGET(CPARAL, IX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IPARA = IX(IDX)
      ELSE
        CALL MSGDMP('E','UGIQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGISVL(IDX, IPARA)

      IF (LFIRST) THEN
        CALL RTIGET('UG', CPARAS, IX, NPARA)
        CALL RLIGET(CPARAL, IX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IX(IDX) = IPARA
      ELSE
        CALL MSGDMP('E','UGISVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGIQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
