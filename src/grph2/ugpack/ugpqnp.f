*-----------------------------------------------------------------------
*     UGPQNP / UGPQID / UGPQCP / UGPQVL / UGPSVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UGPQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA = 37)

      INTEGER   ITYPE(NPARA)
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      LOGICAL   LCHREQ

      EXTERNAL  LCHREQ,LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS( 1) / 'INDEX   ' /, ITYPE( 1) / 1 /
      DATA      CPARAS( 2) / 'LNRMAL  ' /, ITYPE( 2) / 2 /
      DATA      CPARAS( 3) / 'LEQRAT  ' /, ITYPE( 3) / 2 /
      DATA      CPARAS( 4) / 'XFACT1  ' /, ITYPE( 4) / 3 /
      DATA      CPARAS( 5) / 'YFACT1  ' /, ITYPE( 5) / 3 /
      DATA      CPARAS( 6) / 'XFACT2  ' /, ITYPE( 6) / 3 /
      DATA      CPARAS( 7) / 'YFACT2  ' /, ITYPE( 7) / 3 /
      DATA      CPARAS( 8) / 'LMSG    ' /, ITYPE( 8) / 2 /
      DATA      CPARAS( 9) / 'ICENT   ' /, ITYPE( 9) / 1 /
      DATA      CPARAS(10) / 'LMISSP  ' /, ITYPE(10) / 2 /
      DATA      CPARAS(11) / 'ITYPE1  ' /, ITYPE(11) / 1 /
      DATA      CPARAS(12) / 'LSMALL  ' /, ITYPE(12) / 2 /
      DATA      CPARAS(13) / 'RSMALL  ' /, ITYPE(13) / 3 /
      DATA      CPARAS(14) / 'ITYPE2  ' /, ITYPE(14) / 1 /
      DATA      CPARAS(15) / 'RSIZEM  ' /, ITYPE(15) / 3 /
      DATA      CPARAS(16) / 'RSIZET  ' /, ITYPE(16) / 3 /
      DATA      CPARAS(17) / 'XTTL    ' /, ITYPE(17) / 3 /
      DATA      CPARAS(18) / 'IXINT   ' /, ITYPE(18) / 1 /
      DATA      CPARAS(19) / 'IYINT   ' /, ITYPE(19) / 1 /

*     < PARAMETERS FOR UNIT VECTOR >

      DATA      CPARAS(20) / 'LUNIT   ' /, ITYPE(20) / 2 /
      DATA      CPARAS(21) / 'LUMSG   ' /, ITYPE(21) / 2 /
      DATA      CPARAS(22) / 'VXUOFF  ' /, ITYPE(22) / 3 /
      DATA      CPARAS(23) / 'VYUOFF  ' /, ITYPE(23) / 3 /
      DATA      CPARAS(24) / 'VXULOC  ' /, ITYPE(24) / 3 /
      DATA      CPARAS(25) / 'VYULOC  ' /, ITYPE(25) / 3 /
      DATA      CPARAS(26) / 'UXUNIT  ' /, ITYPE(26) / 3 /
      DATA      CPARAS(27) / 'UYUNIT  ' /, ITYPE(27) / 3 /
      DATA      CPARAS(28) / 'VXUNIT  ' /, ITYPE(28) / 3 /
      DATA      CPARAS(29) / 'VYUNIT  ' /, ITYPE(29) / 3 /

*     < PARAMETERS FOR TITLES OF UNIT VECTOR >

      DATA      CPARAS(30) / 'IUNTTL  ' /, ITYPE(30) / 1 /
      DATA      CPARAS(31) / 'RSIZEUT ' /, ITYPE(31) / 3 /
      DATA      CPARAS(32) / 'IUINDX  ' /, ITYPE(32) / 1 /
      DATA      CPARAS(33) / 'IUTXRO  ' /, ITYPE(33) / 1 /
      DATA      CPARAS(34) / 'IUTYRO  ' /, ITYPE(34) / 1 /
      DATA      CPARAS(35) / 'VUTOFF  ' /, ITYPE(35) / 3 /
      DATA      CPARAS(36) / 'RHFACT  ' /, ITYPE(36) / 3 /
      DATA      CPARAS(37) / 'RUNDEF  ' /, ITYPE(37) / 3 /

*     / LONG NAME /

      DATA      CPARAL( 1) / 'VECTOR_INDEX' /
      DATA      CPARAL( 2) / 'ENABLE_VECTOR_NORMALIZING' /
      DATA      CPARAL( 3) / 'FIX_VECTOR_PROPORTION' /
      DATA      CPARAL( 4) / 'VECTOR_SCALING_FACTOR_X' /
      DATA      CPARAL( 5) / 'VECTOR_SCALING_FACTOR_Y' /
      DATA      CPARAL( 6) / '****XFACT2  ' /
      DATA      CPARAL( 7) / '****YFACT2  ' /
      DATA      CPARAL( 8) / 'ENABLE_VECTOR_MESSAGE' /
      DATA      CPARAL( 9) / 'VECTOR_GRID_POSITION' /
      DATA      CPARAL(10) / 'ENABLE_MISS_VECTER_MARKER' /
      DATA      CPARAL(11) / 'MISS_VECTOR_MARKER_TYPE' /
      DATA      CPARAL(12) / 'ENABLE_SMALL_VECTOR_MARKER' /
      DATA      CPARAL(13) / 'SMALL_VECTOR_THRESHOLD' /
      DATA      CPARAL(14) / 'SMALL_VECTOR_MARKER_TYPE' /
      DATA      CPARAL(15) / 'MISS_VECTOR_MARKER_SIZE' /
      DATA      CPARAL(16) / 'VECTOR_MESSAGE_HEIGHT' /
      DATA      CPARAL(17) / 'VECTOR_MESSAGE_POSITION' /
      DATA      CPARAL(18) / '****IXINT   ' /
      DATA      CPARAL(19) / '****IYINT   ' /

*     < PARAMETERS FOR UNIT VECTOR >

      DATA      CPARAL(20) / 'DRAW_UNIT_VECTOR' /
      DATA      CPARAL(21) / 'DRAW_UNIT_VECTOR_MESSAGE' /
      DATA      CPARAL(22) / 'UNIT_VECTOR_X_OFFSET' /
      DATA      CPARAL(23) / 'UNIT_VECTOR_Y_OFFSET' /
      DATA      CPARAL(24) / 'UNIT_VECTOR_X_LOCATION' /
      DATA      CPARAL(25) / 'UNIT_VECTOR_Y_LOCATION' /
      DATA      CPARAL(26) / 'UNIT_VECTOR_X_LENGTH' /
      DATA      CPARAL(27) / 'UNIT_VECTOR_Y_LENGTH' /
      DATA      CPARAL(28) / 'UNIT_VECTOR_X_NORMALIZED_LENGTH' /
      DATA      CPARAL(29) / 'UNIT_VECTOR_Y_NORMALIZED_LENGTH' /

*     < PARAMETERS FOR TITLES OF UNIT VECTOR >

      DATA      CPARAL(30) / '****IUNTTL  ' /
      DATA      CPARAL(31) / 'UNIT_VECTOR_TITLE_HEIGHT' /
      DATA      CPARAL(32) / 'UNIT_VECTOR_TITLE_INDEX' /
      DATA      CPARAL(33) / 'UNIT_VECTOR_X_TITLE_ANGLE' /
      DATA      CPARAL(34) / 'UNIT_VECTOR_Y_TITLE_ANGLE' /
      DATA      CPARAL(35) / '****VUTOFF  ' /
      DATA      CPARAL(36) / '****RHFACT  ' /
      DATA      CPARAL(37) / '----RUNDEF  ' /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGPQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','UGPQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGPQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','UGPQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGPQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','UGPQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGPQIT(IDX, ITP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        ITP = ITYPE(IDX)
      ELSE
        CALL MSGDMP('E','UGPQIT','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGPQVL(IDX, IPARA)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IF (ITYPE(IDX) .EQ. 1) THEN
          CALL UGIQID(CPARAS(IDX), ID)
          CALL UGIQVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 2) THEN
          CALL UGLQID(CPARAS(IDX), ID)
          CALL UGLQVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 3) THEN
          CALL UGRQID(CPARAS(IDX), ID)
          CALL UGRQVL(ID, IPARA)
        END IF
      ELSE
        CALL MSGDMP('E','UGPQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGPSVL(IDX, IPARA)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IF (ITYPE(IDX) .EQ. 1) THEN
          CALL UGIQID(CPARAS(IDX), ID)
          CALL UGISVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 2) THEN
          CALL UGLQID(CPARAS(IDX), ID)
          CALL UGLSVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 3) THEN
          CALL UGRQID(CPARAS(IDX), ID)
          CALL UGRSVL(ID, IPARA)
        END IF
      ELSE
        CALL MSGDMP('E','UGPSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGPQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
