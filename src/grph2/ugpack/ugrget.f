*-----------------------------------------------------------------------
*     UGRGET / UGRSET / UGRSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UGRGET(CP, RPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40


      CALL UGRQID(CP, IDX)
      CALL UGRQVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGRSET(CP, RPARA)

      CALL UGRQID(CP, IDX)
      CALL UGRSVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UGRSTX(CP, RPARA)

      RP = RPARA
      CALL UGRQID(CP, IDX)

*     / SHORT NAME /

      CALL UGRQCP(IDX, CX)
      CALL RTRGET('UG', CX, RP, 1)

*     / LONG NAME /

      CALL UGRQCL(IDX, CL)
      CALL RLRGET(CL, RP, 1)

      CALL UGRSVL(IDX,RP)

      RETURN
      END
