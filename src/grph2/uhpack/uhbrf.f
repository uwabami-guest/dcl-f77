*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UHBRF(N,UPX1,UPX2,UPY)

      REAL      UPX1(*),UPX2(*),UPY(*)


      CALL UUQFRT(ITYPE)
      CALL UUQFRI(INDEX)
      CALL UUQBRS(RSIZE)

      CALL UHBRFZ(N,UPX1,UPX2,UPY,ITYPE,INDEX,RSIZE)

      END
