*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UHBRFZ(N,UPX1,UPX2,UPY,ITYPE,INDEX,RSIZE)

      REAL      UPX1(*),UPX2(*),UPY(*)

      LOGICAL   LMISS, LYUNI, LXC1, LXC2
      CHARACTER COBJ*80

      COMMON    /SZBLS2/ LCLIP
      LOGICAL   LCLIP


      IF (N.LT.1) THEN
        CALL MSGDMP('E','UHBRFZ','NUMBER OF POINTS IS LESS THAN 1.')
      END IF
      IF (ITYPE.EQ.0) THEN
        CALL MSGDMP('M','UHBRFZ','LINE TYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','UHBRFZ','LINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','UHBRFZ','LINE INDEX IS LESS THAN 0.')
      END IF
      IF (RSIZE.EQ.0) THEN
        CALL MSGDMP('M','UHBRFZ','MARKER SIZE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (RSIZE.LT.0) THEN
        CALL MSGDMP('E','UHBRFZ','ERROR MARKER SIZE IS LESS THAN ZERO.')
      END IF

      CALL SGLGET('LCLIP' , LCLIP )
      CALL GLRGET('RUNDEF', RUNDEF)
      CALL GLRGET('RMISS' , RMISS)
      CALL GLLGET('LMISS' , LMISS)

      WRITE(COBJ,'(2I8,F8.5)') ITYPE, INDEX, RSIZE
      CALL CDBLK(COBJ)
      CALL SWOOPN('UHBRFZ',COBJ)

      CALL SZSLTI(ITYPE,INDEX)

      LYUNI = UPY(1).EQ.RUNDEF
      LXC1  = UPX1(1).EQ.RUNDEF
      LXC2  = UPX2(1).EQ.RUNDEF

      IF (LYUNI) THEN
        CALL UUQIDV(UYMIN, UYMAX)
        IF (UYMIN.EQ.RUNDEF) CALL SGRGET('UYMIN', UYMIN)
        IF (UYMAX.EQ.RUNDEF) CALL SGRGET('UYMAX', UYMAX)
        DY = (UYMAX-UYMIN)/(N-1)
      END IF

      IF (LXC1 .OR. LXC2) THEN
        CALL UURGET('UREF', UREF)
      END IF

      DO 20 I=1,N
        IF (LYUNI) THEN
          UYY = UYMIN + DY*(I-1)
        ELSE
          UYY = UPY(I)
        END IF

        IF (LXC1) THEN
          UXX1 = UREF
        ELSE
          UXX1 = UPX1(I)
        END IF

        IF (LXC2) THEN
          UXX2 = UREF
        ELSE
          UXX2 = UPX2(I)
        END IF

        IF (.NOT.
     #    ((UYY.EQ.RMISS .OR. UXX1.EQ.RMISS .OR. UXX2.EQ.RMISS)
     #     .AND. LMISS)) THEN

          CALL STFTRF(UXX1, UYY, VX1, VYY)
          CALL STFTRF(UXX2, UYY, VX2, VYY)

          CALL SZOPLV
          CALL SZMVLV(VX2, VYY-RSIZE/2.)
          CALL SZPLLV(VX2, VYY+RSIZE/2.)
          CALL SZPLLV(VX1, VYY+RSIZE/2.)
          CALL SZPLLV(VX1, VYY-RSIZE/2.)
          CALL SZPLLV(VX2, VYY-RSIZE/2.)
          CALL SZCLLV
        END IF
   20 CONTINUE

      CALL SWOCLS('UHBRFZ')

      END
