*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UHBRLZ(N,UPX,UPY,ITYPE,INDEX,RSIZE)

      REAL      UPX(*),UPY(*)

      LOGICAL   LFLAG, LMISS, LYUNI
      CHARACTER COBJ*80

      COMMON    /SZBLS2/ LCLIP
      LOGICAL   LCLIP


      IF (N.LT.2) THEN
        CALL MSGDMP('E','UHBRLZ','NUMBER OF POINTS IS LESS THAN 2.')
      END IF
      IF (ITYPE.EQ.0) THEN
        CALL MSGDMP('M','UHBRLZ','LINETYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','UHBRLZ','LINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','UHBRLZ','LINE INDEX IS LESS THAN 0.')
      END IF
      IF (RSIZE.EQ.0) THEN
        CALL MSGDMP('M','UHBRLZ','BAR SIZE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (RSIZE.LT.0) THEN
        CALL MSGDMP('E','UVBRLZ','BAR SIZE IS LESS THAN ZERO.')
      END IF

      CALL SGLGET('LCLIP' , LCLIP )
      CALL GLRGET('RUNDEF', RUNDEF)
      CALL GLRGET('RMISS' , RMISS)
      CALL GLLGET('LMISS' , LMISS)

      IF (UPX(1).EQ.RUNDEF) THEN
        CALL MSGDMP('E', 'UHBRLZ', 'RUNDEF CAN NOT BE UESED FOR UPY.')
      END IF

      WRITE(COBJ,'(2I8)') ITYPE,INDEX
      CALL CDBLK(COBJ)
      CALL SWOOPN('UHBRLZ',COBJ)

      CALL SZSLTI(ITYPE,INDEX)
      CALL SZOPLU

      LYUNI = UPY(1).EQ.RUNDEF

      IF (LYUNI) THEN
        CALL UUQIDV(UYMIN, UYMAX)
        IF (UYMIN.EQ.RUNDEF) CALL SGRGET('UYMIN', UYMIN)
        IF (UYMAX.EQ.RUNDEF) CALL SGRGET('UYMAX', UYMAX)
        DY = (UYMAX-UYMIN)/(N-1)
      END IF

      CALL SZOPLV

      LFLAG=.FALSE.
      DO 20 I=1,N
        IF (LYUNI) THEN
          UYY = UYMIN + DY*(I-1)
        ELSE
          UYY = UPY(I)
        END IF

        IF ((UYY.EQ.RMISS .OR. UPX(I).EQ.RMISS) .AND. LMISS) THEN
          LFLAG=.FALSE.
        ELSE
          CALL STFTRF(UPX(I), UYY, VXX, VYY)
          IF (LFLAG) THEN
            CALL SZPLLV(VXX,VYY-RSIZE/2.)
            CALL SZPLLV(VXX,VYY+RSIZE/2.)
          ELSE
            CALL SZMVLV(VXX,VYY-RSIZE/2.)
            CALL SZPLLV(VXX,VYY+RSIZE/2.)
            LFLAG=.TRUE.
          END IF
        END IF
   20 CONTINUE

      CALL SZCLLV
      CALL SWOCLS('UHBRLZ')

      END
