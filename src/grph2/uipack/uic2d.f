*-----------------------------------------------------------------------
      SUBROUTINE UIC2D(U, V,IRGB)

      LOGICAL LSET

      DATA IR1, IR2, IR3, IR4 / Z'88', Z'FF', Z'00', Z'88'/
      DATA IG1, IG2, IG3, IG4 / Z'FF', Z'88', Z'88', Z'00'/
      DATA IB1, IB2, IB3, IB4 / Z'00', Z'00', Z'FF', Z'FF'/

      DATA LSET /.FALSE./

      SAVE

      UU = (U-U1)/DU
      VV = (V-V1)/DV

      C1 = (1.-UU)*VV
      C2 =     UU *VV
      C3 = (1.-UU)*(1.-VV)
      C4 =     UU *(1.-VV)

      JR = IR1*C1 + IR2*C2 + IR3*C3 + IR4*C4
      JG = IG1*C1 + IG2*C2 + IG3*C3 + IG4*C4
      JB = IB1*C1 + IB2*C2 + IB3*C3 + IB4*C4

      CALL UIFPAC(JR, JG, JB, IRGB)

      RETURN

*-----------------------------------------------------------------------
      ENTRY UI2INI(UMIN, UMAX, VMIN, VMAX)

      IF(.NOT.LSET) THEN
        U1 = UMIN
        U2 = UMAX
        V1 = VMIN
        V2 = VMAX
      END IF

      DU = U2-U1
      DV = V2-V1

      RETURN
*-----------------------------------------------------------------------
      ENTRY UISCR2(UMIN, UMAX, VMIN, VMAX)

      U1 = UMIN
      U2 = UMAX
      V1 = VMIN
      V2 = VMAX
      LSET = .TRUE.

      RETURN
*-----------------------------------------------------------------------
      ENTRY UIQCR2(UMIN, UMAX, VMIN, VMAX)

      UMIN = U1
      UMAX = U2
      VMIN = V1
      VMAX = V2

      RETURN
*-----------------------------------------------------------------------
      ENTRY UISCMP(IRGB1, IRGB2, IRGB3, IRGB4)
      
      CALL UIIPAC(IRGB1, IR1, IG1, IB1)
      CALL UIIPAC(IRGB2, IR2, IG2, IB2)
      CALL UIIPAC(IRGB3, IR3, IG3, IB3)
      CALL UIIPAC(IRGB4, IR4, IG4, IB4)

      END
