*-----------------------------------------------------------------------
*     UILQNP / UILQID / UILQCP / UILQVL / UILSVL
*-----------------------------------------------------------------------
      SUBROUTINE UILQNP(NCP)

      LOGICAL   LPARA
      CHARACTER CP*(*)

      PARAMETER (NPARA = 7)

      LOGICAL   LX(NPARA)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ,LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS(1) / 'LCELLX  ' /, LX(1) / .FALSE. /
      DATA      CPARAS(2) / 'LCELLY  ' /, LX(2) / .FALSE. /
      DATA      CPARAS(3) / 'LCORNER ' /, LX(3) / .FALSE. /
      DATA      CPARAS(4) / 'LCYCLE  ' /, LX(4) / .FALSE. /
      DATA      CPARAS(5) / 'LMASK   ' /, LX(5) / .FALSE. /
      DATA      CPARAS(6) / 'LEMBOSS ' /, LX(6) / .FALSE. /
      DATA      CPARAS(7) / 'LSPHERE ' /, LX(7) / .FALSE. /
      
*     / LONG NAME /

      DATA      CPARAL(1) / 'CELL_MODE_X ' /
      DATA      CPARAL(2) / 'CELL_MODE_Y ' /
      DATA      CPARAL(3) / 'FILL_CORNER ' /
      DATA      CPARAL(4) / 'CYCLE_COLOR ' /
      DATA      CPARAL(5) / 'MASK_COLOR  ' /
      DATA      CPARAL(6) / 'EMBOSS      ' /
      DATA      CPARAL(7) / 'SPHERICAL_MAPPING' /

      DATA      LFIRST / .TRUE. /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY UILQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','UILQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UILQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','UILQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UILQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','UILQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UILQVL(IDX, LPARA)

      IF (LFIRST) THEN
        CALL RTLGET('UE', CPARAS, LX, NPARA)
        CALL RLLGET(CPARAL, LX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        LPARA = LX(IDX)
      ELSE
        CALL MSGDMP('E','UILQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UILSVL(IDX, LPARA)

      IF (LFIRST) THEN
        CALL RTLGET('UE', CPARAS, LX, NPARA)
        CALL RLLGET(CPARAL, LX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        LX(IDX) = LPARA
      ELSE
        CALL MSGDMP('E','UILSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UILQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
