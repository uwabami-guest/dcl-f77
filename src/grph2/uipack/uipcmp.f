*---------------------------------------------------------------
      SUBROUTINE UIPCMP(XMIN,XMAX,YMIN,YMAX,CPOS)
      CHARACTER*(*) CPOS

      REAL  U(2,2),V(2,2)
      LOGICAL LCX, LCY,LEM

      CALL UIQCR2(U1, U2, V1, V2)

      U(1,1) = U1
      U(1,2) = U1
      U(2,1) = U2
      U(2,2) = U2

      V(1,1) = V1
      V(1,2) = V2
      V(2,1) = V1
      V(2,2) = V2

      CALL GRFIG
      CALL SGSTRN( 1 )
      CALL SGSVPT( XMIN,  XMAX, YMIN,  YMAX )
      CALL SGSWND( U1,  U2,  V1,  V2 )
      CALL SGSTRF

      CALL UILGET('CELL_MODE_X', LCX)
      CALL UILGET('CELL_MODE_Y', LCY)
      CALL UILGET('EMBOSS     ', LEM)

      CALL UILSET('CELL_MODE_X', .FALSE.)
      CALL UILSET('CELL_MODE_Y', .FALSE.)
      CALL UILSET('EMBOSS     ', .FALSE.)

      CALL UWSGXB(U1, U2, 2)
      CALL UWSGYB(V1, V2, 2)
      CALL UIPDA2(U,V,2,2,2)
      CALL SLPVPR(3)
      CALL USDAXS

      CALL UILSET('CELL_MODE_X', LCX)
      CALL UILSET('CELL_MODE_Y', LCY)
      CALL UILSET('EMBOSS     ', LEM)

      END
