*-----------------------------------------------------------------------
      SUBROUTINE UIPDAT(Z, MX, NX, NY)

      REAL      Z(MX,*)

      PARAMETER (MAXPXL=4000)
      INTEGER   IMAGE(MAXPXL)

      CALL  UIPDAZ(Z, MX, NX, NY, IMAGE, MAXPXL)
      END
