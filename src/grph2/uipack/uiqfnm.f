*-----------------------------------------------------------------------
*     FULL COLOR QUERY FILENAME
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UIQFNM(CPARA,CFNAME)

      CHARACTER CPARA*(*),CFNAME*(*)

      PARAMETER (MAXP=3,MAXF=3)

      CHARACTER CPLIST(MAXP)*1024,CFLIST(MAXF)*1024


      CPLIST(1)=' '
      CALL GLCGET('DUPATH',CPLIST(2))
      CALL GLCGET('DSPATH',CPLIST(3))

      DO 10 N=1,MAXF
        CFLIST(N)=CPARA
   10 CONTINUE

      CALL CLOWER(CFLIST(2))
      CALL CUPPER(CFLIST(3))
      CALL CFSRCH(CPLIST,MAXP,CFLIST,MAXF,CFNAME)

      END
