*-----------------------------------------------------------------------
*     ULLQNP / ULLQID / ULLQCP / ULLQVL / ULLSVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE ULLQNP(NCP)

      LOGICAL   LPARA
      CHARACTER CP*(*)

      CHARACTER CMSG*80

      SAVE


      NCP = 0

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULLQID(CP, IDX)

      IDX = 0

      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','ULLQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULLQCP(IDX, CP)

      CALL MSGDMP('E','ULLQCP','IDX IS OUT OF RANGE.')

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULLQCL(IDX, CP)

      CALL MSGDMP('E','ULLQCL','IDX IS OUT OF RANGE.')

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULLQVL(IDX, LPARA)

      LPARA = .FALSE.

      CALL MSGDMP('E','ULLQVL','IDX IS OUT OF RANGE.')

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULLSVL(IDX, LPARA)

      CALL MSGDMP('E','ULLSVL','IDX IS OUT OF RANGE.')

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULLQIN(CP, IN)

      IN = 0

      RETURN
      END
