*-----------------------------------------------------------------------
*     ULPQNP / ULPQID / ULPQCP / ULPQVL / ULPSVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE ULPQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA = 4)

      INTEGER   ITYPE(NPARA)
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      LOGICAL   LCHREQ

      EXTERNAL  LCHREQ,LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS(1) / 'IXCHR   ' /, ITYPE(1) / 1 /
      DATA      CPARAS(2) / 'IYCHR   ' /, ITYPE(2) / 1 /
      DATA      CPARAS(3) / 'IXTYPE  ' /, ITYPE(3) / 1 /
      DATA      CPARAS(4) / 'IYTYPE  ' /, ITYPE(4) / 1 /

*     / LONG NAME /

      DATA      CPARAL(1) / 'LOG_X_LABEL_CHAR' /
      DATA      CPARAL(2) / 'LOG_Y_LABEL_CHAR' /
      DATA      CPARAL(3) / '****IXTYPE  ' /
      DATA      CPARAL(4) / '****IYTYPE  ' /

*     IXCHR /IYCHR  : CHARACTER NUMBER OF * FOR 5*10E1 IN X/Y-AXIS
*     IXTYPE/IXTYPE : 1-4  1 ... 10|2" 2*10|2" 5*10|2" 10|3"  ETC
*                          2 ... 10|2" 2       5       10|3"  ETC
*                          3 ... 100   200     500     1000   ETC
*                          4 ... 100   2       5       1000   ETC
*               FORMAT FOR 3 OR 4 DEPENDS ON THAT SET BY ULSFMT


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULPQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','ULPQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULPQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','ULPQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULPQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','ULPQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULPQIT(IDX, ITP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        ITP = ITYPE(IDX)
      ELSE
        CALL MSGDMP('E','ULPQIT','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULPQVL(IDX, IPARA)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IF (ITYPE(IDX) .EQ. 1) THEN
          CALL ULIQID(CPARAS(IDX), ID)
          CALL ULIQVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 2) THEN
          CALL ULLQID(CPARAS(IDX), ID)
          CALL ULLQVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 3) THEN
          CALL ULRQID(CPARAS(IDX), ID)
          CALL ULRQVL(ID, IPARA)
        END IF
      ELSE
        CALL MSGDMP('E','ULPQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULPSVL(IDX, IPARA)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IF (ITYPE(IDX) .EQ. 1) THEN
          CALL ULIQID(CPARAS(IDX), ID)
          CALL ULISVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 2) THEN
          CALL ULLQID(CPARAS(IDX), ID)
          CALL ULLSVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 3) THEN
          CALL ULRQID(CPARAS(IDX), ID)
          CALL ULRSVL(ID, IPARA)
        END IF
      ELSE
        CALL MSGDMP('E','ULPSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULPQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
