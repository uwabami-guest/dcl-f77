*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE ULYLBL ( BL, NBL, INUM )

*     LABEL BUFFER FOR Y-AXIS

      DIMENSION YBL(10,4),NYBL(4),BL(*)

      SAVE

      DATA YBL/1.,10.,0.,0.,0.,0.,0.,0.,0.,0.,
     &         1.,2.,10.,0.,0.,0.,0.,0.,0.,0.,
     &         1.,2.,5.,10.,0.,0.,0.,0.,0.,0.,
     &         1.,2.,3.,4.,5.,6.,7.,8.,9.,10./
      DATA NYBL/1,2,3,9/


      DO 10 IBL=1,NYBL(INUM)+1
        BL(IBL)=YBL(IBL,INUM)
   10 CONTINUE
      NBL=NYBL(INUM)

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULSYBL ( BL, NBL )

      NYBL(4)=NBL
      DO 20 IBL=1,NBL
        YBL(IBL,4)=BL(IBL)
   20 CONTINUE
      YBL(NBL+1,4)=10.

      RETURN
*-----------------------------------------------------------------------
      ENTRY ULQYBL ( BL, NBL )

      NBL=NYBL(4)
      DO 30 IBL=1,NBL
        BL(IBL)=YBL(IBL,4)
   30 CONTINUE

      RETURN
      END
