*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMBNDC(XMIN, XMAX, YMIN, YMAX)

      PARAMETER (NMAX = 360)

      EXTERNAL  RFPI


      CALL GLRGET('RUNDEF', RUNDEF)

      CALL UMQCWD(XCNTR, YCNTR, R)

      IF (XCNTR.EQ.RUNDEF .OR. YCNTR.EQ.RUNDEF .OR.
     +        R.EQ.RUNDEF ) RETURN

      CALL STFRAD(R, R, RR, RR)
      CALL STFRAD(XCNTR, YCNTR, UXC,   UYC)
      CALL STFROT(UXC,   UYC,   TXC,   TYC)

      IF (XMIN.EQ.RUNDEF) THEN
        CALL STFTRN(TXC,   TYC,   XMIN,  YMIN)
        XMAX = XMIN
        YMAX = YMIN
      END IF

      PI = RFPI()
      PSI0 = PI/2. - TYC
      DT = 2*PI/NMAX

      DO 10 I=1, NMAX
        TH = DT*I-PI
        XX = COS(PSI0)*COS(RR) + SIN(PSI0)*SIN(RR)*COS(TH)
	IF (XX.GE.1) THEN
          PSI = 0.
        ELSE IF (XX.LE.-1) THEN
          PSI = PI
        ELSE
          PSI = ACOS(XX)
        END IF
	
        IF (PSI0.EQ.0 .OR. PSI0.EQ.PI) THEN
          XLM = TH
	ELSE IF(PSI.EQ.0 .OR. PSI.EQ.PI) THEN
          XLM = 0.
        ELSE
          XX = (COS(RR)-COS(PSI)*COS(PSI0))/(SIN(PSI)*SIN(PSI0))
          IF (XX.GE.1) THEN
            XLM = 0
          ELSE IF (XX.LE.-1) THEN
            XLM = PI
          ELSE
            XLM = ACOS(XX)
          END IF
	  XLM = SIGN(XLM,TH)
        END IF
	
        TX = XMPLON(TXC + XLM)
	TY = PI/2. - PSI

	CALL STFTRN(TX, TY, VX, VY)
        XMAX = MAX (XMAX, VX)
        XMIN = MIN (XMIN, VX)
        YMAX = MAX (YMAX, VY)
        YMIN = MIN (YMIN, VY)
   10 CONTINUE

      END
