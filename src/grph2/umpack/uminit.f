*-----------------------------------------------------------------------
*     CONTROL ROUTINES
*-----------------------------------------------------------------------
*     INITIALIZATION
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMINIT


      CALL GLRGET('RUNDEF', RUNDEF)
      CALL UMSCNT(RUNDEF, RUNDEF, RUNDEF)
      CALL UMSCWD(RUNDEF, RUNDEF, RUNDEF)
      CALL UMRSET('DGRIDMJ', RUNDEF)
      CALL UMRSET('DGRIDMN', RUNDEF)
      CALL UMRPNT

      END
