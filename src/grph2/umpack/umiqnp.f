*-----------------------------------------------------------------------
*     UMIQNP / UMIQID / UMIQCP / UMIQVL / UMISVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMIQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA = 11)

      INTEGER   IX(NPARA)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ, LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS( 1) / 'INDEXMJ ' /, IX( 1) /    3 /
      DATA      CPARAS( 2) / 'INDEXMN ' /, IX( 2) /    1 /
      DATA      CPARAS( 3) / 'ITYPEMJ ' /, IX( 3) /    1 /
      DATA      CPARAS( 4) / 'ITYPEMN ' /, IX( 4) /    3 /
      DATA      CPARAS( 5) / 'INDEXBND' /, IX( 5) /    3 /
      DATA      CPARAS( 6) / 'MAXBND  ' /, IX( 6) /  300 /
      DATA      CPARAS( 7) / 'IGROUP  ' /, IX( 7) /    1 /
      DATA      CPARAS( 8) / 'INDEXOUT' /, IX( 8) /    1 /
      DATA      CPARAS( 9) / 'ITYPEOUT' /, IX( 9) /    1 /
      DATA      CPARAS(10) / 'IPATLAND' /, IX(10) / 1999 /
      DATA      CPARAS(11) / 'IPATLAKE' /, IX(11) / 9999 /

*     / LONG NAME /

      DATA      CPARAL( 1) / 'MAP_MAJOR_LINE_INDEX' /
      DATA      CPARAL( 2) / 'MAP_MINOR_LINE_INDEX' /
      DATA      CPARAL( 3) / 'MAP_MAJOR_LINE_TYPE' /
      DATA      CPARAL( 4) / 'MAP_MINOR_LINE_TYPE' /
      DATA      CPARAL( 5) / 'MAP_BOUNDARY_INDEX' /
      DATA      CPARAL( 6) / 'MAP_BOUNDARY_LINE_MAX' /
      DATA      CPARAL( 7) / '****IGROUP  ' /
      DATA      CPARAL( 8) / 'MAP_OUTLINE_INDEX' /
      DATA      CPARAL( 9) / 'MAP_OUTLINE_TYPE' /
      DATA      CPARAL(10) / 'SHADE PATTERN ON LAND' /
      DATA      CPARAL(11) / 'SHADE PATTERN ON LAKE' /

      DATA      LFIRST / .TRUE. /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMIQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','UMIQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMIQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','UMIQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMIQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','UMIQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMIQVL(IDX, IPARA)

      IF (LFIRST) THEN
        CALL RTIGET('UM', CPARAS, IX, NPARA)
        CALL RLIGET(CPARAL, IX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IPARA = IX(IDX)
      ELSE
        CALL MSGDMP('E','UMIQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMISVL(IDX, IPARA)

      IF (LFIRST) THEN
        CALL RTIGET('UM', CPARAS, IX, NPARA)
        CALL RLIGET(CPARAL, IX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IX(IDX) = IPARA
      ELSE
        CALL MSGDMP('E','UMISVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMIQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
