*-----------------------------------------------------------------------
*     UMLGET / UMLSET / UMLSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMLGET(CP, LPARA)

      LOGICAL   LPARA
      CHARACTER CP*(*)

      LOGICAL   LP
      CHARACTER CX*8
      CHARACTER CL*40


      CALL UMLQID(CP, IDX)
      CALL UMLQVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMLSET(CP, LPARA)

      CALL UMLQID(CP, IDX)
      CALL UMLSVL(IDX, LPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMLSTX(CP, LPARA)

      LP = LPARA
      CALL UMLQID(CP, IDX)

*     / SHORT NAME /

      CALL UMLQCP(IDX, CX)
      CALL RTLGET('UM', CX, LP, 1)

*     / LONG NAME /

      CALL UMLQCL(IDX, CL)
      CALL RLLGET(CL, LP, 1)

      CALL UMLSVL(IDX,LP)

      RETURN
      END
