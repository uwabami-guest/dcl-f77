*-----------------------------------------------------------------------
*     DRAW LIMB OF MAP
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMPLIM

      PARAMETER (EPSIL=1.0E-4)

      REAL      XBND(2),YBND(2)

      LOGICAL   LREQA,LDIFF

      EXTERNAL  RFPI,LREQA


      PI=RFPI()

*     / GET INTERNAL PARAMETERS /

      CALL SZQCLX(XBND(1),XBND(2))
      CALL SZQCLY(YBND(1),YBND(2))
      CALL UMIGET('INDEXBND',INDEX)
      CALL UMIGET('MAXBND',NMAX)

*     / SET INTERNAL PARAMETER /

      CALL SZSLTI(1,INDEX)

*     / DRAW SIDE BOUNDARIES /

      CALL STFTRN(XBND(1), 0., X1, Y1)
      CALL STFTRN(XBND(2), 0., X2, Y2)
      LDIFF = .NOT.(LREQA(X1, X2, EPSIL) .AND. LREQA(Y1, Y2, EPSIL))

      IF (LDIFF) THEN
        DO 20 I=1,2
          CALL SZOPLV
          CALL STFTRN(XBND(I),YBND(1),XX,YY)
          CALL SZMVLV(XX,YY)
          DO 10 J=1,NMAX
            Y=J*(YBND(2) - YBND(1))/NMAX + YBND(1)
            CALL STFTRN(XBND(I),Y,XX,YY)
            CALL SZPLLV(XX,YY)
   10     CONTINUE
          CALL SZCLLV
   20   CONTINUE
      END IF

*     / DRAW POLES /

      IF (XBND(1).EQ.XBND(2)) THEN
        XBND(1)=-PI
        XBND(2)= PI
      END IF

      DO 40 I=1,2

        CALL STFTRN(XBND(1), YBND(I), X1, Y1)
        CALL STFTRN(XBND(2), YBND(I), X2, Y2)
        CALL STFTRN(     0., YBND(I), X3, Y3)

        LDIFF = .NOT.(LREQA(X1, X3, EPSIL) .AND. LREQA(Y1, Y3, EPSIL))
     +     .OR. .NOT.(LREQA(X2, X3, EPSIL) .AND. LREQA(Y2, Y3, EPSIL))

        IF (LDIFF) THEN
          CALL SZOPLV
          CALL STFTRN(XBND(1),YBND(I),XX,YY)
          CALL SZMVLV(XX,YY)
          DO 30 J=1,NMAX
            X=J*(XBND(2)-XBND(1))/NMAX+XBND(1)
            CALL STFTRN(X,YBND(I),XX,YY)
            CALL SZPLLV(XX,YY)
   30     CONTINUE
          CALL SZCLLV
        END IF

   40 CONTINUE

      END
