*-----------------------------------------------------------------------
*     UMPQNP / UMPQID / UMPQCP / UMPQVL / UMPSVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMPQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA = 20)

      INTEGER   ITYPE(NPARA)
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      LOGICAL   LCHREQ

      EXTERNAL  LCHREQ,LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS( 1) / 'INDEXMJ ' /, ITYPE( 1) / 1 /
      DATA      CPARAS( 2) / 'INDEXMN ' /, ITYPE( 2) / 1 /
      DATA      CPARAS( 3) / 'ITYPEMJ ' /, ITYPE( 3) / 1 /
      DATA      CPARAS( 4) / 'ITYPEMN ' /, ITYPE( 4) / 1 /
      DATA      CPARAS( 5) / 'DGRIDMJ ' /, ITYPE( 5) / 3 /
      DATA      CPARAS( 6) / 'DGRIDMN ' /, ITYPE( 6) / 3 /
      DATA      CPARAS( 7) / 'DGRPLMJ ' /, ITYPE( 7) / 3 /
      DATA      CPARAS( 8) / 'DGRPLMN ' /, ITYPE( 8) / 3 /
      DATA      CPARAS( 9) / 'LGRIDMJ ' /, ITYPE( 9) / 2 /
      DATA      CPARAS(10) / 'LGRIDMN ' /, ITYPE(10) / 2 /
      DATA      CPARAS(11) / 'INDEXBND' /, ITYPE(11) / 1 /
      DATA      CPARAS(12) / 'MAXBND  ' /, ITYPE(12) / 1 /
      DATA      CPARAS(13) / 'IGROUP  ' /, ITYPE(13) / 1 /
      DATA      CPARAS(14) / 'INDEXOUT' /, ITYPE(14) / 1 /
      DATA      CPARAS(15) / 'ITYPEOUT' /, ITYPE(15) / 1 /
      DATA      CPARAS(16) / 'LGLOBE  ' /, ITYPE(16) / 2 /
      DATA      CPARAS(17) / 'LWHINT  ' /, ITYPE(17) / 2 /
      DATA      CPARAS(18) / 'IPATLAND' /, ITYPE(18) / 1 /
      DATA      CPARAS(19) / 'IPATLAKE' /, ITYPE(19) / 1 /
      DATA      CPARAS(20) / 'LFILLAKE' /, ITYPE(20) / 2 /

*     / LONG NAME /

      DATA      CPARAL( 1) / 'MAP_MAJOR_LINE_INDEX' /
      DATA      CPARAL( 2) / 'MAP_MINOR_LINE_INDEX' /
      DATA      CPARAL( 3) / 'MAP_MAJOR_LINE_TYPE' /
      DATA      CPARAL( 4) / 'MAP_MINOR_LINE_TYPE' /
      DATA      CPARAL( 5) / 'MAP_MAJOR_LINE_INTERVAL' /
      DATA      CPARAL( 6) / 'MAP_MINOR_LINE_INTERVAL' /
      DATA      CPARAL( 7) / 'MAP_MAJOR_LINE_POLAR_LIMIT' /
      DATA      CPARAL( 8) / 'MAP_MINOR_LINE_POLAR_LIMIT' /
      DATA      CPARAL( 9) / 'ENABLE_MAP_MAJOR_LINE' /
      DATA      CPARAL(10) / 'ENABLE_MAP_MINOR_LINE' /
      DATA      CPARAL(11) / 'MAP_BOUNDARY_INDEX' /
      DATA      CPARAL(12) / 'MAP_BOUNDARY_LINE_MAX' /
      DATA      CPARAL(13) / '****IGROUP  ' /
      DATA      CPARAL(14) / 'MAP_OUTLINE_INDEX' /
      DATA      CPARAL(15) / 'MAP_OUTLINE_TYPE' /
      DATA      CPARAL(16) / 'ENABLE_GLOBAL_MAPPING' /
      DATA      CPARAL(17) / '----LWHINT  ' /
      DATA      CPARAL(18) / 'SHADE PATTERN ON LAND' /
      DATA      CPARAL(19) / 'SHADE PATTERN ON LAKE' /
      DATA      CPARAL(20) / 'ENABLE_PAINT_LAKE' /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMPQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','UMPQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMPQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','UMPQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMPQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','UMPQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMPQIT(IDX, ITP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        ITP = ITYPE(IDX)
      ELSE
        CALL MSGDMP('E','UMPQIT','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMPQVL(IDX, IPARA)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IF (ITYPE(IDX) .EQ. 1) THEN
          CALL UMIQID(CPARAS(IDX), ID)
          CALL UMIQVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 2) THEN
          CALL UMLQID(CPARAS(IDX), ID)
          CALL UMLQVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 3) THEN
          CALL UMRQID(CPARAS(IDX), ID)
          CALL UMRQVL(ID, IPARA)
        END IF
      ELSE
        CALL MSGDMP('E','UMPQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMPSVL(IDX, IPARA)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IF (ITYPE(IDX) .EQ. 1) THEN
          CALL UMIQID(CPARAS(IDX), ID)
          CALL UMISVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 2) THEN
          CALL UMLQID(CPARAS(IDX), ID)
          CALL UMLSVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 3) THEN
          CALL UMRQID(CPARAS(IDX), ID)
          CALL UMRSVL(ID, IPARA)
        END IF
      ELSE
        CALL MSGDMP('E','UMPSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMPQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
