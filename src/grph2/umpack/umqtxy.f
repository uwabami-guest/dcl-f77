*-----------------------------------------------------------------------
*     T-WINDOW
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMQTXY(TXMINZ, TXMAXZ, TYMINZ, TYMAXZ)

      COMMON    /UMWK1/ITR, RUNDEF, IUNDEF, PI, CPR, CPD, CP

      SAVE


      CALL SGQTXY(TXMIN,  TXMAX,  TYMIN,  TYMAX)
      CALL STFRAD(TXMIN, TXMAX, TXMINZ, TXMAXZ)
      IF (ITR.EQ.11) THEN
        TYMINZ = -CP*75.
        TYMAXZ =  CP*75.
      ELSE IF (ITR.EQ.31) THEN
        TYMINZ =  CP*0.
        TYMAXZ =  CP*90.
      ELSE IF (ITR.EQ.22) THEN
        CALL SGRGET('STLAT1', STLAT1)
        CALL SGRGET('STLAT2', STLAT2)
        IF (STLAT1+STLAT2.GE.0.) THEN
          TYMINZ =  CP*0.
          TYMAXZ =  CP*90.
        ELSE
          TYMINZ = -CP*90.
          TYMAXZ =  CP*0.
        END IF
      ELSE
        CALL STFRAD(TYMIN, TYMAX, TYMINZ, TYMAXZ)
      END IF
      END
