*-----------------------------------------------------------------------
*     CIRCLE WINDOW
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMSCWD(XCNTR, YCNTR, R)

      SAVE


      XCNTRZ = XCNTR
      YCNTRZ = YCNTR
      RZ     = R

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMQCWD(XCNTR, YCNTR, R)

      XCNTR = XCNTRZ
      YCNTR = YCNTRZ
      R     = RZ

      RETURN
      END
