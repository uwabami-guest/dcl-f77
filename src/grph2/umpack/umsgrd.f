*-----------------------------------------------------------------------
*     DETERMINE GRID SPACING
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMSGRD


      CALL UMRGET('DGRIDMJ', DGRMJX)
      CALL UMRGET('DGRIDMN', DGRMNX)
      CALL GLRGET('RUNDEF' , RUNDEF)

      CALL SGQVPT(VXMIN, VXMAX, VYMIN, VYMAX)
      CALL STITRF((VXMIN+VXMAX)/2., (VYMIN+VYMAX)/2., UXC, UYC)

      VR = SQRT((VXMAX-VXMIN)**2 + (VYMAX-VYMIN)**2)/SQRT(2.)
      IF (UXC.NE.RUNDEF .AND. UYC.NE.RUNDEF .AND.
     +   UYC.LT.89.    .AND. UYC.GT.-89.) THEN
        CALL STFTRF(UXC-0.5, UYC-0.5, VX1, VY1)
        CALL STFTRF(UXC+0.5, UYC+0.5, VX2, VY2)
	R = SQRT((VX2-VX1)**2 + (VY2-VY1)**2)/VR/0.01234
      ELSE
        CALL SGRGET('SIMFAC', R)
	R = R/VR
      END IF

      IF (R .LE. 1.) THEN
        DGRMJ = 90.
        DGRMN = 30.
      ELSE IF (R .LE. 2.) THEN
        DGRMJ = 45.
        DGRMN = 15.
      ELSE IF (R .LE. 3.) THEN
        DGRMJ = 30.
        DGRMN = 10.
      ELSE IF (R .LE. 4.) THEN
        DGRMJ = 20.
        DGRMN =  5.
      ELSE IF (R .LE. 8.) THEN
        DGRMJ = 10.
        DGRMN =  2.
      ELSE IF (R .LE. 15.) THEN
        DGRMJ =  5.
        DGRMN =  1.
      ELSE IF (R .LE. 30.) THEN
        DGRMJ =  2.
        DGRMN =  0.5
      ELSE
        DGRMJ =  1.
        DGRMN =  0.2
      END IF

      IF (DGRMJX.EQ.RUNDEF) CALL UMRSET('DGRIDMJ', DGRMJ)
      IF (DGRMNX.EQ.RUNDEF) CALL UMRSET('DGRIDMN', DGRMN)

      END
