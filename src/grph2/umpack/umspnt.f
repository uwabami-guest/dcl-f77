*-----------------------------------------------------------------------
*     SCATTERED POINTS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMSPNT(N, UX, UY)

      REAL      UX(*), UY(*)

      PARAMETER (NMAX=64)

      REAL      UXZ(NMAX), UYZ(NMAX)

      SAVE

      DATA      NDATA / 0 /


      DO 10 I=1, N
        IF (NDATA .GE. NMAX) THEN
          CALL MSGDMP('W', 'UMSPNT', 'TOO MANY POINTS IGNORED.')
          RETURN
        END IF
        NDATA = NDATA + 1
        UXZ(NDATA) = UX(NDATA)
        UYZ(NDATA) = UY(NDATA)
   10 CONTINUE

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMQPNT(N, UXA, UYA)

      UXA = UXZ(N)
      UYA = UYZ(N)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMQPTN(N)

      N = NDATA

      RETURN
*-----------------------------------------------------------------------
      ENTRY UMRPNT

      NDATA = 0

      RETURN
      END
