*-----------------------------------------------------------------------
*     UMSPWD : WINDOW MODE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UMSPWD

      COMMON    /UMWK1/ITR, RUNDEF, IUNDEF, PI, CPR, CPD, CP

      SAVE


      CALL SGQMPL(PLX, PLY, PLROT)
      IF (PLX.NE.RUNDEF .AND. PLY.NE. RUNDEF .AND.
     +    PLROT.NE.RUNDEF ) RETURN

      CALL SGQWND (UXMIN, UXMAX, UYMIN, UYMAX)

      IF (UXMIN.EQ.RUNDEF .OR. UXMAX.EQ.RUNDEF .OR.
     +    UYMIN.EQ.RUNDEF .OR. UYMAX.EQ.RUNDEF ) RETURN

      DX = UXMAX - UXMIN
      IF ( DX .LE. 0. ) DX = DX + 2*PI*CPR
      PLX = UXMIN + DX/2.

      IF (ITR.GE.30) THEN
        PLY = (UYMIN+UYMAX)/2.
      ELSE
        PLY =  CPD*90.
      END IF

      CALL SGSMPL(PLX, PLY, 0.)

*------------------------ STANDARD LATITUDE ---------------------------

      CALL SGRGET('STLAT1',STLAT1)
      CALL SGRGET('STLAT2',STLAT2)

      IF (ITR.EQ.20 .OR. ITR.EQ.21 .OR. ITR.EQ.23) THEN
        IF (UYMIN.NE.RUNDEF .AND. UYMAX.NE.RUNDEF) THEN
	  IF (STLAT1 .EQ. RUNDEF) STLAT1 =  (UYMIN+UYMAX)/2.
          IF (STLAT1 .EQ. 0.) CALL MSGDMP ('E', 'UMSPWD',
     +                 'INVALID WINDOW FOR CONICAL PROJECTION.')
        ELSE
          IF (STLAT1 .EQ. RUNDEF) STLAT1 = CPD*35
        END IF
        CALL SGRSET('STLAT1',STLAT1)
      ELSE IF (ITR .EQ. 22) THEN
        IF (UYMIN.NE.RUNDEF .AND. UYMAX.NE.RUNDEF) THEN
          IF (STLAT1 .EQ. RUNDEF) STLAT1 = UYMIN
          IF (STLAT2 .EQ. RUNDEF) STLAT2 = UYMAX
	ELSE
          IF (STLAT1 .EQ. RUNDEF) STLAT1 = CPD*35
          IF (STLAT2 .EQ. RUNDEF) STLAT2 = CPD*45
        END IF
        CALL SGRSET('STLAT1', STLAT1)
        CALL SGRSET('STLAT2', STLAT2)
      END IF

      END
