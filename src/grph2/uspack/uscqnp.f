*-----------------------------------------------------------------------
*     USCQNP
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USCQNP(NCP)

      CHARACTER CP*(*), CVAL*(*)

      PARAMETER (NPARA = 11)

      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CX(NPARA)*80, CMSG*80
      LOGICAL   LCHREQ, LFIRST
      SAVE

* ---- short name ----
      DATA      CPARAS( 1) / 'CXSIDE  ' /, CX( 1) / 'BT' /
      DATA      CPARAS( 2) / 'CYSIDE  ' /, CX( 2) / 'LR' /
      DATA      CPARAS( 3) / 'CXSPOS  ' /, CX( 3) / 'R ' /
      DATA      CPARAS( 4) / 'CYSPOS  ' /, CX( 4) / 'T ' /
      DATA      CPARAS( 5) / 'CBLKT   ' /, CX( 5) / '()' /
      DATA      CPARAS( 6) / 'CXTTL   ' /, CX( 6) / ' ' /
      DATA      CPARAS( 7) / 'CYTTL   ' /, CX( 7) / ' ' /
      DATA      CPARAS( 8) / 'CXUNIT  ' /, CX( 8) / ' ' /
      DATA      CPARAS( 9) / 'CYUNIT  ' /, CX( 9) / ' ' /
      DATA      CPARAS(10) / 'CXFMT   ' /, CX(10) / ' ' /
      DATA      CPARAS(11) / 'CYFMT   ' /, CX(11) / ' ' /

* ---- long name ----
      DATA      CPARAL( 1) / 'X_AXIS_POS    ' /
      DATA      CPARAL( 2) / 'Y_AXIS_POS    ' /
      DATA      CPARAL( 3) / 'X_SUBLABEL_POS' /
      DATA      CPARAL( 4) / 'Y_SUBLABEL_POS' /
      DATA      CPARAL( 5) / 'SUBLABEL_BRACKET' /
      DATA      CPARAL( 6) / '****CXTTL   ' /
      DATA      CPARAL( 7) / '****CYTTL   ' /
      DATA      CPARAL( 8) / '****CXUNIT  ' /
      DATA      CPARAL( 9) / '****CYUNIT  ' /
      DATA      CPARAL(10) / '****CXFMT   ' /
      DATA      CPARAL(11) / '****CYFMT   ' /

      DATA      LFIRST /.TRUE./

      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY USCQID(CP, IDX)

      DO 10 N=1,NPARA
        IF (LCHREQ(CP,CPARAS(N)) .OR. LCHREQ(CP,CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE

      CMSG='PARAMETER "'//CP(1:LENC(CP))//'" IS NOT DEFINED.'
      CALL MSGDMP('E','USCQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY USCQCP(IDX,CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
         CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','USCQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY USCQCL(IDX,CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
         CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','USCQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY USCQVL(IDX,CVAL)

      IF(LFIRST) THEN
        CALL RTCGET('US', CPARAS, CX, NPARA)
        CALL RLCGET(CPARAL, CX, NPARA)
        LFIRST = .FALSE.
      ENDIF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CVAL=CX(IDX)
      ELSE
        CALL MSGDMP('E','USCQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY USCSVL(IDX,CVAL)

      IF(LFIRST) THEN
        CALL RTCGET('US', CPARAS, CX, NPARA)
        CALL RLCGET(CPARAL, CX, NPARA)
        LFIRST = .FALSE.
      ENDIF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CX(IDX)=CVAL
      ELSE
        CALL MSGDMP('E','USCSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY USCQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
 20   CONTINUE

      IN = 0

      RETURN
      END
