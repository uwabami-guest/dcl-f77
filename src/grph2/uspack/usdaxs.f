*-----------------------------------------------------------------------
*     USPACK DRAW DEFAULT AXIS                        S.Sakai  99/10/09
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USDAXS
      CHARACTER  CSIDE*4, CS*1
      EXTERNAL   LENZ

*----------------------------- X-AXIS ----------------------------------

      CALL USCGET('CXSIDE', CSIDE)
      NCS = LENZ(CSIDE)
      DO 100 I=1, NCS
        CS = CSIDE(I:I)
        CALL CUPPER(CS)
        IF(CS.EQ.'U') CS='H'
        CALL USAXSC(CS)
  100 CONTINUE

*----------------------------- Y-AXIS ----------------------------------

      CALL USCGET('CYSIDE', CSIDE)
      NCS = LENZ(CSIDE)
      DO 200 I=1, NCS
        CS = CSIDE(I:I)
        CALL CUPPER(CS)
        IF(CS.EQ.'U') CS='V'
        CALL USAXSC(CS)
  200 CONTINUE

      END
