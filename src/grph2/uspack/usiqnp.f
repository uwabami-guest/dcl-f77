*-----------------------------------------------------------------------
*     USIQNP / USIQID / USIQCP / USIQVL / USISVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USIQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA = 13)
      PARAMETER (IUNDEF = -999)

      INTEGER   IX(NPARA)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ, LENC

      SAVE

* ---- SHORT NAME ----
*     / CONTROL PARAMETERS /
      DATA      CPARAS( 1) / 'IRESET  ' /, IX( 1) / 1 /
*     / PARAMETERS FOR USUSCU /
      DATA      CPARAS( 2) / 'MXDGTX  ' /, IX( 2) / 4 /
      DATA      CPARAS( 3) / 'MXDGTY  ' /, IX( 3) / 4 /
      DATA      CPARAS( 4) / 'NBLANK1 ' /, IX( 4) / 1 /
      DATA      CPARAS( 5) / 'NBLANK2 ' /, IX( 5) / 2 /
*     / PARAMETERS FOR USUSCL /
      DATA      CPARAS( 6) / 'NLBLX   ' /, IX( 6) / IUNDEF /
      DATA      CPARAS( 7) / 'NLBLY   ' /, IX( 7) / IUNDEF /
      DATA      CPARAS( 8) / 'NTICKSX ' /, IX( 8) / IUNDEF /
      DATA      CPARAS( 9) / 'NTICKSY ' /, IX( 9) / IUNDEF /
      DATA      CPARAS(10) / 'ITYPEX  ' /, IX(10) / IUNDEF /
      DATA      CPARAS(11) / 'ITYPEY  ' /, IX(11) / IUNDEF /
*     / PARAMETERS FOR USXSUB & USYSUB /
      DATA      CPARAS(12) / 'MXDGTSX ' /, IX(12) / 6 /
      DATA      CPARAS(13) / 'MXDGTSY ' /, IX(13) / 6 /

* ---- LONG NAME ----
*     / CONTROL PARAMETERS /
      DATA      CPARAL( 1) / '****IRESET  ' /
*     / PARAMETERS FOR USUSCU /
      DATA      CPARAL( 2) / 'X_LABEL_MAX_CHAR' /
      DATA      CPARAL( 3) / 'X_LABEL_MAX_CHAR' /
      DATA      CPARAL( 4) / 'LABEL_GAP_PARALLEL' /
      DATA      CPARAL( 5) / 'LABEL_GAP_RIGHT_ANGLE' /
*     / PARAMETERS FOR USUSCL /
      DATA      CPARAL( 6) / 'LOG_X_LABEL_NUMBER' /
      DATA      CPARAL( 7) / 'LOG_Y_LABEL_NUMBER' /
      DATA      CPARAL( 8) / 'LOG_X_TICKS_NUMBER' /
      DATA      CPARAL( 9) / 'LOG_X_TICKS_NUMBER' /
      DATA      CPARAL(10) / 'LOG_X_LABEL_FORMAT' /
      DATA      CPARAL(11) / 'LOG_X_LABEL_FORMAT' /
*     / PARAMETERS FOR USXSUB & USYSUB /
      DATA      CPARAL(12) / 'X_SUBLABEL_MAX_CHAR' /
      DATA      CPARAL(13) / 'Y_SUBLABEL_MAX_CHAR' /

      DATA      LFIRST / .TRUE. /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY USIQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','USIQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY USIQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','USIQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY USIQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','USIQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY USIQVL(IDX, IPARA)

      IF (LFIRST) THEN
        CALL RTIGET('US', CPARAS, IX, NPARA)
        CALL RLIGET(CPARAL, IX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IPARA = IX(IDX)
      ELSE
        CALL MSGDMP('E','USIQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY USISVL(IDX, IPARA)

      IF (LFIRST) THEN
        CALL RTIGET('US', CPARAS, IX, NPARA)
        CALL RLIGET(CPARAL, IX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IX(IDX) = IPARA
      ELSE
        CALL MSGDMP('E','USISVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY USIQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
