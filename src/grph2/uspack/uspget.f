*-----------------------------------------------------------------------
*     USPGET / USPSET / USPSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USPGET(CP,IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8, CL*40


      CALL USPQID(CP, IDX)
      CALL USPQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY USPSET(CP, IPARA)

      CALL USPQID(CP, IDX)
      CALL USPSVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY USPSTX(CP, IPARA)

      IP = IPARA
      CALL USPQID(CP, IDX)
      CALL USPQIT(IDX, IT)
      CALL USPQCP(IDX, CX)
      CALL USPQCL(IDX, CL)
      IF (IT .EQ. 1) THEN
        CALL RTIGET('US', CX, IP, 1)
        CALL RLIGET(CL, IP, 1)
        CALL USIQID(CP, IDX)
        CALL USISVL(IDX, IP)
      ELSE IF (IT .EQ. 2) THEN
        CALL RTLGET('US', CX, IP, 1)
        CALL RLLGET(CL, IP, 1)
        CALL USLQID(CP, IDX)
        CALL USLSVL(IDX, IP)
      ELSE IF (IT .EQ. 3) THEN
        CALL RTRGET('US', CX, IP, 1)
        CALL RLRGET(CL, IP, 1)
        CALL USRQID(CP, IDX)
        CALL USRSVL(IDX, IP)
      END IF

      RETURN
      END
