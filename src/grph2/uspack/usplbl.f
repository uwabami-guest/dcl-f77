*-----------------------------------------------------------------------
*     USPACK DRAW LABEL                               S.Sakai  99/10/09
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USPLBL(CSIDE, ISLCT, POS, CH, NC, N)

      CHARACTER  CSIDE*(*), CH(*)*(*), CS
      REAL       POS(N)
      EXTERNAL   LENZ

      NCS = LENZ(CSIDE)
      DO 100 I=1, NCS
        CS = CSIDE(I:I)
        CALL CUPPER(CS)

        IF(CS.EQ.'T' .OR. CS.EQ.'B' .OR. CS.EQ.'H') THEN
          IF(CS.EQ.'H') CS='U'
          CALL UXPLBL(CS, ISLCT, POS, CH, NC, N)

        ELSEIF(CS.EQ.'L' .OR. CS.EQ.'R' .OR. CS.EQ.'V') THEN
          IF(CS.EQ.'V') CS='U'
          CALL UYPLBL(CS, ISLCT, POS, CH, NC, N)
        ENDIF

  100 CONTINUE

      END
