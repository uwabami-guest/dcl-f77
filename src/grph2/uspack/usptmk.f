*-----------------------------------------------------------------------
*     USPACK DRAW TICK MARK                           S.Sakai  99/10/09
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USPTMK(CSIDE, ISLCT, POS, N)

      CHARACTER  CSIDE*(*), CS
      REAL       POS(N)
      EXTERNAL   LENZ

      NCS = LENZ(CSIDE)
      DO 100 I=1, NCS
        CS = CSIDE(I:I)
        CALL CUPPER(CS)

        IF(CS.EQ.'T' .OR. CS.EQ.'B' .OR. CS.EQ.'H') THEN
          IF(CS.EQ.'H') CS='U'
          CALL UXPTMK(CS, ISLCT, POS, N)

        ELSEIF(CS.EQ.'L' .OR. CS.EQ.'R' .OR. CS.EQ.'V') THEN
          IF(CS.EQ.'V') CS='U'
          CALL UYPTMK(CS, ISLCT, POS, N)
        ENDIF

  100 CONTINUE

      END
