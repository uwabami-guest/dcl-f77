*-----------------------------------------------------------------------
*     USRGET / USRSET / USRSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USRGET(CP, RPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40


      CALL USRQID(CP, IDX)
      CALL USRQVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY USRSET(CP, RPARA)

      CALL USRQID(CP, IDX)
      CALL USRSVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY USRSTX(CP, RPARA)

      RP = RPARA
      CALL USRQID(CP, IDX)

*     / SHORT NAME /

      CALL USRQCP(IDX, CX)
      CALL RTRGET('US', CX, RP, 1)

*     / LONG NAME /

      CALL USRQCL(IDX, CL)
      CALL RLRGET(CL, RP, 1)

      CALL USRSVL(IDX,RP)

      RETURN
      END
