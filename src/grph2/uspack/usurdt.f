*-----------------------------------------------------------------------
*     USPACK ROUND UMIN AND UMAX (UNIFORM)            S.Sakai  95/09/06
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USURDT(UMIN, UMAX, VMIN, VMAX, DUT)

      REAL      SC1 (4)
      LOGICAL   LEPSL, LREQ
      SAVE
      DATA      SC1 / 1., 2., 5., 10./

*------------------------- ARGUMENT CHECK ------------------------------

      IF(UMIN.GT.UMAX)
     &   CALL MSGDMP('E', 'USURDT', 'UMIN > UMAX.')

      IF(VMIN.GT.VMAX)
     &   CALL MSGDMP('E', 'USURDT', 'VMIN > VMAX.')

*-----------------------------------------------------------------------

      CALL GNSAVE
      CALL GNSBLK(SC1, 4)
      CALL GLLGET('LEPSL', LEPSL)
      CALL GLLSET('LEPSL', .TRUE.)

      IF(LREQ(UMAX,UMIN)) THEN
        IF(LREQ(UMAX,0.)) THEN
          UMAX = 1.
          UMIN = -1
        ELSE
          UMAX = MAX(UMAX, 0.)
          UMIN = MIN(UMIN, 0.)
        ENDIF
      ENDIF

*-----------------------------------------------------------------------

      CALL USRGET('TFACT'   , CT)
      CALL UZRGET('RSIZEL1' , CW)
      DVT  = CW*CT

      DO 100 I=1, 2
        DUT = ABS( (UMAX-UMIN)/(VMAX-VMIN)*DVT )
        CALL GNLE(DUT, BX, IEXP)
        DUT = BX*1.D1**IEXP
        UMAX = DUT * IRGE(UMAX/DUT)
        UMIN = DUT * IRLE(UMIN/DUT)
  100 CONTINUE

      CALL GLLSET('LEPSL', LEPSL)
      CALL GNRSET

      END
