*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USWAPZ(X1, X2, N)
      REAL X1(N), X2(N)

      DO 10 I=1, N
        XTMP  = X1(I)
        X1(I) = X2(I)
        X2(I) = XTMP
   10 CONTINUE

      END
