*-----------------------------------------------------------------------
*      USPACK NUMBER OF DIGIT                        S. Sakai  89/03/23
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE USZDGT(UMIN,  UMAX,  DUL,   MAXDGT,
     #                  UOFF,  UFACT, NDGT,  LDGT)
      CHARACTER CDUL*12

      CALL GLRGET('RUNDEF', RUNDEF)
      IF(UMIN.GE.UMAX) CALL MSGDMP('E', 'USZDGT', 'NMIN MUST BE < NMAX')
      IF(DUL.LE.0) CALL MSGDMP('E', 'USZDGT', 'DUL MUST BE POSITIVE.')

      NMAX = IRLE(UMAX/DUL)
      NMIN = IRGE(UMIN/DUL)
      IF(UOFF.EQ.RUNDEF) THEN
        IUOFF = 0
        I1 = 1
      ELSE
        IUOFF = NINT(UOFF/DUL)
        I1 = 2
      ENDIF

*-----------------------------------------------------------------------

      DO 100 I = I1, 2
        UMAXF = (NMAX - IUOFF)*DUL
        UMINF = (NMIN - IUOFF)*DUL

        IF(UMINF.NE.0) NMN = IRLE( LOG10( ABS(UMINF) ) )
        IF(UMAXF.NE.0) NMX = IRLE( LOG10( ABS(UMAXF) ) )
        IF(UMINF.EQ.0) NMN = NMX
        IF(UMAXF.EQ.0) NMX = NMN

        NMXS = 0
        NMNS = 0
        IF(UMAXF.LT.0) NMXS = 1
        IF(UMINF.LT.0) NMNS = 1

        NDMAX = MAX(NMX, NMN)
        NDMAXS = MAX(NMX+NMXS, NMN+NMNS)
        WRITE(CDUL, '(E12.5)') DUL
        READ(CDUL, '(T10,I3)') NDD
        DO 50 J=4, 9
          IF(CDUL(J:J) .EQ. '0') GOTO 55
          NDD = NDD - 1
   50   CONTINUE
   55   CONTINUE

        NDTRY = NDMAXS - NDD + 1
        IF(NDTRY.LE.MAXDGT) GOTO 200

        NOFF = IRLE(LOG10(UMAXF-UMINF))
        DL = 1.D1**(NOFF+1)
        UOFF = IRLT(UMAXF/DL) * DL
        IUOFF = IRLE( UOFF / DUL )
  100 CONTINUE

      CALL MSGDMP('W', 'USLDGT',
     #            'LABEL WIDTH IS GREATER THAN MAXDGT')
  200 CONTINUE

      UOFF = DUL*IUOFF

*-----------------------------------------------------------------------

      IF(UFACT.NE.RUNDEF) THEN
        NFAC = IRLE(LOG10(UFACT))
        NDMAXS = NDMAXS - NFAC
        NDMAX  = NDMAX  - NFAC
        NDD    = NDD    - NFAC
      ENDIF

      IF(NDD.GE.0) THEN
        NDGT = NDMAXS + 1
        LDGT = 0
      ELSEIF(NDMAX .LE. 0) THEN
        NDGT = -NDD + 2
        IF(UMINF.LT.0) NDGT = NDGT+1
        LDGT = -NDD
      ELSE
        NDGT = NDMAXS - NDD + 2
        LDGT = -NDD
      ENDIF

      IF(UFACT.NE.RUNDEF) THEN
        IF(NDGT.GT.MAXDGT) CALL MSGDMP('W', 'USLDGT',
     #              'LABEL WIDTH IS GREATER THAN MAXDGT')
      ELSE
        IF(NDGT.LE.MAXDGT) THEN
          UFACT = 1.
        ELSE
          NDGT = NDMAXS - NDD + 1
          LDGT = 0
          UFACT = 1.D1**NDD
        ENDIF
      ENDIF

      RETURN
      END
