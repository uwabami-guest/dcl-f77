*-----------------------------------------------------------------------
*     POLYMARKER PRIMITIVE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UUMRK(N,UPX,UPY)

      REAL      UPX(*),UPY(*)


      CALL UUQMKT(ITYPE)
      CALL UUQMKI(INDEX)
      CALL UUQMKS(RSIZE)

      CALL UUMRKZ(N,UPX,UPY,ITYPE,INDEX,RSIZE)

      END
