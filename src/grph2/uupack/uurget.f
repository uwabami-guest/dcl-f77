*-----------------------------------------------------------------------
*     UURGET / UURSET / UURSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UURGET(CP, RPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40


      CALL UURQID(CP, IDX)
      CALL UURQVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UURSET(CP, RPARA)

      CALL UURQID(CP, IDX)
      CALL UURSVL(IDX, RPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UURSTX(CP, RPARA)

      RP = RPARA
      CALL UURQID(CP, IDX)

*     / SHORT NAME /

      CALL UURQCP(IDX, CX)
      CALL RTRGET('UU', CX, RP, 1)

*     / LONG NAME /

      CALL UURQCL(IDX, CL)
      CALL RLRGET(CL, RP, 1)

      CALL UURSVL(IDX,RP)

      RETURN
      END
