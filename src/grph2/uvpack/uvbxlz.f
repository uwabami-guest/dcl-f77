*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UVBXLZ(N,UPX,UPY,ITYPE,INDEX)

      REAL      UPX(*),UPY(*)

      LOGICAL   LFLAG, LMISS, LXUNI
      CHARACTER COBJ*80

      COMMON    /SZBLS2/ LCLIP
      LOGICAL   LCLIP


      IF (N.LT.2) THEN
        CALL MSGDMP('E','UVBXLZ','NUMBER OF POINTS IS LESS THAN 2.')
      END IF
      IF (ITYPE.EQ.0) THEN
        CALL MSGDMP('M','UVBXLZ','LINETYPE IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.EQ.0) THEN
        CALL MSGDMP('M','UVBXLZ','LINE INDEX IS 0 / DO NOTHING.')
        RETURN
      END IF
      IF (INDEX.LT.0) THEN
        CALL MSGDMP('E','UVBXLZ','LINE INDEX IS LESS THAN 0.')
      END IF

      CALL SGLGET('LCLIP' , LCLIP )
      CALL GLRGET('RUNDEF', RUNDEF)
      CALL GLRGET('RMISS' , RMISS)
      CALL GLLGET('LMISS' , LMISS)

      IF (UPY(1).EQ.RUNDEF) THEN
        CALL MSGDMP('E', 'UVBXLZ', 'RUNDEF CAN NOT BE UESED FOR UPY.')
      END IF

      WRITE(COBJ,'(2I8)') ITYPE,INDEX
      CALL CDBLK(COBJ)
      CALL SWOOPN('UVBXLZ',COBJ)

      CALL SZSLTI(ITYPE,INDEX)
      CALL SZOPLU

      LXUNI = UPX(1).EQ.RUNDEF

      IF (LXUNI) THEN
        CALL UUQIDV(UXMIN, UXMAX)
        IF (UXMIN.EQ.RUNDEF) CALL SGRGET('UXMIN', UXMIN)
        IF (UXMAX.EQ.RUNDEF) CALL SGRGET('UXMAX', UXMAX)
        DX = (UXMAX-UXMIN)/N
      END IF

      CALL SZOPLU

      LFLAG=.FALSE.
      DO 20 I=1,N
        IF (LXUNI) THEN
          UX1 = UXMIN + DX*(I-1)
          UX2 = UXMIN + DX*I
        ELSE
          UX1 = UPX(I)
          UX2 = UPX(I+1)
        END IF

        IF ((UX1.EQ.RMISS .OR. UX2.EQ.RMISS .OR. UPY(I).EQ.RMISS)
     #    .AND. LMISS) THEN
          LFLAG=.FALSE.
        ELSE
          IF (LFLAG) THEN
            CALL SZPLLU(UX1,UPY(I))
            CALL SZPLLU(UX2,UPY(I))
          ELSE
            CALL SZMVLU(UX1,UPY(I))
            CALL SZPLLU(UX2,UPY(I))
            LFLAG=.TRUE.
          END IF
        END IF
   20 CONTINUE

      CALL SZCLLU
      CALL SWOCLS('UVBXLZ')

      END
