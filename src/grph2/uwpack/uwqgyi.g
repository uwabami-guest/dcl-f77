*-----------------------------------------------------------------------
*     UWQGYI / UWIGYI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UWQGYI(UY,IUY,FRAC)

      LOGICAL   LASCND

      COMMON    /UWBLKY/ LEQDYZ,NYZ,UYMINZ,UYMAXZ,DYZ,YPZ
      PARAMETER (NW=@MAXNGRID)
      LOGICAL   LEQDYZ
      REAL      YPZ(NW)

      SAVE

      DATA      IY /1/


      IF (.NOT.(UYMINA.LE.UY .AND. UY.LE.UYMAXA)) THEN
        IUY = IUNDEF
        FRAC = 0
        RETURN
      END IF

      IF (LEQDYZ) THEN
        YNORM = (UY-UYMINZ)/DYZ
        IUY = MIN(INT(YNORM)+1, NYZ-1)
        FRAC = YNORM-IUY+1
      ELSE
        IF (LASCND) THEN
          IF (UY.GT.YPZ(IY)) THEN
            DO 10 I=IY, NYZ-2
              IF (UY.LE.YPZ(I+1)) GO TO 30
   10       CONTINUE
          ELSE
            DO 20 I=IY, 2, -1
              IF (UY.GT.YPZ(I)) GO TO 30
   20       CONTINUE
          END IF
   30     CONTINUE
        ELSE
          IF (UY.GT.YPZ(IY)) THEN
            DO 110 I=IY-1, 1, -1
              IF (UY.LE.YPZ(I)) GO TO 130
  110       CONTINUE
          ELSE
            DO 120 I=IY, NYZ-2
              IF (UY.GT.YPZ(I+1)) GO TO 130
  120       CONTINUE
          END IF
  130     CONTINUE
        END IF
        IY = I
        IUY = I
        FRAC = (UY-YPZ(I))/(YPZ(I+1)-YPZ(I))
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UWIGYI

      IY = 1

      CALL GLIGET('IUNDEF', IUNDEF)

      IF (.NOT.LEQDYZ) THEN
        LASCND = YPZ(NYZ).GT.YPZ(1)
      END IF

      UYMAXA = MAX(UYMINZ, UYMAXZ)
      UYMINA = MIN(UYMINZ, UYMAXZ)

      RETURN
      END
