*-----------------------------------------------------------------------
*     UWSGXA / UWSGXB
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UWSGXA(XP,NX)

      REAL      XP(*)
      LOGICAL   LSETX

      LOGICAL   LSETXZ

      COMMON    /UWBLKX/ LEQDXZ,NXZ,UXMINZ,UXMAXZ,DXZ,XPZ
      PARAMETER (NW=@MAXNGRID)
      LOGICAL   LEQDXZ
      REAL      XPZ(NW)

      SAVE

      DATA      LSETXZ/.FALSE./


      IF (NX.LT.2) THEN
        CALL MSGDMP('E','UWSGXA','NUMBER OF POINTS IS INVALID.')
      END IF
      IF (NX.GT.NW) THEN
        CALL MSGDMP('E','UWSGXA','WORKING AREA IS NOT ENOUGH.')
      END IF

      LEQDXZ=.FALSE.
      NXZ=NX
      CALL VRSET(XP,XPZ,NX,1,1)
      UXMINZ=XP(1)
      UXMAXZ=XP(NX)
      LSETXZ=.TRUE.

      CALL UWIGXI

      RETURN
*-----------------------------------------------------------------------
      ENTRY UWQGXA(XP,NX)

      NX=NXZ
      CALL VRSET(XPZ,XP,NX,1,1)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UWSGXB(UXMIN,UXMAX,NX)

      IF (UXMIN.EQ.UXMAX) THEN
        CALL MSGDMP('E','UWSGXB','UXMIN = UXMAX.')
      END IF

      LEQDXZ=.TRUE.
      NXZ=NX
      UXMINZ=UXMIN
      UXMAXZ=UXMAX
      DXZ=(UXMAX-UXMIN)/(NX-1)
      LSETXZ=.TRUE.

      CALL UWIGXI

      RETURN
*-----------------------------------------------------------------------
      ENTRY UWQGXB(UXMIN,UXMAX,NX)

      NX=NXZ
      UXMIN=UXMINZ
      UXMAX=UXMAXZ

      RETURN
*-----------------------------------------------------------------------
      ENTRY UWSGXZ(LSETX)

      LSETXZ=LSETX

      RETURN
*-----------------------------------------------------------------------
      ENTRY UWQGXZ(LSETX)

      LSETX=LSETXZ

      RETURN
      END
