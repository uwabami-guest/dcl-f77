*-----------------------------------------------------------------------
*     LUXCHK : CHECK SIDE PARAMETER
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LUXCHK(CSIDE)

      CHARACTER CSIDE*(*)

      CHARACTER CSX*1


      CSX=CSIDE(1:1)
      CALL CUPPER(CSX)
      LUXCHK=CSX.EQ.'B' .OR. CSX.EQ.'T' .OR. CSX.EQ.'U'

      END
