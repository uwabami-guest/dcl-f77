*-----------------------------------------------------------------------
*     UXPNUM : PLOT NUMBERS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UXPNUM(CSIDE,ISLCT,UX,N)

      PARAMETER (NCH=40,LCH=12)

      REAL      UX(*)
      CHARACTER CSIDE*1

      LOGICAL   LUXCHK
      CHARACTER CFMTZ*16,CH(NCH)*(LCH)


*     / CHECK ARGUMENTS /

      IF (.NOT.(LUXCHK(CSIDE))) THEN
        CALL MSGDMP('E','UXPNUM','SIDE PARAMETER IS INVALID.')
      END IF
      IF (.NOT.(0.LE.ISLCT .AND. ISLCT.LE.2)) THEN
        CALL MSGDMP('E','UXPNUM','''ISLCT'' IS INVALID.')
      END IF
      IF (N.LE.0) THEN
        CALL MSGDMP('E','UXPNUM','NUMBER OF POINTS IS INVALID.')
      ELSE IF (N.GT.NCH) THEN
        CALL MSGDMP('E','UXPNUM','WORKING AREA IS NOT ENOUGH.')
      END IF

*     / GENERATE CHARACTERS /

      CALL UZCGET('CXFMT', CFMTZ)
      DO 10 I=1,N
        CALL CHVAL(CFMTZ,UX(I),CH(I))
   10 CONTINUE

*     / UXPLBL CALL /

      CALL UXPLBL(CSIDE,ISLCT,UX,CH,LCH,N)

      END
