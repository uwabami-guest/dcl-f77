*-----------------------------------------------------------------------
*     BASIC ROUTINES
*-----------------------------------------------------------------------
*     UXPTMZ : PLOT TICKMARKS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UXPTMZ(UX,N,UPY,ROFFX,RTICK,INDEX)

      REAL      UX(*)

      LOGICAL   LCLIPZ


      IF (N.LE.0) THEN
        CALL MSGDMP('E','UXPTMZ','NUMBER OF POINTS IS INVALID.')
      END IF
      IF (INDEX.LE.0) THEN
        CALL MSGDMP('E','UXPTMZ','LINE INDEX IS INVALID.')
      END IF

      CALL SGLGET('LCLIP',LCLIPZ)
      CALL SGLSET('LCLIP',.FALSE.)

      CALL SZLNOP(INDEX)
      DO 10 I=1,N
        CALL STFTRF(UX(I),UPY,VPX,VPY)
        VPY=VPY+ROFFX
        CALL SZLNZV(VPX,VPY,VPX,VPY+RTICK)
   10 CONTINUE
      CALL SZLNCL

      CALL SGLSET('LCLIP',LCLIPZ)

      END
