*-----------------------------------------------------------------------
*     UXPTTL : PLOT TITLE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UXPTTL(CSIDE,ISLCT,CTTL,PX)

      CHARACTER CSIDE*1,CTTL*(*)

      REAL      UX(2)
      LOGICAL   LBOUND,LBMSG,LUXCHK,LCHREQ
      CHARACTER CSLCT*1


      IF (.NOT.(LUXCHK(CSIDE))) THEN
        CALL MSGDMP('E','UXPTTL','SIDE PARAMETER IS INVALID.')
      END IF
      IF (.NOT.(0.LE.ISLCT .AND. ISLCT.LE.2)) THEN
        CALL MSGDMP('E','UXPTTL','''ISLCT'' IS INVALID.')
      END IF
      NCTL=LEN(CTTL)
      IF (.NOT.(0.LT.NCTL)) THEN
        CALL MSGDMP('M','UXPTTL','CHARACTER LENGTH IS ZERO.')
        RETURN
      END IF

      WRITE(CSLCT,'(I1)') ISLCT

      CALL UZRGET('ROFFX'//CSIDE,ROFFX)
      CALL UZRGET('RSIZEC'//CSLCT,RSIZE)
      CALL UZIGET('IROTCX'//CSIDE,IROTA)
      CALL UZIGET('INDEXL'//CSLCT,INDEX)
      CALL UZRGET('PAD1',PAD)
      CALL UZLGET('LBOUND',LBOUND)
      CALL UZLGET('LBMSG',LBMSG)
      ICENT=0

      CALL SGQWND(UXMN,UXMX,UYMN,UYMX)
      IF (.NOT.LCHREQ(CSIDE,'U')) THEN
        IF (LCHREQ(CSIDE,'B')) THEN
          POSY=UYMN
          IFLAG=-1
        ELSE
          POSY=UYMX
          IFLAG=+1
        END IF
      ELSE
        CALL UZRGET('UYUSER',POSY)
        CALL UZIGET('IFLAG',IFLAG)
        IFLAG=SIGN(1,IFLAG)
      END IF

      UX(1)=UXMN
      UX(2)=UXMX

      JROTA=MOD(IROTA+2,4)-2
      IF (JROTA.EQ.-2) THEN
        JROTA=0
      END IF

      NC=LENC(CTTL)
      CALL SZQTXW(CTTL,LCW,WXCH,WYCH)
      IF (JROTA.EQ.0) THEN
        RLC=WYCH
      ELSE
        RLC=WXCH
      END IF

      IC=JROTA*ICENT*IFLAG
      ROFFZ=ROFFX+RSIZE*(PAD+RLC*(1+IC)*0.5)*IFLAG
      ROFFX=ROFFX+RSIZE*(PAD+RLC)*IFLAG

      CALL UXPLBB(UX,CTTL,NC,2,POSY,ROFFZ,RSIZE,IROTA,ICENT,INDEX,
     +            PX,LBOUND,LBMSG)

      CALL UZRSET('ROFFX'//CSIDE,ROFFX)

      END
