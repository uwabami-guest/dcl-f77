*-----------------------------------------------------------------------
*     CONTROL ROUTINES
*-----------------------------------------------------------------------
*     UXSAXZ : OFFSET FOR AXIS
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UXSAXZ(CSIDE,ROFFX)

      CHARACTER CSIDE*1

      LOGICAL   LUXCHK


      IF (.NOT.LUXCHK(CSIDE)) THEN
        CALL MSGDMP('E','UXSAXZ','SIDE PARAMETER IS INVALID.')
      END IF

      CALL UZRSET('ROFFX'//CSIDE,ROFFX)
      CALL UZRSET('ROFGX'//CSIDE,ROFFX)

      END
