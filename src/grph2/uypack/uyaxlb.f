*-----------------------------------------------------------------------
*     HIGHER LEVEL APPLICATIONS
*-----------------------------------------------------------------------
*     UYAXLB : PLOT Y-AXIS (SPECIFY LABELS)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UYAXLB(CSIDE,UY1,N1,UY2,CH,NC,N2)

      REAL      UY1(*),UY2(*)
      CHARACTER CSIDE*1,CH(*)*(*)

      LOGICAL   LABEL,LUYCHK


      IF (.NOT.(LUYCHK(CSIDE))) THEN
        CALL MSGDMP('E','UYAXLB','SIDE PARAMETER IS INVALID.')
      END IF

      CALL UYSOFF

      CALL UYPAXS(CSIDE,2)

      IF (N1.GE.1) THEN
        CALL UYPTMK(CSIDE,1,UY1,N1)
      END IF

      IF (N2.GE.1) THEN
        CALL UYPTMK(CSIDE,2,UY2,N2)
        CALL UZLGET('LABELY'//CSIDE,LABEL)
        IF (LABEL) THEN
          CALL UYPLBL(CSIDE,1,UY2,CH,NC,N2)
        END IF
      END IF

      CALL UYROFF

      END
