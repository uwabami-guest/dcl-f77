*-----------------------------------------------------------------------
*     UYMTTL : PLOT MAIN (LARGE) TITLE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UYMTTL(CSIDE,CTTL,PX)

      CHARACTER CSIDE*1,CTTL*(*)

      LOGICAL   LUYCHK


      IF (.NOT.(LUYCHK(CSIDE))) THEN
        CALL MSGDMP('E','UYMTTL','SIDE PARAMETER IS INVALID.')
      END IF

      CALL UYPTTL(CSIDE,2,CTTL,PX)

      END
