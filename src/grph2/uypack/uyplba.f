*-----------------------------------------------------------------------
*     UYPLBA : PLOT LABELS ( AT THE POINTS )
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UYPLBA(UY,CH,NC,N,UPX,ROFFY,RSIZE,IROTA,ICENT,INDEX)

      REAL      UY(*)
      CHARACTER CH(*)*(*)

      LOGICAL   LCLIPZ


      IF (NC.LE.0) THEN
        CALL MSGDMP('E','UYPLBA',
     +       'CHARACTER LENGTH IS LESS THAN OR EQUAL TO ZERO.')
      END IF
      IF (N.LE.0) THEN
        CALL MSGDMP('E','UYPLBA','NUMBER OF POINTS IS INVALID.')
      END IF
      IF (RSIZE.LE.0) THEN
        CALL MSGDMP('E','UYPLBA','TEXT HEIGHT IS LESS THAN ZERO.')
      END IF
      IF (.NOT.(-1.LE.ICENT .AND. ICENT.LE.1)) THEN
        CALL MSGDMP('E','UYPLBA','CENTERING OPTION IS INVALID.')
      END IF
      IF (INDEX.LE.0) THEN
        CALL MSGDMP('E','UYPLBA','TEXT INDEX IS INVALID.')
      END IF

      CALL SGLGET('LCLIP',LCLIPZ)
      CALL SGLSET('LCLIP',.FALSE.)

      CALL SZTXOP(RSIZE,IROTA*90,ICENT,INDEX)
      DO 10 I=1,N
        LC = LENC(CH(I))
        CALL STFTRF(UPX,UY(I),VPX,VPY)
        VPX=VPX+ROFFY
        CALL SZTXZV(VPX,VPY,CH(I)(1:LC))
   10 CONTINUE
      CALL SZTXCL

      CALL SGLSET('LCLIP',LCLIPZ)

      END
