*-----------------------------------------------------------------------
*     UYPTTL : PLOT TITLE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UYPTTL(CSIDE,ISLCT,CTTL,PX)

      CHARACTER CSIDE*1,CTTL*(*)

      REAL      UY(2)
      LOGICAL   LBOUND,LBMSG,LUYCHK,LCHREQ
      CHARACTER CSLCT*1


      IF (.NOT.(LUYCHK(CSIDE))) THEN
        CALL MSGDMP('E','UYPTTL','SIDE PARAMETER IS INVALID.')
      END IF
      IF (.NOT.(0.LE.ISLCT .AND. ISLCT.LE.2)) THEN
        CALL MSGDMP('E','UYPTTL','''ISLCT'' IS INVALID.')
      END IF
      NCTL=LEN(CTTL)
      IF (.NOT.(0.LT.NCTL)) THEN
        CALL MSGDMP('M','UYPTTL','CHARACTER LENGTH IS ZERO.')
        RETURN
      END IF

      WRITE(CSLCT,'(I1)') ISLCT

      CALL UZRGET('ROFFY'//CSIDE,ROFFY)
      CALL UZRGET('RSIZEC'//CSLCT,RSIZE)
      CALL UZIGET('IROTCY'//CSIDE,IROTA)
      CALL UZIGET('INDEXL'//CSLCT,INDEX)
      CALL UZRGET('PAD1',PAD)
      CALL UZLGET('LBOUND',LBOUND)
      CALL UZLGET('LBMSG',LBMSG)
      ICENT=0

      CALL SGQWND(UXMN,UXMX,UYMN,UYMX)
      IF (.NOT.LCHREQ(CSIDE,'U')) THEN
        IF (LCHREQ(CSIDE,'L')) THEN
          POSX=UXMN
          IFLAG=-1
        ELSE
          POSX=UXMX
          IFLAG=+1
        END IF
      ELSE
        CALL UZRGET('UXUSER',POSX)
        CALL UZIGET('IFLAG',IFLAG)
        IFLAG=SIGN(1,IFLAG)
      END IF

      UY(1)=UYMN
      UY(2)=UYMX

      JROTA=MOD(IROTA+3,4)-2
      IF (JROTA.EQ.-2) THEN
        JROTA=0
      END IF

      NC=LENC(CTTL)
      CALL SZQTXW(CTTL,LCW,WXCH,WYCH)
      IF (JROTA.EQ.0) THEN
        RLC=WYCH
      ELSE
        RLC=WXCH
      END IF

      IC=JROTA*ICENT*IFLAG
      ROFFZ=ROFFY+RSIZE*(PAD+RLC*(1+IC)*0.5)*IFLAG
      ROFFY=ROFFY+RSIZE*(PAD+RLC)*IFLAG

      CALL UYPLBB(UY,CTTL,NC,2,POSX,ROFFZ,RSIZE,IROTA,ICENT,INDEX,
     +            PX,LBOUND,LBMSG)

      CALL UZRSET('ROFFY'//CSIDE,ROFFY)

      END
