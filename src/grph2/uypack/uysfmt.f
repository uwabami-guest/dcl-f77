*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UYSFMT(CFMT)

      CHARACTER CFMT*(*)

      CHARACTER CFMTZ*16


      CALL UZCGET('CYFMT', CFMTZ)
      NC=LENC(CFMT)
      IF (NC.GE.2 .AND. CFMT(1:1).EQ.'+') THEN
        IF (LENC(CFMTZ).EQ.1 .OR. CFMTZ(2:2).EQ.'+') THEN
          CFMTZ(2:NC+1)=CFMT
        ELSE
          CALL MSGDMP('E','UYSFMT','SUB OPTION CANNOT BE USED.')
        END IF
      ELSE
        CFMTZ=CFMT
      END IF

      CALL UZCSET('CYFMT', CFMTZ)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UYQFMT(CFMT)

      CALL UZCGET('CYFMT', CFMT)

      RETURN
      END
