*-----------------------------------------------------------------------
*     UZCQNP
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UZCQNP(NCP)

      CHARACTER CP*(*), CVAL*(*)

      PARAMETER (NPARA = 2)

      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CX(NPARA)*80, CMSG*80

      SAVE

*     / SHORT NAME /

      DATA      CPARAS( 1)/'CXFMT  '/, CX( 1) /'B'/
      DATA      CPARAS( 2)/'CYFMT  '/, CX( 2) /'B'/

*     / LONG NAME /

      DATA      CPARAL( 1) / '****CXFMT  ' /
      DATA      CPARAL( 2) / '****CYFMT  ' /

      DATA      LFIRST /.TRUE./


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZCQID(CP, IDX)

      DO 10 N=1,NPARA
        IF (LCHREQ(CP,CPARAS(N)) .OR. LCHREQ(CP,CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE

      CMSG = 'PARAMETER "'//CP(1:LENC(CP))//'" IS NOT DEFINED.'
      CALL MSGDMP('E','UZCQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZCQCP(IDX,CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','UZCQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZCQCL(IDX,CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','UZCQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZCQVL(IDX,CVAL)

      IF(LFIRST) THEN
        CALL RTCGET('UZ', CPARAS, CX, NPARA)
        CALL RLCGET(CPARAL, CX, NPARA)
        LFIRST = .FALSE.
      ENDIF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CVAL = CX(IDX)
      ELSE
        CALL MSGDMP('E','UZCQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZCSVL(IDX,CVAL)

      IF(LFIRST) THEN
        CALL RTCGET('UZ', CPARAS, CX, NPARA)
        CALL RLCGET(CPARAL, CX, NPARA)
        LFIRST = .FALSE.
      ENDIF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CX(IDX) = CVAL
      ELSE
        CALL MSGDMP('E','UZCSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZCQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZCSAV(IU)

      WRITE(UNIT=IU,IOSTAT=IOS) CX
      IF (IOS.NE.0) THEN
        CALL MSGDMP('E','UZCSAV','IOSTAT IS NOT ZERO.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZCRST(IU)

      READ(UNIT=IU,IOSTAT=IOS) CX
      IF (IOS.NE.0) THEN
        CALL MSGDMP('E','UZCRST','IOSTAT IS NOT ZERO.')
      END IF

      RETURN
      END
