*-----------------------------------------------------------------------
*     UZIGET / UZISET / UZISTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE UZIGET(CP, IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*8
      CHARACTER CL*40


      CALL UZIQID(CP, IDX)
      CALL UZIQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZISET(CP, IPARA)

      CALL UZIQID(CP, IDX)
      CALL UZISVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY UZISTX(CP, IPARA)

      IP = IPARA
      CALL UZIQID(CP, IDX)

*     / SHORT NAME /

      CALL UZIQCP(IDX, CX)
      CALL RTIGET('UZ', CX, IP, 1)

*     / LONG NAME /

      CALL UZIQCL(IDX, CL)
      CALL RLIGET(CL, IP, 1)

      CALL UZISVL(IDX,IP)

      RETURN
      END
