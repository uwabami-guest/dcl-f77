*-----------------------------------------------------------------------
*     COORDINATE TRANSFORMATION (2D CARTESIAN -> HYPERBOLIC)
*                                              93/02/18   S.SAKAI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE CT2CH(X, Y, U, V)


      U = X*X - Y*Y
      V = 2*X*Y

      END
