*-----------------------------------------------------------------------
*     COORDINATE TRANSFORMATION (3D SPHERICAL -> CARTESIAN )
*                                              93/02/18   S.SAKAI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE CT3SC(R, THETA, PHI, X, Y, Z)


      X = R*SIN(THETA)*COS(PHI)
      Y = R*SIN(THETA)*SIN(PHI)
      Z = R*COS(THETA)

      END
