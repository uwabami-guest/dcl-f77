*-----------------------------------------------------------------------
*     IMOD
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION IMOD(IX,ID)


      IF (ID.LE.0) THEN
        CALL MSGDMP('E','IMOD  ','ID .LE. 0.')
      END IF

      IMOD=MOD(MOD(IX,ID)+ID,ID)

      END
