*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RR2D(X)

      EXTERNAL  RFPI


      RR2D=X*180/RFPI()

      END
