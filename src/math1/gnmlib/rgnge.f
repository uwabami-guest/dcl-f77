*-----------------------------------------------------------------------
*     RGNGE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RGNGE(RX)

      EXTERNAL  REXP


      CALL GNGE(RX,BX,IP)
      RGNGE=REXP(BX,10,IP)

      END
