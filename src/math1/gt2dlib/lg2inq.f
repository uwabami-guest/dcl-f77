*-----------------------------------------------------------------------
*     LG2INQ
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
*     LG2INQ  whether included in a convex quadrilateral
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LG2INQ(CX,CY,
     &                         CX00,CX10,CX01,CX11,CY00,CY10,CY01,CY11)

      EXTERNAL LRLE1, LRGE1, LREQ1
      LOGICAL  LRLE1, LRGE1, LREQ1
      A = CX10-CX00
      B = CX01-CX00
      C = CY10-CY00
      D = CY01-CY00
      DET = A*D - B*C
      IF (LREQ1(DET,0.0)) THEN
        LG2INQ = .FALSE.
        RETURN
      ELSE
        S = ( D*(CX-CX00)  - B*(CY-CY00) )/DET
        T = ( -C*(CX-CX00) + A*(CY-CY00) )/DET
        IF (LRGE1(S,0.0) .AND. LRGE1(T,0.0) .AND. LRLE1(S+T,1.0) ) THEN
          LG2INQ = .TRUE.
          RETURN
        ENDIF
      ENDIF
        
      A = CX10-CX11
      B = CX01-CX11
      C = CY10-CY11
      D = CY01-CY11
      DET = A*D - B*C
      IF (LREQ1(DET,0.0)) THEN
        LG2INQ = .FALSE.
        RETURN
      ELSE
        S = ( D*(CX-CX11)  - B*(CY-CY11) )/DET
        T = ( -C*(CX-CX11) + A*(CY-CY11) )/DET
        IF (LRGE1(S,0.0) .AND. LRGE1(T,0.0) .AND. LRLE1(S+T,1.0) ) THEN
          LG2INQ = .TRUE.
          RETURN
        ENDIF
      ENDIF

      LG2INQ = .FALSE.

      END
