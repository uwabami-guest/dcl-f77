*-----------------------------------------------------------------------
*     INDXMF
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION INDXMF(CX,N,JD,CH)

      CHARACTER CX*(*),CH*(*)

      LOGICAL   LCHREQ


      NC=LEN(CH)
      INDXMF=0
      DO 10 I=1,N
        J1=JD*(I-1)+1
        J2=JD*(I-1)+NC
        IF (LCHREQ(CX(J1:J2),CH)) THEN
          INDXMF=I
          RETURN
        END IF
   10 CONTINUE

      END
