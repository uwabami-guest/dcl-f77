*-----------------------------------------------------------------------
*     INDXML
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION INDXML(CX,N,JD,CH)

      CHARACTER CX*(*),CH*(*)

      LOGICAL   LCHREQ


      NC=LEN(CH)
      INDXML=0
      DO 10 I=1,N
        J1=JD*(I-1)+1
        J2=JD*(I-1)+NC
        IF (LCHREQ(CX(J1:J2),CH)) THEN
          INDXML=I
        END IF
   10 CONTINUE

      END
