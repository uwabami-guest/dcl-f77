*-----------------------------------------------------------------------
*     INDXRF
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION INDXRF(RX,N,JD,RR)

      REAL      RX(*)


      INDXRF=0
      DO 10 I=1,N
        J=JD*(I-1)+1
        IF (RX(J).EQ.RR) THEN
          INDXRF=I
          RETURN
        END IF
   10 CONTINUE

      END
