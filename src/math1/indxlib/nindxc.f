*-----------------------------------------------------------------------
*     NINDXC
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION NINDXC(CX,N,JD,CH)

      CHARACTER CX(*)*1,CH*1


      NINDXC=0
      DO 10 I=1,N
        J=JD*(I-1)+1
        IF (CX(J).EQ.CH) THEN
          NINDXC=NINDXC+1
        END IF
   10 CONTINUE

      END
