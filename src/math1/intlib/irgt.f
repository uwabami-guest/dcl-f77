*-----------------------------------------------------------------------
*     IRGT
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      INTEGER FUNCTION IRGT(RX)

      LOGICAL   LREQ

      EXTERNAL  LREQ


      NX=NINT(RX)
      IF (LREQ(RX,REAL(NX))) THEN
        IRGT=NX+1
      ELSE
        IRGT=INT(RX)+INT(RX-INT(RX)+1)
      END IF

      END
