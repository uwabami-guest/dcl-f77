*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LRGT1(X,Y)

      LOGICAL   LRNE1

      EXTERNAL  LRNE1


      LRGT1=X.GT.Y .AND. LRNE1(X,Y)

      END
