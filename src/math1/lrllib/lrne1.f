*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LRNE1(X,Y)

      LOGICAL   LFRST

      SAVE

      DATA      LFRST/.TRUE./


      IF (LFRST) THEN
        CALL GLRGET('REPSL',REPSL)
        LFRST=.FALSE.
      END IF

      CALL GLRGET('RFACT',RFACT)
      EP=REPSL*RFACT

      LRNE1=ABS(X-Y).GT.(ABS(X)+ABS(Y))*EP/2

      END
