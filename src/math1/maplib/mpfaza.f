*-----------------------------------------------------------------------
*     MAP PROJECTION (LANBERT AZIMUTHAL EQUAL-AREA)    93/02/20 S.SAKAI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MPFAZA(XLON, YLAT, X, Y)

      EXTERNAL  RFPI


      PI = RFPI()
      TH = PI/2 - YLAT
      IF (TH .GT. PI) TH = PI
      IF (TH .LT. 0.) TH = 0

      R =  2*SIN(TH/2)
      X =  R*SIN(XLON)
      Y = -R*COS(XLON)

      RETURN
*-----------------------------------------------------------------------
      ENTRY MPIAZA(X, Y, XLON, YLAT)

      PI = RFPI()
      R = SQRT(X*X + Y*Y)

      IF (R .EQ. 0.) THEN
        XLON = 0.
        YLAT = PI/2.
        RETURN
      ELSE IF (R .LE. 2) THEN
        XLON = ATAN2(X, -Y)
        YLAT = PI/2 - 2*ASIN(R/2)
        RETURN
      END IF

      CALL GLRGET('RUNDEF',RNA)
      XLON = RNA
      YLAT = RNA

      END
