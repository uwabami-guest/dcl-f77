*-----------------------------------------------------------------------
*     MAP PROJECTION (BONNE)                           93/02/20 S.SAKAI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MPFBON(XLON, YLAT, X, Y)

      EXTERNAL  RFPI, XMPLON

      SAVE


      TH  = PI/2 - S*YLAT
      R   = TH + CC

      IF (R .EQ. 0) THEN
        X = 0.
        Y = 0.
      ELSE
        DLM = XMPLON(XLON)*SIN(TH)/R
        X =    R*SIN(DLM)
        Y = -S*R*COS(DLM)
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY MPIBON(X, Y, XLON, YLAT)

      R    = SQRT(X*X + Y*Y)
      TH   = R - CC
      YLAT = S*(PI/2 - TH)

      IF (R .EQ. 0.) THEN
        XLON = 0.
        RETURN
      ELSE IF (ABS(YLAT) .LT. PI/2) THEN
        XLON = ATAN2(X, -S*Y)/SIN(TH)*R
        IF (ABS(XLON) .LE. PI) RETURN
      ELSE IF (ABS(YLAT) .EQ. PI/2) THEN
        XLON = 0
        RETURN
      END IF
      XLON = RNA
      YLAT = RNA

      RETURN
*-----------------------------------------------------------------------
      ENTRY MPSBON(YLAT0)

      PI = RFPI()
      CALL GLRGET('RUNDEF',RNA)

      S  = SIGN(1., YLAT0)
      TH = PI/2 - ABS(YLAT0)
      CC = TAN(TH) - TH

      END
