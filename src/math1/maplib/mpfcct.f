*-----------------------------------------------------------------------
*     MAP PROJECTION (CENTRAL CONICAL)              2008-01-28 E. TOYODA
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MPSCCT(YLAT0)
      REAL N
      PARAMETER (EPSL = 1.0E-5)
      PARAMETER (PI = ATAN(1.0) * 4.0)
      EXTERNAL  XMPLON

      DATA PHI0 / 0.7854 /
      DATA R0   / 1.0 /
      DATA N    / 0.7071 /
      DATA S    / 1.0 /
      SAVE PHI0, R0, N, S
      PHI0 = ABS(YLAT0)
      R0 = TAN(PI * 0.5 - PHI0)
      N = SIN(PHI0)
      S = SIGN(1., YLAT0)
      RETURN

      ENTRY MPFCCT(XLON, YLAT, X, Y)

      XX = XMPLON(XLON)
      R = R0 + TAN(PHI0 - S * YLAT)
      X = R * SIN(N * XX)
      Y = -S * R * COS(N * XX)

      RETURN
*-----------------------------------------------------------------------
      ENTRY MPICCT(X, Y, XLON, YLAT)

      R = SQRT(X * X + Y * Y)
      IF (R .EQ. 0) THEN
        XLON = 0.0
      ELSE
        XLON = ATAN2(X, -S*Y) / N
      ENDIF
      YLAT = S * (PHI0 - ATAN(R - R0))

      CALL GLRGET('RUNDEF',RNA)
      XLON = RNA
      YLAT = RNA

      END
