*-----------------------------------------------------------------------
*     MAP PROJECTION (EQUAL-AREA CYLINDRICAL)       2007-10-14 E. TOYODA
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MPFCYA(XLON, YLAT, X, Y)

      EXTERNAL  RFPI, XMPLON

        PI = RFPI()
        X = XMPLON(XLON)
        Y = SIN(YLAT)
        Y = MAX(-1.0, MIN(1.0, Y))

      RETURN
*-----------------------------------------------------------------------
      ENTRY MPICYA(X, Y, XLON, YLAT)

        PI = RFPI()
        XLON = X
        YLAT = ASIN(Y)
        IF (ABS(XLON) .LE. PI .AND. ABS(YLAT) .LE. PI/2) RETURN

        CALL GLRGET('RUNDEF',RNA)
        XLON = RNA
        YLAT = RNA

      END
