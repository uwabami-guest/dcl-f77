*-----------------------------------------------------------------------
*     MAP PROJECTION (CENTRAL CYLINDRICAL)          2007-10-14 E. TOYODA
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MPFCYC(XLON, YLAT, X, Y)

      PARAMETER (EPSL = 1.0E-5)
      EXTERNAL  RFPI, XMPLON

      PI = RFPI()
      X = XMPLON(XLON)
      YY = MAX(-PI * 0.5 + EPSL, MIN(PI * 0.5 - EPSL, YLAT))
      Y = TAN(YY)

      RETURN
*-----------------------------------------------------------------------
      ENTRY MPICYC(X, Y, XLON, YLAT)

      PI = RFPI()
      XLON = X
      YLAT = ATAN(Y)
      IF (ABS(XLON) .LE. PI .AND. ABS(YLAT) .LE. PI/2) RETURN

      CALL GLRGET('RUNDEF',RNA)
      XLON = RNA
      YLAT = RNA

      END
