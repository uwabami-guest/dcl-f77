*-----------------------------------------------------------------------
*     MAP PROJECTION (MILLER CYLINDRICAL)          2007-10-14 E. TOYODA
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MPFMIL(XLON, YLAT, X, Y)

      EXTERNAL  RFPI, XMPLON

      PI = RFPI()
      X = XMPLON(XLON)
      YY = MAX(-PI * 0.5, MIN(PI * 0.5, YLAT))
      Y = 1.25 * LOG(TAN(PI * 0.25 + YY*.4))

      RETURN
*-----------------------------------------------------------------------
      ENTRY MPIMIL(X, Y, XLON, YLAT)

      PI = RFPI()
      XLON = X
      YLAT = ATAN(SINH(Y * 0.8)) * 1.25
      IF (ABS(XLON) .LE. PI .AND. ABS(YLAT) .LE. PI/2) RETURN

      CALL GLRGET('RUNDEF',RNA)
      XLON = RNA
      YLAT = RNA

      END
