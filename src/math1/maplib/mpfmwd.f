*-----------------------------------------------------------------------
*     MAP PROJECTION (MOLLWEIDE)                       93/02/20 S.SAKAI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MPFMWD(XLON, YLAT, X, Y)

      PARAMETER (A = 1.4142136)

      EXTERNAL  RFPI, XMPLON, MPZMWD


      PI = RFPI()
      ALPHA = YLAT
      CALL MPZNWT(MPZMWD, YLAT, ALPHA)

      X = 2*A*XMPLON(XLON)*COS(ALPHA)/PI
      Y = A*SIN(ALPHA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY MPIMWD(X, Y, XLON, YLAT)

      PI = RFPI()
      IF (ABS(Y) .LT. A) THEN
        ALPHA = ASIN(Y/A)
        XLON  = X/A/COS(ALPHA)*PI/2
        IF (ABS(XLON) .LE. PI) THEN
          YLAT  = ASIN((2*ALPHA+SIN(2*ALPHA))/PI)
          RETURN
        END IF
      ELSE IF (ABS(Y) .EQ. A .AND. X .EQ. 0) THEN
        XLON = 0.
        YLAT = Y/A/2*PI
        RETURN
      END IF

      CALL GLRGET('RUNDEF',RNA)
      XLON = RNA
      YLAT = RNA

      END
