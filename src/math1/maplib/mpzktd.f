*-----------------------------------------------------------------------
*     FUNCTION OF LATITUDE (KITADA)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE MPZKTD(PHI, ALPHA, F, DF)

      PARAMETER (SQ3 = 1.7320508)

      EXTERNAL  RFPI


      PI = RFPI()
      B  = 2*PI/3. + SQ3/2

      F  = 2*ALPHA +   SIN(2*ALPHA) - B*SIN(PHI)
      DF = 2       + 2*COS(2*ALPHA)

      END
