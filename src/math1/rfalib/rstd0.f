*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RSTD0(RX,N,JX)

      REAL      RX(*)

      EXTERNAL  RAVE0


      AVE=RAVE0(RX,N,JX)
      SUM=0
      DO 10 I=1,JX*(N-1)+1,JX
        SUM=SUM+(RX(I)-AVE)**2
   10 CONTINUE
      RSTD0=SQRT(SUM/N)

      END
