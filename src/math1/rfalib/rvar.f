*-----------------------------------------------------------------------
*     RVAR
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RVAR(RX,N,JX)

      REAL      RX(*)

      LOGICAL   LMISS

      EXTERNAL  RVAR0,RVAR1


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        RVAR=RVAR1(RX,N,JX)
      ELSE
        RVAR=RVAR0(RX,N,JX)
      END IF

      END
