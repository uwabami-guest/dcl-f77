*-----------------------------------------------------------------------
*     DXFLOC
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE DXFLOC(ND,NS,NP,NCP)

      INTEGER   NS(*),NP(*)


      NCP=NP(1)
      NSX=1
      DO 10 N=1,ND-1
        NSX=NSX*NS(N)
        NCP=NCP+(NP(N+1)-1)*NSX
   10 CONTINUE

      END
