
# SYSLIB を移植するときの注意

SYSLIB の中は, システムに依存する定数を管理したり, ファイル操作をおこ
なったりするルーチンが含まれており, 移植の際注意が必要である. ただし, 
ソースレベルでの機種依存性はない. (つまり FORTRAN 77 の仕様の範囲で書
くことができる. )

* GLIQNP/GLRQNP/GLCQNP に関する注意

GLIQNP/GLRQNP はシステムに依存するパラメータを含んでいるので, 処理系に
応じて書きかえてやる必要がある. 特に注意が必要なパラメータは: 

  'INTMAX'  : 1語で表現できる最大の整数
  'REALMAX' : 1語で表現できる最大の実数
  'REALMIN' : 1語で表現できる最小の正の実数
  'REPSL'   : 相対誤差の基準値(実数表現の相対誤差の最大値の10倍)

である. 詳しくは, 「MATH1」マニュアルを参照されたい. これらのパラメー
タの設定は make の際, トップディレクトリにある Mkinclude に定義されて
いる値を参照して gliqnp.g, glrqnp.g のテンプレート部分(@ではじまる)を
置き換えることによっておこなわれ, その結果 gliqnp.f, glrqnp.f, が生成
される. これら以外についてはシステムに応じて適宜変更されたい.

INTMAX などは, ふつう configure によって自動的に設定される. 

また GLCQNP もシステムに依存するパラメータを含んでいるので, 処理系に応
じて書きかえてやる必要がある. 特に注意が必要なパラメータは:

  'DSPATH'  : システムがあらかじめ用意するパス名

である. 詳しくは, 「MATH1」マニュアルを参照されたい. このパラメータの
設定は make の際, トップディレクトリにある Mkinclude に定義されている
DBASEDIR の値を参照して glcqnp.g のテンプレート部分(@ではじまる)を置き
換えることによっておこなわれ, その結果 glcqnp.f が生成される. これら以
外についてはシステムに応じて適宜変更されたい.

* RPNENV/RPNOPT/RPNXFL に関する注意

rpnenv.f, rpnopt.f, rpnxfl.f は実行時オプションをそれぞれ環境変数, コ
マンドライン引数, 外部のファイルから読み込むためのルーチンである. これ
らは, math1 の性格からやや離れたものであるが, 実行時オプションの管理は, 
「電脳ライブラリ」の最下位でなされるべきものであるので, SYSLIB に入れ
てある. rpnxfl.f のルーチン内では, 長さ 80 の文字型変数で1レコードずつ
読むようになっている. メインフレーム系で, 1レコードが 80 より小さい固
定長のファイルを読もうとするとき, この部分で不都合を起こす可能性がある. 
その場合は, レコード長を 80 とするか, 宣言してある文字変数の長さを1レ
コード長以下にしてやる必要がある.

* IUFOPN に関する注意

IUFOPN も本来は math1 の性格からやや離れたものであるが, rtpxfl.f,
rtcxlf.f を実装するために SYSLIB に入れてある. IUFOPN では，装置番号を
1から順に99までを調べて，「存在し」，「オープンされていない」装置番号
を返す．そのチェックを装置 INQUIRE 文によっておこなっているが，装置の
「存在」に関して機種依存性が見られたので，装置の「存在」を示す論理変数
をつねに .TRUE. とすることにしてある．

* CFSRCH に関する注意

このルーチンはパス名の先頭部分と末尾部分のリストの組合せから, 存在する
ファイル名を探して, その名前を返すようなルーチンであり, システムに依存
する. つまり, CFSRCH は UNIX のようなツリー構造を持ったファイルシステ
ムを念頭において作成されており, メインフレーム系のようなファイルシステ
ムにおいてはファイル名検索のルールを別途定義してやる必要がある. しかし, 
現在用意されているルーチンでも, 内部的には単に2種類の文字型リストを組
合せてファイルが存在するかどうかを調べているだけなので, 「先頭部分」, 
「末尾部分」という定義にこだわらずに, ファイル名を構成する要素としてこ
れらを与えてやればそのまま利用することができる. ただし, 「パス名の先頭
部分として空白を指定するとカレントディレクトリを検索する」という仕様の
制約から, 空白を指定しないようにするか, 空白が指定されたときの動作を別
途定義する(たとえば, 何もしない)必要がある.

このディレクトリに用意されているプログラムでは, ファイル名を「先頭部分」
と「末尾部分」の文字列を単に連結することによって生成する. したがって, 
たとえばファイル名 /usr/local/bin/dclfrt について, 「先頭部分」として
は /usr/local/bin/ を指定し, 「末尾部分」としては dclfrt を指定するこ
と(「先頭部分」の最後の / を忘れないように注意されたい). 

* LCHREQ に関する注意

SYSLIB の中には, xxPGET/xxPSET (実際にはその下請けの xxPQID)の引数 CP 
に関して大文字と小文字の区別なく文字列を比較するためのルーチン LCHREQ 
がある. ASCII コードについては最初のプログラムを用いればよい. その他の
コード系については2つめのプログラムを用いればよい. ただしこの場合は 
misc1/chglib の cupper を用いていることに注意すること. また大文字しか
許さないようなシステムにおいては LCHREQ を最後のプログラムのようにして
やればよい.

なお, このパッケージでは ASCII コード系に対応するものを標準としている.

*-----------------------------------------------------------------------
*     LCHREQ FOR ASCII CHARACTER CODE
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LCHREQ(CH1,CH2)

      CHARACTER CH1*(*),CH2*(*)


      LCHREQ=.TRUE.
      LCH1=LEN(CH1)
      LCH2=LEN(CH2)
      LCHMAX=MAX(LCH1,LCH2)
      LCHMIN=MIN(LCH1,LCH2)

      DO 10 I=1,LCHMIN
        IDX1=ICHAR(CH1(I:I))
        IDX2=ICHAR(CH2(I:I))
        IF (IDX1.NE.IDX2) THEN
          IF (65.LE.IDX1 .AND. IDX1.LE.90) THEN
            LCHREQ=(IDX2-IDX1).EQ.32
          ELSE IF (97.LE.IDX1 .AND. IDX1.LE.122) THEN
            LCHREQ=(IDX1-IDX2).EQ.32
          ELSE
            LCHREQ=.FALSE.
          END IF
          IF (.NOT.LCHREQ) RETURN
        END IF
   10 CONTINUE
      IF (LCH1.EQ.LCH2) RETURN

      IBLK=ICHAR(' ')
      IF (LCH1.GT.LCH2) THEN
        DO 20 I=LCHMIN+1,LCHMAX
          LCHREQ=ICHAR(CH1(I:I)).EQ.IBLK
          IF (.NOT.LCHREQ) RETURN
   20   CONTINUE
      ELSE
        DO 30 I=LCHMIN+1,LCHMAX
          LCHREQ=ICHAR(CH2(I:I)).EQ.IBLK
          IF (.NOT.LCHREQ) RETURN
   30   CONTINUE
      END IF

      END
*-----------------------------------------------------------------------
*     LCHREQ FOR GENERAL CHARACTER CODE (CUPPER IN MISC1/CHGLIB IS USED)
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LCHREQ(CH1,CH2)

      CHARACTER CH1*(*),CH2*(*)

      CHARACTER CX1*1,CX2*1


      LCHREQ=.TRUE.
      LCH1=LEN(CH1)
      LCH2=LEN(CH2)
      LCHMAX=MAX(LCH1,LCH2)
      LCHMIN=MIN(LCH1,LCH2)

      DO 10 I=1,LCHMIN
        CX1=CH1(I:I)
        CX2=CH2(I:I)
        CALL CUPPER(CX1)
        CALL CUPPER(CX2)
        LCHREQ=LCHREQ.AND.(CX1.EQ.CX2)
        IF (.NOT.LCHREQ) RETURN
   10 CONTINUE
      IF (LCH1.EQ.LCH2) RETURN

      IBLK=ICHAR(' ')
      IF (LCH1.GT.LCH2) THEN
        DO 20 I=LCHMIN+1,LCHMAX
          LCHREQ=ICHAR(CH1(I:I)).EQ.IBLK
          IF (.NOT.LCHREQ) RETURN
   20   CONTINUE
      ELSE
        DO 30 I=LCHMIN+1,LCHMAX
          LCHREQ=ICHAR(CH2(I:I)).EQ.IBLK
          IF (.NOT.LCHREQ) RETURN
   30   CONTINUE
      END IF

      END
*-----------------------------------------------------------------------
*     LCHREQ FOR THE SYSTEM ONLY ALLOWING UPPER CASE CHARACTERS
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LCHREQ(CH1,CH2)

      CHARACTER CH1*(*),CH2*(*)


      LCHREQ=CH1.EQ.CH2

      END
