*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE CFSRCH(CPLIST,NP,CFLIST,NF,CFNAME)

      CHARACTER CPLIST(*)*1024,CFLIST(*)*1024,CFNAME*(*)

      CHARACTER CDSN*1024
      LOGICAL   LEXIST

      EXTERNAL  LENC


      DO 10 M=1,NP
        DO 20 N=1,NF
          CDSN=CPLIST(M)(1:LENC(CPLIST(M)))//CFLIST(N)
          CALL CLADJ(CDSN)
          INQUIRE(FILE=CDSN,EXIST=LEXIST)
          IF (LEXIST) THEN
            IF (LENC(CDSN).LE.LEN(CFNAME)) THEN
              CFNAME=CDSN
              RETURN
            ELSE
              CALL MSGDMP('E','CFSRCH',
     +          'LENGTH OF CHARACTER IS TOO SHORT TO STORE FILE NAME.')
            END IF
          END IF
   20   CONTINUE
   10 CONTINUE

      CFNAME=' '

      END
