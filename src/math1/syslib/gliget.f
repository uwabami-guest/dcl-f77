*-----------------------------------------------------------------------
*     GLIGET / GLISET / GLISTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE GLIGET(CP, IPARA)

      CHARACTER CP*(*)

      CHARACTER CX*40


      CALL GLIQID(CP, IDX)
      CALL GLIQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLISET(CP, IPARA)

      CALL GLIQID(CP, IDX)
      CALL GLISVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLISTX(CP, IPARA)

      IP = IPARA
      CALL GLIQID(CP, IDX)

*     / SHORT NAME /

      CALL GLIQCP(IDX, CX)
      CALL RTIGET('GL', CX, IP, 1)

*     / LONG NAME /

      CALL GLIQCL(IDX, CX)
      CALL RLIGET(CX, IP, 1)

      CALL GLISVL(IDX, IP)

      RETURN
      END
