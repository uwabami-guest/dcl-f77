*-----------------------------------------------------------------------
*     PARAMETER CONTROL (GENERIC)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE GLPQNP(NCP)

      CHARACTER CP*(*)

      PARAMETER (NPARA = 20)

      INTEGER   ITYPE(NPARA)
      LOGICAL   LCHREQ
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      EXTERNAL  LCHREQ, LENC

      SAVE

*     / SHORT NAME /

      DATA CPARAS( 1) / 'LMISS   ' /, ITYPE( 1) / 2 /
      DATA CPARAS( 2) / 'IMISS   ' /, ITYPE( 2) / 1 /
      DATA CPARAS( 3) / 'RMISS   ' /, ITYPE( 3) / 3 /
      DATA CPARAS( 4) / 'IUNDEF  ' /, ITYPE( 4) / 1 /
      DATA CPARAS( 5) / 'RUNDEF  ' /, ITYPE( 5) / 3 /
      DATA CPARAS( 6) / 'LEPSL   ' /, ITYPE( 6) / 2 /
      DATA CPARAS( 7) / 'REPSL   ' /, ITYPE( 7) / 3 /
      DATA CPARAS( 8) / 'RFACT   ' /, ITYPE( 8) / 3 /

      DATA CPARAS( 9) / 'INTMAX  ' /, ITYPE( 9) / 1 /
      DATA CPARAS(10) / 'REALMAX ' /, ITYPE(10) / 3 /
      DATA CPARAS(11) / 'REALMIN ' /, ITYPE(11) / 3 /
      DATA CPARAS(12) / 'NBITSPW ' /, ITYPE(12) / 1 /
      DATA CPARAS(13) / 'NCHRSPW ' /, ITYPE(13) / 1 /

      DATA CPARAS(14) / 'IIUNIT  ' /, ITYPE(14) / 1 /
      DATA CPARAS(15) / 'IOUNIT  ' /, ITYPE(15) / 1 /
      DATA CPARAS(16) / 'MSGUNIT ' /, ITYPE(16) / 1 /
      DATA CPARAS(17) / 'MAXMSG  ' /, ITYPE(17) / 1 /
      DATA CPARAS(18) / 'MSGLEV  ' /, ITYPE(18) / 1 /
      DATA CPARAS(19) / 'NLNSIZE ' /, ITYPE(19) / 1 /
      DATA CPARAS(20) / 'LLMSG   ' /, ITYPE(20) / 2 /

*     / LONG NAME /

      DATA CPARAL( 1) / 'INTERPRET_MISSING_VALUE' /
      DATA CPARAL( 2) / 'MISSING_INT         ' /
      DATA CPARAL( 3) / 'MISSING_REAL        ' /
      DATA CPARAL( 4) / 'UNDEFINED_INT       ' /
      DATA CPARAL( 5) / 'UNDEFINED_REAL      ' /
      DATA CPARAL( 6) / 'INTERPRET_TRUNCATION' /
      DATA CPARAL( 7) / 'TRUNCATION_ERROR    ' /
      DATA CPARAL( 8) / 'TRUNCATION_FACTOR   ' /

      DATA CPARAL( 9) / 'MAX_INT             ' /
      DATA CPARAL(10) / 'MAX_REAL            ' /
      DATA CPARAL(11) / 'MIN_REAL            ' /
      DATA CPARAL(12) / 'WORD_LENGTH_IN_BIT  ' /
      DATA CPARAL(13) / 'WORD_LENGTH_IN_CHAR ' /

      DATA CPARAL(14) / 'INPUT_UNIT          ' /
      DATA CPARAL(15) / 'OUTPUT_UNIT         ' /
      DATA CPARAL(16) / 'MESSAGE_UNIT        ' /
      DATA CPARAL(17) / 'MAX_MESSAGE_NUMBER  ' /
      DATA CPARAL(18) / 'MESSAGE_LEVEL       ' /
      DATA CPARAL(19) / 'LINE_SIZE           ' /
      DATA CPARAL(20) / 'ENABLE_LONG_MESSAGE ' /


      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLPQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP,CPARAS(N)) .OR. LCHREQ(CP,CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E', 'GLPQID', CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLPQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E', 'GLPQCP', 'IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLPQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E', 'GLPQCL', 'IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLPQIT(IDX, ITP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        ITP = ITYPE(IDX)
      ELSE
        CALL MSGDMP('E','GLPQIT','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLPQVL(IDX, IPARA)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IF (ITYPE(IDX) .EQ. 1) THEN
          CALL GLIQID(CPARAS(IDX), ID)
          CALL GLIQVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 2) THEN
          CALL GLLQID(CPARAS(IDX), ID)
          CALL GLLQVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 3) THEN
          CALL GLRQID(CPARAS(IDX), ID)
          CALL GLRQVL(ID, IPARA)
        END IF
      ELSE
        CALL MSGDMP('E','GLPQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLPSVL(IDX, IPARA)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IF (ITYPE(IDX) .EQ. 1) THEN
          CALL GLIQID(CPARAS(IDX), ID)
          CALL GLISVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 2) THEN
          CALL GLLQID(CPARAS(IDX), ID)
          CALL GLLSVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 3) THEN
          CALL GLRQID(CPARAS(IDX), ID)
          CALL GLRSVL(ID, IPARA)
        END IF
      ELSE
        CALL MSGDMP('E','GLPSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY GLPQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
