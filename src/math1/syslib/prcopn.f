*-----------------------------------------------------------------------
*     PROCESS TABLE HANDLER
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE PRCOPN(CPROC)

      CHARACTER CPROC*(*)

      PARAMETER (MAXLVL=16)

      CHARACTER CMSGX*200,CPROCZ(0:MAXLVL)*32

      SAVE

      DATA NLEVEL    / 0 /
      DATA CPROCZ(0) /'MAIN  '/


      NLEVEL = NLEVEL + 1
      CPROCZ(NLEVEL) = CPROC

      RETURN
*-----------------------------------------------------------------------
      ENTRY PRCCLS(CPROC)

      IF (CPROC .EQ. CPROCZ(NLEVEL)) THEN
        CPROCZ(NLEVEL) = ' '
        NLEVEL = NLEVEL - 1
      ELSE
        CALL GLIGET('MSGUNIT',IUNIT)
        CALL GLIGET('NLNSIZE',LNSIZE)
        N0 = MIN(NLEVEL, 1)
        CMSGX='*** ERROR (PRCCLS IN '//CPROCZ(N0)//
     &        ') * PROCESS '//CPROCZ(NLEVEL)//
     &        ' HAS NOT BEEN CLOSED YET.'
        CALL MSZDMP(CMSGX,IUNIT,LNSIZE)
        CALL OSABRT
        STOP
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY PRCLVL(NLEV)

      NLEV = NLEVEL

      RETURN
*-----------------------------------------------------------------------
      ENTRY PRCNAM(NLEV, CPROC)

      IF (NLEV .LE. NLEVEL) THEN
        CPROC = CPROCZ(NLEV)
      ELSE
        CALL GLIGET('MSGUNIT',IUNIT)
        CALL GLIGET('NLNSIZE',LNSIZE)
        N0 = MIN(NLEVEL, 1)
        CMSGX='*** ERROR (PRCNAM IN '//CPROCZ(N0)//
     &        ') * TOO LARGE PROCESS LEVEL (NLEV).'
        CALL MSZDMP(CMSGX,IUNIT,LNSIZE)
        CALL OSABRT
        STOP
      END IF

      RETURN
      END
