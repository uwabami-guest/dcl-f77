*-----------------------------------------------------------------------
*     GET EXTERNAL PARAMETER FROM COMMAND LINE
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE RPNOPT

      LOGICAL   LPARA
      CHARACTER CPFIX*(*), CP*(*), CPARA*(*)

      LOGICAL   LCHREQ, LFROMC
      CHARACTER CARG*80, CPX*80, CSEP*1, COPT*1, CDIV*1,
     +          CSEPX*1, COPTX*1, CDIVX*1

      EXTERNAL  LCHREQ, LENC, IFROMC, LFROMC, RFROMC

      SAVE

      DATA      CSEPX / '_' /, COPTX / '-' /, CDIVX / '=' /


      CALL OSGENV('DCLENVCHAR',CSEP)
      IF (CSEP.EQ.' ') CSEP=CSEPX

      CALL OSGENV('DCLOPTCHAR',COPT)
      IF (COPT.EQ.' ') COPT=COPTX

      CALL OSGENV('DCLSETCHAR',CDIV)
      IF (CDIV.EQ.' ') CDIV=CDIVX

      RETURN
*-----------------------------------------------------------------------
*     GET INTEGER PARAMETER WITH SHORT NAME
*-----------------------------------------------------------------------
      ENTRY RTIOPT(CPFIX, CP, IPARA)

      CPX = COPT//CPFIX(1:LENC(CPFIX))//CSEP//CP

      CALL OSQARN(MAX)
      DO 10 N = 1, MAX
        CALL OSGARG(N, CARG)
        NP = INDEX(CARG, CDIV)
        IF (NP.NE.0) THEN
          IF (LCHREQ(CARG(1:NP-1), CPX)) THEN
            IPARA = IFROMC(CARG(NP+1:LENC(CARG)))
            RETURN
          END IF
        END IF
 10   CONTINUE

      RETURN
*-----------------------------------------------------------------------
*     GET LOGICAL PARAMETER WITH SHORT NAME
*-----------------------------------------------------------------------
      ENTRY RTLOPT(CPFIX, CP, LPARA)

      CPX = COPT//CPFIX(1:LENC(CPFIX))//CSEP//CP

      CALL OSQARN(MAX)
      DO 20 N = 1, MAX
        CALL OSGARG(N, CARG)
        NP = INDEX(CARG, CDIV)
        IF (NP .NE .0) THEN
          IF (LCHREQ(CARG(1:NP-1), CPX)) THEN
            LPARA = LFROMC(CARG(NP+1:LENC(CARG)))
            RETURN
          END IF
        END IF
   20 CONTINUE

      RETURN
*-----------------------------------------------------------------------
*     GET REAL PARAMETER WITH SHORT NAME
*-----------------------------------------------------------------------
      ENTRY RTROPT(CPFIX, CP, RPARA)

      CPX = COPT//CPFIX(1:LENC(CPFIX))//CSEP//CP

      CALL OSQARN(MAX)
      DO 30 N = 1, MAX
        CALL OSGARG(N, CARG)
        NP = INDEX(CARG, CDIV)
        IF (NP .NE .0) THEN
          IF (LCHREQ(CARG(1:NP-1), CPX)) THEN
            RPARA = RFROMC(CARG(NP+1:LENC(CARG)))
            RETURN
          END IF
        END IF
   30 CONTINUE

      RETURN
*-----------------------------------------------------------------------
*     GET CHARACTER PARAMETER WITH SHORT NAME
*-----------------------------------------------------------------------
      ENTRY RTCOPT(CPFIX, CP, CPARA)

      CPX = COPT//CPFIX(1:LENC(CPFIX))//CSEP//CP

      CALL OSQARN(MAX)
      DO 40 N = 1, MAX
        CALL OSGARG(N, CARG)
        NP = INDEX(CARG, CDIV)
        IF (NP .NE. 0) THEN
          IF (LCHREQ(CARG(1:NP-1), CPX)) THEN
            CPARA = CARG(NP+1:LENC(CARG))
            RETURN
          END IF
        END IF
   40 CONTINUE

      RETURN
*-----------------------------------------------------------------------
*     GET INTEGER PARAMETER WITH LONG NAME
*-----------------------------------------------------------------------
      ENTRY RLIOPT(CP, IPARA)

      CPX = COPT//CP

      CALL OSQARN(MAX)
      DO 50 N = 1, MAX
        CALL OSGARG(N, CARG)
        NP = INDEX(CARG, CDIV)
        IF (NP.NE.0) THEN
          IF (LCHREQ(CARG(1:NP-1), CPX)) THEN
            IPARA = IFROMC(CARG(NP+1:LENC(CARG)))
            RETURN
          END IF
        END IF
   50 CONTINUE

      RETURN
*-----------------------------------------------------------------------
*     GET LOGICAL PARAMETER WITH LONG NAME
*-----------------------------------------------------------------------
      ENTRY RLLOPT(CP, LPARA)

      CPX = COPT//CP

      CALL OSQARN(MAX)
      DO 60 N = 1, MAX
        CALL OSGARG(N, CARG)
        NP = INDEX(CARG, CDIV)
        IF (NP.NE.0) THEN
          IF (LCHREQ(CARG(1:NP-1), CPX)) THEN
            LPARA = LFROMC(CARG(NP+1:LENC(CARG)))
            RETURN
          END IF
        END IF
   60 CONTINUE

      RETURN
*-----------------------------------------------------------------------
*     GET REAL PARAMETER WITH LONG NAME
*-----------------------------------------------------------------------
      ENTRY RLROPT(CP, RPARA)

      CPX = COPT//CP

      CALL OSQARN(MAX)
      DO 70 N = 1, MAX
        CALL OSGARG(N, CARG)
        NP = INDEX(CARG, CDIV)
        IF (NP.NE.0) THEN
          IF (LCHREQ(CARG(1:NP-1), CPX)) THEN
            RPARA = RFROMC(CARG(NP+1:LENC(CARG)))
            RETURN
          END IF
        END IF
   70 CONTINUE

      RETURN
*-----------------------------------------------------------------------
*     GET CHARACTER PARAMETER WITH LONG NAME
*-----------------------------------------------------------------------
      ENTRY RLCOPT(CP, CPARA)

      CPX = COPT//CP

      CALL OSQARN(MAX)
      DO 80 N = 1, MAX
        CALL OSGARG(N, CARG)
        NP = INDEX(CARG, CDIV)
        IF (NP .NE. 0) THEN
          IF (LCHREQ(CARG(1:NP-1), CPX)) THEN
            CPARA = CARG(NP+1:LENC(CARG))
            RETURN
          END IF
        END IF
   80 CONTINUE

      RETURN
      END
