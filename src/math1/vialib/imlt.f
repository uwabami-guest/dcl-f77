*-----------------------------------------------------------------------
*     IMLT
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE IMLT(IX,N,JX,II)

      INTEGER   IX(*)

      LOGICAL   LMISS


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        CALL IMLT1(IX,N,JX,II)
      ELSE
        CALL IMLT0(IX,N,JX,II)
      END IF

      END
