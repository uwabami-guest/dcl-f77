*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VIINC1(IX,IY,N,JX,JY,II)

      INTEGER   IX(*),IY(*)


      CALL GLIGET('IMISS',IMISS)
      KX=1-JX
      KY=1-JY
      DO 10 J=1,N
        KX=KX+JX
        KY=KY+JY
        IF (IX(KX).NE.IMISS) THEN
          IY(KY)=IX(KX)+II
        ELSE
          IY(KY)=IMISS
        END IF
   10 CONTINUE

      END
