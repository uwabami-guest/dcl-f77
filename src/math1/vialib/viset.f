*-----------------------------------------------------------------------
*     VISET
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VISET(IX,IY,N,JX,JY)

      INTEGER   IX(*),IY(*)

*     ---------------------------
      ENTRY VISET0(IX,IY,N,JX,JY)
      ENTRY VISET1(IX,IY,N,JX,JY)
*     ---------------------------

      KX=1-JX
      KY=1-JY
      DO 10 J=1,N
        KX=KX+JX
        KY=KY+JY
        IY(KY)=IX(KX)
   10 CONTINUE

      END
