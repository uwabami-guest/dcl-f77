*-----------------------------------------------------------------------
*     VIFNB
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VIFNB(IX,IY,IZ,N,JX,JY,JZ,IFNB)

      INTEGER   IX(*),IY(*),IZ(*)

      LOGICAL   LMISS

      EXTERNAL  IFNB


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        CALL VIFNB1(IX,IY,IZ,N,JX,JY,JZ,IFNB)
      ELSE
        CALL VIFNB0(IX,IY,IZ,N,JX,JY,JZ,IFNB)
      END IF

      END
