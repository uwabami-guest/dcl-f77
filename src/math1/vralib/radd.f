*-----------------------------------------------------------------------
*     RADD
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE RADD(RX,N,JX,RR)

      REAL      RX(*)

      LOGICAL   LMISS


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        CALL RADD1(RX,N,JX,RR)
      ELSE
        CALL RADD0(RX,N,JX,RR)
      END IF

      END
