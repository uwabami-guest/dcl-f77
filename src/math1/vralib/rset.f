*-----------------------------------------------------------------------
*     RSET
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE RSET(RX,N,JX,RR)

      REAL      RX(*)

      LOGICAL   LMISS


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        CALL RSET1(RX,N,JX,RR)
      ELSE
        CALL RSET0(RX,N,JX,RR)
      END IF

      END
