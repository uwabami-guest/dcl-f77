*-----------------------------------------------------------------------
*     VRFCT
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VRFCT(RX,RY,N,JX,JY,RR)

      REAL      RX(*),RY(*)

      LOGICAL   LMISS


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        CALL VRFCT1(RX,RY,N,JX,JY,RR)
      ELSE
        CALL VRFCT0(RX,RY,N,JX,JY,RR)
      END IF

      END
