*-----------------------------------------------------------------------
*     VRFNA
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VRFNA(RX,RY,N,JX,JY,RFNA)

      REAL      RX(*),RY(*)

      LOGICAL   LMISS

      REAL      RFNA
      EXTERNAL  RFNA


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        CALL VRFNA1(RX,RY,N,JX,JY,RFNA)
      ELSE
        CALL VRFNA0(RX,RY,N,JX,JY,RFNA)
      END IF

      END
