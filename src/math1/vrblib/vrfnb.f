*-----------------------------------------------------------------------
*     VRFNB
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VRFNB(RX,RY,RZ,N,JX,JY,JZ,RFNB)

      REAL      RX(*),RY(*),RZ(*)

      LOGICAL   LMISS

      REAL      RFNB
      EXTERNAL  RFNB


      CALL GLLGET('LMISS',LMISS)
      IF (LMISS) THEN
        CALL VRFNB1(RX,RY,RZ,N,JX,JY,JZ,RFNB)
      ELSE
        CALL VRFNB0(RX,RY,RZ,N,JX,JY,JZ,RFNB)
      END IF

      END
