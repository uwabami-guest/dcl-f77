*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE VRFNB0(RX,RY,RZ,N,JX,JY,JZ,RFNB)

      REAL      RX(*),RY(*),RZ(*)

      REAL      RFNB
      EXTERNAL  RFNB


      KX=1-JX
      KY=1-JY
      KZ=1-JZ
      DO 10 J=1,N
        KX=KX+JX
        KY=KY+JY
        KZ=KZ+JZ
        RZ(KZ)=RFNB(RX(KX),RY(KY))
   10 CONTINUE

      END
