*-----------------------------------------------------------------------
*     2nd order Runge-Kutta algorithm routine.
*                                                 Oct. 5, 1990  S.Sakai
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE ODRK2(N, FCN, T, DT, X, DX, XOUT, WORK)
      DIMENSION  X(N), XOUT(N), DX(N), WORK(N)

      DO 10 I=1,N
        XOUT(I) = X(I) + DT*DX(I)
   10 CONTINUE
      TT = T+DT
      CALL FCN(N, TT, XOUT, WORK)

      DTT = DT/2.
      DO 20 I=1,N
        XOUT(I) = XOUT(I) + DTT*(WORK(I) - DX(I))
  20  CONTINUE

      RETURN
      END
