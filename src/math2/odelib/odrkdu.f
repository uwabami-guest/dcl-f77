*-----------------------------------------------------------------------
*    Runge-Kutta driver (Uniform step)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE ODRKDU(N, ALGR, FCN, T, TEND, ISTEP, X, WORK)

      DIMENSION  X(N), WORK(N,*)
*                      WORK(N,2) FOR RKG, ODRK2, ODRK1
*                      WORK(N,4) FOR ODRK4
      EXTERNAL   FCN, ALGR

      IF(N.LT.1) CALL MSGDMP('E', 'ODRKDU', 'INVALID N.')

      T0 = T
      DT = (TEND-T0)/ISTEP
      DO 30 J=1, ISTEP
        CALL FCN (N, T, X, WORK(1,1))
        CALL ALGR(N, FCN, T, DT, X, WORK(1,1), X, WORK(1,2))
        T = T0 + DT*J
  30  CONTINUE

      RETURN
      END
