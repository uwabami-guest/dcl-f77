***********************************************************************
*     TRANSFORM WAVE TO GRID FROM M=M1 TO M=M2 AT A LATITUDE
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHFWGJ(MM,IM,M1,M2,WJ,GJ,H,WFFT)

      REAL WJ(-MM:MM),GJ(-IM:IM)
      REAL H(0:2*IM-1)
      REAL WFFT(*)

*     IM.GE.MM+1

      IF(M1.GT.0) THEN
        H(0)=0
      ELSE
        H(0)=WJ(0)
      END IF
      DO 10 M=1,M1-1
        H(2*M-1)=0
        H(2*M  )=0
   10 CONTINUE
      DO 20 M=M1,M2
        H(2*M-1)=WJ( M)
        H(2*M  )=WJ(-M)
   20 CONTINUE
      DO 30 K=2*M2+1,2*IM-1
        H(K)=0
   30 CONTINUE
      CALL SHFFTB(2*IM,H,WFFT)
      GJ(0)=H(0)
      DO 40 I=1,IM
        GJ( I)=H(I)
        GJ(-I)=H(2*IM-I)
   40 CONTINUE

      END
