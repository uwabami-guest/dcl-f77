***********************************************************************
*     TRANSFORM WAVE TO GRID FOR M>0
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHFWGM(JM,IM,M,WR,WI,G,H,WFFT)

      REAL WR(-JM:JM),WI(-JM:JM),G(-IM:IM,-JM:JM)
      REAL H(0:2*IM-1)
      REAL WFFT(*)

*     IM.GE.MM+1

      DO 30 J=-JM,JM
        DO 10 K=0,2*IM-1
          H(K)=0
   10   CONTINUE
        H(2*M-1)=WR(J)
        H(2*M  )=WI(J)
        CALL SHFFTB(2*IM,H,WFFT)
        G(0,J)=H(0)
        DO 20 I=1,IM
          G( I,J)=H(I)
          G(-I,J)=H(2*IM-I)
   20   CONTINUE
   30 CONTINUE

      END
