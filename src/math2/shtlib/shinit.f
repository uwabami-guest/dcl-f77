************************************************************************
*     INITIALIZE ALL VARIABLES TO SAVE
************************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHINIT(MM,JM,IM,Y,YS,YC,X,XS,XC,XW,Z,PY,PX,R,WFFT)

      REAL Y(0:JM),YS(0:JM),YC(0:JM)
      REAL X(0:JM),XS(0:JM),XC(0:JM),XW(0:JM)
      REAL Z(JM,0:JM,4)
      REAL PY(2,0:JM,0:MM)
      REAL PX(2,0:JM,0:MM)
      REAL R((MM+1)*(MM+1))
      REAL WFFT(*)

      CALL SHINIY(JM,Y,YS,YC)
      CALL SHINIX(JM,X,XS,XC,XW)
      CALL SHINIZ(JM,X,Y,Z)
      CALL SHINIP(MM,JM,YS,YC,PY)
      CALL SHINIP(MM,JM,XS,XC,PX)
      CALL SHINIC(MM,JM,XW,PX)
      CALL SHINIR(MM,R)
      CALL SHINIF(2*IM,WFFT) 

      END
