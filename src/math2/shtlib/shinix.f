************************************************************************
*     INITIALIZE GAUSSIAN LATITUDES AND WEIGHT
************************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHINIX(JM,X,XS,XC,XW)

      IMPLICIT REAL*8(A-H,O-Z)
      PARAMETER(PI=3.1415926535897932385D0,EPS=1D-15)
      REAL X(0:JM),XS(0:JM),XC(0:JM),XW(0:JM)

      NM=2*JM+1

      DO 30 J=0,JM
        XD=SIN(PI*2*J/(2*NM+1))
   10   CONTINUE
          P0=1
          P1=XD
          DO 20 N=2,NM
            P2=((2*N-1)*XD*P1-(N-1)*P0)/N
            P0=P1
            P1=P2
   20     CONTINUE
          DP1=NM*(P0-XD*P1)/(1-XD*XD)
          DX=P1/DP1
          XD=XD-DX
        IF(ABS(DX).GT.EPS) GOTO 10
        X(J)=ASIN(XD)
        XS(J)=XD
        XC(J)=SQRT(1-XD*XD)
        XW(J)=2D0/(P0*P0*NM*NM)*XC(J)*XC(J)
   30 CONTINUE

      END
