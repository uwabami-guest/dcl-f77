***********************************************************************
*     FORWARD LEGENDRE TRANSFORMATION (UPPER LEVEL)
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHLFWU(MM,JM,M,ISW,WM,SM,SD,XS,XC,PM,WY,WX,PX,R,Z)

      REAL SM(M:MM),WM(-JM:JM)
      REAL SD(0:MM+1)
      REAL XS(0:JM),XC(0:JM)
      REAL PM(0:MM+1,0:JM)
      REAL PX(2,0:JM,0:MM)
      REAL WY(0:JM,2),WX(0:JM,2)
      REAL R((MM+1)*(MM+1)),Z(JM,0:JM,4)

      CALL SHPPMA(MM,JM,M,PM,XS,PX,R)
      CALL SHLFWM(MM,JM,M,ISW,WM,SM,SD,XC,PM,WY,WX,R,Z)

      END
