***********************************************************************
*     TRANSFORM SD TO SM
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHLSDS(MM,M,ISW,SD,SM,R)

      REAL SD(0:MM+1)
      REAL SM(M:MM)
      REAL R((MM+1)*(MM+1))

*     ISW=-1: X-DIFFERENTIAL, ISW=0: NORMAL, ISW=1: Y-DIFFERENTIAL

      IF(ISW.EQ.0) THEN
        DO 10 N=M,MM
          SM(N)=SD(N)
   10   CONTINUE
      ELSE IF(ISW.EQ.-1) THEN
        DO 20 N=M,MM
          SM(N)=-M*SD(N)
   20   CONTINUE
      ELSE
        LB=M*(2*MM-M)+M
        IF(M.NE.MM) THEN
          N=M
          SM(N)=N*R(LB+N+1)*SD(N+1)
          DO 30 N=M+1,MM
            SM(N)=N*R(LB+N+1)*SD(N+1)-(N+1)*R(LB+N)*SD(N-1)
   30     CONTINUE
        ELSE IF(M.EQ.MM) THEN
          N=MM
          SM(N)=N*R(LB+N+1)*SD(N+1)
        END IF
      END IF      

      END
