***********************************************************************
*     TRANSFORM SPECTRA INTO WAVE
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHMS2W(MM,JM,ISW,S,W,SD,PM,YS,YC,PY,R)

      REAL S((MM+1)*(MM+1)),W(-JM:JM,-MM:MM)
      REAL SD(0:MM+1),PM(0:MM+1,0:JM)
      REAL YS(0:JM),YC(0:JM)
      REAL PY(2,0:JM,0:MM)
      REAL R((MM+1)*(MM+1))

      CALL SHMSWA(MM,JM,ISW,0,MM,S,W,SD,PM,YS,YC,PY,R)

      END
