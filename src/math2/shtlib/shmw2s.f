***********************************************************************
*     TRANSFORM WAVE INTO SPECTRA
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHMW2S(MM,JM,ISW,W,S,SD,PM,XS,XC,PX,WY,WX,R,Z)

      REAL W(-JM:JM,-MM:MM),S((MM+1)*(MM+1))
      REAL SD(0:MM+1),PM(0:MM+1,0:JM)
      REAL XS(0:JM),XC(0:JM)
      REAL PX(2,0:JM,0:MM)
      REAL WY(0:JM,2),WX(0:JM,2)
      REAL R((MM+1)*(MM+1)),Z(JM,0:JM,4)

      M=0
      IF(ISW.EQ.-1) THEN
        DO 10 N=0,MM
          S(N+1)=0
   10   CONTINUE
      ELSE 
        CALL SHPPMA(MM,JM,M,PM,XS,PX,R)
        CALL SHLFWM(MM,JM,M,ISW,W(-JM,0),S,SD,XC,PM,WY,WX,R,Z)
      END IF

      DO 20 M=1,MM
        CALL SHNM2L(MM,M,M,LR,LI)
        CALL SHPPMA(MM,JM,M,PM,XS,PX,R)
        CALL SHLFWM(MM,JM,M,ISW,W(-JM, M),S(LR),SD,XC,PM,WY,WX,R,Z)
        CALL SHLFWM(MM,JM,M,ISW,W(-JM,-M),S(LI),SD,XC,PM,WY,WX,R,Z)
        IF(ISW.EQ.-1) THEN
          CALL SHMSRI(MM,M,S(LR),S(LI))
        END IF
   20 CONTINUE

      END
