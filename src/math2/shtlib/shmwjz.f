***********************************************************************
*     TRANSFORM SPECTRA INTO WAVE FOR M=0 AT A LATITUDE
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHMWJZ(MM,JM,ISW,J,S,WJD,SD,PMJ,YS,YC,PY,R)

      REAL S((MM+1)*(MM+1)),WJD
      REAL SD(0:MM+1),PMJ(0:MM+1)
      REAL YS(0:JM),YC(0:JM)
      REAL PY(2,0:JM,0:MM)
      REAL R((MM+1)*(MM+1))

      JD=ABS(J)

      M=0
      IF(ISW.EQ.-1) THEN
        WJD=0
      ELSE 
        CALL SHPPMJ(MM,JM,M,JD,PMJ,YS,PY,R)
        CALL SHLBWJ(MM,JM,M,J,ISW,S,WJD,SD,PMJ,YC,R)
      END IF

      END
