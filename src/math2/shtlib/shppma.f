***********************************************************************
*     CALCULATE LEGENDRE FUNCTIONS OF ZONAL WAVENUMBER M
***********************************************************************
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE SHPPMA(MM,JM,M,PM,YS,PY,R)

      REAL PM(0:MM+1,0:JM)
      REAL YS(0:JM),PY(2,0:JM,0:MM),R((MM+1)*(MM+1))

      DO 10 J=0,JM
        CALL SHPPMJ(MM,JM,M,J,PM(0,J),YS,PY,R)
   10 CONTINUE

      END
