*-----------------------------------------------------------------------
*     LCHR
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LCHR(CHAR,CREF)

      CHARACTER CHAR*(*),CREF*(*)

      LOGICAL   LCHRF,LCHRA,LCHRS,LCHRL,LCHRD,LCHRB,LCHRC

      EXTERNAL  LCHRF,LCHRA,LCHRS,LCHRL,LCHRD,LCHRB,LCHRC


      NCH=LEN(CHAR)
      NCR=LEN(CREF)
      IF (NCH.NE.NCR) THEN
        CALL MSGDMP('E','LCHR  ',
     +      'LENGTH OF CHAR IS NOT EQUAL TO THAT OF CREF.')
      END IF

      LCHR=.TRUE.
      DO 10 I=1,NCH
        IF (CREF(I:I).EQ.'F') THEN
          LCHR=LCHR.AND.LCHRF(CHAR(I:I))
        ELSE IF (CREF(I:I).EQ.'A') THEN
          LCHR=LCHR.AND.LCHRA(CHAR(I:I))
        ELSE IF (CREF(I:I).EQ.'S') THEN
          LCHR=LCHR.AND.LCHRS(CHAR(I:I))
        ELSE IF (CREF(I:I).EQ.'L') THEN
          LCHR=LCHR.AND.LCHRL(CHAR(I:I))
        ELSE IF (CREF(I:I).EQ.'D') THEN
          LCHR=LCHR.AND.LCHRD(CHAR(I:I))
        ELSE IF (CREF(I:I).EQ.'B') THEN
          LCHR=LCHR.AND.LCHRB(CHAR(I:I))
        ELSE IF (CREF(I:I).EQ.'C') THEN
          LCHR=LCHR.AND.LCHRC(CHAR(I:I))
        ELSE
          CALL MSGDMP('E','LCHR  ','INVALID REFERENCE NAME.')
        END IF
        IF (.NOT.LCHR) RETURN
   10 CONTINUE

      END
