*-----------------------------------------------------------------------
*     LCHRB
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LCHRB(CH)

      CHARACTER CH*(*)

      PARAMETER (NC=1)

      CHARACTER CLST(NC)*1

      SAVE

      EXTERNAL  INDXCF

      DATA      CLST/' '/


      NCH=LEN(CH)
      LCHRB=.TRUE.
      DO 10 I=1,NCH
        NIDX=INDXCF(CLST,NC,1,CH(I:I))
        LCHRB=LCHRB.AND.NIDX.NE.0
        IF (.NOT.LCHRB) RETURN
   10 CONTINUE

      END
