*-----------------------------------------------------------------------
*     LCHRL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      LOGICAL FUNCTION LCHRL(CH)

      CHARACTER CH*(*)

      PARAMETER (NC=26)

      CHARACTER CLST(NC)*1

      SAVE

      EXTERNAL  INDXCF

      DATA      CLST/ 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J',
     +                'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T',
     +                'U', 'V', 'W', 'X', 'Y', 'Z'/


      NCH=LEN(CH)
      LCHRL=.TRUE.
      DO 10 I=1,NCH
        NIDX=INDXCF(CLST,NC,1,CH(I:I))
        LCHRL=LCHRL.AND.NIDX.NE.0
        IF (.NOT.LCHRL) RETURN
   10 CONTINUE

      END
