/*
 *    clckst (written in C)
 *
 *    Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
 *
 */

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include "../../../config.h"

#define TRUE   1             /* numeric value for true  */
#define FALSE  0             /* numeric value for false */

#ifndef CLOCKS_PER_SEC
#define CLOCKS_PER_SEC CLK_PER_SEC
#endif

#ifndef CLK_TCK
#define CLK_TCK CLK_RSL_TCK
#endif

static int lfirst = TRUE;
static clock_t time0;

#ifndef WINDOWS
void clckst_(void)
#else
void CLCKST(void)
#endif
{
    time0 = clock();
    lfirst = FALSE;
}

#ifndef WINDOWS
void clckgt_(DCL_REAL *time)
#else
void CLCKGT(DCL_REAL *time)
#endif
{
    if (lfirst){
	fprintf (stderr, "*** Error in clckgt : ");
	fprintf (stderr, "Clckst must be called prior to this routine.\n");
	exit(1);
    }
    else{
	*time = (float) (clock() - time0) / (float) CLOCKS_PER_SEC;
    }
}

#ifndef WINDOWS
void clckdt_(DCL_REAL *dt0)
#else
void CLCKDT(DCL_REAL *dt0)
#endif
{
    *dt0 = 1.0 / CLK_TCK;
}
