*-----------------------------------------------------------------------
      SUBROUTINE RNORML(V,W,N,M,X,Y)

      REAL    V(N,M),W(N,M),R,MAX,MIN

      MAX = 0.0
      MIN = 0.0

      DO 10 I=1,N
      DO 20 J=1,M
        IF (V(I,J).GE.MAX) THEN
          MAX=V(I,J)
        ENDIF
        IF (V(I,J).LE.MIN) THEN
          MIN=V(I,J)
        ENDIF
 20   CONTINUE
 10   CONTINUE

      IF (MAX.NE.MIN) THEN
        R = (Y-X)/(MAX-MIN)
      ENDIF

      IF (MAX.NE.MIN) THEN
      DO 30 I=1,N
      DO 40 J=1,M
        W(I,J)=X+R*(V(I,J)-MIN)
 40   CONTINUE
 30   CONTINUE
      ELSE IF (MAX.GE.Y) THEN
      DO 50 I=1,N
      DO 60 J=1,M
        W(I,J) = Y
 60   CONTINUE
 50   CONTINUE
      ELSE IF (MIN.LE.X) THEN
      DO 70 I=1,N
      DO 80 J=1,M
        W(I,J) = X
 80   CONTINUE
 70   CONTINUE
      ELSE
      DO 90 I=1,N
      DO 100 J=1,M
        W(I,J) = V(I,J)
 100  CONTINUE
  90  CONTINUE
      END IF

      RETURN
      END
