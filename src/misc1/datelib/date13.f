*-----------------------------------------------------------------------
*     DATE13
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE DATE13(IDATE,IY,IM,ID)

*     IDATE : DATE (IY*10000+IM*100+ID)                         (I/ )
*     IY    : YEAR                                              ( /O)
*     IM    : MONTH                                             ( /O)
*     ID    : DAY                                               ( /O)


      IY=IDATE/10000
      IM=(IDATE-IY*10000)/100
      ID=IDATE-IY*10000-IM*100

      END
