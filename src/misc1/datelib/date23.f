*-----------------------------------------------------------------------
*     DATE23
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE DATE23(IY,IM,ID,ITD)

*     IY   : YEAR                                               (I/O)
*     IM   : MONTH                                              ( /O)
*     ID   : DAY                                                ( /O)
*     ITD  : TOTAL DAY                                          (I/ )

      INTEGER   MN(12)
      LOGICAL   LEAP

      SAVE

      DATA      MN/31,28,31,30,31,30,31,31,30,31,30,31/


      LEAP=(MOD(IY,4).EQ.0 .AND. MOD(IY,100).NE.0)
     +     .OR. MOD(IY,400).EQ.0
      IF (LEAP) THEN
        MN(2)=29
      ELSE
        MN(2)=28
      END IF

      ID=ITD
      IM=1

   11 IF (.NOT.(ID.GT.MN(IM))) GO TO 10
        ID=ID-MN(IM)
        IM=IM+1
        GO TO 11
   10 CONTINUE

      END
