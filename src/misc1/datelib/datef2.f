*-----------------------------------------------------------------------
*     DATEF2
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE DATEF2(N,IY,ITD,NY,NTD)

      EXTERNAL  NDYEAR


      NY=IY
      NTD=ITD+N
      IF (NTD.LE.0) THEN
   11   IF (.NOT.(NTD.LE.0)) GO TO 10
          NTD=NTD+NDYEAR(NY-1)
          NY=NY-1
          GO TO 11
   10   CONTINUE
      ELSE
   16   IF (.NOT.(NTD.GT.NDYEAR(NY))) GO TO 15
          NTD=NTD-NDYEAR(NY)
          NY=NY+1
          GO TO 16
   15   CONTINUE
      END IF

      END
