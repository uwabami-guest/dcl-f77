*-----------------------------------------------------------------------
*     DATEG3
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE DATEG3(N,IY,IM,ID,NY,NM,ND)


      CALL DATE32(IY,IM,ID,ITD)
      CALL DATE32(NY,NM,ND,NTD)
      CALL DATEG2(N,IY,ITD,NY,NTD)

      END
