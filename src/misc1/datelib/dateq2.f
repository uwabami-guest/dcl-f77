*-----------------------------------------------------------------------
*     DATEQ2
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE DATEQ2(IY, ITD)

*     IY    : YEAR                                              ( /O)
*     ITD   : TOTAL DAY                                         ( /O)


      CALL DATEQ3(IY, IM, ID)
      CALL DATE32(IY, IM, ID, ITD)

      END
