*-----------------------------------------------------------------------
*     HEXDCI
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE HEXDCI(CP,IP)

      CHARACTER CP*(*)

      LOGICAL   LFST,LCHREQ,LCHAR
      CHARACTER CHEX(0:15)*1

      SAVE

      EXTERNAL  LCHREQ

      DATA      CHEX/ '0', '1', '2', '3', '4', '5', '6', '7',
     +                '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'/
      DATA      LFST/.TRUE./


      IF (LFST) THEN
        CALL GLIGET('NBITSPW',NBITS)
        NHEX=NBITS/4
        LFST=.FALSE.
      END IF

      NH=LEN(CP)
      IP=0
      DO 10 I=1,MIN(NHEX,NH)
        ISKIP=NBITS-I*4
        IDX=NH-I+1
        LCHAR=.FALSE.
        DO 15 J=0,15
          IF (LCHREQ(CP(IDX:IDX),CHEX(J))) THEN
            IPX=J
            LCHAR=.TRUE.
            GO TO 20
          END IF
   15   CONTINUE
   20   CONTINUE
        IF (.NOT.LCHAR) THEN
          CALL MSGDMP('E','HEXDCI','INVALID HEXADECIMAL CHARACTER.')
        END IF
        CALL SBYTE(IP,IPX,ISKIP,4)
   10 CONTINUE

      END
