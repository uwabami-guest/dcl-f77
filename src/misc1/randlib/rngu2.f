*-----------------------------------------------------------------------
*     RANDOM NUMBER GENERATOR (SHUFFLING METHOD)
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION RNGU2(ISEED)

      PARAMETER (M1=259200, IA1=7141, IC1=54773, RM1=1./M1)
      PARAMETER (M2=243000, IA2=4561, IC2=51349)

      REAL      R(97)
      LOGICAL   LFIRST

      SAVE

      DATA      LFIRST / .TRUE. /


      IF (ISEED.NE.0) THEN

        IX1 = MOD(IC1+ABS(ISEED), M1)
        IX1 = MOD(IA1*IX1+IC1, M1)
        IX1 = MOD(IA1*IX1+IC1, M1)
        IX2 = MOD(IX1,M2)

        DO 100 J=1,97
          IX1  = MOD(IA1*IX1+IC1,M1)
          R(J) = REAL(IX1)*RM1
  100   CONTINUE

        ISEED  = 0
        LFIRST = .FALSE.

      END IF

      IF (LFIRST) CALL MSGDMP('E', 'RNGU2',
     #     'ISEED MUST BE > 0 FOR 1ST CALL.')
      IX1 = MOD(IA1*IX1+IC1, M1)
      IX2 = MOD(IA2*IX2+IC2, M2)

      J = 1+(97*IX2)/M2
      RNGU2 = R(J)
      R(J) = REAL(IX1)*RM1

      END
