*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION R4IBM(II)

      LOGICAL   LFRST

      SAVE

      EXTERNAL  ISHIFT

      DATA      LFRST/.TRUE./


      IF (LFRST) THEN
        ISMASK=0
        CALL SBYTE(ISMASK,2** 1-1,0, 1)
        IEMASK=0
        CALL SBYTE(IEMASK,2** 7-1,1, 7)
        IXMASK=0
        CALL SBYTE(IXMASK,2**24-1,8,24)
        LFRST=.FALSE.
      END IF

      IF (II.EQ.0) THEN
        R4IBM=0
        RETURN
      END IF

      IS=ISHIFT(IAND(ISMASK,II),-31)
      IE=ISHIFT(IAND(IEMASK,II),-24)
      IX=IAND(IXMASK,II)

      IF (IS.NE.0) THEN
        R4IBM = - (IX/2.0**20) * 16.0**(IE-65)
      ELSE
        R4IBM = + (IX/2.0**20) * 16.0**(IE-65)
      END IF

      END
