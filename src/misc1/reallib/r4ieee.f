*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      REAL FUNCTION R4IEEE(II)

      LOGICAL   LFRST

      SAVE

      EXTERNAL  ISHIFT

      DATA      LFRST/.TRUE./


      IF (LFRST) THEN
        ISMASK=0
        CALL SBYTE(ISMASK,2** 1-1,0, 1)
        IEMASK=0
        CALL SBYTE(IEMASK,2** 8-1,1, 8)
        IXMASK=0
        CALL SBYTE(IXMASK,2**23-1,9,23)
        LFRST=.FALSE.
      END IF

      IF (II.EQ.0) THEN
        R4IEEE=0
        RETURN
      END IF

      IS=ISHIFT(IAND(ISMASK,II),-31)
      IE=ISHIFT(IAND(IEMASK,II),-23)
      IX=IAND(IXMASK,II)

      IF (IE.EQ.0) THEN
        R4IEEE = (-1)**IS * (IX/2.0**23) * 2.0**(IE-126)
      ELSE
        R4IEEE = (-1)**IS * (1+IX/2.0**23) * 2.0**(IE-127)
      END IF

      END
