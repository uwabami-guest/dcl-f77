*-----------------------------------------------------------------------
*     TIME13
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE TIME13(ITIME,IH,IM,IS)

*     ITIME : TIME (IH*10000+IM*100+IS)                         (I/ )
*     IH    : HOUR                                              ( /O)
*     IM    : MINUTE                                            ( /O)
*     IS    : SECOND                                            ( /O)


      IH=ITIME/10000
      IM=(ITIME-IH*10000)/100
      IS=ITIME-IH*10000-IM*100

      END
