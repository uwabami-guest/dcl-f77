*-----------------------------------------------------------------------
*     TIME31
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE TIME31(ITIME,IH,IM,IS)

*     ITIME : TIME (IH*10000+IM*100+IS)                         ( /O)
*     IH    : HOUR                                              (I/ )
*     IM    : MINUTE                                            (I/ )
*     IS    : SECOND                                            (I/ )


      ITIME=IH*10000+IM*100+IS

      END
