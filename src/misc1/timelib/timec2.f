*-----------------------------------------------------------------------
*     TIMEC2
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE TIMEC2(CFORM,ITT)

      CHARACTER CFORM*(*)


      CALL TIME23(IH,IM,IS,ITT)
      CALL TIMEC3(CFORM,IH,IM,IS)

      END
