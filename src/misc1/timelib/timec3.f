*-----------------------------------------------------------------------
*     TIMEC3
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE TIMEC3(CFORM,IH,IM,IS)

      CHARACTER CFORM*(*)

      PARAMETER (NN=3)

      INTEGER   JD(NN)
      CHARACTER CD(NN)*1,CFMT*6

      SAVE

      EXTERNAL  LENC,INDXCF,INDXCL

      DATA      CD/'H','M','S'/


      MC=LENC(CFORM)
      JD(1)=IH
      JD(2)=IM
      JD(3)=IS
      DO 10 I=1,3
        IDX1=INDXCF(CFORM,MC,1,CD(I))
        IDX2=INDXCL(CFORM,MC,1,CD(I))
        IF (IDX1.GT.0) THEN
          NCM=IDX2-IDX1+1
          WRITE(CFMT,'(A2,I1,A1,I1,A1)') '(I',NCM,'.',NCM,')'
          WRITE(CFORM(IDX1:IDX2),CFMT) MOD(JD(I),10**NCM)
        END IF
   10 CONTINUE

      END
