SUBROUTINE WR_USGI_(A,B,C) BIND(C,NAME='usgi__')
  USE iso_c_binding
  INTEGER(c_int),VALUE :: B
  CHARACTER(1,c_char) :: A(B)
  INTEGER(c_int) :: C(*)
  CHARACTER(3) :: USGI

  CALL CHAR_TRIM(USGI(C),A,3)

END

SUBROUTINE WR_CSGI_(A,B,C) BIND(C,NAME='csgi__')
  USE iso_c_binding
  CHARACTER(1,c_char) :: A
  INTEGER(c_int),VALUE :: B
  INTEGER(c_int) :: C(*)
  CHARACTER(1) :: CSGI

  A=CSGI(C)

END

INTEGER(c_int) FUNCTION WR_IAND_(A,B) BIND(C,NAME='iand__')
  USE iso_c_binding
  INTEGER(c_int) :: A(*)
  INTEGER(c_int) :: B(*)
  CALL iand(A,B)
END

INTEGER(c_int) FUNCTION WR_IOR_(A,B) BIND(C,NAME='ior__')
  USE iso_c_binding
  INTEGER(c_int) :: A(*)
  INTEGER(c_int) :: B(*)
  CALL ior(A,B)
END


INTEGER(c_int) FUNCTION WR_USAXLB_(A,B,C,D,E,F,G,H,I) BIND(C,NAME='usaxlb__')
  USE iso_c_binding

  CHARACTER(1,c_char) :: A(*)
  REAL(c_float) :: B(*)
  INTEGER(c_int) :: C(*)
  REAL(c_float) :: D(*)
  CHARACTER(1,c_char) :: E(*)
  INTEGER(c_int) :: F
  INTEGER(c_int) :: G
  INTEGER(c_int),VALUE :: H
  INTEGER(c_int),VALUE :: I
  CHARACTER(F) :: EA(G)
  CHARACTER(:,C_char),ALLOCATABLE :: AA

  Allocate(Character(H,C_char) :: AA )

  CALL CHAR_TRIM(A,AA,H)
  CALL CHAR_TRIM2(E,EA,F,G,I)

  WR_USAXLB_=usaxlb(AA,B,C,D,EA,F,G)

END


INTEGER(c_int) FUNCTION WR_UXAXLB_(A,B,C,D,E,F,G,H,I) BIND(C,NAME='uxaxlb__')
  USE iso_c_binding

  CHARACTER(1,c_char) :: A(*)
  REAL(c_float) :: B(*)
  INTEGER(c_int) :: C(*)
  REAL(c_float) :: D(*)
  CHARACTER(1,c_char) :: E(*)
  INTEGER(c_int) :: F
  INTEGER(c_int) :: G
  INTEGER(c_int),VALUE :: H
  INTEGER(c_int),VALUE :: I
  CHARACTER(F) :: EA(G)
  CHARACTER(:,C_char),ALLOCATABLE :: AA

  Allocate(Character(H,C_char) :: AA )

  CALL CHAR_TRIM(A,AA,H)
  CALL CHAR_TRIM2(E,EA,F,G,I)

  WR_UXAXLB_=uxaxlb(AA,B,C,D,EA,F,G)
END

INTEGER(c_int) FUNCTION WR_UYAXLB_(A,B,C,D,E,F,G,H,I) BIND(C,NAME='uyaxlb__')
  USE iso_c_binding

  CHARACTER(1,c_char) :: A(*)
  REAL(c_float) :: B(*)
  INTEGER(c_int) :: C(*)
  REAL(c_float) :: D(*)
  CHARACTER(1,c_char) :: E(*)
  INTEGER(c_int) :: F
  INTEGER(c_int) :: G
  INTEGER(c_int),VALUE :: H
  INTEGER(c_int),VALUE :: I
  CHARACTER(:,C_char),ALLOCATABLE :: AA
  CHARACTER(F) :: EA(G)

  Allocate(Character(H,C_char) :: AA )

  CALL CHAR_TRIM(A,AA,H)
  CALL CHAR_TRIM2(E,EA,F,G,I)

  WR_UYAXLB_=uyaxlb(AA,B,C,D,EA,F,G)
END

INTEGER(c_int) FUNCTION WR_USPLBL_(A,B,C,D,E,F,G,H) BIND(C,NAME='usplbl__')
  USE iso_c_binding

  CHARACTER(1,c_char) :: A(*)
  INTEGER(c_int) :: B(*)
  REAL(c_float) :: C(*)
  CHARACTER(1,c_char) :: D(*)
  INTEGER(c_int) :: E
  INTEGER(c_int) :: F
  INTEGER(c_int),VALUE :: G
  INTEGER(c_int),VALUE :: H
  CHARACTER(E) :: DA(F)
  CHARACTER(:,C_char),ALLOCATABLE :: AA

  Allocate(Character(G,C_char) :: AA )

  CALL CHAR_TRIM(A,AA,G)
  CALL CHAR_TRIM2(D,DA,E,F,H)

  WR_USPLBL_=usplbl(AA,B,C,DA,E,F)
END

INTEGER(c_int) FUNCTION WR_UXPLBL_(A,B,C,D,E,F,G,H) BIND(C,NAME='uxplbl__')
  USE iso_c_binding

  CHARACTER(1,c_char) :: A(*)
  INTEGER(c_int) :: B(*)
  REAL(c_float) :: C(*)
  CHARACTER(1,c_char) :: D(*)
  INTEGER(c_int) :: E
  INTEGER(c_int) :: F
  INTEGER(c_int),VALUE :: G
  INTEGER(c_int),VALUE :: H
  CHARACTER(E) :: DA(F)
  CHARACTER(:,C_char),ALLOCATABLE :: AA

  Allocate(Character(G,C_char) :: AA )

  CALL CHAR_TRIM(A,AA,G)
  CALL CHAR_TRIM2(D,DA,E,F,H)

  WR_UXPLBL_=uxplbl(AA,B,C,DA,E,F)
END

INTEGER(c_int) FUNCTION WR_UYPLBL_(A,B,C,D,E,F,G,H) BIND(C,NAME='uyplbl__')
  USE iso_c_binding

  CHARACTER(1,c_char) :: A(*)
  INTEGER(c_int) :: B(*)
  REAL(c_float) :: C(*)
  CHARACTER(1,c_char) :: D(*)
  INTEGER(c_int) :: E
  INTEGER(c_int) :: F
  INTEGER(c_int),VALUE :: G
  INTEGER(c_int),VALUE :: H
  CHARACTER(E) :: DA(F)
  CHARACTER(:,C_char),ALLOCATABLE :: AA

  Allocate(Character(G,C_char) :: AA )

  CALL CHAR_TRIM(A,AA,G)
  CALL CHAR_TRIM2(D,DA,E,F,H)

  WR_UYPLBL_=uyplbl(AA,B,C,DA,E,F)
END

INTEGER(c_int) FUNCTION WR_UXPLBA_(A,B,C,D,E,F,G,H,I,J,K) BIND(C,NAME='uxplba__')
  USE iso_c_binding
  REAL(c_float) :: A(*)
  CHARACTER(1,c_char) :: B(*)
  INTEGER(c_int) :: C
  INTEGER(c_int) :: D
  REAL(c_float) :: E(*)
  REAL(c_float) :: F(*)
  REAL(c_float) :: G(*)
  INTEGER(c_int) :: H(*)
  INTEGER(c_int) :: I(*)
  INTEGER(c_int) :: J(*)
  INTEGER(c_int),VALUE :: K
! CVBCL 10
  CHARACTER(C) :: BA(D)

  CALL CHAR_TRIM2(B,BA,C,D,K)


  WR_UXPLBA_=uxplba(A,BA,C,D,E,F,G,H,I,J)
END

INTEGER(c_int) FUNCTION WR_UYPLBA_(A,B,C,D,E,F,G,H,I,J,K) BIND(C,NAME='uyplba__')
  USE iso_c_binding
  REAL(c_float) :: A(*)
  CHARACTER(1,c_char) :: B(*)
  INTEGER(c_int) :: C
  INTEGER(c_int) :: D
  REAL(c_float) :: E(*)
  REAL(c_float) :: F(*)
  REAL(c_float) :: G(*)
  INTEGER(c_int) :: H(*)
  INTEGER(c_int) :: I(*)
  INTEGER(c_int) :: J(*)
  INTEGER(c_int),VALUE :: K
! CVBCL 10
  CHARACTER(C) :: BA(D)

  CALL CHAR_TRIM2(B,BA,C,D,K)

  WR_UYPLBA_=uyplba(A,BA,C,D,E,F,G,H,I,J)
END

INTEGER(c_int) FUNCTION WR_UXPLBB_(A,B,C,D,E,F,G,H,I,J,K,L,M,N) BIND(C,NAME='uxplbb__')
  USE iso_c_binding
  REAL(c_float) :: A(*)
  CHARACTER(1,c_char) :: B(*)
  INTEGER(c_int) :: C
  INTEGER(c_int) :: D
  REAL(c_float) :: E(*)
  REAL(c_float) :: F(*)
  REAL(c_float) :: G(*)
  INTEGER(c_int) :: H(*)
  INTEGER(c_int) :: I(*)
  INTEGER(c_int) :: J(*)
  REAL(c_float) :: K(*)
  INTEGER(c_int) :: L(*)
  INTEGER(c_int) :: M(*)
  INTEGER(c_int),VALUE :: N
! CVBCL 13
  CHARACTER(C) :: BA(D)

  CALL CHAR_TRIM2(B,BA,C,D,N)

  WR_UXPLBB_=uxplbb(A,BA,C,D,E,F,G,H,I,J,K,L,M)
END

INTEGER(c_int) FUNCTION WR_UYPLBB_(A,B,C,D,E,F,G,H,I,J,K,L,M,N) BIND(C,NAME='uyplbb__')
  USE iso_c_binding
  REAL(c_float) :: A(*)
  CHARACTER(1,c_char) :: B(*)
  INTEGER(c_int) :: C
  INTEGER(c_int) :: D
  REAL(c_float) :: E(*)
  REAL(c_float) :: F(*)
  REAL(c_float) :: G(*)
  INTEGER(c_int) :: H(*)
  INTEGER(c_int) :: I(*)
  INTEGER(c_int) :: J(*)
  REAL(c_float) :: K(*)
  INTEGER(c_int) :: L(*)
  INTEGER(c_int) :: M(*)
  INTEGER(c_int),VALUE :: N
! CVBCL 13
  CHARACTER(C) :: BA(D)

  CALL CHAR_TRIM2(B,BA,C,D,N)

  WR_UYPLBB_=uyplbb(A,BA,C,D,E,F,G,H,I,J,K,L,M)
END
