!-------------------------------------------------
!interface module of chklib
!-------------------------------------------------
module chk_interface
  interface

    function lchrb(c)                             !空白かどうかを判別する．
      character(len=1), intent(in) :: c           !文字種類を調べる長さ1の文字型の引数
      logical lchrb                               !c が空白なら .true., そうでなければ.false.を返す論理型関数値
    end function
      
    function lchrc(c)                             !通貨記号かどうかを判別する
      character(len=1), intent(in) :: c           !文字種類を調べる長さ1の文字型の引数
      logical lchrc                               !c が通貨記号なら .true., そうでなければ.false.を返す論理型関数値
    end function
      
    function lchrs(c)                             !特殊文字かどうかを判別する
      character(len=1), intent(in) :: c           !文字種類を調べる長さ1の文字型の引数
      logical lchrs                               !c が特殊文字なら .true., そうでなければ.false.を返す論理型関数値．
    end function
      
    function lchrl(c)                             !英字かどうかを判別する
      character(len=1), intent(in) :: c           !文字種類を調べる長さ1の文字型の引数
      logical lchrl                               !c が英字なら .true., そうでなければ.false.を返す論理型関数値．
    end function
      
    function lchrd(c)                             !数字かどうかを判別する
      character(len=1), intent(in) :: c           !文字種類を調べる長さ1の文字型の引数
      logical lchrd                               !c が数字なら .true., そうでなければ.false.を返す論理型関数値
    end function
      
    function lchra(c)                             !英数字かどうかを判別する
      character(len=1), intent(in) :: c           !文字種類を調べる長さ1の文字型の引数
      logical lchra                               !c が英数字なら .true., そうでなければ.false.を返す論理型関数値
    end function
      
    function lchrf(c)                             !fortran文字かどうかを判別する
      character(len=1), intent(in) :: c           !文字種類を調べる長さ1の文字型の引数
      logical lchrf                               !c がfortran文字なら .true., そうでなければ.false.を返す論理型関数値
    end function
      
    function lchr(char,cref)                      !文字列の種類を判別する． 調べる文字列の種類は，テンプレートとして別の文字列で与える．
      character(len=*), intent(in) :: char        !文字種類を調べる文字列
      character(len=*), intent(in) :: cref        !文字列の種類を与えるテンプレート文字列
      logical lchr                                !charがcrefで指定した 文字種と一致していれば.true., そうでなければ .false.を返す論理型関数値
    end function

  end interface
end module
!chklib library end ----
