!-------------------------------------------------
!interface module of datelib
!-------------------------------------------------
module dat_interface
  interface

    subroutine date12(idate,iy,itd)               !1型の日付を2型の日付に変換する．
      integer,   intent(in) :: idate              !1型の日付
      integer,   intent(out) :: iy                !年
      integer,   intent(out) :: itd               !通しの日付
    end subroutine
      
    subroutine date13(idate,iy,im,id)             !1型の日付を3型の日付に変換する．
      integer,   intent(in) :: idate              !1型の日付
      integer,   intent(out) :: iy                !年
      integer,   intent(out) :: im                !月
      integer,   intent(out) :: id                !日
    end subroutine
      
    subroutine date21(idate,iy,itd)               !2型の日付を1型の日付に変換する．
      integer,   intent(out) :: idate              !1型の日付
      integer,   intent(in) :: iy                !年
      integer,   intent(in) :: itd               !通しの日付
    end subroutine
      
    subroutine date23(iy,im,id,itd)               !2型の日付を3型の日付に変換する．
      integer,   intent(inout) :: iy                 !年
      integer,   intent(out) :: im                !月
      integer,   intent(out) :: id                !日
      integer,   intent(in) :: itd               !通しの日付
    end subroutine
      
    subroutine date31(idate,iy,im,id)             !3型の日付を1型の日付に変換する．
      integer,   intent(out) :: idate              !1型の日付
      integer,   intent(in) :: iy                !年
      integer,   intent(in) :: im                !月
      integer,   intent(in) :: id                !日
    end subroutine
      
    subroutine date32(iy,im,id,itd)               !3型の日付を2型の日付に変換する．
      integer,   intent(inout) :: iy                 !年
      integer,   intent(in) :: im                 !月
      integer,   intent(in) :: id                 !日
      integer,   intent(out) :: itd               !通しの日付
    end subroutine
      
    subroutine datef1(n,idate,ndate)              !idateのn日後(ndate)を求める．
      integer,   intent(in) :: idate              !1型の日付
      integer,   intent(out) :: ndate             !1型の日付
      integer,   intent(in) :: n                  !日付の差（日数）.
    end subroutine
      
    subroutine datef2(n,iy,itd,ny,ntd)            !iy,itdのn日後(ny,ntd)を求める．
      integer,   intent(in) :: n                  !日付の差（日数）.
      integer,   intent(in) :: iy                 !年
      integer,   intent(in) :: itd                !通しの日付
      integer,   intent(out) :: ny                !年
      integer,   intent(out) :: ntd               !通しの日付
    end subroutine
      
    subroutine datef3(n,iy,im,id,ny,nm,nd)        !iy,im,idのn日後(ny,nm,nd)を求める．
      integer,   intent(in) :: n                  !日付の差（日数）.
      integer,   intent(in) :: iy                 !年
      integer,   intent(in) :: im                 !月
      integer,   intent(in) :: id                 !日
      integer,   intent(out) :: ny                !年
      integer,   intent(out) :: nm                !月
      integer,   intent(out) :: nd                !日
    end subroutine
      
    subroutine dateg1(n,idate,ndate)              !idateの何(n)日後がndateかを求める．
      integer,   intent(in) :: idate              !1型の日付
      integer,   intent(in) :: ndate              !1型の日付
      integer,   intent(out) :: n                 !日付の差（日数）
    end subroutine
      
    subroutine dateg2(n,iy,itd,ny,ntd)            !iy,itdの何(n)日後がny,ntdかを求める．
      integer,   intent(in) :: iy                 !年
      integer,   intent(in) :: ny                 !年
      integer,   intent(in) :: itd                !通しの日付
      integer,   intent(in) :: ntd                !通しの日付
      integer,   intent(out) :: n                 !日付の差（日数）
    end subroutine
      
    subroutine dateg3(n,iy,im,id,ny,nm,nd)        !iy,im,idの何(n)日後がny,nm,ndかを求める
      integer,   intent(in) :: iy                 !年
      integer,   intent(in) :: ny                 !年
      integer,   intent(in) :: im                 !月
      integer,   intent(in) :: nm                 !月
      integer,   intent(in) :: id                 !日
      integer,   intent(in) :: nd                 !日
      integer,   intent(out) :: n                 !日付の差（日数）
    end subroutine
      
    subroutine dateq1(idate)                      !今日の1型の日付を求める．
      integer,   intent(out) :: idate             !1型の日付
    end subroutine
      
    subroutine dateq2(iy,itd)                     !今日の2型の日付を求める．
      integer,   intent(out) :: iy                !年
      integer,   intent(out) :: itd               !通しの日付
    end subroutine
      
    subroutine dateq3(iy,im,id)                   !今日の3型の日付を求める．
      integer,   intent(out) :: iy                !年
      integer,   intent(out) :: im                !月
      integer,   intent(out) :: id                !日
    end subroutine
      
    subroutine datec1(cform,idate)                !idateをcformに従って表現してcformで返す
      integer,   intent(in) :: idate              !1型の日付
      character(len=*), intent(inout) :: cform    !日付のフォーマット
    end subroutine
      
    subroutine datec2(cform,iy,itd)               !iy,itdをcformに従って表現してcformで返す
      integer,   intent(in) :: iy                 !年
      integer,   intent(in) :: itd                !通しの日付
      character(len=*), intent(inout) :: cform    !日付のフォーマット
    end subroutine
      
    subroutine datec3(cform,iy,im,id)             !iy,im,idをcformに従って表現してcformで返す
      integer,   intent(in) :: iy                 !年
      integer,   intent(in) :: im                 !月
      integer,   intent(in) :: id                 !日
      character(len=*), intent(inout) :: cform    !日付のフォーマット
    end subroutine
!----------------------------------------------------------------
    function ndate1(idate,ndate)                  !idateの何(n)日後がndateかを求める
      integer,   intent(in) :: idate              !1型の日付
      integer,   intent(in) :: ndate              !1型の日付
      integer ndate1                              !1型の日付の差を与える関数値
    end function
      
    function ndate2(iy,itd,ny,ntd)                !iy,itdの何(n)日後がny,ntdかを求める．
      integer,   intent(in) :: iy                 !年
      integer,   intent(in) :: ny                 !年
      integer,   intent(in) :: itd                !通しの日付
      integer,   intent(in) :: ntd                !通しの日付
      integer ndate2                              !2型の日付の差を与える関数値
    end function
      
    function ndate3(iy,im,id,ny,nm,nd)            !iy,im,idの何(n)日後がny,nm,ndかを求める
      integer,   intent(in) :: iy                 !年
      integer,   intent(in) :: ny                 !年
      integer,   intent(in) :: im                 !月
      integer,   intent(in) :: nm                 !月
      integer,   intent(in) :: id                 !日
      integer,   intent(in) :: nd                 !日
      integer ndate3                              !3型の日付の差を与える関数値
    end function
      
    function iweek1(idate)                        !1型の日付idateに対応する曜日番号を返す
      integer,   intent(in) :: idate              !1型の日付
      integer iweek1                              !1型の日付に対応する曜日番号を与える関数値
    end function
      
    function iweek2(iy,itd)                       !2型の日付iy,itdに対応する曜日番号を返す．
      integer,   intent(in) :: iy                 !年
      integer,   intent(in) :: itd                !通しの日付
      integer iweek2                              !2型の日付に対応する曜日番号を与える関数値
    end function
      
    function iweek3(iy,im,id)                     !3型の日付iy,im, idに対応する曜日番号を返す．
      integer,   intent(in) :: iy                 !年
      integer,   intent(in) :: im                 !月
      integer,   intent(in) :: id                 !日
      integer iweek3                              !3型の日付に対応する曜日番号を与える関数値
    end function
      
    function ndmon(iy,im)                         !iy年im月は何日あるかを返す．
      integer,   intent(in) :: iy                 !年
      integer,   intent(in) :: im                 !月
      integer ndmon                               !指定した月の日数を与える関数値
    end function
      
    function ndyear(iy)                           !iy年は何日あるかを返す．
      integer,   intent(in) :: iy                 !年
      integer ndyear                              !指定した年の日数を与える関数値
    end function
      
    function cmon(im)                             !文字型の月名を返す文字型関数．
      integer,   intent(in) :: im                 !月
      character(len=9)      :: cmon               !文字型の月名を返す文字型関数値
    end function
      
    function cweek(iw)                            !文字型の曜日を返す文字型関数．
      integer,   intent(in) :: iw                 !曜日番号
      character(len=9)      :: cweek              !文字型の曜日を返す文字型関数値
    end function

  end interface
end module
!datelib library end ----
