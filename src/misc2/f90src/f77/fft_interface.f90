!-------------------------------------------------
!interface module of fftlib
!-------------------------------------------------
module fft_interface
  interface
!---------------------------------------------------------
!周期実数値データのフーリエ変換
    subroutine rffti(n,wsave)                     !初期化をおこなう
      integer,   intent(in) :: n                  !処理するデータの長さ
      real,      intent(out), dimension(*) :: wsave !作業用配列．長さは少なくとも2n+15以上
    end subroutine
      
    subroutine rfftf(n,r,wsave)                   !フーリエ順変換をおこなう
      integer,   intent(in) :: n                  !処理するデータの長さ
      real,      intent(in), dimension(*) :: wsave !作業用配列．長さは少なくとも2n+15以上
      real,      intent(inout), dimension(*) :: r !処理する実数型配列
    end subroutine
      
    subroutine rfftb(n,r,wsave)                   !フーリエ逆変換をおこなう．
      integer,   intent(in) :: n                  !処理するデータの長さ
      real,      intent(in), dimension(*) :: wsave !作業用配列．長さは少なくとも2n+15以上
      real,      intent(inout), dimension(*) :: r !処理する実数型配列
    end subroutine
!---------------------------------------------------------
!rffti, rfftf, rfftbの簡易型サブルーチン
    subroutine ezffti(n,wsave)                    !初期化をおこなう;
      integer,   intent(in) :: n                  !処理するデータの長さ
      real,      intent(out), dimension(*) :: wsave !作業用配列
    end subroutine
      
    subroutine ezfftf(n,r,a0,a,b,wsave)           !フーリエ順変換をおこなう;
      integer,   intent(in) :: n                  !処理するデータの長さ
      real,      intent(in), dimension(*) :: wsave !作業用配列
      real,      intent(in), dimension(*) :: r    !処理する実数型配列
      real,      intent(out) :: a0                !上記定義における a_0 および a_0
      real,      intent(out), dimension(*) :: a   !nが偶数のとき n/2, nが奇数のとき(n-1)/2の長さの実数型配列
      real,      intent(out), dimension(*) :: b   !
    end subroutine
      
    subroutine ezfftb(n,r,a0,a,b,wsave)           !ezfftbはフーリエ逆変換をおこなう．
      integer,   intent(in) :: n                  !処理するデータの長さ
      real,      intent(in), dimension(*) :: wsave !作業用配列
      real,      intent(out), dimension(*) :: r   !処理する実数型配列
      real,      intent(in) :: a0                 !上記定義における a_0 および a_0
      real,      intent(in), dimension(*) :: a    !nが偶数のとき n/2, nが奇数のとき(n-1)/2の長さの実数型配列
      real,      intent(in), dimension(*) :: b 
    end subroutine
!---------------------------------------------------------
!奇の周期データのsine変換をおこなう． 
    subroutine sinti(n,wsave)                     !初期化をおこなう;
      integer,   intent(in) :: n                  !処理するデータの長さ
      real,      intent(out), dimension(*) :: wsave !作業用配列
    end subroutine
      
    subroutine sint(n,r,wsave)                    !sine変換をおこなう．
      integer,   intent(in) :: n                  !処理するデータの長さ
      real,      intent(in), dimension(*) :: wsave !作業用配列
      real,      intent(inout), dimension(*) :: r !処理する実数型配列
    end subroutine
!---------------------------------------------------------
!偶の周期データのcosine変換をおこなう 
    subroutine costi(n,wsave)                     !初期化をおこなう
      integer,   intent(in) :: n                  !処理するデータの長さ
      real,      intent(out), dimension(*) :: wsave !作業用配列
    end subroutine
      
    subroutine cost(n,r,wsave)                    !cosine変換をおこなう．
      integer,   intent(in) :: n                  !処理するデータの長さ
      real,      intent(in), dimension(*) :: wsave !作業用配列
      real,      intent(inout), dimension(*) :: r !処理する実数型配列
    end subroutine
!---------------------------------------------------------
!奇数波数成分のみのsin変換をおこなう．
    subroutine sinqi(n,wsave)                     !初期化をおこなう
      integer,   intent(in) :: n                  !処理するデータの長さ
      real,      intent(out), dimension(*) :: wsave !作業用配列
    end subroutine
      
    subroutine sinqf(n,r,wsave)                   !sine順変換をおこなう
      integer,   intent(in) :: n                  !処理するデータの長さ
      real,      intent(in), dimension(*) :: wsave !作業用配列
      real,      intent(inout), dimension(*) :: r !処理する実数型配列
    end subroutine
      
    subroutine sinqb(n,r,wsave)                   !逆変換をおこなう．
      integer,   intent(in) :: n                  !処理するデータの長さ
      real,      intent(in), dimension(*) :: wsave !作業用配列
      real,      intent(inout), dimension(*) :: r !処理する実数型配列
    end subroutine
!---------------------------------------------------------
!偶数波数成分のみのcossine変換をおこなう．
    subroutine cosqi(n,wsave)                     !初期化をおこなう
      integer,   intent(in) :: n                  !処理するデータの長さ
      real,      intent(out), dimension(*) :: wsave !作業用配列
    end subroutine
      
    subroutine cosqf(n,r,wsave)                   !cosine順変換をおこなう
      integer,   intent(in) :: n                  !処理するデータの長さ
      real,      intent(in), dimension(*) :: wsave !作業用配列
      real,      intent(inout), dimension(*) :: r !処理する実数型配列
    end subroutine
      
    subroutine cosqb(n,r,wsave)                   !cosine逆変換をおこなう．
      integer,   intent(in) :: n                  !処理するデータの長さ
      real,      intent(in), dimension(*) :: wsave !作業用配列
      real,      intent(inout), dimension(*) :: r !処理する実数型配列
    end subroutine
!---------------------------------------------------------
!周期複素数データのフーリエ変換をおこなう
    subroutine cffti(n,wsave)                     !初期化をおこなう
      integer,   intent(in) :: n                  !処理するデータの長さ
      real,      intent(out), dimension(*) :: wsave !作業用配列
    end subroutine
      
    subroutine cfftf(n,c,wsave)                   !フーリエ順変換をおこなう
      integer,   intent(in) :: n                  !処理するデータの長さ
      real,      intent(in), dimension(*) :: wsave !作業用配列
      complex,   intent(inout), dimension(*) :: c !処理する複素数型配列
    end subroutine
      
    subroutine cfftb(n,c,wsave)                   !フーリエ逆変換をおこなう．
      integer,   intent(in) :: n                  !処理するデータの長さ
      real,      intent(in), dimension(*) :: wsave !作業用配列
      complex,   intent(inout), dimension(*) :: c !処理する複素数型配列
    end subroutine

  end interface
end module
!fftlib library end ----
