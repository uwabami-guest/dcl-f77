!-------------------------------------------------
!interface module of hexlib
!-------------------------------------------------
module hex_interface
  interface

    subroutine hexdic(ip,cp)                      !ビットパターンを16進表現の文字列化する．
      integer,   intent(in) :: ip                 !ビットパターンを調べる 1語長の引数
      character(len=*), intent(out) :: cp         !16進表現の文字列を返す文字型の引数
    end subroutine
      
    subroutine hexdci(cp,ip)                      !16進表現の文字列をビットパターン化する．
      character(len=*), intent(in) :: cp          !16進表現の文字列を与える文字型の引数
      integer,   intent(out) :: ip                !ビットパターンを返す 1語長の引数
    end subroutine

  end interface
end module
!hexlib library end ----
