!-------------------------------------------------
!interface module of odelib
!-------------------------------------------------
module ode_interface
  interface
!----------------------------------------------------------------------
!アルゴリズムルーチン
    subroutine odrkg(n,fcn,t,dt,x,dx,xout,work)   !ルンゲ-クッタ-ギル
      integer,   intent(in) :: n                  !被積分変数（方程式）の数
      external  fcn                                !dxを計算するサブルーチン名
      real,      intent(in) :: t                  !独立変数tの値
      real,      intent(in) :: dt                 !積分ステップ幅.
      real,      intent(in), dimension(n) :: x    !被積分変数のt=t における値
      real,      intent(inout), dimension(n) :: dx !被積分変数のt=t における微分値
      real,      intent(inout), dimension(n) :: xout !被積分変数のt=t+dtにおける値
      real,      intent(out), dimension(n) :: work !作業変数
    end subroutine
      
    subroutine odrk4(n,fcn,t,dt,x,dx,xout,work)   !4次精度のルンゲ-クッタ
      integer,   intent(in) :: n                  !被積分変数（方程式）の数
      external  fcn                                !dxを計算するサブルーチン名
      real,      intent(in) :: t                  !独立変数tの値
      real,      intent(in) :: dt                 !積分ステップ幅
      real,      intent(in), dimension(n) :: x    !被積分変数のt=t における値
      real,      intent(inout), dimension(n) :: dx !被積分変数のt=t における微分値
      real,      intent(inout), dimension(n) :: xout !被積分変数のt=t+dtにおける値
      real,      intent(out), dimension(n,3) :: work !作業変数
    end subroutine
      
    subroutine odrk2(n,fcn,t,dt,x,dx,xout,work)   !2次精度のルンゲ-クッタ
      integer,   intent(in) :: n                  !被積分変数（方程式）の数
      external  fcn                                !dxを計算するサブルーチン名
      real,      intent(in) :: t                  !独立変数tの値
      real,      intent(in) :: dt                 !積分ステップ幅
      real,      intent(in), dimension(n) :: x    !被積分変数のt=t における値
      real,      intent(inout), dimension(n) :: dx !被積分変数のt=t における微分値
      real,      intent(inout), dimension(n) :: xout !被積分変数のt=t+dtにおける値
      real,      intent(out), dimension(n) :: work !作業変数
    end subroutine
      
    subroutine odrk1(n,fcn,t,dt,x,dx,xout,work)   !が1次精度のルンゲ-クッタである．
      integer,   intent(in) :: n                  !被積分変数（方程式）の数
      external  fcn                                !dxを計算するサブルーチン名
      real,      intent(in) :: t                  !独立変数tの値
      real,      intent(in) :: dt                 !積分ステップ幅
      real,      intent(in), dimension(n) :: x    !被積分変数のt=t における値
      real,      intent(inout), dimension(n) :: dx !被積分変数のt=t における微分値
      real,      intent(inout), dimension(n) :: xout !被積分変数のt=t+dtにおける値
      real,      intent(out), dimension(n) :: work !作業変数
    end subroutine
!--------------------------------------------------------------------
!ステッパルーチン
    subroutine odrkgr(n,fcn,t,dt,epsl,x,work)     !可変幅のきざみを求める
      integer,   intent(in) :: n                  !被積分変数（方程式）の数
      external  fcn                                !dxを計算するサブルーチン名
      real,      intent(in) :: t                  !独立変数tの値
      real,      intent(inout) :: dt              !積分ステップ幅
      real,      intent(in) :: epsl               !要求精度
      real,      intent(inout), dimension(n) :: x !被積分変数のt=t における値を入力し,t=t+ 2 における値 を出力する．
      real,      intent(out), dimension(n,5) :: work !作業変数
    end subroutine
      
    subroutine odrkgs(n,fcn,t,dt,epsl,x,work)     !固定幅のきざみを求める．
      integer,   intent(in) :: n                  !被積分変数（方程式）の数
      external  fcn                                !dxを計算するサブルーチン名
      real,      intent(in) :: t                  !独立変数tの値
      real,      intent(inout) :: dt              !積分ステップ幅
      real,      intent(in) :: epsl               !要求精度
      real,      intent(inout), dimension(n) :: x !被積分変数のt=t における値を入力し,t=t+ 2 における値 を出力する
      real,      intent(out), dimension(n,3) :: work !作業変数
    end subroutine
      
    subroutine odrk4r(n,fcn,t,dt,epsl,x,work)     !可変幅のきざみを求める．
      integer,   intent(in) :: n                  !被積分変数（方程式）の数
      external  fcn                                !dxを計算するサブルーチン名
      real,      intent(in) :: t                  !独立変数tの値
      real,      intent(inout) :: dt              !積分ステップ幅
      real,      intent(in) :: epsl               !要求精度
      real,      intent(inout), dimension(n) :: x !被積分変数のt=t における値を入力し,t=t+ 2 における値 を出力する
      real,      intent(out), dimension(n,7) :: work !作業変数
    end subroutine
      
    subroutine odrk4s(n,fcn,t,dt,epsl,x,work)     !固定幅のきざみを求める．  
      integer,   intent(in) :: n                  !被積分変数（方程式）の数
      external  fcn                                !dxを計算するサブルーチン名
      real,      intent(in) :: t                  !独立変数tの値
      real,      intent(inout) :: dt              !積分ステップ幅
      real,      intent(in) :: epsl               !要求精度
      real,      intent(inout), dimension(n) :: x !被積分変数のt=t における値を入力し,t=t+ 2 における値 を出力する
      real,      intent(out), dimension(n,5) :: work !作業変数
    end subroutine
!-----------------------------------------------------------------
!ドライバルーチン 
    subroutine odrkdu(n,algor,fcn,t,tend,istep,x,work) !固定刻幅で積分を行う．   
      integer,   intent(in) :: n                  !被積分変数（方程式）の数
      external  algor                              !使用するアルゴリズムルーチン名
      external  fcn                                !dxを計算するサブルーチン名
      real,      intent(inout) :: t               !積分を始める独立変数tの値．
      real,      intent(in) :: tend               !積分を終了する独立変数tの値
      integer,   intent(in) :: istep              !ステップ数．
      real,      intent(inout), dimension(n) :: x !被積分変数のt=tにおける値を入力し， t=tendにおける値を出力する．
      real,      intent(out), dimension(n,*) :: work !作業変数
    end subroutine
      
    subroutine odrkdv(n,steper,fcn,t,tint,dt,x,work) !要求精度を満たすように積分を行う．
      integer,   intent(in) :: n                  !被積分変数（方程式）の数
      external  steper                             !使用するアルゴリズムルーチン名
      external  fcn                                !dxを計算するサブルーチン名
      real,      intent(inout) :: t               !積分を始める独立変数tの値
      real,      intent(in) :: tint               !積分を終了する独立変数tの値
      real,      intent(inout) :: dt              !ステップ幅の初期値
      real,      intent(inout), dimension(n) :: x !被積分変数のt=tにおける値を入力し， t=tendにおける値を出力する．
      real,      intent(out), dimension(n,*) :: work !作業変数
    end subroutine
!--------------------------------------------------------------------
!内部変数管理ルーチン
    subroutine odpget(cpara,ipara)                !odrkdv で使用する内部変数を参照する．
      character(len=*), intent(in) :: cpara       !内部変数の名前
      integer,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine odiget(cpara,ipara)                !odrkdv で使用する内部変数を参照する．
      character(len=*), intent(in) :: cpara       !内部変数の名前
      integer,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine odrget(cpara,ipara)                !odrkdv で使用する内部変数を参照する．
      character(len=*), intent(in) :: cpara       !内部変数の名前
      real,      intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine odlget(cpara,ipara)                !odrkdv で使用する内部変数を参照する．
      character(len=*), intent(in) :: cpara       !内部変数の名前
      logical,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine odpset(cpara,ipara)                !odrkdv で使用する内部変数を設定する．
      character(len=*), intent(in) :: cpara       !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine odiset(cpara,ipara)                !odrkdv で使用する内部変数を設定する．
      character(len=*), intent(in) :: cpara       !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine odrset(cpara,ipara)                !odrkdv で使用する内部変数を設定する．
      character(len=*), intent(in) :: cpara       !内部変数の名前
      real,      intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine odlset(cpara,ipara)                !odrkdv で使用する内部変数を設定する．
      character(len=*), intent(in) :: cpara       !内部変数の名前
      logical,   intent(in) :: ipara              !内部変数の値
    end subroutine

  end interface
end module
!odelib library end ----
