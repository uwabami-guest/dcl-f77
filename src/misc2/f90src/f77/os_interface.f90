!-------------------------------------------------
!interface module of oslib
!-------------------------------------------------
module os_interface
  interface

    subroutine osexec(cmd)                        !os コマンドを実行する．
      character(len=*), intent(in) :: cmd         !コマンド名
    end subroutine
      
    subroutine osgenv(cename,cval)                !環境変数の値を取得する．
      character(len=*), intent(in) :: cename      !環境変数名
      character(len=*), intent(out) :: cval       !環境変数の値
    end subroutine
      
    subroutine osqarn(n)                          !コマンドライン引数の数 N を得る．
      integer,   intent(out) :: n                  !コマンドライン引数の数
    end subroutine
      
    subroutine osgarg(n,char)                     !N 番目のコマンドライン引数 CHAR を得る． 
      integer,   intent(in) :: n                  !コマンドライン引数の位置
      character(len=*), intent(out) :: char       !n 番目のコマンドライン引数
    end subroutine
      
    subroutine osabrt()                           !エラー処理をおこなって強制終了する．
    end subroutine

  end interface
end module
!oslib library end ----
