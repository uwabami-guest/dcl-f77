!-------------------------------------------------
!     interface module of sgpack
!-------------------------------------------------
module sg_interface
    interface
!---------------------------------------------------------
!コントロール
        subroutine sgopn(iws)  !出力装置のオープン
            integer,   intent(in) :: iws !ワークステーション番号                        
        end subroutine
                                  
        subroutine sgfrm()  !フレームの設定．
        end subroutine

        subroutine sgcls()  !出力装置のクローズ
        end subroutine

        subroutine sgpget(cp,ipara) !内部変数の参照．
            character(len=*), intent(in) :: cp !内部変数の名前
            integer,   intent(out) :: ipara       !内部変数の値
        end subroutine

        subroutine sgiget(cp,ipara) 
            character(len=*), intent(in) :: cp !内部変数の名前
            integer,   intent(out) :: ipara       !内部変数の値
        end subroutine

        subroutine sgrget(cp,ipara) 
            character(len=*), intent(in) :: cp !内部変数の名前
            real,      intent(out) :: ipara       !内部変数の値
        end subroutine

        subroutine sglget(cp,ipara) 
            character(len=*), intent(in) :: cp !内部変数の名前
            logical,   intent(out) :: ipara       !内部変数の値
        end subroutine

        subroutine sgpset(cp,ipara)
            character(len=*), intent(in) :: cp !内部変数の名前
            integer,   intent(in) :: ipara       !内部変数の値
        end subroutine

        subroutine sgiset(cp,ipara)
            character(len=*), intent(in) :: cp !内部変数の名前
            integer,   intent(in) :: ipara       !内部変数の値
        end subroutine

        subroutine sgrset(cp,ipara)
            character(len=*), intent(in) :: cp !内部変数の名前
            real,      intent(in) :: ipara       !内部変数の値
        end subroutine

        subroutine sglset(cp,ipara)
            character(len=*), intent(in) :: cp !内部変数の名前
            logical,   intent(in) :: ipara       !内部変数の値
        end subroutine

        subroutine sgpstx(cp,ipara)
            character(len=*), intent(in) :: cp !内部変数の名前
            integer,   intent(in) :: ipara       !内部変数の値
        end subroutine

        subroutine sgistx(cp,ipara)
            character(len=*), intent(in) :: cp !内部変数の名前
            integer,   intent(in) :: ipara       !内部変数の値
        end subroutine

        subroutine sgrstx(cp,ipara)
            character(len=*), intent(in) :: cp !内部変数の名前
            real,      intent(in) :: ipara       !内部変数の値
        end subroutine

        subroutine sglstx(cp,ipara)
            character(len=*), intent(in) :: cp !内部変数の名前
            logical,   intent(in) :: ipara       !内部変数の値
        end subroutine

        subroutine sgpwsn() !ワークステーション名のリスト
        end subroutine
!------------------------------------------------------------------------
!正規化変換
        subroutine sgsvpt(vxmin,vxmax,vymin,vymax)  !ビューポートの設定．
            real,      intent(in) :: vxmin  !ビューポート
            real,      intent(in) :: vxmax
            real,      intent(in) :: vymin
            real,      intent(in) :: vymax
        end subroutine

        subroutine sgswnd(uxmin,uxmax,uymin,uymax) !ウインドウの設定．
            real,      intent(in) :: uxmin !ウインドウ
            real,      intent(in) :: uxmax
            real,      intent(in) :: uymin
            real,      intent(in) :: uymax
        end subroutine

        subroutine sgssim(simfac,vxoff,vyoff)  !相似変換の設定．
            real,      intent(in) :: simfac  !スケーリングファクター
            real,      intent(in) :: vxoff     !原点のオフセット
            real,      intent(in) :: vyoff   
        end subroutine

        subroutine sgsmpl(plx,ply,plrot)  !地図投影の極の設定．
            real,      intent(in) :: plx   !地図投影の際の回転角
            real,      intent(in) :: ply   
            real,      intent(in) :: plrot
        end subroutine

        subroutine sgstrn(itr) !変換関数の番号による設定．
            integer,   intent(in) :: itr   !変換関数番号
        end subroutine

        subroutine sgtrsl(cts,ctl) !略称から名称を求める．
            character(len=*), intent(in) :: cts 
      character(len=*), intent(out) :: ctl
        end subroutine

        subroutine sgtrsn(cts,ntx) !略称から変換関数番号を求める．
            character(len=*), intent(in) :: cts 
      integer,   intent(out) :: ntx
        end subroutine

        subroutine sgtrls(ctl,cts) !名称から略称を求める．
      character(len=*), intent(in) :: ctl
            character(len=*), intent(out) :: cts 
        end subroutine

        subroutine sgtrln(ctl,ntx) !名称から変換関数番号を求める．
      character(len=*), intent(in) :: ctl
            integer,   intent(out) :: ntx
        end subroutine

        subroutine sgtrns(ntx,cts) !変換関数番号から略称を求める．
            integer,   intent(in) :: ntx
      character(len=*), intent(out) :: cts
        end subroutine

        subroutine sgtrnl(ntx,ctl) !変換関数番号から名称を求める．
            integer,   intent(in) :: ntx
      character(len=*), intent(out) :: ctl
        end subroutine

        subroutine sgstrf()  !変換関数の確定．   
        end subroutine

        subroutine sgqvpt(vxmin,vxmax,vymin,vymax) 
            real,      intent(out) :: vxmin      !ビューポート
            real,      intent(out) :: vxmax      
            real,      intent(out) :: vymin      
            real,      intent(out) :: vymax      
        end subroutine

        subroutine sgqwnd(uxmin,uxmax,uymin,uymax) 
            real,      intent(out) :: uxmin      !ウインドウ
            real,      intent(out) :: uxmax      
            real,      intent(out) :: uymin      
            real,      intent(out) :: uymax      
        end subroutine

        subroutine sgqsim(simfac,vxoff,vyoff)  
            real,      intent(out) :: simfac !相似変換のスケーリングファクター
            real,      intent(out) :: vxoff     !原点のオフセット
            real,      intent(out) :: vyoff  
        end subroutine

        subroutine sgqmpl(plx,ply,plrot)  
            real,      intent(out) :: plx     !地図投影の際の回転角
            real,      intent(out) :: ply     
            real,      intent(out) :: plrot     
        end subroutine

        subroutine sgqtrn(itr) 
            integer,   intent(out) :: itr     !変換関数番号
        end subroutine
!------------------------------------------------------------------------
!ポリラインプリミティブ
        subroutine sgplzu(n,upx,upy,itype,index) !u 座標系で折れ線を描く．  
            integer,   intent(in) :: n                 !配列UPX, UPYなどの長さ
            real,      intent(in), dimension(*) :: upx !U座標の値を与える
            real,      intent(in), dimension(*) :: upy !U座標の値を与える                                   
            integer,   intent(in) :: itype           !ラインタイプ．
            integer,   intent(in) :: index           !ラインインデクス
        end subroutine

        subroutine sgplzv(n,vpx,vpy,itype,index)  !v 座標系で折れ線を描く．    
            integer,   intent(in) :: n               !配列VPX, VPYなどの長さ
            real,      intent(in), dimension(*) :: vpx !V座標の値を与える
            real,      intent(in), dimension(*) :: vpy !V座標の値を与える
            integer,   intent(in) :: itype               !ラインタイプ．
            integer,   intent(in) :: index           !ラインインデクス
        end subroutine

        subroutine sgplzr(n,rpx,rpy,itype,index)  !r 座標系で折れ線を描く．  
            integer,   intent(in) :: n            !配列RPX, RPYなどの長さ
            real,      intent(in), dimension(*) :: rpx !r座標の値を与える
            real,      intent(in), dimension(*) :: rpy !r座標の値を与える
            integer,   intent(in) :: itype           !ラインタイプ
            integer,   intent(in) :: index           !ラインインデクス
        end subroutine

        subroutine sgplu(n,upx,upy) !u 座標系で折れ線を描く．
            integer,   intent(in) :: n           !配列UPX, UPYなどの長さ
            real,      intent(in), dimension(*) :: upx !u座標の値を与える
            real,      intent(in), dimension(*) :: upy 
        end subroutine

        subroutine sgplv(n,vpx,vpy)  !v 座標系で折れ線を描く． 
            integer,   intent(in) :: n           !配列VPX, VPYなどの長さ
            real,      intent(in), dimension(*) :: vpx !v座標の値を与える
            real,      intent(in), dimension(*) :: vpy 
        end subroutine

        subroutine sgplr(n,rpx,rpy)  !r 座標系で折れ線を描く．  
            integer,   intent(in) :: n
            real,      intent(in), dimension(*) :: rpx !r座標の値を与える
            real,      intent(in), dimension(*) :: rpy 
        end subroutine

        subroutine sgsplt(itype)  !ラインタイプを設定する．   
            integer,   intent(in) :: itype    !線種
        end subroutine

        subroutine sgspli(index)  !ラインインデクスの設定．   
            integer,   intent(in) :: index    !ラインインデクス 
        end subroutine

        subroutine sgsplc(charx)  !ラベルの文字列設定．  
            character(len=*), intent(in) :: charx  !描く文字列（初期値は'A')
        end subroutine

        subroutine sgspls(rsize)  !ラベルの文字高設定  
            real,      intent(in) :: rsize  !文字列の高さをV座標系の単位で指定（初期値は0.02)                   
        end subroutine
 
        subroutine sgnplc()  !ラベルの最後の文字番号を増やす．
        end subroutine

        subroutine sgqplt(itype)  
            integer,   intent(out) :: itype       !折れ線の線種
        end subroutine

        subroutine sgqpli(index) 
            integer,   intent(out) :: index       !折れ線のラインインデクス
        end subroutine

        subroutine sgqplc(charx)   
            character(len=*), intent(out) :: charx !描く文字列（初期値は'A')
        end subroutine

        subroutine sgqpls(rsize)   
            real,      intent(out) :: rsize    !文字列の高さをV座標系の単位で指定する（初期値は0.02)
        end subroutine
!------------------------------------------------------------------------------
! ポリマーカープリミティブ
        subroutine sgpmzu(n,upx,upy,itype,index,rsize) !u 座標系でマーカー列を描く．  
            integer,   intent(in) :: n           !マーカーを打つ点のU座標
            real,      intent(in), dimension(*) :: upx
            real,      intent(in), dimension(*) :: upy 
            integer,   intent(in) :: itype           !マーカータイプ
            integer,   intent(in) :: index           !マーカーのラインインデクス
            real,      intent(in) :: rsize           !マーカーのサイズ．
        end subroutine

        subroutine sgpmzv(n,vpx,vpy,itype,index,rsize) !v 座標系でマーカー列を描く．    
            integer,   intent(in) :: n               !配列VPX, VPYのなど長さ
            real,      intent(in), dimension(*) :: vpx !マーカーを打つ点のV座標
            real,      intent(in), dimension(*) :: vpy
            integer,   intent(in) :: itype           !マーカータイプ
            integer,   intent(in) :: index           !マーカーのラインインデクス
            real,      intent(in) :: rsize           !マーカーのサイズ
        end subroutine

        subroutine sgpmzr(n,rpx,rpy,itype,index,rsize)  !r 座標系でマーカー列を描く．
            integer,   intent(in) :: n                 !配列RPX, RPYのなど長さ
            real,      intent(in), dimension(*) :: rpx !マーカーを打つ点のR座標
            real,      intent(in), dimension(*) :: rpy 
            integer,   intent(in) :: itype           !マーカータイプ
            integer,   intent(in) :: index           !マーカーのラインインデクス
            real,      intent(in) :: rsize           !マーカーのサイズ
        end subroutine

        subroutine sgpmu(n,upx,upy) !u 座標系でマーカー列を描く． 
            integer,   intent(in) :: n           !配列UPX, UPYのなど長さ
            real,      intent(in), dimension(*) :: upx !マーカーを打つ点のU座標
            real,      intent(in), dimension(*) :: upy !(X, Y)座標の値を与える
        end subroutine

        subroutine sgpmv(n,vpx,vpy)  !v 座標系でマーカー列を描く．    
            integer,   intent(in) :: n           !配列VPX, VPYのなど長さ
            real,      intent(in), dimension(*) :: vpx !マーカーを打つ点のV座標
            real,      intent(in), dimension(*) :: vpy 
        end subroutine

        subroutine sgpmr(n,rpx,rpy)  !r 座標系でマーカー列を描く．   
            integer,   intent(in) :: n           !配列RPX, RPYのなど長さ
            real,      intent(in), dimension(*) :: rpx !マーカーを打つ点のR座標
            real,      intent(in), dimension(*) :: rpy 
        end subroutine

        subroutine sgspmt(itype)  !マーカータイプの設定．    
            integer,   intent(in) :: itype    !マーカータイプ
        end subroutine

        subroutine sgspmi(index)  !マーカーのラインインデクスの設定  
            integer,   intent(in) :: index     !ラインインデクス 
        end subroutine
 
        subroutine sgspms(rsize)   !マーカーの大きさ設定．     
            real,      intent(in) :: rsize     !マーカーの大きさ
        end subroutine

        subroutine sgqpmt(itype)  
            integer,   intent(out) :: itype     !マーカータイプ
        end subroutine

        subroutine sgqpmi(index)
            integer,   intent(out) :: index     !ラインインデクス 
        end subroutine

        subroutine sgqpms(rsize)
            real,      intent(out) :: rsize     !マーカーの大きさ
        end subroutine
!------------------------------------------------------------------
!     テキスト
        subroutine sgtxzu(ux,uy,chars,rsize,irota,icent,index) !u 座標系で文字列を描く．
            real,      intent(in) :: ux          !U座標系における文字の位置
            real,      intent(in) :: uy          
            character(len=*), intent(in) :: chars !描く文字列
            real,      intent(in) :: rsize          !文字の高さ
            integer,   intent(in) :: irota          !文字列の角度
            integer,   intent(in) :: icent          !センタリングオプション
            integer,   intent(in) :: index          !文字のラインインデクス
        end subroutine

        subroutine sgtxzv(vx,vy,chars,rsize,irota,icent,index) !v 座標系で文字列を描く． 
            real,      intent(in) :: vx          !V座標系における文字の位置
            real,      intent(in) :: vy           
            character(len=*), intent(in) :: chars !描く文字列
            real,      intent(in) :: rsize          !文字の高さ
            integer,   intent(in) :: irota          !文字列の角度
            integer,   intent(in) :: icent          !センタリングオプション
            integer,   intent(in) :: index          !文字のラインインデクス
        end subroutine

        subroutine sgtxzr(rx,ry,chars,rsize,irota,icent,index) !r座標系で文字列を描く．  
            real,      intent(in) :: rx          !R座標系における文字の位置
            real,      intent(in) :: ry          
            character(len=*), intent(in) :: chars     !描く文字列
            real,      intent(in) :: rsize           !文字の高さ
            integer,   intent(in) :: irota          !文字列の角度
            integer,   intent(in) :: icent          !センタリングオプション
            integer,   intent(in) :: index           !文字のラインインデクス
        end subroutine

        subroutine sgtxu(ux,uy,chars)  !u 座標系で文字列を描く．    
            real,      intent(in) :: ux        !U座標系における文字の位置
            real,      intent(in) :: uy        
            character(len=*), intent(in) :: chars    !描く文字列
        end subroutine

        subroutine sgtxv(vx,vy,chars)  !v 座標系で文字列を描く．      
            real,      intent(in) :: vx        !V座標系における文字の位置
            real,      intent(in) :: vy
            character(len=*), intent(in) :: chars    !描く文字列
        end subroutine

        subroutine sgtxr(rx,ry,chars)  !r 座標系で文字列を描く．     
            real,      intent(in) :: rx              !R座標系における文字の位置
            real,      intent(in) :: ry        
            character(len=*), intent(in) :: chars    !描く文字列
        end subroutine
                                  
        subroutine sgstxs(rsize)  !文字の高さ設定．  
            real,      intent(in) :: rsize  !文字の高さ
        end subroutine
 
        subroutine sgstxr(irota)  !文字列の角度の設定．  
            integer,   intent(in) :: irota   !文字列の傾きを度の単位で与える 
        end subroutine

        subroutine sgstxi(index)  !文字列のラインインデクスの設定．                 
            integer,   intent(in) :: index     !ラインインデクス 
        end subroutine
 
        subroutine sgstxc(icent)  !文字列のセンタリングオプション設定
            integer,   intent(in) :: icent   !センタリングオプション
        end subroutine

        subroutine sgqtxs(rsize) 
            real,      intent(out) :: rsize  !文字の高さ
        end subroutine

        subroutine sgqtxr(irota)
            integer,   intent(out) :: irota      !文字列の傾きを度の単位で与える
        end subroutine

        subroutine sgqtxi(index) 
            integer,   intent(out) :: index      !ラインインデクス
        end subroutine

        subroutine sgqtxc(icent)  
            integer,   intent(out) :: icent   !センタリングオプション
        end subroutine
!---------------------------------------------------------------------------------------------
! トーンプリミティブ
        subroutine sgtnzu(n,upx,upy,itpat)  !u 座標系で多角形領域の塗りつぶし．
            integer,   intent(in) :: n           !配列UPX, UPYの長さ
            real,      intent(in), dimension(*) :: upx !多角形を定義する頂点のU座標
            real,      intent(in), dimension(*) :: upy 
            integer,   intent(in) :: itpat
        end subroutine

        subroutine sgtnzv(n,vpx,vpy,itpat)  !v 座標系で多角形領域の塗りつぶし
            integer,   intent(in) :: n           !配列VPX, VPYの長さ
            real,      intent(in), dimension(*) :: vpx !多角形を定義する頂点のV座標
            real,      intent(in), dimension(*) :: vpy 
            integer,   intent(in) :: itpat
        end subroutine

        subroutine sgtnzr(n,rpx,rpy,itpat)  !r 座標系で多角形領域の塗りつぶし．
            integer,   intent(in) :: n           !配列RPX, RPYの長さ
            real,      intent(in), dimension(*) :: rpx !多角形を定義する頂点のR座標
            real,      intent(in), dimension(*) :: rpy 
            integer,   intent(in) :: itpat
        end subroutine

        subroutine sgtnu(n,upx,upy)  !u 座標系で多角形領域の塗りつぶし．
            integer,   intent(in) :: n                !配列UPX, UPYの長さ
            real,      intent(in), dimension(*) :: upx !多角形を定義する頂点のU座標
            real,      intent(in), dimension(*) :: upy 
        end subroutine

        subroutine sgtnv(n,vpx,vpy)  !v 座標系で多角形領域の塗りつぶし
            integer,   intent(in) :: n           !配列VPX, VPYの長さ
            real,      intent(in), dimension(*) :: vpx !多角形を定義する頂点のV座標
            real,      intent(in), dimension(*) :: vpy 
        end subroutine

        subroutine sgtnr(n,rpx,rpy)  !r 座標系で多角形領域の塗りつぶし．
            integer,   intent(in) :: n           !配列RPX, RPYの長さ
            real,      intent(in), dimension(*) :: rpx !多角形を定義する頂点のR座標
            real,      intent(in), dimension(*) :: rpy 
        end subroutine

        subroutine sgstnp(itpat) ! トーンパターン番号設定
            integer,   intent(in) :: itpat  !トーンパターン番号
        end subroutine

        subroutine sgqtnp(itpat) !現在設定されているトーンパターン番号参照
            integer,   intent(out) :: itpat     !現在設定されているトーンパターン番号
        end subroutine
!--------------------------------------------------------------------------------------
!ラインサブプリミティブ
             subroutine sglnzu(ux1,uy1,ux2,uy2,index) !u座標系で線分を描く． 
            real,      intent(in) :: ux1    !U座標系における線分の始点の座標
            real,      intent(in) :: uy1            
            real,      intent(in) :: ux2    !U座標系における線分の終点の座標
            real,      intent(in) :: uy2
            integer,   intent(in) :: index    !SGSLNI参照
        end subroutine

        subroutine sglnzv(vx1,vy1,vx2,vy2,index) !v座標系で線分を描く． 
            real,      intent(in) :: vx1     !V座標系における線分の始点の座標
            real,      intent(in) :: vy1            
            real,      intent(in) :: vx2     !V座標系における線分の終点の座標
            real,      intent(in) :: vy2
            integer,   intent(in) :: index     !SGSLNI参照
        end subroutine

        subroutine sglnzr(rx1,ry1,rx2,ry2,index)  !r座標系で線分を描く 
            real,      intent(in) :: rx1      !R座標系における線分の始点の座標
            real,      intent(in) :: ry1            
            real,      intent(in) :: rx2      !R座標系における線分の終点の座標
            real,      intent(in) :: ry2
            integer,   intent(in) :: index       !SGSLNI参照
        end subroutine
 
        subroutine sglnu(ux1,uy1,ux2,uy2) !       u座標系で線分を描く．
            real,      intent(in) :: ux1      !U座標系における線分の始点の座標
            real,      intent(in) :: uy1
            real,      intent(in) :: ux2      !U座標系における線分の終点の座標
            real,      intent(in) :: uy2
        end subroutine

        subroutine sglnv(vx1,vy1,vx2,vy2)  !v座標系で線分を描く． 
            real,      intent(in) :: vx1  !V座標系における線分の始点の座標
            real,      intent(in) :: vy1  
            real,      intent(in) :: vx2  !V座標系における線分の終点の座標
            real,      intent(in) :: vy2
        end subroutine

        subroutine sglnr(rx1,ry1,rx2,ry2) 
            real,      intent(in) :: rx1  !R座標系における線分の始点の座標
            real,      intent(in) :: ry1
            real,      intent(in) :: rx2  !R座標系における線分の終点の座標
            real,      intent(in) :: ry2
        end subroutine
 
        subroutine sgslni(index)  !ラインインデクスを設定する．
            integer,   intent(in) :: index  !線分のラインインデクス 
        end subroutine
!-------------------------------------------------------------------------
!     アローサブプリミティブ
        subroutine sglazu(ux1,uy1,ux2,uy2,itype,index)  !u座標系で矢印付き線分を描く
            real,      intent(in) :: ux1    !U座標系における線分の始点の座標
            real,      intent(in) :: uy1
            real,      intent(in) :: ux2    !u座標系における線分の終点の座標
            real,      intent(in) :: uy2
            integer,   intent(in) :: itype  !ラインタイプ
            integer,   intent(in) :: index  !ラインインデクス
        end subroutine

        subroutine sglazv(vx1,vy1,vx2,vy2,itype,index) !v座標系で矢印付き線分を描く
            real,      intent(in) :: vx1  !U座標系における線分の始点の座標
            real,      intent(in) :: vy1
            real,      intent(in) :: vx2  !R座標系における線分の終点の座標
            real,      intent(in) :: vy2
            integer,   intent(in) :: itype  !ラインタイプ
            integer,   intent(in) :: index    !ラインインデクス
        end subroutine

        subroutine sglazr(rx1,ry1,rx2,ry2,itype,index) !r座標系で矢印付き線分を描く
            real,      intent(in) :: rx1   !r座標系における線分の始点の座標
            real,      intent(in) :: ry1
            real,      intent(in) :: rx2   !r座標系における線分の終点の座標
            real,      intent(in) :: ry2
            integer,   intent(in) :: itype !ラインタイプ
            integer,   intent(in) :: index !ラインインデクス
        end subroutine

        subroutine sglau(ux1,uy1,ux2,uy2) !u座標系で矢じり付き線分を描く． 
            real,      intent(in) :: ux1  !uU座標系における線分の始点の座標
            real,      intent(in) :: uy1
            real,      intent(in) :: ux2  !u座標系における線分の終点の座標
            real,      intent(in) :: uy2
        end subroutine

        subroutine sglav(vx1,vy1,vx2,vy2)     !v座標系で矢じり付き線分を描く． 
            real,      intent(in) :: vx1  !v座標系における線分の始点の座標
            real,      intent(in) :: vy1
            real,      intent(in) :: vx2  !v座標系における線分の終点の座標
            real,      intent(in) :: vy2
        end subroutine

        subroutine sglar(rx1,ry1,rx2,ry2)       !r座標系で矢じり付き線分を描く． 
            real,      intent(in) :: rx1    !r座標系における線分の始点の座標
            real,      intent(in) :: ry1
            real,      intent(in) :: rx2    !r座標系における線分の終点の座標
            real,      intent(in) :: ry2
        end subroutine

        subroutine sgslat(itype)  !描く線分のラインタイプを設定する． 
            integer,   intent(in) :: itype  !線分のラインタイプ 
        end subroutine

        subroutine sgslai(index) !描く線分の ラインインデクスを設定する
            integer,   intent(in) :: index  !線分のラインインデクス 
        end subroutine

        subroutine sgqlat(itype) !現在設定されているラインタイプ
            integer,   intent(out) :: itype   !線分のラインタイプ
        end subroutine

        subroutine sgqlai(index) !現在設定されているラインインデクス
            integer,   intent(out) :: index      !線分のラインインデクス 
        end subroutine
!---------------------------------------------------------------
!文字関数
        function csgi(ic) !文字番号に対応する文字を返す．
            integer,   intent(in) :: ic    !DCL文字番号
            character(len=1)      :: csgi  !DCL文字番号に対応する文字
        end function

        function isgc(ch)   !文字に対応する文字番号を返す．
            character(len=1),intent(in) :: ch    !与えるの文字（文字長は1)
            integer              :: isgc  !文字に対応するDCL文字番号
        end function
        end interface
      end module
