!-------------------------------------------------
!interface module of shtlib
!-------------------------------------------------
module sht_interface
  interface

    subroutine shtint(mm,jm,im,work)              !初期化ルーチン． 
      integer,   intent(in) :: mm                 !入力．切断波数
      integer,   intent(in) :: jm                 !入力．南北分割数の1/2
      integer,   intent(in) :: im                 !入力．東西分割数の1/2
      real,      intent(out), dimension(*) :: work !出力．shtlibの他のルーチンで用いられる作業領域
    end subroutine
      
    subroutine shtnml(mm,n,m,lr,li)               !スペクトルデータの格納位置を求める．
      integer,   intent(in) :: mm                 !入力．切断波数
      integer,   intent(in) :: n                  !入力．全波数
      integer,   intent(in) :: m                  !入力．帯状波数(
      integer,   intent(out) :: lr                !出力．
      integer,   intent(out) :: li                !出力．
    end subroutine
      
    subroutine shtlap(mm,ind,a,b)                 !スペクトルデータに対してラプラシアンを演算する．
      integer,   intent(in) :: mm                 !入力．切断波数(m).
      integer,   intent(in) :: ind                !入力．ラプラシアンの演算形式を指定する
      real,      intent(in), dimension(*) :: a    !入力．
      real,      intent(out), dimension(*) :: b   !出力．
    end subroutine
      
    subroutine shts2w(mm,jm,isw,s,w,work)         !スペクトルデータからウエーブデータへの変換
      integer,   intent(in) :: mm                 !入力．切断 波数(m)
      integer,   intent(in) :: jm                 !入力． 南北分割数の1/2(j)
      integer,   intent(in) :: isw                !入力． 変換の種類の指定
      real,      intent(in), dimension(*) :: s    !入力． スペクトルデータ
      real,      intent(out), dimension(*) :: w   !出力．ウエーブデータ
      real,      intent(out), dimension(*) :: work !shtintで初期化された作業領域
    end subroutine
      
    subroutine shtw2g(mm,jm,im,w,g,work)          !ウエーブデータからグリッドデータへの変換 
      integer,   intent(in) :: mm                 !入力．切断波数(m)
      integer,   intent(in) :: jm                 !入力．南北分割数の1/2(j)
      integer,   intent(in) :: im                 !入力．東西分割数の1/2(i)
      real,      intent(in), dimension(*) :: w    !入力．ウエーブデータ
      real,      intent(out), dimension(*) :: g   !出力．グリッドデータ
      real,      intent(out), dimension(*) :: work !shtintで初期化された作業領域
    end subroutine
      
    subroutine shts2g(mm,jm,im,isw,s,w,g,work)    !スペクトルデータからグリッドデータへの変換
      integer,   intent(in) :: mm                 !入力．切断波数(m).
      integer,   intent(in) :: jm                 !入力．南北分割数の1/2(j)
      integer,   intent(in) :: im                 !入力．東西分割数の1/2(i)
      integer,   intent(in) :: isw                !入力．変換の種類の指定
      real,      intent(in), dimension(*) :: s    !入力．スペクトルデータ
      real,      intent(out), dimension(*) :: w   !出力．ウエーブデータ
      real,      intent(out), dimension(*) :: g   !出力．グリッドデータ
      real,      intent(out), dimension(*) :: work !shtintで初期化された作業領域
    end subroutine
      
    subroutine shtg2w(mm,jm,im,g,w,work)          !グリッドデータからウエーブデータへの変換
      integer,   intent(in) :: mm                 !入力．切断波数(m)
      integer,   intent(in) :: jm                 !入力．南北分割数の1/2(j)
      integer,   intent(in) :: im                 !入力．東西分割数の1/2(i)
      real,      intent(in), dimension(*) :: g    !入力．変換の種類の指定
      real,      intent(out), dimension(*) :: w   !出力．ウエーブデータ
      real,      intent(out), dimension(*) :: work !shtintで初期化された作業領域
    end subroutine
      
    subroutine shtw2s(mm,jm,isw,w,s,work)         !ウエーブデータからスペクトルデータへの変換  
      integer,   intent(in) :: mm                 !入力．切断波数(m)
      integer,   intent(in) :: jm                 !入力．南北分割数の1/2(j)
      integer,   intent(in) :: isw                !入力．変換の種類の指定
      real,      intent(in), dimension(*) :: w    !入力．ウエーブデータ
      real,      intent(out), dimension(*) :: s   !出力．スペクトルデータ
      real,      intent(out), dimension(*) :: work !shtintで初期化された作業領域
    end subroutine
      
    subroutine shtg2s(mm,jm,im,isw,g,w,s,work)    !グリッドデータからスペクトルデータへの変換
      integer,   intent(in) :: mm                 !入力．切断波数(m)
      integer,   intent(in) :: jm                 !入力．南北分割数の1/2(j)
      integer,   intent(in) :: im                 !入力．東西分割数の1/2(i)
      integer,   intent(in) :: isw                !入力．変換の種類の指定
      real,      intent(in), dimension(*) :: g    !入力．グリッドデータ
      real,      intent(out), dimension(*) :: w   !出力．ウエーブデータ
      real,      intent(out), dimension(*) :: s   !出力．スペクトルデータ
      real,      intent(out), dimension(*) :: work !shtintで初期化された作業領域
    end subroutine
!----------------------------------------------------------
!下位ルーチン     
    subroutine shtswa(mm,jm,isw,m1,m2,s,w,work)   !SHTS2Wの下位ルーチン
      integer,   intent(in) :: mm                 !入力．切断波数(m)
      integer,   intent(in) :: jm                 !入力．南北分割数の1/2(j)
      integer,   intent(in) :: isw                !入力．変換の種類の指定
      integer,   intent(in) :: m1                 !入力．変換する波数区間の最小値
      integer,   intent(in) :: m2                 !入力．変換する波数区間の最大値
      real,      intent(in), dimension(*) :: s    !入力．スペクトルデータ
      real,      intent(out), dimension(*) :: w   !出力．ウエーブデータ
      real,      intent(out), dimension(*) :: work !shtintで初期化された作業領域
    end subroutine
      
    subroutine shtwga(mm,jm,im,m1,m2,w,g,work)    !SHTW2Gの下位ルーチン   
      integer,   intent(in) :: mm                 !入力．切断波数(m)
      integer,   intent(in) :: jm                 !入力．南北分割数の1/2(j)
      integer,   intent(in) :: im                 !入力．東西分割数の1/2(i)
      integer,   intent(in) :: m1                 !入力．変換する波数区間の最小値
      integer,   intent(in) :: m2                 !入力．変換する波数区間の最大値
      real,      intent(in), dimension(*) :: w    !入力．ウエーブデータ
      real,      intent(out), dimension(*) :: g   !出力．グリッドデータ
      real,      intent(out), dimension(*) :: work !shtintで初期化された作業領域
    end subroutine
      
    subroutine shtsga(mm,jm,im,isw,m1,m2,s,w,g,work) !SHTS2Gの下位ルーチン．
      integer,   intent(in) :: mm                 !入力．切断波数(m)
      integer,   intent(in) :: jm                 !入力．南北分割数の1/2(j)
      integer,   intent(in) :: im                 !入力．東西分割数の1/2(i)
      integer,   intent(in) :: isw                !入力．変換の種類の指定
      integer,   intent(in) :: m1                 !入力．変換する波数区間の最小値
      integer,   intent(in) :: m2                 !入力．変換する波数区間の最大値
      real,      intent(in), dimension(*) :: s    !入力．スペクトルデータ
      real,      intent(out), dimension(*) :: w   !出力．ウエーブデータ
      real,      intent(out), dimension(*) :: g   !出力．グリッドデータ
      real,      intent(out), dimension(*) :: work !shtintで初期化された作業領域
    end subroutine
      
    subroutine shtswm(mm,jm,m,isw,s,wr,wi,work)   !SHTS2Wの下位ルーチン 
      integer,   intent(in) :: mm                 !入力．切断波数(m)
      integer,   intent(in) :: jm                 !入力．南北分割数の1/2(j)
      integer,   intent(in) :: m                  !入力．変換する波数
      integer,   intent(in) :: isw                !入力．変換の種類の指定
      real,      intent(in), dimension(*) :: s    !入力．スペクトルデータ
      real,      intent(out), dimension(*) :: wr  !出力． w^m（φ）の実数部分
      real,      intent(out), dimension(*) :: wi  !出力． w^m（φ）の虚数部分
      real,      intent(out), dimension(*) :: work !shtintで初期化された作業領域
    end subroutine
      
    subroutine shtwgm(mm,jm,im,m,wr,wi,g,work)    !SHTW2Gの下位ルーチン 
      integer,   intent(in) :: mm                 !入力．切断波数(m)
      integer,   intent(in) :: jm                 !入力．南北分割数の1/2(j)
      integer,   intent(in) :: im                 !入力．東西分割数の1/2(i)
      integer,   intent(in) :: m                  !入力．変換する波数
      real,      intent(in), dimension(*) :: wr   !入力． w^m（φ）の実数部分
      real,      intent(in), dimension(*) :: wi   !入力． w^m（φ）の虚数部分
      real,      intent(out), dimension(*) :: g   !出力．グリッドデータ
      real,      intent(out), dimension(*) :: work !shtintで初期化された作業領域
    end subroutine
      
    subroutine shtsgm(mm,jm,im,m,isw,s,wr,wi,g,work) !SHTS2Gの下位ルーチ
      integer,   intent(in) :: mm                 !入力．切断波数(m)
      integer,   intent(in) :: jm                 !入力．南北分割数の1/2(j)
      integer,   intent(in) :: im                 !入力．東西分割数の1/2(i)
      integer,   intent(in) :: m                  !入力．変換する波数
      integer,   intent(in) :: isw                !入力．変換の種類の指定
      real,      intent(in), dimension(*) :: s    !入力．スペクトルデータ
      real,      intent(out), dimension(*) :: wr  !出力． w^m（φ）の実数部分
      real,      intent(out), dimension(*) :: wi  !出力． w^m（φ）の虚数部分
      real,      intent(out), dimension(*) :: g   !出力．グリッドデータ
      real,      intent(out), dimension(*) :: work !shtintで初期化された作業領域
    end subroutine
      
    subroutine shtswz(mm,jm,isw,s,wz,work)        !SHTS2Wの下位ルーチン
      integer,   intent(in) :: mm                 !入力．切断波数(m)
      integer,   intent(in) :: jm                 !入力．南北分割数の1/2(j)
      integer,   intent(in) :: isw                !入力．変換の種類の指定
      real,      intent(in), dimension(*) :: s    !入力．スペクトルデータ
      real,      intent(out), dimension(*) :: wz  !出力． w^0（φ）が格納される
      real,      intent(out), dimension(*) :: work !shtintで初期化された作業領域
    end subroutine
      
    subroutine shtwgz(jm,im,wz,g)                 !SHTW2Gの下位ルーチン
      integer,   intent(in) :: jm                 !入力．南北分割数の1/2(j)
      integer,   intent(in) :: im                 !入力．東西分割数の1/2(i)
      real,      intent(in), dimension(*) :: wz   !入力．ウエーブデータ(w^0（φ）)
      real,      intent(out), dimension(*) :: g   !出力．グリッドデータ
    end subroutine
      
    subroutine shtsgz(mm,jm,im,isw,s,wz,g,work)   !SHTS2Gの下位ルーチン s
      integer,   intent(in) :: mm                 !入力．切断波数(m)
      integer,   intent(in) :: jm                 !入力．南北分割数の1/2(j)
      integer,   intent(in) :: im                 !入力．東西分割数の1/2(i)
      integer,   intent(in) :: isw                !入力．変換の種類の指定
      real,      intent(in), dimension(*) :: s    !入力．スペクトルデータ
      real,      intent(out), dimension(*) :: wz  !出力．ウエーブデータ(w^0（φ）)
      real,      intent(out), dimension(*) :: g   !出力．グリッドデータ
      real,      intent(out), dimension(*) :: work !shtintで初期化された作業領域
    end subroutine
      
    subroutine shtswj(mm,jm,isw,j,m1,m2,s,wj,work) !SHTS2Wの下位ルーチン
      integer,   intent(in) :: mm                 !入力．切断波数(m)
      integer,   intent(in) :: jm                 !入力．南北分割数の1/2(j)
      integer,   intent(in) :: isw                !入力．変換の種類の指定
      integer,   intent(in) :: j                  !入力．変換を行う緯度円の指定
      integer,   intent(in) :: m1                 !入力．変換する波数区間の最小値
      integer,   intent(in) :: m2                 !入力．変換する波数区間の最大値
      real,      intent(in), dimension(*) :: s    !入力．スペクトルデータ
      real,      intent(out), dimension(*) :: wj  !出力． w^m（φ_j) が格納される． 長さ2*mm+1の配列
      real,      intent(out), dimension(*) :: work !shtintで初期化された作業領域
    end subroutine
      
    subroutine shtwgj(mm,im,m1,m2,wj,gj,work)     !SHTW2Gの下位ルーチン．
      integer,   intent(in) :: mm                 !入力．切断波数(m)
      integer,   intent(in) :: im                 !入力．東西分割数の1/2(i)
      integer,   intent(in) :: m1                 !入力．変換する波数区間の最小値
      integer,   intent(in) :: m2                 !入力．変換する波数区間の最大値
      real,      intent(in), dimension(*) :: wj   !入力． w^m（φ_j)
      real,      intent(out), dimension(*) :: gj  !出力．グリッドデータ．長さ2*im+1の配列
      real,      intent(out), dimension(*) :: work !shtintで初期化された作業領域
    end subroutine
      
    subroutine shtsgj(mm,jm,im,isw,j,m1,m2,s,wj,gj,work) !SHTS2Gの下位  
      integer,   intent(in) :: mm                 !入力．切断波数(m)
      integer,   intent(in) :: im                 !入力．東西分割数の1/2(i)
      integer,   intent(in) :: jm                 !入力．南北分割数の1/2(j)
      integer,   intent(in) :: isw                !入力．変換の種類の指定
      integer,   intent(in) :: j                  !入力．変換を行う緯度円の指定
      integer,   intent(in) :: m1                 !入力．変換する波数区間の最小値
      integer,   intent(in) :: m2                 !入力．変換する波数区間の最大値
      real,      intent(in), dimension(*) :: s    !入力．スペクトルデータ
      real,      intent(out), dimension(*) :: wj  !出力． w^m（φ_j)
      real,      intent(out), dimension(*) :: gj  !出力．グリッドデータ
      real,      intent(out), dimension(*) :: work !shtintで初期化された作業領域
    end subroutine
      
    subroutine shtfun(mm,jm,m,fun,work)           !ルジャンドル陪関数を計算する．
      integer,   intent(in) :: mm                 !入力．切断波数(m)
      integer,   intent(in) :: jm                 !入力．南北分割数の1/2(j)
      integer,   intent(in) :: m                  !入力．求めるルジャンドル陪関数の帯状波数(m).
      real,      intent(out), dimension(*) :: fun !p^m_n(φ）が格納される長さ (2*jm+1)*(mm-m+1)の配列
      real,      intent(out), dimension(*) :: work !shtintで初期化された作業領域
    end subroutine
      
    subroutine shtlfw(mm,jm,m,isw,wm,sm,work)     !ルジャンドル正変換を行う．
      integer,   intent(in) :: mm                 !入力．切断波数(m)
      integer,   intent(in) :: jm                 !入力．南北分割数の1/2(j)
      integer,   intent(in) :: isw                !入力．変換の種類の指定
      integer,   intent(in) :: m                  !入力．変換を行う帯状波数(m).
      real,      intent(in), dimension(*) :: wm   !入力．ウエーブデータ
      real,      intent(out), dimension(*) :: sm  !出力．スペクトルデータ
      real,      intent(out), dimension(*) :: work !shtintで初期化された作業領域
    end subroutine
      
    subroutine shtlbw(mm,jm,m,isw,sm,wm,work)     !ルジャンドル逆変換を行う．
      integer,   intent(in) :: mm                 !入力．切断波数(m)
      integer,   intent(in) :: jm                 !入力．南北分割数の1/2(j)
      integer,   intent(in) :: isw                !入力．変換の種類の指定
      integer,   intent(in) :: m                  !入力．変換を行う帯状波数(m).
      real,      intent(in), dimension(*) :: sm   !入力．スペクトルデータ．長さmm-m+1の配列
      real,      intent(out), dimension(*) :: wm  !出力．ウエーブデータ． 長さ(2*jm+1)の配列
      real,      intent(out), dimension(*) :: work !shtintで初期化された作業領域
    end subroutine

  end interface
end module
!shtlib library end ----
