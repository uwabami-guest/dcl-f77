!-------------------------------------------------
!     interface module of slpack
!-------------------------------------------------
module sl_interface
    interface
!-----------------------------------------------------------------------
!主要ルーチン
        subroutine slinit(wxmax,wymax,fact) !初期化
              real,      intent(in) :: wxmax  !最大作画範囲(W座標)                            
              real,      intent(in) :: wymax       
              real,      intent(in) :: fact    !単位長と実長との比
        end subroutine

        subroutine slsize(csize)  !第1フレームの再設定
            character(len=3), intent(in) :: csize  !フレームの大きさと向き
        end subroutine

        subroutine slform(dxa,dya)  !第1フレームの再設定
            real,      intent(in) :: dxa       !フレームの横,縦の実長, 単位cm
            real,      intent(in) :: dya
        end subroutine

        subroutine sldiv(cform,ix,iy) !フレームの分割
            character(len=1), intent(in) :: cform  !方向を指定
            integer,   intent(in) :: ix          !X方向, Y方向の分割数
            integer,   intent(in) :: iy
        end subroutine

        subroutine slmgn(xl,xr,yb,yt)   !マージンの設定． 
            real,      intent(in) :: xl  !左辺，右辺，下辺，上辺のマージン
            real,      intent(in) :: xr
            real,      intent(in) :: yb
            real,      intent(in) :: yt
        end subroutine

        subroutine slrat(rx,ry)  !縦横比の設定．   
            real,      intent(in) :: rx  !フレームの縦横比を与える
            real,      intent(in) :: ry
        end subroutine

        subroutine slsttl(cttl,cside,px,py,ht,nt)  !タイトルの設定．        
            character(len=*), intent(in) :: cttl  !描く文字列
            character(len=1), intent(in) :: cside !文字列を描く場所
            real,      intent(in) :: px         !文字列の位置を-1.0から+1.0までの実数値で指定
            real,      intent(in) :: py        
            real,      intent(in) :: ht         !文字の高さ(R座標)
            integer,   intent(in) :: nt         !何番目の文字列かを指定する
        end subroutine
!--------------------------------------------------------------------
!作画境界
        subroutine slpvpr(index)  !ビューポートの枠を描く．
            integer,   intent(in) :: index  !線分のラインインデクス 
        end subroutine

        subroutine slpwwr(index)  !ウインドウの枠を描く
            integer,   intent(in) :: index  !線分のラインインデクス 
        end subroutine

        subroutine slpwvr(index)  !最大作画領域の枠を描く．
            integer,   intent(in) :: index    !線分のラインインデクス 
        end subroutine

        subroutine slpvpc(index,rc)  !ビューポートのコーナーマークを描く
            integer,   intent(in) :: index    !線分のラインインデクス 
            real,      intent(in) :: rc    !コーナーマークの長さ
        end subroutine                             

        subroutine slpwwc(index,rc)  !ウインドウのコーナーマークを描く
            integer,   intent(in) :: index    !線分のラインインデクス 
            real,      intent(in) :: rc     !コーナーマークの長さ
        end subroutine

        subroutine slpwvc(index,rc)  !最大作画領域のコーナーマークを描く．
            integer,   intent(in) :: index  !線分のラインインデクス 
            real,      intent(in) :: rc    !コーナーマークの長さ
        end subroutine

   end interface
end module

