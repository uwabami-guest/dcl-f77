!-------------------------------------------------
!     interface module of swpack
!-------------------------------------------------
module sw_interface
    interface
!--------------------------------------------------------------
!コントロールルーチン
        subroutine swdopn()  !デバイスのオープン
        end subroutine

        subroutine swdcls()  !デバイスのクローズ
        end subroutine

        subroutine swpopn()  !ページのオープン 
        end subroutine
                             
        subroutine swpcls()  !ページのクローズ 
        end subroutine

        subroutine swoopn(cobj,comm)  !オブジェクトのオープン 
            character(len=*), intent(in) :: cobj  !オブジェクト名
            character(len=*), intent(in) :: comm  !コメント
        end subroutine
                              
        subroutine swocls(cobj) !オブジェクトのクローズ  
            character(len=*), intent(in) :: cobj  !オブジェクト名
        end subroutine

        subroutine swpget(cp,ipara) ! 内部変数参照   
            character(len=*), intent(in) :: cp      !内部変数の名前
            integer,   intent(out) :: ipara          !(整数・実数・論理型の）内部変数の値
        end subroutine

        subroutine swiget(cp,ipara)
            character(len=*), intent(in) :: cp      !内部変数の名前
            integer,   intent(out) :: ipara          !(整数・実数・論理型の）内部変数の値
        end subroutine

        subroutine swrget(cp,ipara)
            character(len=*), intent(in) :: cp      !内部変数の名前
            real,      intent(out) :: ipara          !(整数・実数・論理型の）内部変数の値
        end subroutine

        subroutine swlget(cp,ipara)
            character(len=*), intent(in) :: cp      !内部変数の名前
            logical,   intent(out) :: ipara          !(整数・実数・論理型の）内部変数の値
        end subroutine
                           
        subroutine swpset(cp,ipara)  !内部変数設定 
            character(len=*), intent(in) :: cp      !内部変数の名前
            integer,   intent(in) :: ipara          !(整数・実数・論理型の）内部変数の値
        end subroutine

        subroutine swiset(cp,ipara)
            character(len=*), intent(in) :: cp      !内部変数の名前
            integer,   intent(in) :: ipara          !(整数・実数・論理型の）内部変数の値
        end subroutine

        subroutine swrset(cp,ipara)
            character(len=*), intent(in) :: cp      !内部変数の名前
            real,      intent(in) :: ipara          !(整数・実数・論理型の）内部変数の値
        end subroutine

        subroutine swlset(cp,ipara)
            character(len=*), intent(in) :: cp      !内部変数の名前
            logical,   intent(in) :: ipara          !(整数・実数・論理型の）内部変数の値
        end subroutine
                             
        subroutine swpstx(cp,ipara) !内部変数の設定 
            character(len=*), intent(in) :: cp      !内部変数の名前
            integer,   intent(in) :: ipara          !(整数・実数・論理型の）内部変数の値
        end subroutine

        subroutine swistx(cp,ipara) 
            character(len=*), intent(in) :: cp      !内部変数の名前
            integer,   intent(in) :: ipara          !(整数・実数・論理型の）内部変数の値
        end subroutine

        subroutine swrstx(cp,ipara) 
            character(len=*), intent(in) :: cp      !内部変数の名前
            real,      intent(in) :: ipara          !(整数・実数・論理型の）内部変数の値
        end subroutine

        subroutine swlstx(cp,ipara) 
            character(len=*), intent(in) :: cp      !内部変数の名前
            logical,   intent(in) :: ipara          !(整数・実数・論理型の）内部変数の値
        end subroutine
                         
        subroutine swcget(cp,cpara)    !文字型内部変数参照 
            character(len=*), intent(in) :: cp        !内部変数の名前
            character(len=*), intent(out) :: cpara  !(文字型の）内部変数の値
        end subroutine
                              
        subroutine swcset(cp,cpara)!文字型内部変数設定
            character(len=*), intent(in) :: cp       !内部変数の名前
            character(len=*), intent(in) :: cpara   !(文字型の）内部変数の値
        end subroutine

        subroutine  swcstx(cp,cpara)!文字型内部変数の設定
            character(len=*), intent(in) :: cp       !内部変数の名前
            character(len=*), intent(in) :: cpara    !(文字型の）内部変数の値
        end subroutine

        subroutine  swpqnp(ncp)
            integer,   intent(out) :: ncp
        end subroutine

        subroutine  swpqid(cp,idx)
            character(len=*), intent(in) :: cp
            integer,   intent(out) :: idx
        end subroutine

        subroutine  swpqcp(idx,cp)
            integer,   intent(in) :: idx
            character(len=*), intent(out) :: cp
        end subroutine

        subroutine  swpqvl(idx,cp)
            integer,   intent(in) :: idx
            character(len=*), intent(out) :: cp
        end subroutine

        subroutine  swpsvl(idx,cp)
            integer,   intent(in) :: idx
            character(len=*), intent(out) :: cp
        end subroutine

        subroutine  swcqnp(ncp)
            integer,   intent(out) :: ncp
        end subroutine

        subroutine  swcqid(cp,idx)
            character(len=*), intent(in) :: cp
            integer,   intent(out) :: idx
        end subroutine

        subroutine  swcqcp(idx,cp)
            integer,   intent(in) :: idx
            character(len=*), intent(out) :: cp
        end subroutine

        subroutine  swcqvl(idx,cval)
            integer,   intent(in) :: idx
            character(len=*), intent(out) :: cval
        end subroutine

        subroutine  swcsvl(idx,cval)
            integer,   intent(in) :: idx
            character(len=*), intent(out) :: cval
        end subroutine
!------------------------------------------------------------------------------------
!     描画ルーチン
        subroutine swswdi(iwdidx)  !線幅指定 
            integer,   intent(in) :: iwdidx         !線分の太さに関するインデクス
        end subroutine
                             
        subroutine swscli(iclidx)  !線色指定 
            integer,   intent(in) :: iclidx         !線分の色に関するインデクス
        end subroutine
                             
        subroutine swgopn()!プリミティブのオープン 
        end subroutine
                              
        subroutine swgcls()!プリミティブのクローズ 
        end subroutine
                            
        subroutine swgmov(wx,wy) ! ペンアップ
            real,      intent(in) :: wx             !ワークステーション座標
            real,      intent(in) :: wy
        end subroutine
                             
        subroutine swgplt(wx,wy) !ペンダウン
            real,      intent(in) :: wx             !ワークステーション座標
            real,      intent(in) :: wy
        end subroutine
                              
        subroutine swgton(np,wpx,wpy,itpat)  !ハードフィル
            integer,   intent(in) :: np               !配列WPX,WPYの大きさ
            real,      intent(in), dimension(*) :: wpx       !塗りつぶす領域の境界のX座標
            real,      intent(in), dimension(*) :: wpy       !塗りつぶす領域の境界のY座標
            integer,   intent(in) :: itpat                   !塗りつぶしパターン番号
        end subroutine
  
        subroutine swiopn(iwx,iwy,imw,imh)  !イメージオープン                            
            integer,   intent(in) :: iwx  !イメージの左上の座標
            integer,   intent(in) :: iwy
            integer,   intent(in) :: imw  !イメージの大きさ
            integer,   intent(in) :: imh
        end subroutine
                            
        subroutine swidat(image,nlen)  ! イメージデータ  
            integer,   intent(in) :: image       !イメージデータ
            integer,   intent(in) :: nlen       !イメージデータの長さ
        end subroutine
                           
        subroutine swicls()  !イメージクローズ  
        end subroutine
!---------------------------------------------------------------------------------
!     マウス
        subroutine swqpnt(wx,wy,mb)  !マウスポイント取得 
            real,      intent(out) :: wx    !マウスポイントの位置
            real,      intent(out) :: wy
            integer,   intent(out) :: mb    !ボタンの種類
        end subroutine

        subroutine swfint(wx,wy,iwx,iwy)  !ワークステーション座標からイメージ座標への変換  
            real,      intent(inout) :: wx       !ワークステーション座標
            real,      intent(inout) :: wy
            integer,   intent(inout) :: iwx       !イメージ座標
            integer,   intent(inout) :: iwy
        end subroutine
                              
        subroutine swiint(iwx,iwy,wx,wy) !イメージ座標からワークステーション座標への変換 
            real,      intent(inout) :: wx      !イメージ座標
            real,      intent(inout) :: wy
            integer,   intent(inout) :: iwx      !ワークステーション座標
            integer,   intent(inout) :: iwy
        end subroutine
!------------------------------------------------------------------------------------
!問い合わせなど
        subroutine swqwdc(lwd)  !線幅可変能力の取得  
            logical,   intent(out) :: lwd   !線幅を変える能力がある時 .TRUE. 
        end subroutine
                             
        subroutine swqclc(lcl)  !線色可変能力の取得
            logical,   intent(out) :: lcl    !線色を変える能力がある時 .TRUE. 
        end subroutine
                              
        subroutine swqtnc(ltn) !hard fill 能力の取得
            logical,   intent(out) :: ltn     !ハードフィルを行う能力がある時 .TRUE. 
        end subroutine
 
        subroutine swqimc(lim)  !イメージ描画能力の取得                             
            logical,   intent(out) :: lim     !イメージ描画を行う能力がある時 .TRUE. 
        end subroutine
                             
        subroutine swqptc(lpt) !ポインティング能力の取得 
            logical,   intent(out) :: lpt     !マウスポイントの位置を調べる能力がある時 .TRUE. 
        end subroutine
                               
        subroutine swqfnm(cpara,cfname) !各種データベースファイル名の取得
            character(len=*), intent(in) :: cpara     !データベース名を示す内部変数名
            character(len=*), intent(out) :: cfname     !データベースファイル名
        end subroutine
                              
        subroutine swqrct(xmin,xmax,ymin,ymax,fact)  !最大作画領域取得
            real,      intent(out) :: xmin  !作画領域の左下のX座標． 
            real,      intent(out) :: xmax  !作画領域の右上のX座標． 
            real,      intent(out) :: ymin  !作画領域の左下のY座標． 
            real,      intent(out) :: ymax  !作画領域の右上のY座標． 
            real,      intent(out) :: fact  !座標系の1単位の長さ(cm). 
        end subroutine
                             
        subroutine swsrot(iwtrot) !画面の回転指定  
            integer,   intent(in) :: iwtrot     !1 :正立; 2 :90度回転
        end subroutine
  end interface
end module                                                
