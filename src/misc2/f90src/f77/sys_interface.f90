!-------------------------------------------------
!interface module of syslib
!-------------------------------------------------
module sys_interface
  interface
!------------------------------------------------------------
    subroutine glpget(cp,ipara)                   !内部変数を参照する
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine gliget(cp,ipara)                   !内部変数を参照する
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine glrget(cp,ipara)                   !内部変数を参照する
      character(len=*), intent(in) :: cp          !内部変数の名前
      real,            intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine gllget(cp,ipara)                   !内部変数を参照する
      character(len=*), intent(in) :: cp          !内部変数の名前
      logical,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine glpset(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine gliset(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine glrset(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      real,      intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine gllset(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      logical,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine glpstx(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine glistx(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine glrstx(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      real,      intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine gllstx(cp,ipara)                   !内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      logical,   intent(in) :: ipara              !内部変数の値
    end subroutine
!------------------------------------------------------------
    subroutine glpqnp(ncp)                        !内部変数の総数を求める．
      integer,   intent(out) :: ncp               !内部変数の値
    end subroutine
      
    subroutine glpqid(cp,idx)                     !内部変数cpの位置を求める．
      character(len=*), intent(in) :: cp
      integer,   intent(out) :: idx
    end subroutine
      
    subroutine glpqcp(idx,cp)                     !idxの位置にある内部変数の名前を参照する．
      integer,   intent(in) :: idx
      character(len=*), intent(out) :: cp
    end subroutine
      
    subroutine glpqvl(idx,ipara)                  !idxの位置にある内部変数の値を参照する．
      integer,   intent(in) :: idx
      integer,   intent(out) :: ipara
    end subroutine
      
    subroutine glpsvl(idx,ipara)                  !idxの位置にある内部変数の値を変更する
      integer,   intent(in) :: idx
      integer,   intent(in) :: ipara
    end subroutine
!------------------------------------------------------------
    subroutine glcget(cp,cpara)                   !文字型の内部変数を参照/変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      character(len=*), intent(out) :: cpara      !内部変数の値
    end subroutine
      
    subroutine glcset(cp,cpara)                   !文字型の内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      character(len=*), intent(in) :: cpara       !内部変数の値
    end subroutine
      
    subroutine glcstx(cp,cpara)                   !文字型の内部変数を変更する
      character(len=*), intent(in) :: cp          !内部変数の名前
      character(len=*), intent(in) :: cpara       !内部変数の値
    end subroutine
      
    subroutine glcqnp(ncp)                        !内部変数の総数を求める．
      integer,   intent(out) :: ncp               !内部変数の値
    end subroutine
      
    subroutine glcqid(cp,idx)                     !内部変数cpの位置を求める．
      character(len=*), intent(in) :: cp
      integer,   intent(out) :: idx
    end subroutine
      
    subroutine glcqcp(idx,cp)                     !idxの位置にある内部変数の名前を参照する．
      integer,   intent(in) :: idx
      character(len=*), intent(out) :: cp
    end subroutine
      
    subroutine glcqvl(idx,cpara)                  !idxの位置にある内部変数の値を参照する．
      integer,   intent(in) :: idx
      character(len=*), intent(out) :: cpara
    end subroutine
      
    subroutine glcsvl(idx,cpara)                  !idxの位置にある内部変数の値を変更する
      integer,   intent(in) :: idx
      character(len=*), intent(in) :: cpara
    end subroutine
!------------------------------------------------------------
    subroutine msgdmp(clev,csub,cmsg)             !メッセージを出力する．
      character(len=1), intent(in) :: clev        !メッセージのレベルを指定する
      character(len=6), intent(in) :: csub        !msgdmpを呼んでいるサブルーチン名を指定する
      character(len=*), intent(in) :: cmsg        !出力するメッセージ
    end subroutine
!------------------------------------------------------------
    subroutine rtpget(cpfix,cp,ipara,n)           !実行時オプションから内部変数を取得する．
      character(len=*), intent(in) :: cpfix       !変数名の前につける接頭辞
      character(len=*), intent(in), dimension(*) :: cp !変数名
      integer,   intent(out), dimension(*) :: ipara !変数の値
      integer,   intent(in) :: n                  !内部変数の数
    end subroutine
      
    subroutine rtcget(cpfix,cp,cpara,n)           !実行時オプションから内部変数を取得する．
      character(len=*), intent(in) :: cpfix       !変数名の前につける接頭辞
      character(len=*), intent(in), dimension(*) :: cp !変数名
      character(len=*), intent(out), dimension(*) :: cpara !変数の値
      integer,   intent(in) :: n                  !内部変数の数
    end subroutine
      
    subroutine rtpenv(cpfix,cp,ipara)             !環境変数の値を得る
      character(len=*), intent(in) :: cpfix
      character(len=*), intent(in), dimension(*) :: cp
      integer,   intent(out) :: ipara
    end subroutine
      
    subroutine rtpopt(cpfix,cp,ipara)             !コマンドラインオプションの値を得る
      character(len=*), intent(in) :: cpfix
      character(len=*), intent(in), dimension(*) :: cp
      integer,   intent(out) :: ipara
    end subroutine
      
    subroutine rtpxfl(cpfix,cp,ipara)             !外部ファイルからオプションの値を得る
      character(len=*), intent(in) :: cpfix
      character(len=*), intent(in), dimension(*) :: cp
      integer,   intent(out) :: ipara
    end subroutine
      
    subroutine rtcenv(cpfix,cp,cval)              !環境変数の値（文字型）を得る
      character(len=*), intent(in) :: cpfix
      character(len=*), intent(in), dimension(*) :: cp
      character(len=*), intent(out) :: cval
    end subroutine
      
    subroutine rtcopt(cpfix,cp,cval)              !コマンドラインオプションの値を得る
      character(len=*), intent(in) :: cpfix
      character(len=*), intent(in), dimension(*) :: cp
      character(len=*), intent(out) :: cval
    end subroutine
      
    subroutine rtcxfl(cpfix,cp,cval)              !外部ファイルからオプションの値を得る
      character(len=*), intent(in) :: cpfix
      character(len=*), intent(in), dimension(*) :: cp
      character(len=*), intent(out) :: cval
    end subroutine

    subroutine cfsrch(cpl,np,cfl,nf,cfn)            !パス名の先頭部分と末尾部分のリストの組合せから，存在するファイル名を探す．
      character(len=*), intent(in), dimension(np) :: cpl !パス名の先頭部分のリスト
      integer,   intent(in) :: np                 !配列cplの長さ
      character(len=*), intent(in), dimension(np) :: cfl !パス名の末尾部分のリスト
      integer,   intent(in) :: nf                 !配列cflの長さ
      character(len=*), intent(out) :: cfn        !最初に見つかったファイル名を返す文字型変数
    end subroutine
!------------------------------------------------------------
      
    function lchreq(ch1,ch2)                      !大文字・小文字の区別なく2つの文字列を比較する．
      logical lchreq                              !2つの文字列が等しいとき .true.を返す論理関数値
      character(len=*), intent(in) :: ch1         !比較する文字列
      character(len=*), intent(in) :: ch2         !比較する文字列
    end function
      
    function iufopn()                             !利用可能なもっとも小さい入出力装置番号を返す．
      integer iufopn                              !装置番号を返す整数値
    end function

  end interface
end module
!syslib library end ----
