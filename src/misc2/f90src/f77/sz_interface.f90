!-------------------------------------------------
!     interface module of szpack
!-------------------------------------------------
module sz_interface
    interface
!-----------------------------------------------------------
!インターフェイスルーチン
        subroutine szplop(itype,index) !ポリラインプリミティブの初期化
            integer,   intent(in) :: itype
            integer,   intent(in) :: index
        end subroutine
                                       
        subroutine szplzu(n,upx,upy)  !ポリラインプリミティブの描画  
            integer,   intent(in) :: n
            real,      intent(in), dimension(*) :: upx
            real,      intent(in), dimension(*) :: upy
        end subroutine

        subroutine szplzv(n,vpx,vpy)  !ポリラインプリミティブの描画  
            integer,   intent(in) :: n
            real,      intent(in), dimension(*) :: vpx
            real,      intent(in), dimension(*) :: vpy
        end subroutine

        subroutine szplzr(n,rpx,rpy)  !ポリラインプリミティブの描画  
            integer,   intent(in) :: n
            real,      intent(in), dimension(*) :: rpx
            real,      intent(in), dimension(*) :: rpy
        end subroutine

        subroutine szplcl() !ポリラインプリミティブの終了．
        end subroutine

        subroutine szpmop(itype,index,rsize)  !ポリマーカープリミティブの初期化．
            integer,   intent(in) :: itype
            integer,   intent(in) :: index
            real,      intent(in) :: rsize
        end subroutine

        subroutine szpmzu(n,px,py)  !ポリマーカープリミティブの描画
            integer,   intent(in) :: n
            real,      intent(in), dimension(*) :: px
            real,      intent(in), dimension(*) :: py
        end subroutine

           subroutine szpmzv(n,px,py) !ポリマーカープリミティブの描画
            integer,   intent(in) :: n
            real,      intent(in), dimension(*) :: px
            real,      intent(in), dimension(*) :: py
        end subroutine

        subroutine szpmzr(n,px,py) !ポリマーカープリミティブの描画
            integer,   intent(in) :: n
            real,      intent(in), dimension(*) :: px
            real,      intent(in), dimension(*) :: py
        end subroutine

        subroutine szpmcl()  !ポリマーカープリミティブの終了                 
        end subroutine


        subroutine sztnop(itpat)   !トーンプリミティブの初期化  ． 
            integer,   intent(in) :: itpat
        end subroutine

        subroutine sztnzu(n,px,py)  !トーンプリミティブの描画 
            integer,   intent(in) :: n
            real,      intent(in), dimension(*) :: px
            real,      intent(in), dimension(*) :: py
        end subroutine

        subroutine sztnzv(n,px,py)  !トーンプリミティブの描画 
            integer,   intent(in) :: n
            real,      intent(in), dimension(*) :: px
            real,      intent(in), dimension(*) :: py
        end subroutine

        subroutine sztnzr(n,px,py)  !トーンプリミティブの描画 
            integer,   intent(in) :: n
            real,      intent(in), dimension(*) :: px
            real,      intent(in), dimension(*) :: py
        end subroutine

        subroutine sztncl() !トーンプリミティブの終了． 
        end subroutine


        subroutine sztxop(rsize,irota,icent,index) !テキストプリミティブの初期化． 
            real,      intent(in) :: rsize
            integer,   intent(in) :: irota
            integer,   intent(in) :: icent
            integer,   intent(in) :: index
        end subroutine

        subroutine sztxzu(x,y,chars) !テキストプリミティブの描画． 
            real,      intent(in) :: x
            real,      intent(in) :: y
            character(len=*), intent(in) :: chars
        end subroutine

        subroutine sztxzv(x,y,chars) !テキストプリミティブの描画． 
            real,      intent(in) :: x
            real,      intent(in) :: y
            character(len=*), intent(in) :: chars
        end subroutine

        subroutine sztxzr(x,y,chars) !テキストプリミティブの描画．
            real,      intent(in) :: x
            real,      intent(in) :: y
            character(len=*), intent(in) :: chars
        end subroutine

        subroutine sztxcl() !テキストプリミティブの終了． 
        end subroutine

        subroutine szlnop(index) !ラインサブプリミティブの初期化． 
            integer,   intent(in) :: index
        end subroutine

        subroutine szlnzu(x1,y1,x2,y2) !ラインサブプリミティブの描画． 
            real,      intent(in) :: x1
            real,      intent(in) :: y1
            real,      intent(in) :: x2
            real,      intent(in) :: y2
        end subroutine
        
        subroutine szlnzv(x1,y1,x2,y2) !ラインサブプリミティブの描画． 
            real,      intent(in) :: x1
            real,      intent(in) :: y1
            real,      intent(in) :: x2
            real,      intent(in) :: y2
        end subroutine
        
        subroutine szlnzr(x1,y1,x2,y2) !ラインサブプリミティブの描画． 
            real,      intent(in) :: x1
            real,      intent(in) :: y1
            real,      intent(in) :: x2
            real,      intent(in) :: y2
        end subroutine

        subroutine szlncl() !ラインサブプリミティブの終了
        end subroutine

        subroutine szlaop(itype,index) !アローサブプリミティブの初期化． 
            integer,   intent(in) :: itype
            integer,   intent(in) :: index
        end subroutine

        subroutine szlazu(x1,y1,x2,y2) !アローサブプリミティブの描画． 
            real,      intent(in) :: x1
            real,      intent(in) :: y1
            real,      intent(in) :: x2
            real,      intent(in) :: y2
        end subroutine
        
        subroutine szlazv(x1,y1,x2,y2) !アローサブプリミティブの描画． 
            real,      intent(in) :: x1
            real,      intent(in) :: y1
            real,      intent(in) :: x2
            real,      intent(in) :: y2
        end subroutine
        
        subroutine szlazr(x1,y1,x2,y2) !アローサブプリミティブの描画． 
            real,      intent(in) :: x1
            real,      intent(in) :: y1
            real,      intent(in) :: x2
            real,      intent(in) :: y2
        end subroutine

        subroutine szlacl() !アローサブプリミティブの終了
        end subroutine

!---------------------------------------------------------------------------------
!        折れ線ルーチン
        subroutine szoplu() !回転/正規化変換， 線形補間
        end subroutine

        subroutine szmvlu(ux,uy) 
            real,      intent(in) :: ux
            real,      intent(in) :: uy
        end subroutine

        subroutine szpllu(ux,uy) 
            real,      intent(in) :: ux
            real,      intent(in) :: uy
        end subroutine
        
        subroutine szcllu() 
        end subroutine



        subroutine szoplt() !地図投影，大円補間，地図境界でのクリッピング 
        end subroutine
        
        subroutine szmvlt(tx,ty)
            real,      intent(in) :: tx
            real,      intent(in) :: ty
        end subroutine
        
        subroutine szpllt(tx,ty)
            real,      intent(in) :: tx
            real,      intent(in) :: ty
        end subroutine
        
        subroutine szcllt() 
        end subroutine

        subroutine szoplv() !view port でのクリッピング
        end subroutine

        subroutine szmvlv(vx,vy) 
            real,      intent(in) :: vx
            real,      intent(in) :: vy
        end subroutine

        subroutine szpllv(vx,vy) 
            real,      intent(in) :: vx
            real,      intent(in) :: vy
        end subroutine

        subroutine szcllv() 
        end subroutine


        subroutine szoplc() !ラベルつき線分への展開 
        end subroutine

        subroutine szmvlc(vx,vy) 
            real,      intent(in) :: vx
            real,      intent(in) :: vy
        end subroutine

        subroutine szpllc(vx,vy) 
            real,      intent(in) :: vx
            real,      intent(in) :: vy
        end subroutine

        subroutine szcllc() 
        end subroutine

        subroutine szopld() !点線の展開
        end subroutine

        subroutine szmvld(vx,vy) 
            real,      intent(in) :: vx
            real,      intent(in) :: vy
        end subroutine

        subroutine szplld(vx,vy)  
            real,      intent(in) :: vx
            real,      intent(in) :: vy
        end subroutine

        subroutine szclld() 
        end subroutine

        subroutine szoplp() !透視変換
        end subroutine

        subroutine szmvlp(vx,vy)  
            real,      intent(in) :: vx
            real,      intent(in) :: vy
        end subroutine

        subroutine szpllp(vx,vy) 
            real,      intent(in) :: vx
            real,      intent(in) :: vy
        end subroutine

        subroutine szcllp() 
        end subroutine

        subroutine szoplr() !ws view port でのクリッピング
        end subroutine

        subroutine szmvlr(rx,ry) 
            real,      intent(in) :: rx
            real,      intent(in) :: ry
        end subroutine

        subroutine szpllr(rx,ry) 
            real,      intent(in) :: rx
            real,      intent(in) :: ry
        end subroutine

        subroutine szcllr() 
        end subroutine

        subroutine szoplz() !ws 変換，直線描画
        end subroutine

        subroutine szmvlz(rx,ry) 
            real,      intent(in) :: rx
            real,      intent(in) :: ry
        end subroutine

        subroutine szpllz(rx,ry) 
            real,      intent(in) :: rx
            real,      intent(in) :: ry
        end subroutine

        subroutine szcllz() 
        end subroutine
!-------------------------------------------------------------------------------
!        トーンルーチン
        subroutine szoptu() !回転/正規化変換， 線形補間
        end subroutine

        subroutine szsttu(ux,uy) 
            real,      intent(in) :: ux
            real,      intent(in) :: uy
        end subroutine

        subroutine szcltu() 
        end subroutine

        subroutine szoptt() !地図投影，大円補間，地図境界でのクリッピング 
        end subroutine

        subroutine szsttt(tx,ty) 
            real,      intent(in) :: tx
            real,      intent(in) :: ty
        end subroutine

        subroutine szcltt() 
        end subroutine

        subroutine szoptv() !view port でのクリッピング
        end subroutine

        subroutine szsttv(vx,vy) 
            real,      intent(in) :: vx
            real,      intent(in) :: vy
        end subroutine

        subroutine szcltv() 
        end subroutine

        subroutine szoptp()  !投影変換，soft/hard fill の switching 
        end subroutine

        subroutine szsttp(vx,vy) 
            real,      intent(in) :: vx
            real,      intent(in) :: vy
        end subroutine

        subroutine szcltp() 
        end subroutine

        subroutine szoptr() !ws view port でのクリッピング
        end subroutine

        subroutine szsttr(rx,ry) 
            real,      intent(in) :: rx
            real,      intent(in) :: ry
        end subroutine

        subroutine szcltr() 
        end subroutine

        subroutine szoptz() !ws 変換，バッファリング
        end subroutine

        subroutine szsttz(rx,ry) 
            real,      intent(in) :: rx
            real,      intent(in) :: ry
        end subroutine

        subroutine szcltz() 
        end subroutine

        subroutine szopts() !バッファリング（ソフトフィル)
        end subroutine

        subroutine szstts(vx,vy) 
            real,      intent(in) :: vx
            real,      intent(in) :: vy
        end subroutine

        subroutine szclts() 
        end subroutine

        subroutine sztnsv(n,vpx,vpy,irota,rspce,itype,index) !ソフトフィル
            integer,   intent(in) :: n
            real,      intent(in), dimension(*) :: vpx
            real,      intent(in), dimension(*) :: vpy
            integer,   intent(in) :: irota
            real,      intent(in) :: rspce
            integer,   intent(in) :: itype
            integer,   intent(in) :: index
        end subroutine
    end interface
end module
