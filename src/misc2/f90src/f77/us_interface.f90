!-------------------------------------------------
!interface module of uspack
!-------------------------------------------------
module us_interface
  interface
!---------------------------------------------------------------
    subroutine usgrph(n, x, y)                    !座標軸とグラフを描く．
      integer,   intent(in) :: n                  !x,y のデータ数．
      real,      intent(in), dimension(*) :: x    !折れ線グラフのx座標
      real,      intent(in), dimension(*) :: y    !折れ線グラフのy座標
    end subroutine
!--------------------------------------------------------
    subroutine usspnt(n, x, y)                    !ウインドウ範囲に含めたいデータの指定．
      integer,   intent(in) :: n                  !x,y のデータ数
      real,      intent(in), dimension(*) :: x    !x座標
      real,      intent(in), dimension(*) :: y    !y座標
    end subroutine
      
    subroutine uspfit()                           !usspntで設定されたデータの最大最小値，正規化変換のパラメタを決める．
    end subroutine
!--------------------------------------------------------
    subroutine usdaxs()                           !デフォルトの座標軸を描く． タイトルが指定されていればタイトルも書く．
    end subroutine
      
    subroutine usxaxs(cxside)                     !現在設定されている正規化変換に対して,座標軸を一本描く．
      character(len=1), intent(in) :: cxside      !座標軸の位置.
    end subroutine
      
    subroutine usyaxs(cyside)                     !現在設定されている正規化変換に対して座標軸を一本描く．
      character(len=1), intent(in) :: cyside      !座標軸の位置 
    end subroutine
      
    subroutine ussttl(cxttl, cxunit, cyttl, cyunit) !座標軸のタイトル,サブラベルの中の単位を指定．
      character(len=*), intent(in) :: cxttl       !x座標軸のタイトル
      character(len=*), intent(in) :: cxunit      !x座標軸の単位
      character(len=*), intent(in) :: cyttl       !y座標軸のタイトル
      character(len=*), intent(in) :: cyunit      !y座標軸の単位
    end subroutine
!--------------------------------------------------------
    subroutine uspget(cp, ipara)                  !内部変数を参照
      character(len=*), intent(in) :: cp          !パラメータ名
      integer,   intent(out) :: ipara             !パラメータ値
    end subroutine
      
    subroutine usiget(cp, ipara)                  !内部変数を参照
      character(len=*), intent(in) :: cp          !パラメータ名
      integer,   intent(out) :: ipara             !パラメータ値
    end subroutine
      
    subroutine usrget(cp, ipara)                  !内部変数を参照
      character(len=*), intent(in) :: cp          !パラメータ名
      real,   intent(out) :: ipara             !パラメータ値
    end subroutine
      
    subroutine uslget(cp, ipara)                  !内部変数を参照
      character(len=*), intent(in) :: cp          !パラメータ名
      logical,   intent(out) :: ipara             !パラメータ値
    end subroutine
      
    subroutine uspset(cp, ipara)                  !内部変数を設定
      character(len=*), intent(in) :: cp          !パラメータ名
      integer,   intent(in) :: ipara              !パラメータ値
    end subroutine
      
    subroutine usiset(cp, ipara)                  !内部変数を設定
      character(len=*), intent(in) :: cp          !パラメータ名
      integer,   intent(in) :: ipara              !パラメータ値
    end subroutine
      
    subroutine usrset(cp, ipara)                  !内部変数を設定
      character(len=*), intent(in) :: cp          !パラメータ名
      real,   intent(in) :: ipara              !パラメータ値
    end subroutine
      
    subroutine uslset(cp, ipara)                  !内部変数を設定
      character(len=*), intent(in) :: cp          !パラメータ名
      logical,   intent(in) :: ipara              !パラメータ値
    end subroutine
      
    subroutine uspstx(cp, ipara)                  !内部変数を設定
      character(len=*), intent(in) :: cp          !ラメータ名
      integer,   intent(in) :: ipara              !パラメータ値
    end subroutine
      
    subroutine usistx(cp, ipara)                  !内部変数を設定
      character(len=*), intent(in) :: cp          !ラメータ名
      integer,   intent(in) :: ipara              !パラメータ値
    end subroutine
      
    subroutine usrstx(cp, ipara)                  !内部変数を設定
      character(len=*), intent(in) :: cp          !ラメータ名
      real,   intent(in) :: ipara              !パラメータ値
    end subroutine
      
    subroutine uslstx(cp, ipara)                  !内部変数を設定
      character(len=*), intent(in) :: cp          !ラメータ名
      logical,   intent(in) :: ipara              !パラメータ値
    end subroutine
      
    subroutine uscget(cp, cpara)                  !文字パラメータを参照する
      character(len=*), intent(in) :: cp          !パラメータ名
      character(len=*), intent(out) :: cpara      !パラメータ値
    end subroutine
      
    subroutine uscset(cp, cpara)                  !文字パラメータを設定する
      character(len=*), intent(in) :: cp          !パラメータ名
      character(len=*), intent(in) :: cpara       !パラメータ値
    end subroutine
      
    subroutine uscstx(cp, cpara)                  !文字パラメータを設定する
      character(len=*), intent(in) :: cp          !パラメータ名
      character(len=*), intent(in) :: cpara       !パラメータ値
    end subroutine
      
    subroutine uspqnp(ncp)                        !内部変数の総数を求める
      integer,   intent(out) :: ncp
    end subroutine
      
    subroutine uspqid(cp,idx)                     !内部変数cpの位置を求める．
      character(len=*), intent(in) :: cp
      integer,   intent(out) :: idx
    end subroutine
      
    subroutine uspqcp(idx,cp)                     !idxの位置にある内部変数の名前を参照する
      character(len=*), intent(out) :: cp
      integer,   intent(in) :: idx
    end subroutine
      
    subroutine uspqvl(idx,ipara)                  !idxの位置にある内部変数の値を参照する
      integer,   intent(in) :: idx
      integer,   intent(out) :: ipara
    end subroutine
      
    subroutine uspsvl(idx,ipara)                  !idxの位置にある内部変数の値を変更する
      integer,   intent(in) :: idx
      integer,   intent(in) :: ipara
    end subroutine
      
    subroutine uscqnp(ncp)                        !内部変数の総数を求める
      integer,   intent(out) :: ncp
    end subroutine
      
    subroutine uscqid(cp,idx)                     !内部変数cpの位置を求める．
      character(len=*), intent(in) :: cp
      integer,   intent(out) :: idx
    end subroutine
      
    subroutine uscqcp(idx,cp)                     !idxの位置にある内部変数の名前を参照する
      character(len=*), intent(out) :: cp
      integer,   intent(in) :: idx
    end subroutine
      
    subroutine uscqvl(idx,cpara)                  !idxの位置にある内部変数の値を参照する
      integer,   intent(in) :: idx
      character(len=*), intent(out) :: cpara
    end subroutine
      
    subroutine uscsvl(idx,cpara)                  !idxの位置にある内部変数の値を変更する
      integer,   intent(in) :: idx
      character(len=*), intent(in) :: cpara
    end subroutine
      
    subroutine usinit()                           !初期値に戻す．
    end subroutine

  end interface
end module
!uspack library end ----
