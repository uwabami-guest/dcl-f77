!-------------------------------------------------
!interface module of uzpack
!-------------------------------------------------
module uz_interface
  interface
    subroutine uzpget(cp,ipara)                   !内部変数を参照
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine uziget(cp,ipara)                   !内部変数を参照
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine uzrget(cp,ipara)                   !内部変数を参照
      character(len=*), intent(in) :: cp          !内部変数の名前
      real,      intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine uzlget(cp,ipara)                   !内部変数を参照
      character(len=*), intent(in) :: cp          !内部変数の名前
      logical,   intent(out) :: ipara             !内部変数の値
    end subroutine
      
    subroutine uzpset(cp,ipara)                   !内部変数を変更
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine uziset(cp,ipara)                   !内部変数を変更
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine uzrset(cp,ipara)                   !内部変数を変更
      character(len=*), intent(in) :: cp          !内部変数の名前
      real,      intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine uzlset(cp,ipara)                   !内部変数を変更
      character(len=*), intent(in) :: cp          !内部変数の名前
      logical,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine uzpstx(cp,ipara)                   !内部変数を変更
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine uzistx(cp,ipara)                   !内部変数を変更
      character(len=*), intent(in) :: cp          !内部変数の名前
      integer,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine uzrstx(cp,ipara)                   !内部変数を変更
      character(len=*), intent(in) :: cp          !内部変数の名前
      real,      intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine uzlstx(cp,ipara)                   !内部変数を変更
      character(len=*), intent(in) :: cp          !内部変数の名前
      logical,   intent(in) :: ipara              !内部変数の値
    end subroutine
      
    subroutine uzpqnp(ncp)                        !内部変数の総数を求める．
      integer,   intent(out) :: ncp               !内部変数の値
    end subroutine
      
    subroutine uzpqid(cp,idx)                     !内部変数cpの位置を求める．
      character(len=*), intent(in) :: cp
      integer,   intent(out) :: idx
    end subroutine
      
    subroutine uzpqcp(idx,cp)                     !idxの位置にある内部変数の名前を参照する．
      integer,   intent(in) :: idx
      character(len=*), intent(out) :: cp
    end subroutine
      
    subroutine uzpqvl(idx,ipara)                  !idxの位置にある内部変数の値を参照する．
      integer,   intent(in) :: idx
      integer,   intent(out) :: ipara
    end subroutine
      
    subroutine uzpsvl(idx,ipara)                  !idxの位置にある内部変数の値を変更する
      integer,   intent(in) :: idx
      integer,   intent(in) :: ipara
    end subroutine
      
    subroutine uzinit()                           !内部変数 'roffzs', 'rofgzs'をすべて0にセットする．
    end subroutine
      
    subroutine uzfact(rfact)                      !内部変数 'rsizeti' 'rsizeli' 'rsizeci'　(i=1,2) をすべて何倍かする．
      real,      intent(in) :: rfact              !何倍するかを指定する実数値
    end subroutine
      
    subroutine uzpsav()                           !現在設定されている内部変数の値すべてをスクラッチファイルに書き出す．
    end subroutine
      
    subroutine uzprst()                           !スクラッチファイルに書き出した値ですべての内部変数を再設定する．
    end subroutine

  end interface
end module
!uzpack library end ----
