!-------------------------------------------------
!interface module of viblib
!-------------------------------------------------
module vib_interface
  interface

    subroutine vifnb(ix,iy,iz,n,jx,jy,jz,ifnb)    !IXとIYにIFNBを作用させてIZに代入する．
      integer,   intent(in), dimension(*) :: ix   !処理する整数型配列
      integer,   intent(in), dimension(*) :: iy
      integer,   intent(out), dimension(*) :: iz
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列ix, iy, izにおいて，処理する配列要素の間隔
      integer,   intent(in) :: jy
      integer,   intent(in) :: jz
      external ifnb                               !引数が2個である整数型関数名
    end subroutine
      
    subroutine viadd(ix,iy,iz,n,jx,jy,jz)         !IXとIYを加えてIZに代入する．
      integer,   intent(in), dimension(*) :: ix   !処理する整数型配列
      integer,   intent(in), dimension(*) :: iy
      integer,   intent(out), dimension(*) :: iz
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列ix, iy, izにおいて，処理する配列要素の間隔
      integer,   intent(in) :: jy
      integer,   intent(in) :: jz
    end subroutine
      
    subroutine visub(ix,iy,iz,n,jx,jy,jz)         !IXからIYを引いてIZに代入する
      integer,   intent(in), dimension(*) :: ix   !処理する整数型配列
      integer,   intent(in), dimension(*) :: iy
      integer,   intent(out), dimension(*) :: iz
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列ix, iy, izにおいて，処理する配列要素の間隔
      integer,   intent(in) :: jy
      integer,   intent(in) :: jz
    end subroutine
      
    subroutine vimlt(ix,iy,iz,n,jx,jy,jz)         !IXとIYを掛けてIZに代入する．
      integer,   intent(in), dimension(*) :: ix   !処理する整数型配列
      integer,   intent(in), dimension(*) :: iy
      integer,   intent(out), dimension(*) :: iz
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列ix, iy, izにおいて，処理する配列要素の間隔
      integer,   intent(in) :: jy
      integer,   intent(in) :: jz
    end subroutine
      
    subroutine vidiv(ix,iy,iz,n,jx,jy,jz)         !IXをIYで割ってIZに代入する
      integer,   intent(in), dimension(*) :: ix   !処理する整数型配列
      integer,   intent(in), dimension(*) :: iy
      integer,   intent(out), dimension(*) :: iz
      integer,   intent(in) :: n                  !処理する配列要素の個数
      integer,   intent(in) :: jx                 !配列ix, iy, izにおいて，処理する配列要素の間隔
      integer,   intent(in) :: jy
      integer,   intent(in) :: jz
    end subroutine

  end interface
end module
!viblib library end ----
