!-------------------------------------------------
!  INTRlib Module
!-------------------------------------------------
module intrlib
  use dcl_common
  interface DclInterpolate
    module procedure DclInterpolateR, DclInterpolateC
  end interface
  private :: DclInterpolateR, DclInterpolateC

  contains

    function DclInterpolateR(x)                !実数型配列の補間をする
      real, intent(in), dimension(:)        :: x !処理する実数型の配列
      real,             dimension(size(x)) :: DclInterpolateReal

      call prcopn('DclInterpolateR')
      DclInterpolateReal = x
      call vrintr(DclInterpolateR,size(x),1)
      call prccls('DclInterpolateR')
    end function
      
    function DclInterpolateC(x)            !複素数型配列の補間をする
      complex, intent(in), dimension(:)        :: x !処理する複素数型の配列
      complex,             dimension(size(x)) :: DclInterpolateComplex

      call prcopn('DclInterpolateC')
      DclInterpolateComplex = x
      call vcintr(DclInterpolateC,size(x),1)
      call prccls('DclInterpolateC')
    end function

end module
