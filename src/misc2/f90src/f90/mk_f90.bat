echo %echo%
:
:  地球流体電脳ライブラリ depend ver.5.1 for DEC Fortran
: 
:  97/10/28  酒井 敏
:
if "%DWIN%"==""   dcl_error
if "%DLIB%"==""   dcl_error
if "%DMODE%"==""  dcl_error

:---------------------------- dcl-win ------------------------------------
:cd src

call df_%DMODE%

df -c dcl_common.f90 /include:%DLIB%\module

df -c *pack.f90 /include:%DLIB%\module
df -c *lib.f90 /include:%DLIB%\module

df -c dcl_parm.f90 /include:%DLIB%\module
df -c dcl.f90 /include:%DLIB%\module

lib /out:%DLIB%\%DMODE%\dcl_f90.lib *.obj
del *.obj
copy *.mod  %DLIB%\module
del *.mod
call mk_lib
