!-------------------------------------------------
!  SLpack module
!-------------------------------------------------
module slpack
  use dcl_common
  interface DclSetFrameSize
    subroutine slsize(size)  !第1フレームの再設定
      character(len=3), intent(in) :: size  !フレームの大きさと向き
    end subroutine

    subroutine slform(width,height)  !第1フレームの再設定
      real,      intent(in) :: width       !フレームの横,縦の実長, 単位cm
      real,      intent(in) :: height
    end subroutine
  end interface

  contains
!-----------------------------------------------------------------------
!主要ルーチン

    subroutine DclDivideFrame(direction,x_num,y_num) !フレームの分割
      character(len=1), intent(in) :: direction  !方向を指定
      integer,   intent(in) :: x_num          !X方向, Y方向の分割数
      integer,   intent(in) :: y_num

      call prcopn('DclDivideFrame')
      call sldiv(direction,x_num,y_num)
      call prccls('DclDivideFrame')
    end subroutine

    subroutine DclSetFrameMargin(left, right, bottom, top)   !マージンの設定． 
      real,      intent(in) :: left, right, bottom, top

      call prcopn('DclSetFrameMargin')
      call slmgn(left, right, bottom, top)
      call prccls('DclSetFrameMargin')
    end subroutine

    subroutine DclSetAspectRatio(x,y)  !縦横比の設定．   
      real, intent(in)           :: x  !フレームの縦横比を与える
      real, intent(in), optional :: y  !フレームの縦横比を与える

      call prcopn('DclSetAspectRatio')
      if(present(y)) then
        yy = y
      else
        yy = 1.
      end if

      call slrat(x,yy)
      call prccls('DclSetAspectRatio')
    end subroutine

    subroutine DclSetFrameTitle(title,side,x_position,y_position,height,num)
      character(len=*),  intent(in) :: title
      character(len=1),  intent(in) :: side
      real,              intent(in) :: x_position, y_position 
      real,              intent(in) :: height
      integer, optional, intent(in) :: num         !何番目の文字列かを指定する

      call prcopn('DclSetFrameTitle')
      if(present(num)) then
        nt = num
      else
        nt = 1
      end if
      call slsttl(title,side,x_position,y_position,height,nt) 
      call prccls('DclSetFrameTitle')
    end subroutine
!--------------------------------------------------------------------
!作画境界
    subroutine DclDrawViewPortFrame(index)  !ビューポートの枠を描く．
      integer,   intent(in) :: index  !線分のラインインデクス 

      call sgoopn('DclDrawViewPortFrame', ' ')
      call slpvpr(index)
      call sgocls('DclDrawViewPortFrame')
    end subroutine

    subroutine DclDrawDeviceWindowFrame(index)  !ウインドウの枠を描く
      integer,   intent(in) :: index  !線分のラインインデクス 

      call sgoopn('DclDrawDeviceWindowFrame', ' ')
      call slpwwr(index)
      call sgocls('DclDrawDeviceWindowFrame')
    end subroutine

    subroutine DclDrawDeviceViewPortFrame(index)  !最大作画領域の枠を描
      integer,   intent(in) :: index    !線分のラインインデクス 

      call sgoopn('DclDrawDeviceViewPortFrame', ' ')
      call slpwvr(index) 
      call sgocls('DclDrawDeviceViewPortFrame')
    end subroutine

    subroutine DclDrawViewPortCorner(index,size)  !ビューポートのコーナーマークを描く
      integer,   intent(in) :: index    !線分のラインインデクス 
      real,      intent(in) :: size    !コーナーマークの長さ

      call sgoopn('DclDrawViewPortCorner', ' ')
      call slpvpc(index,size)
      call sgocls('DclDrawViewPortCorner')
    end subroutine                             

    subroutine DclDrawDeviceWindowCorner(index,size)  !ウインドウのコーナーマークを描く
      integer,   intent(in) :: index    !線分のラインインデクス 
      real,      intent(in) :: size     !コーナーマークの長さ

      call sgoopn('DclDrawDeviceWindowCorner', ' ')
      call slpwwc(index,size)
      call sgocls('DclDrawDeviceWindowCorner')
    end subroutine

    subroutine DclDrawDeviceViewPortCorner(index,size) 
      integer,   intent(in) :: index  !線分のラインインデクス 
      real,      intent(in) :: size    !コーナーマークの長さ

      call sgoopn('DclDrawDeviceViewPortCorner', ' ')
      call slpwvc(index,size)
      call sgocls('DclDrawDeviceViewPortCorner')
    end subroutine

end module

