!-------------------------------------------------
!  TIMElib Module
!-------------------------------------------------
module timelib
  use dcl_common

  contains
    function DclGetTime()
      type(dcl_time) :: DclGetTime

      call prcopn('DclGetTime')
      call timeq3(DclGetTime%hour,DclGetTime%minute,DclGetTime%second)  
      call prccls('DclGetTime')
    end function
      
    subroutine DclFormatTime(cform,time)
      type(dcl_time), intent(in) :: time
      character(len=*), intent(inout) :: cform   !時刻のフォーマット

      call prcopn('DclFormatTime')
      call timec3(cform,time%hour,time%minute,time%second) 
      call prccls('DclFormatTime')
    end subroutine

end module
