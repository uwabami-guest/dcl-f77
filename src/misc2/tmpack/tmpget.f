*-----------------------------------------------------------------------
*     TMPGET / TMPSET / TMPSTX
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE TMPGET(CP, IPARA)
      IMPLICIT NONE

      CHARACTER CP*(*)

      CHARACTER CX*8, CL*40

      INTEGER   IT, IP, IPARA, IDX

      CALL TMPQID(CP, IDX)
      CALL TMPQVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMPSET(CP, IPARA)

      CALL TMPQID(CP, IDX)
      CALL TMPSVL(IDX, IPARA)

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMPSTX(CP,IPARA)

      IP = IPARA
      CALL TMPQID(CP, IDX)
      CALL TMPQIT(IDX, IT)
      CALL TMPQCP(IDX, CX)
      CALL TMPQCL(IDX, CL)
      IF (IT .EQ. 1) THEN
        CALL RTIGET('TM', CX, IP, 1)
        CALL RLIGET(CL, IP, 1)
        CALL TMIQID(CP, IDX)
        CALL TMISVL(IDX, IP)
      ELSE IF (IT .EQ. 2) THEN
        CALL RTLGET('TM', CX, IP, 1)
        CALL RLLGET(CL, IP, 1)
        CALL TMLQID(CP, IDX)
        CALL TMLSVL(IDX, IP)
      ELSE IF (IT .EQ. 3) THEN
        CALL RTRGET('TM', CX, IP, 1)
        CALL RLRGET(CL, IP, 1)
        CALL TMRQID(CP, IDX)
        CALL TMRSVL(IDX, IP)
      END IF

      RETURN
      END
