*-----------------------------------------------------------------------
*     TMPQNP / TMPQID / TMPQCP / TMPQVL / TMPSVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE TMPQNP(NCP)
      IMPLICIT NONE

      CHARACTER CP*(*)

      INTEGER   NPARA
      PARAMETER (NPARA = 11)

      INTEGER   ITYPE(NPARA)
      LOGICAL   LCHREQ
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      INTEGER   LENC
      EXTERNAL  LCHREQ,LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS( 1) / 'GRDTHRES' /, ITYPE( 1) / 1 /
      DATA      CPARAS( 2) / 'ARRWINTV' /, ITYPE( 2) / 3 /
      DATA      CPARAS( 3) / 'SKIPINTV' /, ITYPE( 3) / 1 /
      DATA      CPARAS( 4) / 'STLNDT  ' /, ITYPE( 4) / 3 /
      DATA      CPARAS( 5) / 'STLNNUM ' /, ITYPE( 5) / 1 /
      DATA      CPARAS( 6) / 'PERIODX ' /, ITYPE( 6) / 2 /
      DATA      CPARAS( 7) / 'PERIODY ' /, ITYPE( 7) / 2 /
      DATA      CPARAS( 8) / 'NODRSHRT' /, ITYPE( 8) / 2 /
      DATA      CPARAS( 9) / 'ENDARROW' /, ITYPE( 9) / 2 /
      DATA      CPARAS(10) / 'FIXEDDT ' /, ITYPE(10) / 2 /
      DATA      CPARAS(11) / 'STLNGLIM' /, ITYPE(11) / 1 /

*     / LONG NAME /

      DATA      CPARAL( 1) / 'GRID_PASSING_THRESHOLD' /
      DATA      CPARAL( 2) / 'ARROW_DRAWING_INTERVAL' /
      DATA      CPARAL( 3) / 'SKIPPING_POINT_INTERVAL' /
      DATA      CPARAL( 4) / 'STLN_CALCULATING_INTERVAL' /
      DATA      CPARAL( 5) / 'STREAM_LINE_NUMBERS' /
      DATA      CPARAL( 6) / 'PERIOD_BOUNDARY_XDIRECTION' /
      DATA      CPARAL( 7) / 'PERIOD_BOUNDARY_YDIRECTION' /
      DATA      CPARAL( 8) / 'NO_DRAW_SHORTLINE' /
      DATA      CPARAL( 9) / 'END_ARROW_FLAG' /
      DATA      CPARAL(10) / 'FIXED_TIME_STEP' /
      DATA      CPARAL(11) / 'STREAM_LIMIT_INTERVAL' /

*     / WORKING VALUES /

      INTEGER   N, IDX, ITP, IN, IPARA, ID, NCP



      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMPQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','TMPQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMPQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','TMPQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMPQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','TMPQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMPQIT(IDX, ITP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        ITP = ITYPE(IDX)
      ELSE
        CALL MSGDMP('E','TMPQIT','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMPQVL(IDX, IPARA)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IF (ITYPE(IDX) .EQ. 1) THEN
          CALL TMIQID(CPARAS(IDX), ID)
          CALL TMIQVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 2) THEN
          CALL TMLQID(CPARAS(IDX), ID)
          CALL TMLQVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 3) THEN
          CALL TMRQID(CPARAS(IDX), ID)
          CALL TMRQVL(ID, IPARA)
        END IF
      ELSE
        CALL MSGDMP('E','TMPQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMPSVL(IDX, IPARA)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        IF (ITYPE(IDX) .EQ. 1) THEN
          CALL TMIQID(CPARAS(IDX), ID)
          CALL TMISVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 2) THEN
          CALL TMLQID(CPARAS(IDX), ID)
          CALL TMLSVL(ID, IPARA)
        ELSE IF (ITYPE(IDX) .EQ. 3) THEN
          CALL TMRQID(CPARAS(IDX), ID)
          CALL TMRSVL(ID, IPARA)
        END IF
      ELSE
        CALL MSGDMP('E','TMPSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMPQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
