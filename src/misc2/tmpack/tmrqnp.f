*-----------------------------------------------------------------------
*     TMRQNP / TMRQID / TMRQCP / TMRQVL / TMRSVL
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD Dennou Club. All rights reserved.
*-----------------------------------------------------------------------
      SUBROUTINE TMRQNP(NCP)
      IMPLICIT NONE

      CHARACTER CP*(*)

      INTEGER   NPARA
      REAL      RUNDEF
      PARAMETER (NPARA  = 2)
      PARAMETER (RUNDEF = -999.)

      REAL      RX(NPARA)
      LOGICAL   LCHREQ, LFIRST
      CHARACTER CPARAS(NPARA)*8
      CHARACTER CPARAL(NPARA)*40
      CHARACTER CMSG*80

      INTEGER   LENC
      EXTERNAL  LCHREQ,LENC

      SAVE

*     / SHORT NAME /

      DATA      CPARAS(1) / 'ARRWINTV' /, RX(1) / 0.05 /
      DATA      CPARAS(2) / 'STLNDT  ' /, RX(2) / 0.0 /

*     / LONG NAME /

      DATA      CPARAL(1) / 'ARROW_DRAWING_INTERVAL' /
      DATA      CPARAL(2) / 'STLN_CALCULATING_INTERVAL' /

      DATA      LFIRST / .TRUE. /

      INTEGER   N, NCP
      INTEGER   IDX, IP, IN
      REAL      RPARA

      NCP = NPARA

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMRQID(CP, IDX)

      DO 10 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IDX = N
          RETURN
        END IF
   10 CONTINUE
      CMSG = 'PARAMETER '''//CP(1:LENC(CP))//''' IS NOT DEFINED.'
      CALL MSGDMP('E','TMRQID',CMSG)

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMRQCP(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAS(IDX)
      ELSE
        CALL MSGDMP('E','TMRQCP','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMRQCL(IDX, CP)

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        CP = CPARAL(IDX)
      ELSE
        CALL MSGDMP('E','TMRQCL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMRQVL(IDX, RPARA)

      IF (LFIRST) THEN
        CALL RTRGET('TM', CPARAS, RX, NPARA)
        CALL RLRGET(CPARAL, RX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        RPARA = RX(IDX)
        IF ((IDX.EQ.1 .OR. IDX.EQ.2) .AND. RPARA.EQ.RUNDEF) THEN
          CALL UZRGET('RSIZEL1', RPARA)
        END IF
      ELSE
        CALL MSGDMP('E','TMRQVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMRSVL(IDX, RPARA)

      IF (LFIRST) THEN
        CALL RTRGET('TM', CPARAS, RX, NPARA)
        CALL RLRGET(CPARAL, RX, NPARA)
        LFIRST = .FALSE.
      END IF

      IF (1.LE.IDX .AND. IDX.LE.NPARA) THEN
        RX(IDX) = RPARA
      ELSE
        CALL MSGDMP('E','TMRSVL','IDX IS OUT OF RANGE.')
      END IF

      RETURN
*-----------------------------------------------------------------------
      ENTRY TMRQIN(CP, IN)

      DO 20 N = 1, NPARA
        IF (LCHREQ(CP, CPARAS(N)) .OR. LCHREQ(CP, CPARAL(N))) THEN
          IN = N
          RETURN
        END IF
   20 CONTINUE

      IN = 0

      RETURN
      END
