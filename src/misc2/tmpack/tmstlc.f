*-----------------------------------------------------------------------
*     TMSTLC : THE ROUTINE FOR DRAWING STREAM LINES
*-----------------------------------------------------------------------
*     Copyright (C) 2000-2022 GFD DENNOU CLUB. ALL RIGHTS RESERVED.
*-----------------------------------------------------------------------
      SUBROUTINE TMSTLC( X, Y, U, V, NX, NY )

      IMPLICIT NONE

*-- AGREENMENT
      INTEGER  NX        !* GRID NUMBERS FOR X-DIRECTION
      INTEGER  NY        !* GRID NUMBERS FOR Y-DIRECTION
      REAL     U(NX,NY)  !* VECTOR COMPONENT OF X-DIRECTION
      REAL     V(NX,NY)  !* VECTOR COMPONENT OF Y-DIRECTION
      REAL     X(NX)     !* GRID POINTS OF X-DIRECTION [UNIT:LENGTH]
      REAL     Y(NY)     !* GRID POINTS OF Y-DIRECTION [UNIT:LENGTH]

*-- INTERNAL VARIABLES
      REAL     TRX(NX*NY,2)     !* STREAM LINE OF X-COORDINATE
      REAL     TRY(NX*NY,2)     !* STREAM LINE OF Y-COORDINATE
      INTEGER  I, J, K          !* TMP VALUES
      INTEGER  COUNTER          !* NUMBER OF STREAM LINE FOR DRAW
      INTEGER  TOTNM(NX*NY,2)   !* ARRAY NUMBER IN TOTAL STREAM LINES AT END
      INTEGER  TOTSNM(NX*NY,2)  !* ARRAY NUMBER IN TOTAL STREAM LINES AT START
      REAL     ARROW_THRES
      REAL     DXA, DYA
      REAL     ARROW_LENG
      REAL     VXMIN, VXMAX, VYMIN, VYMAX
      REAL     UXMIN, UXMAX, UYMIN, UYMAX
      REAL     VXRATIO, VYRATIO
      REAL     CIRC_FLAG
      REAL     UNDEF
      INTEGER  N, M, IBLKGE, ARRCNT
      LOGICAL  NO_SHORT, END_ARR

*-- GETTING TMPACK'S PARAMETERS
      CALL TMRGET( 'ARRWINTV', ARROW_THRES )
      CALL TMLGET( 'NODRSHRT', NO_SHORT )
      CALL TMLGET( 'ENDARROW', END_ARR )
      CALL GLRGET( 'RMISS', UNDEF )
      CALL GLLSET( 'LMISS', .TRUE. )

*-- GETTING VIEWPORT PARAMETERS
      CALL SGQVPT( VXMIN, VXMAX, VYMIN, VYMAX)
      CALL SGQWND( UXMIN, UXMAX, UYMIN, UYMAX)
      VXRATIO=(VXMAX-VXMIN)/(UXMAX-UXMIN)
      VYRATIO=(VYMAX-VYMIN)/(UYMAX-UYMIN)

*-- DOING TMSTLN ROUTINE
      CALL TMSTLN( X, Y, U, V, NX, NY, TRX, TRY, TOTNM )

*-- GETTING STREAM LINE NUMBERS
      CALL TMIGET( 'STLNNUM', COUNTER )

*-- SETTING PARAMETER FOR ARROW. (NOW, CONSTANT VALUES)
      CALL SGLSET('LPROP',.FALSE.)
      CALL SGRSET('CONST',0.01)

*-- SETTING START NUMBERS IN TOTAL STREAM LINES
      TOTSNM(1,1:2)=1
      DO 10 I=2,COUNTER
         TOTSNM(I,1)=TOTNM(I-1,1)+1
         TOTSNM(I,2)=TOTNM(I-1,2)+1
 10   CONTINUE

*-- DRAWING STREAM LINES
      DO 82 K=1,2
         DO 81 I=1,COUNTER

*-- CALCULATING THE INTERVAL LENGTH OF DRAWING ARROWS.
            ARROW_LENG=0.0
            ARRCNT=0
            IF(((TOTNM(I,K)-TOTSNM(I,K)+1).GT.1).AND.
     &         ((TOTNM(I,K)-TOTSNM(I,K)+1).LE.NX*NY))THEN
               DO 80 J=TOTSNM(I,K)+1,TOTNM(I,K)
                  IF(TRX(J,K).NE.UNDEF.AND.TRY(J,K).NE.UNDEF.AND.
     &               TRX(J-1,K).NE.UNDEF.AND.TRY(J-1,K).NE.UNDEF
     &               )THEN
                     DXA=(TRX(J,K)-TRX(J-1,K))*VXRATIO
                     DYA=(TRY(J,K)-TRY(J-1,K))*VYRATIO
                     ARROW_LENG=ARROW_LENG+SQRT(DXA*DXA+DYA*DYA)
                     IF(ARROW_LENG.GE.ARROW_THRES)THEN
                        CALL SGLAU( TRX(J-1,K), TRY(J-1,K),
     &                              TRX(J,K), TRY(J,K) )
                        ARROW_LENG=0.0
                        ARRCNT=ARRCNT+1
                     END IF
                  END IF
 80            CONTINUE

*-- IF NO_SHORT IS TRUE, NO DRAWING STREAM LINE WHOSE ARROW NUMBER IS ZERO
*-- 流線の始点と終点が近似点にいれば, 閉じた流線とみなして描く.
*-- 矢羽は終点に描く.
               IF(ARRCNT.EQ.0)THEN
                  IF(NO_SHORT.EQV..TRUE.)THEN
                     IF((TOTNM(I,K)-TOTSNM(I,K)+1).LE.2)THEN
                        GO TO 81
                     ELSE
                        N=IBLKGE( X, NX, TRX(TOTSNM(I,K),K) )
                        M=IBLKGE( X, NX, TRX(TOTNM(I,K),K) )
                        IF(ABS(N-M).GT.1)THEN
                           GO TO 81
                        ELSE
*-- 一周しているのか, 短いだけなのかのチェック
*--  始点, 終点, 終点１つ前の順に増減していれば一周.
                           CIRC_FLAG=
     &                        (TRX(TOTNM(I,K),K)-TRX(TOTNM(I,K)-1,K))
     &                        *(TRX(TOTSNM(I,K),K)-TRX(TOTNM(I,K),K))
                           IF(CIRC_FLAG.LT.0.0)THEN  ! 単に短いだけ
                              GO TO 81
                           ELSE
                              N=IBLKGE( Y, NY, TRY(TOTSNM(I,K),K) )
                              M=IBLKGE( Y, NY, TRY(TOTNM(I,K),K) )
                              IF(ABS(N-M).GT.1)THEN
                                 GO TO 81
                              ELSE
                                 CIRC_FLAG=
     &                         (TRY(TOTNM(I,K),K)-TRY(TOTNM(I,K)-1,K))
     &                         *(TRY(TOTSNM(I,K),K)-TRY(TOTNM(I,K),K))
                                 IF(CIRC_FLAG.LT.0.0)THEN
                                    GO TO 81
!                                 ELSE
!                                 CALL SGLAU( TRX(TOTNM(I,K)-1,K), TRY(TOTNM(I,K)-1,K),
!     &                                       TRX(TOTNM(I,K),K), TRY(TOTNM(I,K),K) )
                                 END IF
                              END IF
                           END IF
                        END IF
                     END IF
                  ELSE
                     IF(END_ARR.EQV..TRUE.)THEN
                        CALL SGLAU( TRX(TOTNM(I,K)-1,K),
     &                              TRY(TOTNM(I,K)-1,K),
     &                              TRX(TOTNM(I,K),K),
     &                              TRY(TOTNM(I,K),K) )
                     END IF
                  END IF
               END IF

               CALL UULIN( (TOTNM(I,K)-TOTSNM(I,K)+1),
     &                     TRX(TOTSNM(I,K):TOTNM(I,K),K),
     &                     TRY(TOTSNM(I,K):TOTNM(I,K),K) )

            END IF

 81      CONTINUE

 82   CONTINUE

      CALL MSGDMP( 'M', 'TMSTLC', 'DRAWING FINISHED.' )

      END SUBROUTINE TMSTLC

